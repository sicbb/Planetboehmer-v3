<?php


function sql_addslashes($a_string = '', $is_like = false)
{
    /*
        Better addslashes for SQL queries.
        Taken from phpMyAdmin.
    */
    if ($is_like) {
        $a_string = str_replace('\\', '\\\\\\\\', $a_string);
    } else {
        $a_string = str_replace('\\', '\\\\', $a_string);
    }
    $a_string = str_replace('\'', '\\\'', $a_string);

    return $a_string;
} // function sql_addslashes($a_string = '', $is_like = FALSE)


function backquote($a_name)
{
    /*
        Add backqouotes to tables and db-names in
        SQL queries. Taken from phpMyAdmin.
    */
    if (!empty($a_name) && $a_name != '*') {
        if (is_array($a_name)) {
            $result = array();
            reset($a_name);
            while (list($key, $val) = each($a_name)) {
                $result[$key] = '`'.$val.'`';
            }

            return $result;
        } else {
            return '`'.$a_name.'`';
        }
    } else {
        return $a_name;
    }
} // function backquote($a_name, $do_it = TRUE)


function make_sql($table)
{
    /*
        Reads the Database table in $table and creates
        SQL Statements for recreating structure and data
    */

        $sql_statements  = "";

        // Add SQL statement to drop existing table
        $sql_statements .= "\n";
    $sql_statements .= "\n";
    $sql_statements .= "#\n";
    $sql_statements .= "# Delete any existing table ".backquote($table)."\n";
    $sql_statements .= "#\n";
    $sql_statements .= "\n";
    $sql_statements .= "DROP TABLE IF EXISTS ".backquote($table).";\n";

        // Table structure

        // Comment in SQL-file
        $sql_statements .= "\n";
    $sql_statements .= "\n";
    $sql_statements .= "#\n";
    $sql_statements .= "# Table structure of table ".backquote($table)."\n";
    $sql_statements .= "#\n";
    $sql_statements .= "\n";

        // Get table structure
        $query = "SHOW CREATE TABLE ".backquote($table);
    $result = mysql_query($query, $GLOBALS["db"]);
    if ($result == true) {
        if (mysql_num_rows($result) > 0) {
            $sql_create_arr = mysql_fetch_array($result);
            $sql_statements .= $sql_create_arr[1];
        }
        mysql_free_result($result);
        $sql_statements .= " ;";
    } // ($result == FALSE)

        // Table data contents

        // Get table contents
        $query = "SELECT * FROM ".backquote($table);
    $result = mysql_query($query, $GLOBALS["db"]);
    if ($result == true) {
        $fields_cnt = mysql_num_fields($result);
        $rows_cnt   = mysql_num_rows($result);
    } // if ($result == FALSE)

        // Comment in SQL-file
        $sql_statements .= "\n";
    $sql_statements .= "\n";
    $sql_statements .= "#\n";
    $sql_statements .= "# Data contents of table ".$table." (".$rows_cnt." records)\n";
    $sql_statements .= "#\n";

        // Checks whether the field is an integer or not
        for ($j = 0; $j < $fields_cnt; $j++) {
            $field_set[$j] = backquote(mysql_field_name($result, $j));
            $type          = mysql_field_type($result, $j);
            if ($type == 'tinyint' || $type == 'smallint' || $type == 'mediumint' || $type == 'int' ||
                $type == 'bigint'  || $type == 'timestamp') {
                $field_num[$j] = true;
            } else {
                $field_num[$j] = false;
            }
        } // end for

        // Sets the scheme
        $entries = 'INSERT INTO '.backquote($table).' VALUES (';
    $search            = array("\x00", "\x0a", "\x0d", "\x1a");    //\x08\\x09, not required
        $replace        = array('\0', '\n', '\r', '\Z');
    $current_row    = 0;
    while ($row = mysql_fetch_row($result)) {
        $current_row++;
        for ($j = 0; $j < $fields_cnt; $j++) {
            if (!isset($row[$j])) {
                $values[]     = 'NULL';
            } elseif ($row[$j] == '0' || $row[$j] != '') {
                // a number
                    if ($field_num[$j]) {
                        $values[] = $row[$j];
                    } else {
                        $values[] = "'".str_replace($search, $replace, sql_addslashes($row[$j]))."'";
                    } //if ($field_num[$j])
            } else {
                $values[]     = "''";
            } // if (!isset($row[$j]))
        } // for ($j = 0; $j < $fields_cnt; $j++)
            $sql_statements .= " \n".$entries.implode(', ', $values).') ;';
        unset($values);
    } // while ($row = mysql_fetch_row($result))
        mysql_free_result($result);

        // Create footer/closing comment in SQL-file
        $sql_statements .= "\n";
    $sql_statements .= "#\n";
    $sql_statements .= "# End of data contents of table ".$table."\n";
    $sql_statements .= "# --------------------------------------------------------\n";
    $sql_statements .= "\n";

    return $sql_statements;
} //function make_sql($table)
;
