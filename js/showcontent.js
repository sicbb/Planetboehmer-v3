function ShowContent (id)
{
	if (document.getElementById("Content" + id).style.display == 'none')
	{
		document.getElementById("ContentImg" + id).src = "img/minus.gif";
		document.getElementById("ContentImg" + id).alt = "Einklappen";
		document.getElementById("ContentImg" + id).title = "Einklappen";
		document.getElementById("Content" + id).style.display = "";
		document.getElementById("tmp").style.display = "none";
		document.getElementById("tmp2").width = "25%";
	}
	else
	{
		document.getElementById("ContentImg" + id).src = "img/plus.gif";
		document.getElementById("ContentImg" + id).alt = "Ausklappen";
		document.getElementById("ContentImg" + id).title = "Ausklappen";
		document.getElementById("Content" + id).style.display = "none";
		document.getElementById("tmp").style.display = "";
		document.getElementById("tmp2").width = "25%";
	}
}