//------------------------------------
// Main Menu functions
//------------------------------------

function findPosX(obj)
{
	var curleft = 0;

	if (document.getElementById || document.all)
	{
		while (obj.offsetParent)
		{
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
		}
	}
	else if (document.layers)
	{
		curleft += obj.x;
	}
	
	return curleft;
}

var NaviHelperLeft = 0;
var PointerName = "hand";
var ActiveDropDown;

function MainMenuOver1(aobject, linkcolor, bgcolor, left, description)
{
	//
	// Set background, color & cursor
	//
	aobject.style.backgroundColor = bgcolor;
	aobject.style.color			  = linkcolor;
	
	if (document.all)
	{
		aobject.style.cursor = "hand";
	}
	else
	{
		aobject.style.cursor = "pointer";
	}
	
	//
	// Show navigation hint
	//
	//document.getElementById('NavigationHint').style.left = left + NaviHelperLeft + 3;
	//document.getElementById('NavigationHint').style.display = 'block';
	//document.getElementById('NavigationHintMsg').innerHTML = description;
	
	//
	// Show dropdown menu
	//
	ShowDropDownMenu (aobject.id + "Div", findPosX(aobject) - NaviHelperLeft - 1);
}

function MainMenuOut1 (aobject, linkcolor, bgcolor)
{
	aobject.style.backgroundColor = bgcolor;
	aobject.style.color			  = linkcolor;

	//
	// Hide navigation hint
	//
	//document.getElementById('NavigationHint').style.display = 'none';
}

function ShowDropDownMenu (LayerName, left)
{
	//nav = LayerName.substring (3, 4);

	//
	// Check weither we have an different element
	//
	if (ActiveDropDown)
	{
		ActiveDropDown.style.display = "none";

		//
		// Get id for the main navi item
		//
		id = ActiveDropDown.id.replace ("Div", "");

		//
		// Set background, color
		//
		if ((ActiveDropDown != document.getElementById(LayerName)) && (document.getElementById(id).style.fontWeight == "normal" || document.getElementById(id).style.fontWeight == ""))
		{
			document.getElementById(id).style.backgroundColor = "#7B7B7B";
			document.getElementById(id).style.color			  = "#CECECE";
		}
	}

	if (document.getElementById(LayerName))
	{
		document.getElementById(LayerName).style.display = "block";
		document.getElementById(LayerName).style.left = left + NaviHelperLeft;
		ActiveDropDown = document.getElementById(LayerName);	

		//
		// Hide form elements / flash
		//
		if (DropDownExceptions != null)
		{
			for (i = 0; i < DropDownExceptions.length; i++)
			{
				document.getElementById(DropDownExceptions[i]).style.visibility = "hidden";
			}
		}
	}
}

function linkz(URI1,F1,URI2,F2,URI3,F3) {
	Frame1=eval("parent."+F1);
	Frame2=eval("parent."+F2);
	Frame3=eval("parent."+F3);
	Frame1.location.href = URI1;
	Frame2.location.href = URI2;
	Frame3.location.href = URI3;
}