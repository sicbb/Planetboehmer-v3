// Als Übergabeparameter wird der dateiname,breite und höhe des Fensters mitgeführt
function MitteFenster(dateiname,b,h)
{
	var eigenschaft,sbreite,shoehe,fenster;


	// stellt die Bildschirmabmessungen fest
	sbreite = screen.width;
	shoehe = screen.height;


	x = (sbreite-b)/2;
	y = (shoehe-h)/2;


	// eigenschaften des fensters
	eigenschaften="left="+x+",top="+y+",screenX="+x+",screenY="+y+",width="+b+",height="+h+",scrollbars=yes,menubar=no,toolbar=no";


	fenster=window.open(dateiname,"_blank",eigenschaften);
	fenster.focus();
}