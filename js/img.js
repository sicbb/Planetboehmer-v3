function imgshow(imageName,imageWidth,imageHeight) {
    alt='Planetboehmer v3.0 - D.C.U.S.';
    bgcolor='#FFFFFF';
    var adj=10;
    var w = screen.width;
    var h = screen.height;
    var byFactor=1;
    var frame=10; /* or 0 */

    if(w<740){
      var lift=0.90;
    }
    if(w>=740 & w<835){
      var lift=0.91;
    }
    if(w>=835){
      var lift=0.93;
    }

        if (imageWidth>w){
                byFactor = w / imageWidth;
                imageWidth = w;
                imageHeight = imageHeight * byFactor;
        }
        if (imageHeight>h-adj){
                byFactor = h / imageHeight;
                imageWidth = (imageWidth * byFactor);
                imageHeight = h;
        }

        var scrWidth = w-adj;
        var scrHeight = (h*lift)-adj;

        if (imageHeight>scrHeight){
                imageHeight=imageHeight*lift;
                imageWidth=imageWidth*lift;
        }

        var posLeft=0
        var posTop=0

        {
                posTop = (scrHeight-imageHeight)/2;
                posLeft = ((w-imageWidth)/2);
                scrHeight = imageHeight-adj+frame;
                scrWidth = imageWidth-adj+frame;
         }

        imageHeight=imageHeight-adj;
        imageWidth=imageWidth-adj;

        newWindow = window.open("","newWindow","width="+scrWidth+",height="+scrHeight+",left="+posLeft+",top="+posTop);
        newWindow.document.open();
        newWindow.document.write('<html><title>Planetboehmer v3.0</title><body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0" bgcolor='+bgcolor+' onBlur="self.close()" onClick="self.close()">');
        newWindow.document.write('<table width='+imageWidth+' border="0" cellspacing="0" cellpadding="0" align="center" height='+scrHeight+' ><tr><td>');
        newWindow.document.write('<img src="'+imageName+'" width='+imageWidth+' height='+imageHeight+' alt="'+alt+'" >');
        newWindow.document.write('</td></tr></table></body></html>');
        newWindow.document.close();
        newWindow.focus();
}
