<?php
    require_once "code/kernel/db_session_class.php";
    require_once "code/kernel/session_class.php";

    try {
        // Globale Session
        $sess = strip_tags($_GET['s']);
        $db_session_obj = new DBSessionClass();
        $session_obj = $db_session_obj->CheckSession($sess);

        function drawnavi($id, $session_obj)
        {
            ?>
			<html>
			<head>
				<title>:: PlanetB&ouml;hmer v3.0 ::</title>
				<link rel="stylesheet" type="text/css" href="css/navigation.css">
				<script language="javascript" type="text/javascript" src="js/common.js"></script>

			</head>

			<body leftmargin="0" topmargin="0">

			<table id="NavigationControl" border="0" cellspacing="0" cellpadding="0" style="width: 700px; height: 20px; border-left: 1px solid black; border-right: 1px solid black;">

			<tr style="height: 2px;">
			<td id="nav1" style="background-color: #A59E70; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 2px; width: 1px;"></td>
			<td id="nav2" style="background-color: #A59E70; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 2px; width: 1px;"></td>
			<td id="nav3" style="background-color: #A59E70; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 2px; width: 1px;"></td>
			<td id="nav4" style="background-color: #A59E70; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 2px; width: 1px;"></td>
			<td id="nav5" style="background-color: #A59E70; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 2px; width: 1px;"></td>
			<td id="nav6" style="background-color: #A59E70; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 2px; width: 1px;"></td>
			<td id="nav7" style="background-color: #A59E70;"><img src="img/navi/none.gif" alt="" style="height: 2px; width: 1px;"></td>
			</tr>

			<tr style="height: 1px;">
			<td bgcolor="#000000" colspan="7"><img src="img/navi/none.gif" style="height: 1px;" alt=""></td>
			</tr>

			<?php if ($id == "1") {
    ?>
			<tr style="height: 18px">
			<td id="nav1_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #FFFFFF; background-color:#A59E70; width:95; font-weight: bold;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 0, 'Hier geht es um den Programmierer und Besitzer dieser Seite')"  onmouseout="MainMenuOut1(this, '#FFFFFF', '#A59E70')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=aboutme','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=1");
    ?>','mainFrame')">About Me</td>
			<td id="nav2_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 95, 'Hier findest Du News und eine Shoutbox')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=news','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=2");
    ?>','mainFrame')">News</td>
			<td id="nav3_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 190, 'Hier findest Du besondere Features der Seite')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=special','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=3");
    ?>','mainFrame')">Specials</td>
			<td id="nav4_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 285, 'Hier findest Du die Aktionen der Bilder Crew')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=dcus','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=4");
    ?>','mainFrame')">Bilder</td>
			<td id="nav5_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 380, 'Hier findest Du Links zu weiteren guten Seiten')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=links','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=5");
    ?>','mainFrame')">Links</td>
			<td id="nav6_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 475, 'Hier findest Du den Inhaber und wichtige Impressum Daten')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=impressum','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=6");
    ?>','mainFrame')">Impressum</td>
			<td id="nav7_level1" align="center" class="navdefaultlink" style="color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 570, 'Hier findest Du den Login und die Interaktive Zone')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=login','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=7");
    ?>','mainFrame')">Login</td>
			</tr>

			<tr style="height: 1px;">
			<td style="background-color: #A59E70; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			</tr>

			<tr style="height: 3px;">
			<td colspan="7" style="background-color: #A59E70;"><img src="img/navi/none.gif" alt="" style="height: 3px; width: 1px;"></td>
			</tr>
			<?php

}
            ?>

			<?php if ($id == "2") {
    ?>
			<tr style="height: 18px">
			<td id="nav1_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 0, 'Hier geht es um den Programmierer und Besitzer dieser Seite')"  onmouseout="MainMenuOut1(this, '#FFFFFF', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=aboutme','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=1");
    ?>','mainFrame')">About Me</td>
			<td id="nav2_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #FFFFFF; background-color:#A59E70; width:95; font-weight: bold;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 95, 'Hier findest Du News und eine Shoutbox')"  onmouseout="MainMenuOut1(this, '#CECECE', '#A59E70')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=news','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=2");
    ?>','mainFrame')">News</td>
			<td id="nav3_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 190, 'Hier findest Du besondere Features der Seite')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=special','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=3");
    ?>','mainFrame')">Specials</td>
			<td id="nav4_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 285, 'Hier findest Du die Aktionen der Bilder Crew')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=dcus','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=4");
    ?>','mainFrame')">Bilder</td>
			<td id="nav5_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 380, 'Hier findest Du Links zu weiteren guten Seiten')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=links','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=5");
    ?>','mainFrame')">Links</td>
			<td id="nav6_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 475, 'Hier findest Du den Inhaber und wichtige Impressum Daten')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=impressum','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=6");
    ?>','mainFrame')">Impressum</td>
			<td id="nav7_level1" align="center" class="navdefaultlink" style="color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 570, 'Hier findest Du den Login und die Interaktive Zone')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=login','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=7");
    ?>','mainFrame')">Login</td>
			</tr>

			<tr style="height: 1px;">
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #A59E70; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			</tr>

			<tr style="height: 3px;">
			<td colspan="7" style="background-color: #A59E70;"><img src="img/navi/none.gif" alt="" style="height: 3px; width: 1px;"></td>
			</tr>
			<?php

}
            ?>

			<?php if ($id == "3") {
    ?>
			<tr style="height: 18px">
			<td id="nav1_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 0, 'Hier geht es um den Programmierer und Besitzer dieser Seite')"  onmouseout="MainMenuOut1(this, '#FFFFFF', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=aboutme','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=1");
    ?>','mainFrame')">About Me</td>
			<td id="nav2_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 95, 'Hier findest Du News und eine Shoutbox')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=news','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=2");
    ?>','mainFrame')">News</td>
			<td id="nav3_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #FFFFFF; background-color:#A59E70; width:95; font-weight: bold;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 190, 'Hier findest Du besondere Features der Seite')"  onmouseout="MainMenuOut1(this, '#CECECE', '#A59E70')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=special','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=3");
    ?>','mainFrame')">Specials</td>
			<td id="nav4_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 285, 'Hier findest Du die Aktionen der Bilder Crew')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=dcus','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=4");
    ?>','mainFrame')">Bilder</td>
			<td id="nav5_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 380, 'Hier findest Du Links zu weiteren guten Seiten')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=links','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=5");
    ?>','mainFrame')">Links</td>
			<td id="nav6_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 475, 'Hier findest Du den Inhaber und wichtige Impressum Daten')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=impressum','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=6");
    ?>','mainFrame')">Impressum</td>
			<td id="nav7_level1" align="center" class="navdefaultlink" style="color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 570, 'Hier findest Du den Login und die Interaktive Zone')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=login','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=7");
    ?>','mainFrame')">Login</td>
			</tr>

			<tr style="height: 1px;">
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #A59E70; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			</tr>

			<tr style="height: 3px;">
			<td colspan="7" style="background-color: #A59E70;"><img src="img/navi/none.gif" alt="" style="height: 3px; width: 1px;"></td>
			</tr>
			<?php

}
            ?>

			<?php if ($id == "4") {
    ?>
			<tr style="height: 18px">
			<td id="nav1_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 0, 'Hier geht es um den Programmierer und Besitzer dieser Seite')"  onmouseout="MainMenuOut1(this, '#FFFFFF', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=aboutme','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=1");
    ?>','mainFrame')">About Me</td>
			<td id="nav2_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 95, 'Hier findest Du News und eine Shoutbox')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=news','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=2");
    ?>','mainFrame')">News</td>
			<td id="nav3_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 190, 'Hier findest Du besondere Features der Seite')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=special','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=3");
    ?>','mainFrame')">Specials</td>
			<td id="nav4_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #FFFFFF; background-color:#A59E70; width:95; font-weight: bold;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 285, 'Hier findest Du die Aktionen der Bilder Crew')"  onmouseout="MainMenuOut1(this, '#CECECE', '#A59E70')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=dcus','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=4");
    ?>','mainFrame')">Bilder</td>
			<td id="nav5_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 380, 'Hier findest Du Links zu weiteren guten Seiten')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=links','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=5");
    ?>','mainFrame')">Links</td>
			<td id="nav6_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 475, 'Hier findest Du den Inhaber und wichtige Impressum Daten')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=impressum','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=6");
    ?>','mainFrame')">Impressum</td>
			<td id="nav7_level1" align="center" class="navdefaultlink" style="color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 570, 'Hier findest Du den Login und die Interaktive Zone')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=login','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=7");
    ?>','mainFrame')">Login</td>
			</tr>

			<tr style="height: 1px;">
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #A59E70; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			</tr>

			<tr style="height: 3px;">
			<td colspan="7" style="background-color: #A59E70;"><img src="img/navi/none.gif" alt="" style="height: 3px; width: 1px;"></td>
			</tr>
			<?php

}
            ?>

			<?php if ($id == "5") {
    ?>
			<tr style="height: 18px">
			<td id="nav1_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 0, 'Hier geht es um den Programmierer und Besitzer dieser Seite')"  onmouseout="MainMenuOut1(this, '#FFFFFF', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=aboutme','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=1");
    ?>','mainFrame')">About Me</td>
			<td id="nav2_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 95, 'Hier findest Du News und eine Shoutbox')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=news','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=2");
    ?>','mainFrame')">News</td>
			<td id="nav3_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 190, 'Hier findest Du besondere Features der Seite')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=special','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=3");
    ?>','mainFrame')">Specials</td>
			<td id="nav4_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 285, 'Hier findest Du die Aktionen der Bilder Crew')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=dcus','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=4");
    ?>','mainFrame')">Bilder</td>
			<td id="nav5_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #FFFFFF; background-color:#A59E70; width:95; font-weight: bold;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 380, 'Hier findest Du Links zu weiteren guten Seiten')"  onmouseout="MainMenuOut1(this, '#CECECE', '#A59E70')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=links','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=5");
    ?>','mainFrame')">Links</td>
			<td id="nav6_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 475, 'Hier findest Du den Inhaber und wichtige Impressum Daten')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=impressum','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=6");
    ?>','mainFrame')">Impressum</td>
			<td id="nav7_level1" align="center" class="navdefaultlink" style="color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 570, 'Hier findest Du den Login und die Interaktive Zone')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=login','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=7");
    ?>','mainFrame')">Login</td>
			</tr>

			<tr style="height: 1px;">
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #A59E70; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			</tr>

			<tr style="height: 3px;">
			<td colspan="7" style="background-color: #A59E70;"><img src="img/navi/none.gif" alt="" style="height: 3px; width: 1px;"></td>
			</tr>
			<?php

}
            ?>

			<?php if ($id == "6") {
    ?>
			<tr style="height: 18px">
			<td id="nav1_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 0, 'Hier geht es um den Programmierer und Besitzer dieser Seite')"  onmouseout="MainMenuOut1(this, '#FFFFFF', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=aboutme','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=1");
    ?>','mainFrame')">About Me</td>
			<td id="nav2_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 95, 'Hier findest Du News und eine Shoutbox')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=news','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=2");
    ?>','mainFrame')">News</td>
			<td id="nav3_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 190, 'Hier findest Du besondere Features der Seite')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=special','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=3");
    ?>','mainFrame')">Specials</td>
			<td id="nav4_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 285, 'Hier findest Du die Aktionen der Bilder Crew')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=dcus','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=4");
    ?>','mainFrame')">Bilder</td>
			<td id="nav5_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 380, 'Hier findest Du Links zu weiteren guten Seiten')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=links','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=5");
    ?>','mainFrame')">Links</td>
			<td id="nav6_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #FFFFFF; background-color:#A59E70; width:95; font-weight: bold;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 475, 'Hier findest Du den Inhaber und wichtige Impressum Daten')"  onmouseout="MainMenuOut1(this, '#CECECE', '#A59E70')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=impressum','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=6");
    ?>','mainFrame')">Impressum</td>
			<td id="nav7_level1" align="center" class="navdefaultlink" style="color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 570, 'Hier findest Du den Login und die Interaktive Zone')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=login','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=7");
    ?>','mainFrame')">Login</td>
			</tr>

			<tr style="height: 1px;">
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #A59E70; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			</tr>

			<tr style="height: 3px;">
			<td colspan="7" style="background-color: #A59E70;"><img src="img/navi/none.gif" alt="" style="height: 3px; width: 1px;"></td>
			</tr>
			<?php

}
            ?>

			<?php if ($id == "7") {
    ?>
			<tr style="height: 18px">
			<td id="nav1_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 0, 'Hier geht es um den Programmierer und Besitzer dieser Seite')"  onmouseout="MainMenuOut1(this, '#FFFFFF', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=aboutme','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=1");
    ?>','mainFrame')">About Me</td>
			<td id="nav2_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 95, 'Hier findest Du News und eine Shoutbox')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=news','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=2");
    ?>','mainFrame')">News</td>
			<td id="nav3_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 190, 'Hier findest Du besondere Features der Seite')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=special','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=3");
    ?>','mainFrame')">Specials</td>
			<td id="nav4_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 285, 'Hier findest Du die Aktionen der Bilder Crew')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=dcus','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=4");
    ?>','mainFrame')">Bilder</td>
			<td id="nav5_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 380, 'Hier findest Du Links zu weiteren guten Seiten')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=links','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=5");
    ?>','mainFrame')">Links</td>
			<td id="nav6_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 475, 'Hier findest Du den Inhaber und wichtige Impressum Daten')"  onmouseout="MainMenuOut1(this, '#CECECE', '#7B7B7B')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=impressum','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=6");
    ?>','mainFrame')">Impressum</td>
			<td id="nav7_level1" align="center" class="navdefaultlink" style="color: #FFFFFF; background-color:#A59E70; width:95; font-weight: bold;" onmouseover="MainMenuOver1(this, '#FFFFFF', '#A59E70', 570, 'Hier findest Du den Login und die Interaktive Zone')"  onmouseout="MainMenuOut1(this, '#CECECE', '#A59E70')" onclick="linkz('oben.php','topFrame','handler.php?s=<?php print($session_obj->GetSession());
    ?>&goto=login','bottomFrame','<?php print($_SERVER['PHP_SELF']."?s=".$session_obj->GetSession()."&id=7");
    ?>','mainFrame')">Login</td>
			</tr>

			<tr style="height: 1px;">
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			<td style="background-color: #A59E70;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
			</tr>

			<tr style="height: 3px;">
			<td colspan="7" style="background-color: #A59E70;"><img src="img/navi/none.gif" alt="" style="height: 3px; width: 1px;"></td>
			</tr>
			<?php

}
            ?>

			<tr style="height: 1px;">
			<td colspan="7" style="background-color: #000000;"><img src="img/navi/none.gif" style="height: 1px" alt=""></td>
			</tr>

			<tr style="height: 1px;">
			<td bgcolor="#000000" colspan="7"><img src="img/navi/none.gif" style="height: 1px" alt=""></td>
			</tr>

			</table>

			</body>
			</html>

<?php

        }

        if (empty($_GET['id'])) {
            $id = "2";
        } else {
            $id = $_GET['id'];
        }
        drawnavi($id, $session_obj);
    } catch (Exception $e) {
        print("<br>".$e->getMessage()."<br>");
    }
?>
