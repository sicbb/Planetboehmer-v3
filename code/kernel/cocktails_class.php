<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Cocktails Class
    */

    class CocktailsClass
    {
        private $id;
        private $name;
        private $user_id;
        private $zutaten;
        private $art;
        private $menge;
        private $alkoholgehalt;
        private $geschmack;
        private $beschreibung;
        private $pic;

        public function __construct($id, $name, $user_id, $zutaten, $art, $menge, $alkoholgehalt, $geschmack, $beschreibung, $pic)
        {
            $this->id = $id;
            $this->name = $name;
            $this->user_id = $user_id;
            $this->zutaten = $zutaten;
            $this->art = $art;
            $this->menge = $menge;
            $this->alkoholgehalt = $alkoholgehalt;
            $this->geschmack = $geschmack;
            $this->beschreibung = $beschreibung;
            $this->pic = $pic;
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId()
        {
            $this->id = $id;
        }

        public function GetName()
        {
            return $this->name;
        }

        public function SetName()
        {
            $this->name = $name;
        }

        public function GetUser_id()
        {
            return $this->user_id;
        }

        public function SetUser_id()
        {
            $this->user_id = $user_id;
        }

        public function GetZutaten()
        {
            return $this->zutaten;
        }

        public function SetZutaten()
        {
            $this->zutaten = $zutaten;
        }

        public function GetArt()
        {
            return $this->art;
        }

        public function SetArt()
        {
            $this->art = $art;
        }

        public function GetMenge()
        {
            return $this->menge;
        }

        public function SetMenge()
        {
            $this->menge = $menge;
        }

        public function GetAlkoholgehalt()
        {
            return $this->alkoholgehalt;
        }

        public function SetAlkoholgehalt()
        {
            $this->alkoholgehalt = $alkoholgehalt;
        }

        public function GetGeschmack()
        {
            return $this->geschmack;
        }

        public function SetGeschmack()
        {
            $this->geschmack = $geschmack;
        }

        public function GetBeschreibung()
        {
            return $this->beschreibung;
        }

        public function SetBeschreibung()
        {
            $this->beschreibung = $beschreibung;
        }

        public function GetPic()
        {
            return $this->pic;
        }

        public function SetPic()
        {
            $this->pic = $pic;
        }

        public function __destruct()
        {
        }
    }
