<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 10/05/06
        Function: UserReg Class
    */

    class UserRegClass
    {
        private $id;
        private $user_id;
        private $email;
        private $reg_date;

        public function __construct($id, $user_id, $email, $reg_date)
        {
            $this->id = $id;
            $this->user_id = $user_id;
            $this->email = $email;
            $this->reg_date = $reg_date;
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId($tid)
        {
            $this->id = $tid;
        }

        public function GetUser_id()
        {
            return $this->user_id;
        }

        public function SetUser_id($tuser_id)
        {
            $this->user_id = $tuser_id;
        }

        public function GetEmail()
        {
            return $this->email;
        }

        public function SetEmail($temail)
        {
            $this->email = $temail;
        }

        public function GetReg_date()
        {
            return $this->reg_date;
        }

        public function SetReg_date($treg_date)
        {
            $this->reg_date = $treg_date;
        }

        public function __destruct()
        {
        }
    }
