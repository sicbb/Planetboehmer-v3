<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Pom_main Class
    */

    class Pom_mainClass
    {
        private $id;
        private $month;
        private $status;
        private $date;

        public function __construct($id, $month, $status, $date)
        {
            $this->id = $id;
            $this->month = $month;
            $this->status = $status;
            $this->date = $date;
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId()
        {
            $this->id = $id;
        }

        public function GetMonth()
        {
            return $this->month;
        }

        public function SetMonth()
        {
            $this->month = $month;
        }

        public function GetStatus()
        {
            return $this->status;
        }

        public function SetStatus()
        {
            $this->status = $status;
        }

        public function GetDate()
        {
            return $this->date;
        }

        public function SetDate()
        {
            $this->date = $date;
        }

        public function __destruct()
        {
        }
    }
