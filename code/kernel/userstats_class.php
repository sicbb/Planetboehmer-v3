<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 06/12/06
        Function: Userstats Class
    */

    class UserstatsClass
    {
        private $id;
        private $user_id;
        private $logins;
        private $im_write;
        private $im_get;
        private $shoutbox_write;
        private $profils_view;
        private $clicks;

        public function __construct($id, $user_id, $logins, $im_write, $im_get, $shoutbox_write, $profils_view, $clicks)
        {
            $this->id = $id;
            $this->user_id = $user_id;
            $this->logins = $logins;
            $this->im_write = $im_write;
            $this->im_get = $im_get;
            $this->shoutbox_write = $shoutbox_write;
            $this->profils_view = $profils_view;
            $this->clicks = $clicks;
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId($tid)
        {
            $this->id = $tid;
        }

        public function GetUser_id()
        {
            return $this->user_id;
        }

        public function SetUser_id($tuser_id)
        {
            $this->user_id = $tuser_id;
        }

        public function GetLogins()
        {
            return $this->logins;
        }

        public function SetLogins($tlogins)
        {
            $this->logins = $tlogins;
        }

        public function GetIm_write()
        {
            return $this->im_write;
        }

        public function SetIm_write($tim_write)
        {
            $this->im_write = $tim_write;
        }

        public function GetIm_get()
        {
            return $this->im_get;
        }

        public function SetIm_get($tim_get)
        {
            $this->im_get = $tim_get;
        }

        public function GetShoutbox_write()
        {
            return $this->shoutbox_write;
        }

        public function SetShoutbox_write($tshoutbox_write)
        {
            $this->shoutbox_write = $tshoutbox_write;
        }

        public function GetProfils_view()
        {
            return $this->profils_view;
        }

        public function SetProfils_view($tprofils_view)
        {
            $this->profils_view = $tprofils_view;
        }

        public function GetClicks()
        {
            return $this->clicks;
        }

        public function SetClicks($tclicks)
        {
            $this->clicks = $tclicks;
        }

        public function __destruct()
        {
        }
    }
