<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: DB Dcuscomments Class
    */

    require_once "db_mapper_class.php";
    require_once "dcuscomments_class.php";

    class DBDcuscommentsClass extends DBMapperClass
    {

        public function __construct()
        {
            parent::__construct();
        }

        public function SelectAllByHeadId($id)
        {
            $sql_query = "SELECT * FROM dcus_comments WHERE dcus_head_id = '".$id."' ORDER BY date DESC";
            $result = $this->ExecSql($sql_query);

            if ($result->num_rows > 0) {
                $i = 0;
                while ($row = $result->fetch_assoc()) {
                    $Dcuscomments_objs[$i++] = new Dcus_commentsClass($row['id'],
                            $row['dcus_head_id'],
                            $row['user_id'],
                            $row['text'],
                            $row['date'],
                            $row['ip']);
                }

                return $Dcuscomments_objs;
            } else {
                return;
            }
        }

        public function GetCommentById($id)
        {
            $sql_query = "SELECT * FROM dcus_comments WHERE id = '".$id."'";
            $result = $this->ExecSql($sql_query);

            if ($result->num_rows == 1) {
                $row = $result->fetch_assoc();

                $Dcuscomments_obj = new Dcus_commentsClass($row['id'],
                            $row['dcus_head_id'],
                            $row['user_id'],
                            $row['text'],
                            $row['date'],
                            $row['ip']);

                return $Dcuscomments_obj;
            } else {
                return;
            }
        }

        public function GetCommentCountByHeadId($id)
        {
            $sql_query = "SELECT count(id) as count FROM dcus_comments WHERE dcus_head_id = '".$id."'";
            $result = $this->ExecSql($sql_query);

            $row = $result->fetch_assoc();

            return $row['count'];
        }

        public function Insert($dcuscomment_obj)
        {
            if ($dcuscomment_obj != null) {
                $sql_query = "INSERT INTO dcus_comments VALUES(NULL, '".$dcuscomment_obj->GetDcus_head_id()."', '".$dcuscomment_obj->GetUser_id()."', '".$dcuscomment_obj->GetText()."', '".$dcuscomment_obj->GetDate()."', '".$dcuscomment_obj->GetIp()."')";
                $this->ExecSql($sql_query);
            }
        }

        public function DeleteById($id)
        {
            $sql_query = "DELETE FROM dcus_comments WHERE id = '".$id."'";
            $this->ExecSql($sql_query);
        }

        public function __destruct()
        {
            parent::__destruct();
        }
    }
