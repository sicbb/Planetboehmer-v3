<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Ip_filter Class
    */

    class Ip_filterClass
    {
        private $id;
        private $ip;

        public function __construct($id, $ip)
        {
            $this->id = $id;
            $this->ip = $ip;
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId()
        {
            $this->id = $id;
        }

        public function GetIp()
        {
            return $this->ip;
        }

        public function SetIp()
        {
            $this->ip = $ip;
        }

        public function __destruct()
        {
        }
    }
