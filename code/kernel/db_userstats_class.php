<?php
    /*
        Author: Daniel Böhmer
        Date: 14/12/05
        Title: DB UserStats Class
    */

    require_once "db_mapper_class.php";
    require_once "userstats_class.php";

    class DBUserStatsClass extends DBMapperClass
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function GetAllUserStats($user_id)
        {
            $sql_query = "SELECT * FROM userstats WHERE user_id = '".$user_id."'";
            $result = $this->ExecSql($sql_query);

            if ($result->num_rows == 1) {
                $row = $result->fetch_assoc();

                $stats_obj = new UserstatsClass($row['id'],
                        $row['user_id'],
                        $row['logins'],
                        $row['im_write'],
                        $row['im_get'],
                        $row['shoutbox_write'],
                        $row['profils_view'],
                        $row['clicks']);

                return $stats_obj;
            } else {
                return;
            }
        }

        public function UpdateUserStats($stats_obj)
        {
            $sql_query = "UPDATE userstats SET logins = '".$stats_obj->GetLogins()."'
				, im_write = '".$stats_obj->GetIm_write()."'
				, im_get = '".$stats_obj->GetIm_get()."'
				, shoutbox_write = '".$stats_obj->GetShoutbox_write()."'
				, profils_view = '".$stats_obj->GetProfils_view()."'
				, clicks = '".$stats_obj->GetClicks()."' WHERE user_id = '".$stats_obj->GetUser_id()."'";
            $this->ExecSql($sql_query);
        }

        public function __destruct()
        {
            parent::__destruct();
        }
    }
