<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 10/05/06
        Function: Shoutbox Class
    */

    class ShoutboxClass
    {
        private $id;
        private $user_id;
        private $kommentar;
        private $date;
        private $ip;

        public function __construct($id, $user_id, $kommentar, $date, $ip)
        {
            $this->id = $id;
            $this->user_id = $user_id;
            $this->kommentar = $kommentar;
            $this->date = $date;
            $this->ip = $ip;
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId($tid)
        {
            $this->id = $tid;
        }

        public function GetUser_id()
        {
            return $this->user_id;
        }

        public function SetUser_id($tuser_id)
        {
            $this->user_id = $tuser_id;
        }

        public function GetKommentar()
        {
            return $this->kommentar;
        }

        public function SetKommentar($tkommentar)
        {
            $this->kommentar = $tkommentar;
        }

        public function GetDate()
        {
            return $this->date;
        }

        public function SetDate($tdate)
        {
            $this->date = $tdate;
        }

        public function GetIp()
        {
            return $this->ip;
        }

        public function SetIp($tip)
        {
            $this->ip = $tip;
        }

        public function __destruct()
        {
        }
    }
