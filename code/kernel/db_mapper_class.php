<?php
    /*
        Author: Philipp Gaida
        Date: 04/02/05
        Title: Database Mapper Class - Database Communiction with MySQL-Server

        Achtung: Single Connection Method!!! (Es kann nur ein Server gleichzeitig angesprochen werden)
    */

    require_once dirname(__FILE__) . '/config.php';

    class DBMapperClass
    {
        private static $Db = null;
        private static $db_host = DB_HOST;
        private static $db_login = DB_USER;
        private static $db_pass = DB_PASS;
        private static $db_name = DB_NAME;
        private static $InstanceCounter = 0;

        private static $executetime = 0;

        private function Connect()
        {
            if (self::$InstanceCounter <= 0) {
                self::$Db = new mysqli(self::$db_host, self::$db_login, self::$db_pass, self::$db_name);

                if (self::$Db->connect_error !== null) {
                    throw new Exception(self::$Db->connect_error);
                }
            }

            self::$InstanceCounter++;
        }

        public function __construct()
        {
            $this->Connect();
        }

        public function ExecSql($sql)
        {
            $time = microtime(true);
            $result = self::$Db->query($sql);

            if (!$result) {
                throw new Exception("ERROR: Fehler bei Abfrage ".$sql."<br>MSG: ".self::$Db->error);
            } else {
                self::$executetime += microtime(true) - $time + 1;

                return $result;
            }
        }

        public function __destruct()
        {
            self::$InstanceCounter--;
            if (self::$InstanceCounter <= 0) {
                //print('<div align="left"><font color="#CCCCCC" size="1">DB:'.round(self::$executetime,4)."</font></div>");
                self::$Db->close();
            }
        }
    }
