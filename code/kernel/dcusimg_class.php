<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Dcusimg Class
    */

    class DcusimgClass
    {
        private $id;
        private $dcus_head_id;
        private $img;

        public function __construct($id, $dcus_head_id, $img)
        {
            $this->id = $id;
            $this->dcus_head_id = $dcus_head_id;
            $this->img = $img;
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId()
        {
            $this->id = $id;
        }

        public function GetDcus_head_id()
        {
            return $this->dcus_head_id;
        }

        public function SetDcus_head_id()
        {
            $this->dcus_head_id = $dcus_head_id;
        }

        public function GetImg()
        {
            return $this->img;
        }

        public function SetImg()
        {
            $this->img = $img;
        }

        public function __destruct()
        {
        }
    }
