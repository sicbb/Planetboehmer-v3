<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: DB User Reg Class
    */

    require_once "db_mapper_class.php";
    require_once "user_reg_class.php";

    class DBUserRegClass extends DBMapperClass
    {

        public function __construct()
        {
            parent::__construct();
        }

        public function InsertUserReg($user_reg_obj)
        {
            if ($user_reg_obj != null) {
                $sql_query = "INSERT INTO user_reg values(NULL, '".$user_reg_obj->GetUser_id()."','".$user_reg_obj->GetEmail()."','".$user_reg_obj->GetReg_date()."')";
                $this->ExecSql($sql_query);
            } else {
                throw new Exception("ERROR: Fehler beim Eintragen des Users!");
            }
        }

        public function __destruct()
        {
            parent::__destruct();
        }
    }
