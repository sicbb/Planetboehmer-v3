<?php
    /*
        Author: Daniel Böhmer
        Date: 14/12/05
        Title: DB Cocktails Class
    */

    require_once "db_mapper_class.php";
    require_once "cocktails_class.php";

    class DBCocktailsClass extends DBMapperClass
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function GetAllCocktails($order = "name")
        {
            $sql_query = "SELECT * FROM cocktails ORDER BY '".$order."'";
            $result = $this->ExecSql($sql_query);

            if ($result->num_rows > 0) {
                $i = 0;
                while ($row = $result->fetch_assoc()) {
                    $new_objs[$i++] = new CocktailsClass($row['id'],
                        $row['name'],
                        $row['user_id'],
                        $row['zutaten'],
                        $row['art'],
                        $row['menge'],
                        $row['alkoholgehalt'],
                        $row['geschmack'],
                        $row['beschreibung'],
                        $row['pic']);
                }

                return $new_objs;
            } else {
                return;
            }
        }

        public function GetCocktailById($id)
        {
            $sql_query = "SELECT * FROM cocktails WHERE id = '".$id."'";
            $result = $this->ExecSql($sql_query);

            if ($result->num_rows == 1) {
                $row = $result->fetch_assoc();

                $new_obj = new CocktailsClass($row['id'],
                        $row['name'],
                        $row['user_id'],
                        $row['zutaten'],
                        $row['art'],
                        $row['menge'],
                        $row['alkoholgehalt'],
                        $row['geschmack'],
                        $row['beschreibung'],
                        $row['pic']);

                return $new_obj;
            } else {
                return;
            }
        }

        public function __destruct()
        {
            parent::__destruct();
        }
    }
