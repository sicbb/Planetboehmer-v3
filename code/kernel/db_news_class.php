<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: DB News Class
    */

    require_once "db_mapper_class.php";
    require_once "news_class.php";

    class DBNewsClass extends DBMapperClass
    {

        public function __construct()
        {
            parent::__construct();
        }

        public function GetAll($length = 10)
        {
            $sql_query = "SELECT * FROM news order by id desc LIMIT ".$length;
            $result = $this->ExecSql($sql_query);
            if ($result->num_rows > 0) {
                $i = 0;
                while ($row = $result->fetch_assoc()) {
                    $new_objs[$i++] = new NewsClass($row['headline'], $row['text'], $row['date'], $row['id'], $row['user_id']);
                }

                return $new_objs;
            }

            return;
        }

        public function Insert($news_obj)
        {
            $sql_query = "INSERT INTO news VALUES(NULL, '".$news_obj->GetHeadline()."', '".$news_obj->GetUser_id()."', '".$news_obj->GetText()."', '".$news_obj->GetDate()."');";
            $this->ExecSql($sql_query);
            $sql_query = "SELECT max(id) as id FROM news";
            $result = $this->ExecSql($sql_query);
            $row = $result->fetch_assoc();
            $news_obj->SetId($row['id']);
        }

        public function DeleteById($id)
        {
            $sql_query = "DELETE FROM news WHERE id =".$id;
            $this->ExecSql($sql_query);
        }

        public function __destruct()
        {
            parent::__destruct();
        }
    }
