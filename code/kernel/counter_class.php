<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Counter Class
    */

    class CounterClass
    {
        private $id;
        private $ip;
        private $timestamp;
        private $session_id;

        public function __construct($id = 0, $ip = "", $timestamp = "", $session_id = 0)
        {
            $this->id = $id;
            if (empty($ip) && empty($timestamp)) {
                $this->ip = $_SERVER['REMOTE_ADDR'];
                $this->timestamp = time();
            } else {
                $this->ip = $ip;
                $this->timestamp = $timestamp;
            }
            $this->session_id = $session_id;
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId($tid)
        {
            $this->id = $tid;
        }

        public function GetIp()
        {
            return $this->ip;
        }

        public function SetIp($tip)
        {
            $this->ip = $tip;
        }

        public function GetTimestamp()
        {
            return $this->timestamp;
        }

        public function SetTimestamp($ttimestamp)
        {
            $this->timestamp = $ttimestamp;
        }

        public function GetSessionid()
        {
            return $this->session_id;
        }

        public function SetSessionid($tsessionid)
        {
            $this->session_id = $tsessionid;
        }

        public function __destruct()
        {
        }
    }
