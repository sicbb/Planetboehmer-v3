<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/26/05
        Function: DB Counter Class
    */

    require_once "db_mapper_class.php";
    require_once "counter_class.php";

    class DBCounterClass extends DBMapperClass
    {

        public function __construct()
        {
            parent::__construct();
        }

        public function GetCounterCount()
        {
            $sql_query = "SELECT count(id) as count FROM counter";
            $result = $this->ExecSql($sql_query);

            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();

                return $row['count'];
            }
        }

        public function InsertCounter($counter_obj, $session_id)
        {
            if ($counter_obj != null) {
                $sql_query = "INSERT INTO counter VALUES (NULL, '".$counter_obj->GetIp()."', '".$counter_obj->GetTimestamp()."', '".$session_id."')";
                $this->ExecSql($sql_query);
            }
        }

        public function GetLastCounterBySession($session_id)
        {
            if (!empty($session_id)) {
                $sql_query = "SELECT max(id) as id FROM counter WHERE session_id = '".$session_id."'";
                $result = $this->ExecSql($sql_query);

                if ($result->num_rows > 0) {
                    $row = $result->fetch_assoc();
                    $maxid = $row['id'];

                    $sql_query = "SELECT * FROM counter WHERE id = '".$maxid."'";
                    $result = $this->ExecSql($sql_query);

                    if ($result->num_rows > 0) {
                        $row = $result->fetch_assoc();
                        $counter_obj = new CounterClass($row['id'], $row['ip'], $row['timestamp']);

                        return $counter_obj;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
        }

        public function GetCountPerDay($date = 0)
        {
            if (!empty($date)) {
                $startstamp = mktime(0, 0, 0, $date['month'], $date['day'], $date['year']);
                $endstamp = mktime(23, 59, 59, $date['month'], $date['day'], $date['year']);
            } else {
                $startstamp = mktime(0, 0, 0, date("m", time()), date("d", time()), date("Y", time()));
                $endstamp = mktime(23, 59, 59, date("m", time()), date("d", time()), date("Y", time()));
            }

            $sql_query = "SELECT count(id) as count FROM counter WHERE timestamp BETWEEN '".$startstamp."' AND '".$endstamp."'";
            $result = $this->ExecSql($sql_query);

            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();

                return $row['count'];
            } else {
                return;
            }
        }

        public function GetCountPerMonth($date = 0)
        {
            if (!empty($date)) {
                $startstamp = mktime(0, 0, 0, $date['month'], 1, $date['year']);
                $days_per_month = cal_days_in_month(CAL_GREGORIAN, $date['month'], $date['year']);
                $endstamp = mktime(23, 59, 59, $date['month'], $days_per_month, $date['year']);
            } else {
                $startstamp = mktime(0, 0, 0, date("m", time()), 1, date("Y", time()));
                $days_per_month = cal_days_in_month(CAL_GREGORIAN, date("m", time()), date("Y", time()));
                $endstamp = mktime(23, 59, 59, date("m", time()), $days_per_month, date("Y", time()));
            }

            $sql_query = "SELECT count(id) as count FROM counter WHERE timestamp BETWEEN '".$startstamp."' AND '".$endstamp."'";
            $result = $this->ExecSql($sql_query);

            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();

                return $row['count'];
            } else {
                return;
            }
        }

        public function GetCounterDataPerDay($day = 0, $month = 0, $year = 0)
        {
            //$daystamp = mktime(0, 0, 0, date("m", time()), date("d", time()), date("Y", time()));
            $daystamp = mktime(0, 0, 0, $month, $day, $year);

            $sql_query = "SELECT * FROM counter WHERE timestamp > '".$daystamp."' ORDER BY timestamp";
            $result = $this->ExecSql($sql_query);

            if ($result->num_rows > 0) {
                $i = 0;
                while ($row = $result->fetch_assoc()) {
                    $counter_objs[$i] = new CounterClass($row['id'], $row['ip'], $row['timestamp'], $row['session_id']);
                }

                return $counter_objs;
            }
        }

        public function GetCounterCountPerHour($day, $month, $year, $hour)
        {
            //$daystamp = mktime(0, 0, 0, date("m", time()), date("d", time()), date("Y", time()));
            $startstamp = mktime($hour, 0, 0, $month, $day, $year);
            $endstamp = mktime($hour+1, 0, 0, $month, $day, $year);

            $sql_query = "SELECT count(id) as count FROM counter WHERE timestamp >= '".$startstamp."' AND timestamp < '".$endstamp."' ORDER BY timestamp";
            $result = $this->ExecSql($sql_query);

            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();

                return $row['count'];
            } else {
                return 0;
            }
        }

        public function __destruct()
        {
            parent::__destruct();
        }
    }
