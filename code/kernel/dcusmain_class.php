<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Dcusmain Class
    */

    class DcusmainClass
    {
        private $id;
        private $dcus_head_id;
        private $text;

        public function __construct($id = 0, $dcus_head_id, $text)
        {
            $this->id = $id;
            $this->dcus_head_id = $dcus_head_id;
            $this->text = $text;
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId()
        {
            $this->id = $id;
        }

        public function GetDcus_head_id()
        {
            return $this->dcus_head_id;
        }

        public function SetDcus_head_id()
        {
            $this->dcus_head_id = $dcus_head_id;
        }

        public function GetText()
        {
            return $this->text;
        }

        public function SetText()
        {
            $this->text = $text;
        }

        public function __destruct()
        {
        }
    }
