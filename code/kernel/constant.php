<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 08/14/05
        Function: Konstanten Class
    */

    define("img_user_dir", "img/user/");
    define("img_dcus_dir", "img/upload/");
    define("img_dcustemp_dir", "img/upload_tmp/");
