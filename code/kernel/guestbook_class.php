<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Guestbook Class
    */

    class GuestbookClass
    {
        private $id;
        private $name;
        private $email;
        private $hp;
        private $icq;
        private $text;
        private $date;
        private $ip;

        public function __construct($id, $name, $email, $hp, $icq, $text, $date, $ip)
        {
            $this->id = $id;
            $this->name = $name;
            $this->email = $email;
            $this->hp = $hp;
            $this->icq = $icq;
            $this->text = $text;
            $this->date = $date;
            $this->ip = $ip;
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId()
        {
            $this->id = $id;
        }

        public function GetName()
        {
            return $this->name;
        }

        public function SetName()
        {
            $this->name = $name;
        }

        public function GetEmail()
        {
            return $this->email;
        }

        public function SetEmail()
        {
            $this->email = $email;
        }

        public function GetHp()
        {
            return $this->hp;
        }

        public function SetHp()
        {
            $this->hp = $hp;
        }

        public function GetIcq()
        {
            return $this->icq;
        }

        public function SetIcq()
        {
            $this->icq = $icq;
        }

        public function GetText()
        {
            return $this->text;
        }

        public function SetText()
        {
            $this->text = $text;
        }

        public function GetDate()
        {
            return $this->date;
        }

        public function SetDate()
        {
            $this->date = $date;
        }

        public function GetIp()
        {
            return $this->ip;
        }

        public function SetIp()
        {
            $this->ip = $ip;
        }

        public function __destruct()
        {
        }
    }
