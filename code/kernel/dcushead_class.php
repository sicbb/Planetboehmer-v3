<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Dcushead Class
    */

    class DcusheadClass
    {
        private $id;
        private $user_id;
        private $headline;
        private $img;
        private $text;
        private $date;
        private $clicks;

        public function __construct($id = 0, $user_id, $headline, $img, $text, $date, $clicks)
        {
            $this->id = $id;
            $this->user_id = $user_id;
            $this->headline = $headline;
            $this->img = $img;
            $this->text = $text;
            $this->date = $date;
            $this->clicks = $clicks;
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId($did)
        {
            $this->id = $did;
        }

        public function GetUser_id()
        {
            return $this->user_id;
        }

        public function SetUser_id($uid)
        {
            $this->user_id = $uid;
        }

        public function GetHeadline()
        {
            return $this->headline;
        }

        public function SetHeadline()
        {
            $this->headline = $headline;
        }

        public function GetImg()
        {
            return $this->img;
        }

        public function SetImg()
        {
            $this->img = $img;
        }

        public function GetText()
        {
            return $this->text;
        }

        public function SetText()
        {
            $this->text = $text;
        }

        public function GetDate()
        {
            return $this->date;
        }

        public function SetDate()
        {
            $this->date = $date;
        }

        public function GetClicks()
        {
            return $this->clicks;
        }

        public function SetClicks($tclicks)
        {
            $this->clicks = $tclicks;
        }

        public function __destruct()
        {
        }
    }
