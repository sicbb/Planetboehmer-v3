<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: News Class
    */

    class NewsClass
    {
        private $id;
        private $headline;
        private $user_id;
        private $text;
        private $date;

        public function __construct($headline, $text, $date = 0, $id = -1, $user_id = 0)
        {
            $this->id = $id;
            $this->headline = $headline;
            $this->user_id = $user_id;
            $this->text = $text;

            if (empty($date)) {
                $this->date = time();
            } else {
                $this->date = $date;
            }
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId($aid)
        {
            $this->id = $aid;
        }

        public function GetHeadline()
        {
            return $this->headline;
        }

        public function SetHeadline()
        {
            $this->headline = $headline;
        }

        public function GetUser_id()
        {
            return $this->user_id;
        }

        public function SetUser_id()
        {
            $this->user_id = $user_id;
        }

        public function GetText()
        {
            return $this->text;
        }

        public function SetText()
        {
            $this->text = $text;
        }

        public function GetDate()
        {
            return $this->date;
        }

        public function SetDate()
        {
            $this->date = $date;
        }

        public function __destruct()
        {
        }
    }
