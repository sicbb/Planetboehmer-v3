<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Pom_img_vote Class
    */

    class Pom_img_voteClass
    {
        private $id;
        private $pom_img_id;
        private $user_id;
        private $pom_main_id;

        public function __construct($id, $pom_img_id, $user_id, $pom_main_id)
        {
            $this->id = $id;
            $this->pom_img_id = $pom_img_id;
            $this->user_id = $user_id;
            $this->pom_main_id = $pom_main_id;
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId()
        {
            $this->id = $id;
        }

        public function GetPom_img_id()
        {
            return $this->pom_img_id;
        }

        public function SetPom_img_id()
        {
            $this->pom_img_id = $pom_img_id;
        }

        public function GetUser_id()
        {
            return $this->user_id;
        }

        public function SetUser_id()
        {
            $this->user_id = $user_id;
        }

        public function GetPom_main_id()
        {
            return $this->pom_main_id;
        }

        public function SetPom_main_id()
        {
            $this->pom_main_id = $pom_main_id;
        }

        public function __destruct()
        {
        }
    }
