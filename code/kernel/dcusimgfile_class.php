<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Dcus_img_file Class
    */

    class Dcus_img_fileClass
    {
        private $id;
        private $date;
        private $count;

        public function __construct($id, $date, $count)
        {
            $this->id = $id;
            $this->date = $date;
            $this->count = $count;
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId()
        {
            $this->id = $id;
        }

        public function GetDate()
        {
            return $this->date;
        }

        public function SetDate()
        {
            $this->date = $date;
        }

        public function GetCount()
        {
            return $this->count;
        }

        public function SetCount()
        {
            $this->count = $count;
        }

        public function __destruct()
        {
        }
    }
