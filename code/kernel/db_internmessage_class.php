<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/22/05
        Function: DB Internmessage Class
    */

    require_once "db_mapper_class.php";
    require_once "internmessage_class.php";

    class DBInternmessageClass extends DBMapperClass
    {

        public function __construct()
        {
            parent::__construct();
        }

        public function GetInByUserId($id)
        {
            $sql_query = "SELECT * FROM intern_message WHERE to_user_id = '".$id."' AND delete_flag != 'true' ORDER BY id DESC";
            $result = $this->ExecSql($sql_query);
            if ($result->num_rows > 0) {
                $i = 0;
                while ($row = $result->fetch_assoc()) {
                    $internmessage_objs[$i++] = new InternmessageClass($row['id'],
                    $row['from_user_id'],
                    $row['to_user_id'],
                    $row['reason'],
                    $row['text'],
                    $row['date'],
                    $row['status'],
                    $row['delete_flag'],
                    $row['ip']);
                }

                return $internmessage_objs;
            } else {
                return;
            }
        }

        public function GetMessageById($id)
        {
            $sql_query = "SELECT * FROM intern_message WHERE id = ".$id;
            $result = $this->ExecSql($sql_query);
            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();
                $internmessage_obj = new InternmessageClass($row['id'],
                    $row['from_user_id'],
                    $row['to_user_id'],
                    $row['reason'],
                    $row['text'],
                    $row['date'],
                    $row['status'],
                    $row['delete_flag'],
                    $row['ip']);

                return $internmessage_obj;
            } else {
                return;
            }
        }

        public function GetOutByUserId($id)
        {
            $sql_query = "SELECT * FROM intern_message WHERE from_user_id = '".$id."' ORDER BY id DESC";
            $result = $this->ExecSql($sql_query);
            if ($result->num_rows > 0) {
                $i = 0;
                while ($row = $result->fetch_assoc()) {
                    $internmessage_objs[$i++] = new InternmessageClass($row['id'],
                    $row['from_user_id'],
                    $row['to_user_id'],
                    $row['reason'],
                    $row['text'],
                    $row['date'],
                    $row['status'],
                    $row['delete_flag'],
                    $row['ip']);
                }

                return $internmessage_objs;
            } else {
                return;
            }
        }

        public function InsertMessage(&$internmessage_obj)
        {
            if (!empty($internmessage_obj)) {
                $sql_query = "INSERT INTO intern_message VALUES (NULL,  '".$internmessage_obj->GetFrom_user_id()."'
					, '".$internmessage_obj->GetTo_user_id()."'
					, '".$internmessage_obj->GetReason()."'
					, '".$internmessage_obj->GetText()."'
					, '".$internmessage_obj->GetDate()."'
					, '".$internmessage_obj->GetStatus()."'
					, '".$internmessage_obj->GetDelete_flag()."'
					, '".$internmessage_obj->GetIp()."')";
                $this->ExecSql($sql_query);
                $sql_query = "SELECT max(id) as id FROM intern_message;";
                $result = $this->ExecSql($sql_query);
                $row = $result->fetch_assoc();
                $internmessage_obj->SetId($row['id']);
            }
        }

        public function UpdateMessage($internmessage_obj)
        {
            if (!empty($internmessage_obj)) {
                $sql_query = "UPDATE intern_message SET from_user_id = '".$internmessage_obj->GetFrom_user_id()."'
					,to_user_id = '".$internmessage_obj->GetTo_user_id()."'
					,reason = '".addslashes($internmessage_obj->GetReason())."'
					,text = '".addslashes($internmessage_obj->GetText())."'
					,date = '".$internmessage_obj->GetDate()."'
					,status = '".$internmessage_obj->GetStatus()."'
					,delete_flag = '".$internmessage_obj->GetDelete_flag()."'
					,ip = '".$internmessage_obj->GetIp()."' WHERE id = '".$internmessage_obj->GetId()."'";
                $this->ExecSql($sql_query);
            }
        }

        public function DeleteOldMessages()
        {
            // Aktuelle Zeit - 2 Wochen
            $stamp = time()-2*7*24*60*60;

            $sql_query = "SELECT id FROM intern_message WHERE date < '".$stamp."'";
            $result = $this->ExecSql($sql_query);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $sql_query = "DELETE FROM intern_message WHERE id = '".$row['id']."'";
                    $this->ExecSql($sql_query);
                }
            }
        }

        public function __destruct()
        {
            parent::__destruct();
        }
    }
