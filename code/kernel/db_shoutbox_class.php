<?php
    /*
        Author: Daniel Böhmer
        Date: 19/05/05
        Title: DB Shoutbox Class
    */

    require_once "db_mapper_class.php";
    require_once "shoutbox_class.php";

    class DBShoutboxClass extends DBMapperClass
    {
        public function __construct()
        {
            parent::__construct();
        }

        public function InsertDB(&$shoutbox_obj)
        {
            $strings = explode(" ", $shoutbox_obj->GetKommentar());
            $newstring = "";
            foreach ($strings as $string) {
                $newstring .= wordwrap($string, 16, "\n", 1)." ";
            }

            $sql_query = "INSERT INTO shoutbox VALUES (NULL, '".$shoutbox_obj->GetUser_id()."', '".$newstring."', ".$shoutbox_obj->GetDate().", '".$shoutbox_obj->GetIp()."');";
            $this->ExecSql($sql_query);
            $sql_query = "SELECT max(id) as id FROM shoutbox;";
            $result = $this->ExecSql($sql_query);
            $row = $result->fetch_assoc();
            $shoutbox_obj->SetId($row['id']);
        }

        public function DelFromDB($id)
        {
            $sql_query = "DELETE FROM shoutbox WHERE id =".$id;
            $this->ExecSql($sql_query);
        }

        public function GetDatById($id)
        {
            $sql_query = "SELECT * FROM shoutbox WHERE id = ".$id;
            $result = $this->ExecSql($sql_query);
            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();
                $shoutbox_obj = new ShoutboxClass($row['id'], $row['user_id'], $row['kommentar'], $row['date'], $row['ip']);
            }

            return $shoutbox_obj;
        }

        public function GetNewsCount()
        {
            $sql_query = "SELECT count(id) as count FROM shoutbox";
            $result = $this->ExecSql($sql_query);
            $row = $result->fetch_assoc();

            return $row['count'];
        }

        public function SelectAll($from, $to)
        {
            $sql_query = "SELECT * FROM shoutbox order by id DESC LIMIT ".$from.",".$to.";";
            $result = $this->ExecSql($sql_query);
            $all = array();
            $length = $result->num_rows;
            for ($i = 0; $i<$length; $i++) {
                $row = $result->fetch_assoc();
                $all[$i] = new ShoutboxClass($row['id'], $row['user_id'], $row['kommentar'], $row['date'], $row['ip']);
            }

            return $all;
        }

        public function __destruct()
        {
            parent::__destruct();
        }
    }
