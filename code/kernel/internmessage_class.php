<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Internmessage Class
    */

    class InternmessageClass
    {
        private $id;
        private $from_user_id;
        private $to_user_id;
        private $reason;
        private $text;
        private $date;
        private $status;
        private $delete_flag;
        private $ip;

        public function __construct($id, $from_user_id, $to_user_id, $reason, $text, $date, $status, $delete_flag, $ip)
        {
            $this->id = $id;
            $this->from_user_id = $from_user_id;
            $this->to_user_id = $to_user_id;
            $this->reason = $reason;
            $this->text = $text;
            if (empty($date)) {
                $this->date = time();
            } else {
                $this->date = $date;
            }
            $this->status = $status;
            $this->delete_flag = $delete_flag;
            if (empty($ip)) {
                $this->ip = $_SERVER['REMOTE_ADDR'];
            } else {
                $this->ip = $ip;
            }
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId()
        {
            $this->id = $id;
        }

        public function GetFrom_user_id()
        {
            return $this->from_user_id;
        }

        public function SetFrom_user_id()
        {
            $this->from_user_id = $from_user_id;
        }

        public function GetTo_user_id()
        {
            return $this->to_user_id;
        }

        public function SetTo_user_id()
        {
            $this->to_user_id = $to_user_id;
        }

        public function GetReason()
        {
            return $this->reason;
        }

        public function SetReason()
        {
            $this->reason = $reason;
        }

        public function GetText()
        {
            return $this->text;
        }

        public function SetText()
        {
            $this->text = $text;
        }

        public function GetDate()
        {
            return $this->date;
        }

        public function SetDate()
        {
            $this->date = $date;
        }

        public function GetStatus()
        {
            return $this->status;
        }

        public function SetStatus($astatus)
        {
            $this->status = $astatus;
        }

        public function GetDelete_flag()
        {
            return $this->delete_flag;
        }

        public function SetDelete_flag($adelete_flag)
        {
            $this->delete_flag = $adelete_flag;
        }

        public function GetIp()
        {
            return $this->ip;
        }

        public function SetIp()
        {
            $this->ip = $ip;
        }

        public function __destruct()
        {
        }
    }
