<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 02/09/06
        Function: Dcus_comments Class
    */

    class Dcus_commentsClass
    {
        private $id;
        private $dcus_head_id;
        private $user_id;
        private $text;
        private $date;
        private $ip;

        public function __construct($id, $dcus_head_id, $user_id, $text, $date, $ip)
        {
            $this->id = $id;
            $this->dcus_head_id = $dcus_head_id;
            $this->user_id = $user_id;
            $this->text = $text;
            $this->date = $date;
            $this->ip = $ip;
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId($tid)
        {
            $this->id = $tid;
        }

        public function GetDcus_head_id()
        {
            return $this->dcus_head_id;
        }

        public function SetDcus_head_id($tdcus_head_id)
        {
            $this->dcus_head_id = $tdcus_head_id;
        }

        public function GetUser_id()
        {
            return $this->user_id;
        }

        public function SetUser_id($tuser_id)
        {
            $this->user_id = $tuser_id;
        }

        public function GetText()
        {
            return $this->text;
        }

        public function SetText($ttext)
        {
            $this->text = $ttext;
        }

        public function GetDate()
        {
            return $this->date;
        }

        public function SetDate($tdate)
        {
            $this->date = $tdate;
        }

        public function GetIp()
        {
            return $this->ip;
        }

        public function SetIp($tip)
        {
            $this->ip = $tip;
        }

        public function __destruct()
        {
        }
    }
