<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: User Class
    */

    require_once "php_salt.php";

    class UserClass
    {
        private $id;
        private $login;
        private $pass;
        private $email;
        private $birthday;
        private $location;
        private $userinfo;
        private $icq;
        private $hp;
        private $picture;
        private $flag;
        private $status;
        private $activationkey;
        private $last_online;
        private $ateam;
        private $emailme;

        public function __construct($id, $login, $pass, $email, $birthday, $location, $userinfo, $icq, $hp, $picture, $flag, $status, $activationkey, $last_online, $ateam, $emailme)
        {
            $this->id = $id;
            $this->login = $login;
            $this->pass = $pass;
            $this->email = $email;
            $this->birthday = $birthday;
            $this->location = $location;
            $this->userinfo = $userinfo;
            $this->icq = $icq;
            $this->hp = $hp;
            $this->picture = $picture;
            $this->flag = $flag;
            $this->status = $status;
            $this->activationkey = $activationkey;
            $this->last_online = $last_online;
            $this->ateam = $ateam;
            $this->emailme = $emailme;
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId($tid)
        {
            $this->id = $tid;
        }

        public function GetLogin()
        {
            return $this->login;
        }

        public function SetLogin($tlogin)
        {
            $this->login = $tlogin;
        }

        public function GetPass()
        {
            return $this->pass;
        }

        public function SetPass($tpass)
        {
            $this->pass = $tpass;
        }

        public function GetEmail()
        {
            return $this->email;
        }

        public function SetEmail($temail)
        {
            $this->email = $temail;
        }

        public function GetLocation()
        {
            return $this->location;
        }

        public function SetLocation($tlocation)
        {
            $this->location = $tlocation;
        }

        public function GetBirthday()
        {
            return $this->birthday;
        }

        public function SetBirthday($tbirthday)
        {
            $this->birthday = $tbirthday;
        }

        public function GetUserinfo()
        {
            return $this->userinfo;
        }

        public function SetUserinfo($tinfo)
        {
            $this->userinfo = $tinfo;
        }

        public function GetIcq()
        {
            return $this->icq;
        }

        public function SetIcq($ticq)
        {
            $this->icq = $ticq;
        }

        public function GetHp()
        {
            return $this->hp;
        }

        public function SetHp($thp)
        {
            $this->hp = $thp;
        }

        public function GetPicture()
        {
            return $this->picture;
        }

        public function SetPicture($tpicture)
        {
            $this->picture = $tpicture;
        }

        public function GetFlag()
        {
            return $this->flag;
        }

        public function SetFlag($tflag)
        {
            $this->flag = $tflag;
        }

        public function GetStatus()
        {
            return $this->status;
        }

        public function SetStatus($tstatus)
        {
            $this->status = $tstatus;
        }

        public function GetActivationkey()
        {
            return $this->activationkey;
        }

        public function SetActivationkey($tkey)
        {
            $this->activationkey = $tkey;
        }

        public function GetLast_online()
        {
            return $this->last_online;
        }

        public function SetLast_online($tonline)
        {
            $this->last_online = $tonline;
        }

        public function GetAteam()
        {
            return $this->ateam;
        }

        public function SetAteam($tateam)
        {
            $this->ateam = $tateam;
        }

        public function GetEmailme()
        {
            return $this->emailme;
        }

        public function SetEmailme($temailme)
        {
            $this->emailme = $temailme;
        }

        private function EncodePass($apass)
        {
            return sha1($apass.SHA1_SALT);
        }

        public function __destruct()
        {
        }
    }
