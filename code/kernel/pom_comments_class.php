<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Pom_comments Class
    */

    class Pom_commentsClass
    {
        private $id;
        private $pom_main_id;
        private $name;
        private $email;
        private $text;
        private $date;
        private $ip;

        public function __construct($id, $pom_main_id, $name, $email, $text, $date, $ip)
        {
            $this->id = $id;
            $this->pom_main_id = $pom_main_id;
            $this->name = $name;
            $this->email = $email;
            $this->text = $text;
            $this->date = $date;
            $this->ip = $ip;
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId()
        {
            $this->id = $id;
        }

        public function GetPom_main_id()
        {
            return $this->pom_main_id;
        }

        public function SetPom_main_id()
        {
            $this->pom_main_id = $pom_main_id;
        }

        public function GetName()
        {
            return $this->name;
        }

        public function SetName()
        {
            $this->name = $name;
        }

        public function GetEmail()
        {
            return $this->email;
        }

        public function SetEmail()
        {
            $this->email = $email;
        }

        public function GetText()
        {
            return $this->text;
        }

        public function SetText()
        {
            $this->text = $text;
        }

        public function GetDate()
        {
            return $this->date;
        }

        public function SetDate()
        {
            $this->date = $date;
        }

        public function GetIp()
        {
            return $this->ip;
        }

        public function SetIp()
        {
            $this->ip = $ip;
        }

        public function __destruct()
        {
        }
    }
