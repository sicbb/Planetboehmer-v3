<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/22/05
        Function: DB Dcusmain Class
    */

    require_once "db_mapper_class.php";
    require_once "dcusmain_class.php";

    class DBDcusmainClass extends DBMapperClass
    {

        public function __construct()
        {
            parent::__construct();
        }

        public function GetMainById($id)
        {
            $sql_query = "SELECT * FROM dcus_main WHERE dcus_head_id = '".$id."'";
            $result = $this->ExecSql($sql_query);
            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();
                $dcusmain_obj = new DcusmainClass($row['id'], $row['dcus_head_id'], $row['text']);

                return $dcusmain_obj;
            } else {
                return;
            }
        }

        public function Insert($dcusmain_obj)
        {
            $sql_query = "INSERT INTO dcus_main VALUES(NULL, '".$dcusmain_obj->GetDcus_head_id()."', '".$dcusmain_obj->GetText()."');";
            $this->ExecSql($sql_query);
            $sql_query = "SELECT max(id) as id FROM dcus_head";
            $result = $this->ExecSql($sql_query);
            $row = $result->fetch_assoc();
            $dcusmain_obj->SetId($row['id']);
        }

        public function DeleteById($id)
        {
            $sql_query = "DELETE FROM dcus_main WHERE id =".$id;
            $this->ExecSql($sql_query);
        }

        public function GetNewsCount()
        {
            $sql_query = "SELECT count(id) as count FROM dcus_main";
            $result = $this->ExecSql($sql_query);
            $row = $result->fetch_assoc();

            return $row['count'];
        }

        public function __destruct()
        {
            parent::__destruct();
        }
    }
