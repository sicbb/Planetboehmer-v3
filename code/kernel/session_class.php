<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Session Class
    */

    define("session_chars", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
    define("session_length", 20);

    class SessionClass
    {
        private $id;
        private $session;
        private $user_id;
        private $sdate;
        private $ip;

        public function __construct($id = 0, $session = "", $user_id = 0, $sdate = 0, $ip = "")
        {
            if (empty($id) && empty($session)) {
                $this->CreateSdate();
                $this->CreateSession();
                $this->ip = $_SERVER['REMOTE_ADDR'];
                $this->user_id = 0;
            } else {
                $this->id = $id;
                $this->session = $session;
                $this->user_id = $user_id;
                $this->sdate = $sdate;
                $this->ip = $ip;
            }
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId($tid)
        {
            $this->id = $tid;
        }

        public function GetSession()
        {
            return $this->session;
        }

        public function SetSession()
        {
            $this->session = $session;
        }

        public function GetUser_id()
        {
            return $this->user_id;
        }

        public function SetUser_id($uid)
        {
            $this->user_id = $uid;
        }

        public function GetSdate()
        {
            return $this->sdate;
        }

        public function SetSdate($tsdate)
        {
            $this->sdate = $tsdate;
        }

        public function GetIp()
        {
            return $this->ip;
        }

        public function SetIp()
        {
            $this->ip = $ip;
        }

        private function CreateSdate()
        {
            $this->sdate = time();
        }

        public function CreateSession()
        {
            $this->session = "";
            srand((double) microtime()*1000000);
            for ($i = 0; $i < session_length; $i++) {
                $rand = rand(1, strlen(session_chars));
                $this->session .= substr(session_chars, $rand-1, 1);
            }
            $this->session = $this->session.$this->sdate;
        }

        public function __destruct()
        {
        }
    }
