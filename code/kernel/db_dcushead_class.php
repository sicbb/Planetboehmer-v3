<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: DB Dcushead Class
    */

    require_once "db_mapper_class.php";
    require_once "dcushead_class.php";

    class DBDcusheadClass extends DBMapperClass
    {

        public function __construct()
        {
            parent::__construct();
        }

        public function SelectAll($from, $to)
        {
            $sql_query = "SELECT * FROM dcus_head order by id DESC LIMIT ".$from.",".$to.";";
            $result = $this->ExecSql($sql_query);
            $all = array();
            $length = $result->num_rows;
            for ($i = 0; $i<$length; $i++) {
                $row = $result->fetch_assoc();
                $all[$i] = new DcusheadClass($row['id'], $row['user_id'], $row['headline'], $row['img'], $row['text'], $row['date'], $row['clicks']);
            }

            return $all;
        }

        public function GetLastId()
        {
            $sql_query = "SELECT max(id) as maxid FROM dcus_head";
            $result = $this->ExecSql($sql_query);

            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();

                return $row['maxid'];
            } else {
                return;
            }
        }

        public function GetHeadById($id)
        {
            $sql_query = "SELECT * FROM dcus_head WHERE id = '".$id."'";
            $result = $this->ExecSql($sql_query);
            if ($result->num_rows > 0) {
                $row = $result->fetch_assoc();
                $dcushead_obj = new DcusheadClass($row['id'], $row['user_id'], $row['headline'], $row['img'], $row['text'], $row['date'], $row['clicks']);

                return $dcushead_obj;
            } else {
                return;
            }
        }

        public function Insert($dcushead_obj)
        {
            $sql_query = "INSERT INTO dcus_head VALUES(NULL, '".$dcushead_obj->GetUser_id()."', '".$dcushead_obj->GetHeadline()."', '".$dcushead_obj->GetImg()."', '".$dcushead_obj->GetText()."', '".$dcushead_obj->GetDate()."', '".$dcushead_obj->GetClicks()."');";
            $this->ExecSql($sql_query);
            $sql_query = "SELECT max(id) as id FROM dcus_head";
            $result = $this->ExecSql($sql_query);
            $row = $result->fetch_assoc();
            $dcushead_obj->SetId($row['id']);
        }

        public function DeleteById($id)
        {
            $sql_query = "DELETE FROM dcus_head WHERE id =".$id;
            $this->ExecSql($sql_query);
        }

        public function GetNewsCount()
        {
            $sql_query = "SELECT count(id) as count FROM dcus_head";
            $result = $this->ExecSql($sql_query);
            $row = $result->fetch_assoc();

            return $row['count'];
        }

        public function UpdateClicks($id)
        {
            $sql_query = "SELECT clicks FROM dcus_head WHERE id = '".$id."'";
            $result = $this->ExecSql($sql_query);
            $row = $result->fetch_assoc();

            $clicks = $row['clicks'];
            $clicks++;

            $sql_query = "UPDATE dcus_head SET clicks = '".$clicks."' WHERE id = '".$id."'";
            $result = $this->ExecSql($sql_query);
        }

        public function __destruct()
        {
            parent::__destruct();
        }
    }
