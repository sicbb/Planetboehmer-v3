<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: DB Session Class
    */

    // max. Sessiondauer
    define("s_length", 30);

    require_once "db_mapper_class.php";
    require_once "session_class.php";
    require_once "php_salt.php";

    class DBSessionClass extends DBMapperClass
    {
        private function SetStandardToOldSession()
        {
            // Aktuelle Zeit - 1 Stunden
            $stamp = time()-3600;

            $sql_query = "SELECT id FROM session WHERE sdate < '".$stamp."' AND user_id != '0'";
            $result = $this->ExecSql($sql_query);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $sql_query = "UPDATE session SET user_id = '0' WHERE id = '".$row['id']."'";
                    $this->ExecSql($sql_query);
                }
            }
        }

        public function SessionExists($session)
        {
            $sql_query = "SELECT id FROM session WHERE session = '".$session."'";
            $result = $this->ExecSql($sql_query);
            if ($result->num_rows == 1) {
                return true;
            } else {
                return false;
            }
        }

        public function GetIdBySession($session)
        {
            if (!empty($session)) {
                $sql_query = "SELECT id FROM session WHERE session = '".$session."'";
                $result = $this->ExecSql($sql_query);
                if ($result->num_rows > 0) {
                    $row = $result->fetch_assoc();

                    return $row['id'];
                }
            }
        }

        public function DeleteDoubleUserSession($user_obj, $session)
        {
            if ($user_obj != null && !empty($session)) {
                $sql_query = "UPDATE session SET user_id = '0' WHERE user_id = '".$user_obj->GetId()."' AND session != '".$session."'";
                //$sql_query = "DELETE FROM session WHERE user_id = '" . $user_obj->GetId() . "' AND session != '" . $session . "'";
                $this->ExecSql($sql_query);
            }
        }

        public function CheckSession($session)
        {
            $this->SetStandardToOldSession();

            $sql_query = "SELECT * FROM session WHERE session = '".$session."';";
            $result = $this->ExecSql($sql_query);

            if ($result->num_rows == 1) {
                $row = $result->fetch_assoc();
                $session_obj = new SessionClass($row['id'], $row['session'], $row['user_id'], $row['sdate'], $row['ip']);
            } else {
                throw new Exception("ERROR: Session nicht vorhanden. Bitte wählen Sie diese <a href='http://www.planetboehmer.de' target='_parent' class='blacklink'>Seite</a> neu an.");    //Session nicht vorhanden oder ungültig!
            }

            return $session_obj;
        }

        public function InsertSession($session_obj)
        {
            $sql_query = "INSERT INTO session VALUES (NULL,'".$session_obj->GetSession()."', ".$session_obj->GetUser_id().", ".$session_obj->GetSdate().", '".$session_obj->GetIp()."')";
            $this->ExecSql($sql_query);
        }

        public function UpdateSession($session_obj)
        {
            //$sql_query = "UPDATE session SET user_id = " . $session_obj->GetUser_id() . " where id=" . $session_obj->GetId() . ";";

            $sql_query = "UPDATE session SET session = '".$session_obj->GetSession()."'
				,user_id = '".$session_obj->GetUser_id()."'
				,sdate = '".$session_obj->GetSdate()."'
				,ip = '".$session_obj->GetIp()."' where id = '".$session_obj->GetId()."'";
            $this->ExecSql($sql_query);
        }

        public function __construct()
        {
            parent::__construct();
        }

        public function __destruct()
        {
            parent::__destruct();
        }
    }
