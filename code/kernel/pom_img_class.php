<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Pom_img Class
    */

    class Pom_imgClass
    {
        private $id;
        private $pom_main_id;
        private $dcus_img_id;

        public function __construct($id, $pom_main_id, $dcus_img_id)
        {
            $this->id = $id;
            $this->pom_main_id = $pom_main_id;
            $this->dcus_img_id = $dcus_img_id;
        }

        public function GetId()
        {
            return $this->id;
        }

        public function SetId()
        {
            $this->id = $id;
        }

        public function GetPom_main_id()
        {
            return $this->pom_main_id;
        }

        public function SetPom_main_id()
        {
            $this->pom_main_id = $pom_main_id;
        }

        public function GetDcus_img_id()
        {
            return $this->dcus_img_id;
        }

        public function SetDcus_img_id()
        {
            $this->dcus_img_id = $dcus_img_id;
        }

        public function __destruct()
        {
        }
    }
