<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: DB User Class
    */

    require_once "db_mapper_class.php";
    require_once "user_class.php";
    require_once "db_user_reg_class.php";
    require_once "user_reg_class.php";
    require_once "db_session_class.php";
    require_once "session_class.php";
    require_once "php_salt.php";

    require "mail/smtp.php";
    require "mail/sasl.php";

    define("reg_chars", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
    define("reg_length", 30);
    define("EMAIL_FROM", "noreply@bb-network.de");
    define("EMAIL_SUBJECT", "Registrierung bei www.planetboehmer.de");
    define("HP_URL", "http://www.bb-network.de/~sicbb/planetboehmer/");

    class DBUserClass extends DBMapperClass
    {

        public function __construct()
        {
            parent::__construct();
        }

        //$session => SessionClass-Objekt
        public function CheckLogin($name, $pass, $session_obj)
        {
            $sql_query = "SELECT * FROM user WHERE login = '".$name."' AND pass = '".sha1($pass.SHA1_SALT)."'";
            $result = $this->ExecSql($sql_query);
            if ($result->num_rows == 1) {
                $row = $result->fetch_assoc();
                $session_obj->SetUser_id($row['id']);
                $user_obj = new UserClass($row['id'],
                    $row['login'],
                    $row['pass'],
                    $row['email'],
                    $row['birthday'],
                    $row['location'],
                    $row['userinfo'],
                    $row['icq'],
                    $row['hp'],
                    $row['picture'],
                    $row['flag'],
                    $row['status'],
                    $row['activationkey'],
                    $row['last_online'],
                    $row['ateam'],
                    $row['emailme']);
                if ($row['status'] == "unlocked") {
                    $db_session_obj = new DBSessionClass();
                    $db_session_obj->UpdateSession($session_obj);    //Der User wird der Session angehängt
                }

                return $user_obj;
            } else {
                return;
            }
        }

        public function GetAllActiv()
        {
            $sql_query = "SELECT * FROM user WHERE id != '0' AND status != 'locked'";
            $result = $this->ExecSql($sql_query);
            if ($result->num_rows > 0) {
                $i = 0;
                while ($row = $result->fetch_assoc()) {
                    $user_objs[$i++] = new UserClass($row['id'],
                    $row['login'],
                    $row['pass'],
                    $row['email'],
                    $row['birthday'],
                    $row['location'],
                    $row['userinfo'],
                    $row['icq'],
                    $row['hp'],
                    $row['picture'],
                    $row['flag'],
                    $row['status'],
                    $row['activationkey'],
                    $row['last_online'],
                    $row['ateam'],
                    $row['emailme']);
                }

                return $user_objs;
            } else {
                return;
            }
        }

        public function GetAllOnline()
        {
            $sql_query = "SELECT * FROM session WHERE user_id > 0";
            $result = $this->ExecSql($sql_query);
            if ($result->num_rows > 0) {
                $i = 0;
                while ($row = $result->fetch_assoc()) {
                    $sql_query = "SELECT * FROM user WHERE id = ".$row['user_id'];
                    $result_tmp = $this->ExecSql($sql_query);
                    $row_tmp = $result_tmp->fetch_assoc();

                    $user_objs[$i++] = new UserClass($row_tmp['id'],
                    $row_tmp['login'],
                    $row_tmp['pass'],
                    $row_tmp['email'],
                    $row_tmp['birthday'],
                    $row_tmp['location'],
                    $row_tmp['userinfo'],
                    $row_tmp['icq'],
                    $row_tmp['hp'],
                    $row_tmp['picture'],
                    $row_tmp['flag'],
                    $row_tmp['status'],
                    $row_tmp['activationkey'],
                    $row_tmp['last_online'],
                    $row_tmp['ateam'],
                    $row_tmp['emailme']);
                }

                return $user_objs;
            } else {
                return;
            }
        }

        public function mail($to, $message, $headers)
        {
            $smtp = new smtp_class();

            $smtp->host_name = "smtp.izzle.org";       /* Change this variable to the address of the SMTP server to relay, like "smtp.myisp.com" */
            $smtp->localhost = "localhost";       /* Your computer address */
            $smtp->direct_delivery = 0;           /* Set to 1 to deliver directly to the recepient SMTP server */
            $smtp->timeout = 10;                  /* Set to the number of seconds wait for a successful connection to the SMTP server */
            $smtp->data_timeout = 0;              /* Set to the number seconds wait for sending or retrieving data from the SMTP server.
            Set to 0 to use the same defined in the timeout variable */
            $smtp->debug = 0;                     /* Set to 1 to output the communication with the SMTP server */
            $smtp->html_debug = 0;                /* Set to 1 to format the debug output as HTML */
            $smtp->pop3_auth_host = "";           /* Set to the POP3 authentication host if your SMTP server requires prior POP3 authentication */
            $smtp->user = "gym";                     /* Set to the user name if the server requires authetication */
            $smtp->realm = "";                    /* Set to the authetication realm, usually the authentication user e-mail domain */
            $smtp->password = "83omega1";                 /* Set to the authetication password */
            $smtp->workstation = "";              /* Workstation name for NTLM authentication */
            $smtp->authentication_mechanism = "TLS"; /* Specify a SASL authentication method like LOGIN, PLAIN, CRAM-MD5, NTLM, etc..
            Leave it empty to make the class negotiate if necessary */

            /*
            * If you need to use the direct delivery mode and this is running under
            * Windows or any other platform that does not have enabled the MX
            * resolution function GetMXRR() , you need to include code that emulates
            * that function so the class knows which SMTP server it should connect
            * to deliver the message directly to the recipient SMTP server.
            */
            if ($smtp->direct_delivery) {
                if (!function_exists("GetMXRR")) {
                    /*
                    * If possible specify in this array the address of at least on local
                    * DNS that may be queried from your network.
                    */
                    $_NAMESERVERS = array();
                    include "mail/getmxrr.php";
                }
                /*
                * If GetMXRR function is available but it is not functional, to use
                * the direct delivery mode, you may use a replacement function.
                */
                /*
                else
                {
                $_NAMESERVERS=array();
                if(count($_NAMESERVERS)==0)
                Unset($_NAMESERVERS);
                include("rrcompat.php");
                $smtp->getmxrr="_getmxrr";
                }
                */
            }

            if (!$smtp->SendMessage($smtp->user, array($to), array($headers), $message)) {
                //echo "Cound not send the message to $to.\nError: " . $smtp->error . "\n";
                return false;
            } else {
                return true;
            }
        }

        public function GetUserById($id)
        {
            $sql_query = "SELECT * FROM user WHERE id = '".$id."'";
            $result = $this->ExecSql($sql_query);
            if ($result->num_rows == 1) {
                $row = $result->fetch_assoc();
                $user_obj = new UserClass($row['id'],
                    $row['login'],
                    $row['pass'],
                    $row['email'],
                    $row['birthday'],
                    $row['location'],
                    $row['userinfo'],
                    $row['icq'],
                    $row['hp'],
                    $row['picture'],
                    $row['flag'],
                    $row['status'],
                    $row['activationkey'],
                    $row['last_online'],
                    $row['ateam'],
                    $row['emailme']);

                return $user_obj;
            } else {
                return;
            }
        }

        public function GetOnlyUserLoginById($id)
        {
            $sql_query = "SELECT login FROM user WHERE id = '".$id."'";
            $result = $this->ExecSql($sql_query);
            if ($result->num_rows == 1) {
                $row = $result->fetch_assoc();

                return $row['login'];
            } else {
                return;
            }
        }

        public function GetUserByLogin($login)
        {
            $sql_query = "SELECT * FROM user WHERE login = '".$login."'";
            $result = $this->ExecSql($sql_query);
            if ($result->num_rows == 1) {
                $row = $result->fetch_assoc();
                $user_obj = new UserClass($row['id'],
                    $row['login'],
                    $row['pass'],
                    $row['email'],
                    $row['birthday'],
                    $row['location'],
                    $row['userinfo'],
                    $row['icq'],
                    $row['hp'],
                    $row['picture'],
                    $row['flag'],
                    $row['status'],
                    $row['activationkey'],
                    $row['last_online'],
                    $row['ateam'],
                    $row['emailme']);

                return $user_obj;
            } else {
                return;
            }
        }

        public function CheckUserExists(&$user_obj)
        {
            if ($user_obj != null) {
                $sql_query = "SELECT id FROM user WHERE login = '".$user_obj->GetLogin()."' OR email = '".$user_obj->GetEmail()."'";
                $result = $this->ExecSql($sql_query);
                if ($result->num_rows > 0) {
                    return true;
                } else {
                    return false;
                }
            }

            return false;
        }

        public function CheckLoginExists($login)
        {
            if (!empty($login)) {
                $sql_query = "SELECT id FROM user WHERE login = '".$login."'";
                $result = $this->ExecSql($sql_query);
                if ($result->num_rows > 0) {
                    return true;
                } else {
                    return false;
                }
            }

            return false;
        }

        public function CheckEmailExists($email)
        {
            if (!empty($email)) {
                $sql_query = "SELECT id FROM user WHERE email = '".$email."'";
                $result = $this->ExecSql($sql_query);
                if ($result->num_rows > 0) {
                    return true;
                } else {
                    return false;
                }
            }

            return false;
        }

        public function CreateRegKey()
        {
            $reg_key = "";
            srand((double) microtime()*1000000);
            for ($i = 0; $i < reg_length; $i++) {
                $rand = rand(1, strlen(reg_chars));
                $reg_key .= substr(reg_chars, $rand-1, 1);
            }

            return $reg_key;
        }

        public function InsertUser(&$user_obj)
        {
            if ($user_obj != null) {
                $sql_query = "INSERT INTO user VALUES (NULL, '".$user_obj->GetLogin()."', '".$user_obj->GetPass()."', '".$user_obj->GetEmail()."', '".$user_obj->GetBirthday()."', '".$user_obj->GetLocation()."', '".$user_obj->GetUserinfo()."', '".$user_obj->GetIcq()."', '".$user_obj->GetHp()."', '".$user_obj->GetPicture()."', '".$user_obj->GetFlag()."', '".$user_obj->GetStatus()."', '".$user_obj->GetActivationkey()."', '".$user_obj->GetLast_online()."', '".$user_obj->GetAteam()."', '".$user_obj->GetEmailme()."')";
                $this->ExecSql($sql_query);

                $sql_query = "SELECT max(id) as id FROM user ";
                $result = $this->ExecSql($sql_query);
                if ($result->num_rows == 1) {
                    $row = $result->fetch_assoc();
                    $user_obj->SetId($row['id']);
                } else {
                    throw new Exception("ERROR: Fehler beim Eintragen des Users ".$user_obj->GetLogin());
                }

                $db_user_reg_obj = new DBUserRegClass();
                $user_reg_obj = new UserRegClass(0, $user_obj->GetId(), $user_obj->GetEmail(), time());

                if ($user_reg_obj != null) {
                    $db_user_reg_obj->InsertUserReg($user_reg_obj);
                } else {
                    throw new Exception("ERROR: Fehler beim Eintragen des Users!");
                }

                // User als not-valid merken
                $reg_key = $this->CreateRegKey();
                $sql_query = "UPDATE user SET activationkey = '".$reg_key."' WHERE id = '".$user_obj->GetId()."'";
                $this->ExecSql($sql_query);
                $this->SendEmailReg($reg_key, $user_obj);

                $sql_query = "INSERT INTO userstats values(NULL, '".$user_obj->GetId()."', '0', '0', '0', '0', '0', '0')";
                $this->ExecSql($sql_query);
            } else {
                throw new Exception("ERROR: Fehler beim Eintragen des Users!");
            }
        }

        private function SendEmailNewPass($email, $login, $pass)
        {
            $message = '<html>
			<body>
			<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr>
			<td><img src="'.HP_URL.'img/planetboehmer_banner.jpg" width="400" height="95" border="0"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td><strong>Sie haben ein neues Passwort angefordert.</strong></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td>Dein Login: '.$login.'</td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td>Neues Passwort: '.$pass.'</td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td>Sie können Ihr Passwort jeder Zeit im Userbereich wieder ändern!</td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td>Kontakt: sicbb@gmx.de</td>
			</tr>
			</table>
			</body>
			</html>';
            $to = $email;
            $headers  = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
            $headers .= "From: www.planetboehmer.de Registrierung <".EMAIL_FROM.">\r\n";
            $headers .= "TO: ".$to."\r\n";
            $headers .= "SUBJECT: Neues Passwort von www.planetboehmer.de\r\n";
            $this->mail($to, $message, $headers);
        }

        private function SendEmailReg($reg_key, $user_obj)
        {
            $message = '<html>
			<body>
			<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr>
			<td><img src="'.HP_URL.'img/planetboehmer_banner.jpg" width="400" height="95" border="0"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td><strong>Danke für Ihre Registrierung bei www.planetboehmer.de</strong></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td><FONT SIZE="" COLOR="#CC0000"><B>Dein Login: '.$user_obj->GetLogin().'</B></FONT></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td>Funktionen:</td>
			</tr>
			<tr>
			<td>&nbsp;&nbsp;-&nbsp;Userbereich (Interne Messages senden und empfangen)</td>
			</tr>
			<tr>
			<td>&nbsp;&nbsp;-&nbsp;Pic of the Month, Shoutbox (Wählen und bestimmen Sie ein Bild des Monats oder schreiben Sie Shoutboxeinträge)</td>
			</tr>
			<tr>
			<td>&nbsp;&nbsp;-&nbsp;Chatfunktion (Java Applet mit Verbindung zu einem IRC Server; <font color="#CC0000">signierte JavaApplets müssen von ihrem Browser akzeptiert werden</font>)</td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td>Aktivierung:</td>
			</tr>
			<tr>
			<td><A HREF="'.HP_URL.'code/reg.php?key='.$reg_key.'">'.HP_URL.'code/reg.php?key='.$reg_key.'</A></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td>Diese Seite soll als Verteilerplatform dienen. Somit ist die Effiziens abhängig vom Engagement der Stundenten. Deswegen bitte Ich diese Seite weiterzuempfehlen. </td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td>Danke.</td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td>Ps.: Die Seite ist ständig im Umbau. Für Anregungen oder Verbesserungen bin ich natürlich immer ansprechbar.</td>
			</tr>
			<tr>
			<td>Kontakt: sicbb@gmx.de</td>
			</tr>
			</table>

			</body>
			</html>';
            $to = $user_obj->GetEmail();
            $headers  = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
            $headers .= "From: www.planetboehmer.de Registrierung <".EMAIL_FROM.">\r\n";
            $headers .= "TO: ".$to."\r\n";
            $headers .= "SUBJECT: ".EMAIL_SUBJECT."\r\n";
            $this->mail($to, $message, $headers);
        }

        public function DeleteUser($user_obj)
        {
            $sql_query = "DELETE FROM user where id = ".$user_obj->GetId();
            $this->ExecSql($sql_query);
        }

        public function LockedUser(&$user_obj)
        {
            // Setzt den User-Status auf "locked"
            $sql_query = "UPDATE user SET status = 'locked' WHERE id = ".$user_obj->GetId();
            $this->ExecSql($sql_query);
            $user_obj->SetStatus("locked");
        }

        public function UnLockedUser(&$user_obj)
        {
            // Setzt den User-Status auf "unlocked"
            $sql_query = "UPDATE user SET status = 'unlocked' WHERE id = ".$user_obj->GetId();
            $this->ExecSql($sql_query);
            $user_obj->SetStatus("unlocked");
        }

        public function CreateNewPass($email)
        {
            $sql_query = "SELECT login FROM user WHERE email = '".$email."'";
            $result = $this->ExecSql($sql_query);
            if ($result->num_rows == 1) {
                $row = $result->fetch_assoc();
                $new_pass = substr($this->CreateRegKey(), 0, 6);
                $sql_query = "UPDATE user SET pass = '".sha1($new_pass.SHA1_SALT)."' WHERE email = '".$email."'";
                $this->ExecSql($sql_query);
                $this->SendEmailNewPass($email, $row['login'], $new_pass);

                return true;
            } else {
                return false;
            }
        }

        public function UpdateUser(&$user_obj)
        {
            $sql_query = "UPDATE user SET login = '".$user_obj->GetLogin()."'
				,pass = '".$user_obj->GetPass()."'
				,email = '".$user_obj->GetEmail()."'
				,birthday = '".$user_obj->GetBirthday()."'
				,location = '".$user_obj->GetLocation()."'
				,userinfo = '".$user_obj->GetUserinfo()."'
				,icq = '".$user_obj->GetIcq()."'
				,hp = '".$user_obj->GetHp()."'
				,picture = '".$user_obj->GetPicture()."'
				,flag = '".$user_obj->GetFlag()."'
				,status = '".$user_obj->GetStatus()."'
				,activationkey = '".$user_obj->GetActivationkey()."'
				,last_online = '".$user_obj->GetLast_online()."'
				,ateam = '".$user_obj->GetAteam()."'
				,emailme = '".$user_obj->GetEmailme()."' where id = ".$user_obj->GetId();
            $this->ExecSql($sql_query);
        }

        public function GetUsersBySearch($search)
        {
            $sql_query = "SELECT * FROM user WHERE login LIKE '".$search."%'";
            $result = $this->ExecSql($sql_query);
            if ($result->num_rows > 0) {
                $i = 0;
                while ($row = $result->fetch_assoc()) {
                    $user_objs[$i++] = new UserClass($row['id'],
                        $row['login'],
                        $row['pass'],
                        $row['email'],
                        $row['birthday'],
                        $row['location'],
                        $row['userinfo'],
                        $row['icq'],
                        $row['hp'],
                        $row['picture'],
                        $row['flag'],
                        $row['status'],
                        $row['activationkey'],
                        $row['last_online'],
                        $row['ateam'],
                        $row['emailme']);
                }

                return $user_objs;
            } else {
                return;
            }
        }

        public function GetAllAteamUser()
        {
            $sql_query = "SELECT * FROM user WHERE ateam = 'true' ORDER BY id";
            $result = $this->ExecSql($sql_query);

            if ($result->num_rows > 0) {
                $i = 0;
                while ($row = $result->fetch_assoc()) {
                    $user_objs[$i++] = new UserClass($row['id'],
                        $row['login'],
                        $row['pass'],
                        $row['email'],
                        $row['birthday'],
                        $row['location'],
                        $row['userinfo'],
                        $row['icq'],
                        $row['hp'],
                        $row['picture'],
                        $row['flag'],
                        $row['status'],
                        $row['activationkey'],
                        $row['last_online'],
                        $row['ateam'],
                        $row['emailme']);
                }

                return $user_objs;
            } else {
                return;
            }
        }

        public function __destruct()
        {
            parent::__destruct();
        }
    }
