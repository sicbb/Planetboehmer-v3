<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: DB Dcusimg Class
    */

    require_once "db_mapper_class.php";
    require_once "dcusimg_class.php";

    class DBDcusimgClass extends DBMapperClass
    {

        public function __construct()
        {
            parent::__construct();
        }

        public function GetAll($id)
        {
            $sql_query = "SELECT * FROM dcus_img WHERE dcus_head_id = ".$id." ORDER BY id";
            $result = $this->ExecSql($sql_query);
            $all = array();
            $length = $result->num_rows;
            for ($i = 0; $i<$length; $i++) {
                $row = $result->fetch_assoc();
                $all[$i] = new DcusimgClass($row['id'], $row['dcus_head_id'], $row['img']);
            }

            return $all;
        }

        public function Insert($dcusimg_objs)
        {
            $sql_query = "INSERT INTO dcus_img VALUES(NULL, '".$dcusimg_objs->GetDcus_head_id()."', '".$dcusimg_objs->GetImg()."');";
            $this->ExecSql($sql_query);
            $sql_query = "SELECT max(id) as id FROM dcus_img";
            $result = $this->ExecSql($sql_query);
            $row = $result->fetch_assoc();
            $dcusimg_objs->SetId($row['id']);
        }

        public function __destruct()
        {
            parent::__destruct();
        }
    }
