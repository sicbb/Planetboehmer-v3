<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/22/05
        Function: Impressum Class
    */

    require_once "kernel/db_session_class.php";
    require_once "kernel/session_class.php";
    require_once "kernel/db_user_class.php";
    require_once "kernel/user_class.php";

    require_once "page_class.php";

    class PageImpressumClass extends PageClass
    {
        public function __construct($get, $post, $session_obj, $user_obj, $files)
        {
            $db_session_obj = new DBSessionClass();

            if ($session_obj != null) {
                if (!empty($get['a'])) {
                    $action = strip_tags($get['a']);
                    if (empty($action)) {
                        $action = strip_tags($post['a']);
                    }
                } else {
                    $action = "";
                }
            }

            //CSS-Stile werden eingebunden
            $this->AddCSS('news');
            $this->AddCSS('forms');

            $msg = "";
            if ($action == "send") {
                $name = strip_tags($post['name']);
                $email = strip_tags($post['email']);
                $betreff = strip_tags($post['betreff']);
                $text = strip_tags($post['text']);

                if (!empty($name) && !empty($email) && !empty($betreff) && !empty($text)) {
                    if (strlen($text) < 1000 && $text != "(Max. 1000 Zeichen)") {
                        $this->SendKontakt($name, $email, $betreff, $text);
                        $this->AddJavaScript('alert("Info: Ihre Nachricht wurde erfolgreich gesendet!")');
                    } else {
                        $this->AddJavaScript('alert("Error: Ihr Text ist zu lang oder ungültig!")');
                    }
                } else {
                    $this->AddJavaScript('alert("Error: Bitte füllen Sie alle Felder korrekt aus!")');
                }
            }
            $this->body .= $this->GetBody($session_obj, $msg);
        }

        private function GetBody($session_obj, $msg)
        {
            return '
			<table width="660" cellspacing="0" cellpadding="0" border="0">
				<tr>
				    <td width="45%"><img src="img/menu/impressum_k.gif" width="100" height="30"></td>
				    <td width="5%">&nbsp;</td>
				    <td width="50%"><img src="img/menu/kontakt_k.gif" width="100" height="30"></td>
  				</tr>

				<tr>
					<td width="45%" valign="top" height="15">&nbsp;</td>
					<td width="5%">&nbsp;</td>
					<td width="50%">&nbsp;</td>
				</tr>

				<tr>
					<td valign="top">'.$this->GetImpressum($session_obj).'</td>
					<td>&nbsp;</td>
					<td valign="top">'.$this->GetKontakt($session_obj, $msg).'</td>
				</tr>

				<tr>
					<td valign="top" colspan="3">'.$this->GetHaftung($session_obj).'</td>
				</tr>
			</table>';
        }

        private function GetImpressum($session_obj)
        {
            $text = $this->UseBox2('<strong>&nbsp;#&nbsp;Verantwortlich für diese Internetpräsenz ist:</strong>', '
				<b>Inhalt:</b><br><a href="mailto:sicbb@bb-network.de" class="orangelink">Daniel Böhmer</a><br><br>
				<b>Design und Programmierung:</b><br><a href="mailto:sicbb@bb-network.de" class="orangelink">Daniel Böhmer</a><br><br>
				<b>Haftung:</b><br>P L A N E T B O E H M E R<br><br>z. Hd. Daniel B&ouml;hmer<br>An den Hüren 145<br>41066 Mönchengladbach
				', '100%', '19', 'left');

            return $text;
        }

        private function GetKontakt($session_obj, $msg)
        {
            $text = $this->UseBox3('Kontaktformular:', 'Hier kannst du Anregungen hinterlassen.',
                '<tr class="boxcomments1">
					<td><span class="smalltext">Name</span> </td>
					<td><input name="name" type="text" maxlength="50" id="name" class="form" size="35" /></td>
				</tr>
				<tr class="boxcomments1">
					<td width="25%"><span class="smalltext">eMail</span></td>
					<td><input name="email" type="text" maxlength="50" id="email" class="form" size="35" /></td>
				</tr>
				<tr class="boxcomments1">
					<td width="25%"><span class="smalltext">Betreff</span></td>
					<td><input name="betreff" type="text" maxlength="50" id="betreff" class="form" size="35" /></td>
				</tr>
				<tr class="boxcomments1">
					<td valign="top"><span class="smalltext">Text</span></td>
					<td><textarea name="text" cols="50" rows="5" id="text" wrap="VIRTUAL" class="form">(Max. 1000 Zeichen)</textarea></td>
				</tr>
				<tr class="boxcomments1">
					<td></td>
					<td><input type="submit" name="submit" value="Senden" class="form"> '.$msg.'</td>
				</tr>', '100%', 'left', 'handler.php?s='.$session_obj->GetSession().'&a=send&goto=impressum');

            return $text;
        }

        private function GetHaftung($session_obj)
        {
            $text = $this->UseBox2('<strong>&nbsp;#&nbsp;Haftungsausschluss</strong>', '
				<b>Kontakt:</b><br>
			Nat&uuml;rlich gibt es au&szlig;er der eMail-Methode noch viele Andere, die wahrscheinlich auch noch schneller funktionieren. Eine dieser M&ouml;glichkeiten ist das IRC (eigentlich der IRC (Internet-Relay-Chat). Nachfolgend eine Anleitung wie man es heil und wohlbeh&uuml;tet in den Chat schaffen kann. <br>
			<br>
			<em>Man suche sich einen Internet-Explorer oder irgendeinen anderen g&auml;ngigen Browser und "klicke sich" (wie im L4m0r-Fachbuch geschrieben steht) auf eine der folgenden Adressen: http://chat.ircnet.org oder http://www.irc.netsurf.de/chat/. <br>
			<br>
			Wenn wir das hinbekommen haben, tragen wir Nickname (der im Chat anzuzeigende Name) Channel (also den "Chat-Raum") ein. Bei http://chat.icnet.org kann man auch einen Server eintragen, hier empfehle ich allerdings den bereits angegebenen zu benutzen (ansonsten klappt es eh nicht). <br>
			<br>
			Nun werdet ihr die Server-MotD (Message of the Day) bekommen, die man als Web-Chat User nicht zu beachten braucht. Danach befindet ihr euch im angegebenen Channel, soweit euer Nick noch nicht besetzt ist oder irgendwelche anderen Probleme aufgetreten sind. <br>
			<br>
			Ab diesem Zeitpunkt k&ouml;nnt ihr die g&auml;ngigen IRC-CMDs benutzen. Welche ihr sicher irgendwo nachlesen koennt: www.google.de. <br>
			<br>
			Ich m&ouml;chte euch an dieser Stelle nahe legen, den Channel #nfi zu joinen (betreten). Denn hier werdet ihr mit sehr hoher Wahrscheinlichkeit jemanden antreffen. Falls ihr keine Informationen zu den IRC-CMDs bekommen habt: /join #nfi (so betritt man einen Channel). <br>
			<br>
			Hier k&ouml;nnt ihr euch &uuml;ber weitere Commands informieren oder einfach nur sinnlos bzw. sinnvoll (liegt immer an euch) rumchatten.
			<br>
			Natürlich kann man mich auch direkt via ICQ ansprechen. ICQ: 77097306 <img src="http://web.icq.com/whitepages/online?icq=77097306&img=5" width="18" heigth="18" border="0" title="ICQ"></em><br>
			<br>
			Eine weitere Variante bietet das obige Kontaktformular.
			<br><br>
			<b>Haftungsausschluss:</b><strong><br><br>
		  	1. Inhalt des Onlineangebotes </strong><br>
			  Der Autor &uuml;bernimmt keinerlei Gew&auml;hr f&uuml;r die Aktualit&auml;t, Korrektheit, Vollst&auml;ndigkeit oder Qualit&auml;t der bereitgestellten Informationen. Haftungsanspr&uuml;che gegen den Autor, welche sich auf Sch&auml;den materieller oder ideeller Art beziehen, die durch die Nutzung oder Nichtnutzung der dargebotenen Informationen bzw. durch die Nutzung fehlerhafter und unvollst&auml;ndiger Informationen verursacht wurden, sind grunds&auml;tzlich ausgeschlossen, sofern seitens des Autors kein nachweislich vors&auml;tzliches oder grob fahrl&auml;ssiges Verschulden vorliegt. Alle Angebote sind freibleibend und unverbindlich. Der Autor beh&auml;lt es sich ausdr&uuml;cklich vor, Teile der Seiten oder das gesamte Angebot ohne gesonderte Ank&uuml;ndigung zu ver&auml;ndern, zu erg&auml;nzen, zu l&ouml;schen oder die Ver&ouml;ffentlichung zeitweise oder endg&uuml;ltig einzustellen. <br>
		  <br>
		  <strong>2. Verweise und Links </strong><br>
		  Bei direkten oder indirekten Verweisen auf fremde Internetseiten ("Links"), die au&szlig;erhalb des Verantwortungsbereiches des Autors liegen, w&uuml;rde eine Haftungsverpflichtung ausschlie&szlig;lich in dem Fall in Kraft treten, in dem der Autor von den Inhalten Kenntnis hat und es ihm technisch m&ouml;glich und zumutbar w&auml;re, die Nutzung im Falle rechtswidriger Inhalte zu verhindern. Der Autor erkl&auml;rt hiermit ausdr&uuml;cklich, dass zum Zeitpunkt der Linksetzung keine illegalen Inhalte auf den zu verlinkenden Seiten erkennbar waren. Auf die aktuelle und zuk&uuml;nftige Gestaltung, die Inhalte oder die Urheberschaft der gelinkten/verkn&uuml;pften Seiten hat der Autor keinerlei Einfluss. Deshalb distanziert er sich hiermit ausdr&uuml;cklich von allen Inhalten aller gelinkten/verkn&uuml;pften Seiten, die nach der Linksetzung ver&auml;ndert wurden. Diese Feststellung gilt f&uuml;r alle innerhalb des eigenen Internetangebotes gesetzten Links und Verweise sowie f&uuml;r Fremdeintr&auml;ge in vom Autor eingerichteten G&auml;steb&uuml;chern, Diskussionsforen und Mailinglisten. F&uuml;r illegale, fehlerhafte oder unvollst&auml;ndige Inhalte und insbesondere f&uuml;r Sch&auml;den, die aus der Nutzung oder Nichtnutzung solcherart dargebotener Informationen entstehen, haftet allein der Anbieter der Seite, auf welche verwiesen wurde, nicht derjenige, der &uuml;ber Links auf die jeweilige Ver&ouml;ffentlichung lediglich verweist. <br>
		  <br>
		  <strong>3. Urheber- und Kennzeichenrecht </strong><br>
		  Der Autor ist bestrebt, in allen Publikationen die Urheberrechte der verwendeten Grafiken, Tondokumente, Videosequenzen und Texte zu beachten, von ihm selbst erstellte Grafiken, Tondokumente, Videosequenzen und Texte zu nutzen oder auf lizenzfreie Grafiken, Tondokumente, Videosequenzen und Texte zur&uuml;ckzugreifen. Alle innerhalb des Internetangebotes genannten und ggf. durch Dritte gesch&uuml;tzten Marken- und Warenzeichen unterliegen uneingeschr&auml;nkt den Bestimmungen des jeweils g&uuml;ltigen Kennzeichenrechts und den Besitzrechten der jeweiligen eingetragenen Eigent&uuml;mer. Allein aufgrund der blo&szlig;en Nennung ist nicht der Schluss zu ziehen, dass Markenzeichen nicht durch Rechte Dritter gesch&uuml;tzt sind! Das Copyright f&uuml;r ver&ouml;ffentlichte, vom Autor selbst erstellte Objekte bleibt allein beim Autor der Seiten. Eine Vervielf&auml;ltigung oder Verwendung solcher Grafiken, Tondokumente, Videosequenzen und Texte in anderen elektronischen oder gedruckten Publikationen ist ohne ausdr&uuml;ckliche Zustimmung des Autors nicht gestattet. <br>
		  <br>
		  <strong>4. Datenschutz </strong><br>
		  Sofern innerhalb des Internetangebotes die M&ouml;glichkeit zur Eingabe pers&ouml;nlicher oder gesch&auml;ftlicher Daten (Emailadressen, Namen, Anschriften) besteht, so erfolgt die Preisgabe dieser Daten seitens des Nutzers auf ausdr&uuml;cklich freiwilliger Basis. Die Inanspruchnahme und Bezahlung aller angebotenen Dienste ist - soweit technisch m&ouml;glich und zumutbar - auch ohne Angabe solcher Daten bzw. unter Angabe anonymisierter Daten oder eines Pseudonyms gestattet. <br>
		  <br>
		  <strong>5. Rechtswirksamkeit </strong><br>
		  Dieser Haftungsausschluss ist als Teil des Internetangebotes zu betrachten, von dem aus auf diese Seite verwiesen wurde. Sofern Teile oder einzelne Formulierungen dieses Textes der geltenden Rechtslage nicht, nicht mehr oder nicht vollst&auml;ndig entsprechen sollten, bleiben die &uuml;brigen Teile des Dokumentes in ihrem Inhalt und ihrer G&uuml;ltigkeit davon unber&uuml;hrt.', '100%', '19', 'left');

            return $text;
        }

        private function SendKontakt($name, $email, $betreff, $text)
        {
            $db_user_obj = new DBUserClass();

            $headers  = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
            $headers .= "From: www.planetboehmer.de Kontakt\r\n";
            $headers .= "TO: sicbb@gmx.de\r\n";
            $headers .= "SUBJECT: ".$betreff." <Planetboehmer>\r\n";

            $text = "Betreff: <strong><u>".$betreff."</u></strong><br><br><strong>".$name."</strong> mit der eMail <strong>".$email."</strong> schrieb: <br><br>".$text;

            if ($db_user_obj->mail("sicbb@gmx.de", $text, $headers)) {
                return true;
            } else {
                return false;
            }
        }
    }
