<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: AboutMe Class
    */

    require_once "kernel/db_session_class.php";
    require_once "kernel/session_class.php";
    require_once "page_class.php";

    class PageAboutmeClass extends PageClass
    {
        public function __construct($get, $post, $session_obj, $user_obj, $files)
        {
            $db_session_obj = new DBSessionClass();

            if ($session_obj != null) {
                if (!empty($get['a'])) {
                    $action = strip_tags($get['a']);
                    if (empty($action)) {
                        $action = strip_tags($post['a']);
                    }
                } else {
                    $action = "";
                }
            }

            //CSS-Stile werden eingebunden
            $this->AddCSS('news');
            $this->AddCSS('forms');

            // Javascript einbinden
            $this->AddJavaScriptFile('showcontent');

            $this->body .= $this->GetBody($session_obj);
        }

        private function GetBody($session_obj)
        {
            return '
			<table width="660" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td width="100%" valign="top">
						<img src="img/menu/aboutme_k.gif" width="100" height="30" border="0" titel="News">
					</td>
				</tr>
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td width="100%" valign="top">
						<table width="100%" cellspacing="0" cellpadding="0" border="0">
							'.$this->GetAboutme($session_obj).'
						</table>
					</td>
				</tr>
			</table>';
        }

        private function GetAboutme($session_obj)
        {
            $age_stamp_geb = mktime(11, 38, 0, 2, 25, 1983);
            $age_stamp_now = time();
            $age = substr(($age_stamp_now - $age_stamp_geb) / 60 / 60 / 24 / 365, 0, 2);

            $text = "<tr><td>".$this->UseBox3('Personendaten', 'Hier kann man meine Daten einsehen.',
                '<tr class="boxcomments1">
					<td width="25%"><span class="smalltext">Name</span></td>
					<td><span class="smalltext">Daniel Böhmer</span></td>
				</tr>
				<tr class="boxcomments1">
					<td><span class="smalltext">Alter</span></td>
					<td><span class="smalltext">'.$age.', geboren @ 11:38 MEZ, 25.02.1983</span></td>
				</tr>
				<tr class="boxcomments1">
					<td valign="top"><span class="smalltext">Staatsangehörigkeit</span></td>
					<td><span class="smalltext">Deutsch <img src="img/76.gif" width="15" height="10" title="Deutsch" alt="Deutsch" border="0"></span></td>
				</tr>
				<tr class="boxcomments1">
					<td valign="top"><span class="smalltext">Wohnort</span></td>
					<td><span class="smalltext">M&ouml;nchengladbach - NRW - Deutschland</span></td>
				</tr>
				<tr class="boxcomments1">
					<td valign="top"><span class="smalltext">Beruf</span></td>
					<td><span class="smalltext">Student und Freiberufler (BB-Network) - Diplom Informatik, FH-Aachen</span></td>
				</tr>
				<tr class="boxcomments1">
					<td valign="top"><span class="smalltext">Sprachkenntnisse</span></td>
					<td><span class="smalltext">Deutsch, Englisch</span></td>
				</tr>
				<tr class="boxcomments1">
					<td valign="top"><span class="smalltext">Was ich mag</span></td>
					<td><span class="smalltext">Partys, Tennis, Programmierung, kluge und nette Leute</span></td>
				</tr>
				<tr class="boxcomments1">
					<td valign="top"><span class="smalltext">Was ich nicht mag</span></td>
					<td><span class="smalltext">arrogante Leute, Rote Ampeln</span></td>
				</tr>
				<tr class="boxcomments1">
					<td valign="top"><span class="smalltext">ICQ</span></td>
					<td><span class="smalltext">77097306</span></td>
				</tr>
				<tr class="boxcomments1">
					<td valign="top"><span class="smalltext">eMail</span></td>
					<td><span class="smalltext"><a href="mailto:sicbb@gmx.de" class="orangelink">sicbb@gmx.de</a> <a href="mailto:sicbb@bb-network.de"><img src="img/email.gif" width="14" height="10" title="eMail" alt="eMail" border="0"></a> (Auf Anfrage gebe ich eventuell meine private Email raus.)</span></td>
				</tr>
				<tr class="boxcomments1">
					<td valign="top"><span class="smalltext">Berufskenntnisse</span></td>
					<td><span class="smalltext">Umfangreiche IT Erfahrung, erweiterte Programmierfähigkeiten in unterschiedlichen Sprachen und Netzwerksystem Fähigkeiten. Anpassung an Leistungsdruck. Ambitionen für neue und kreative Produkte, Ideen oder Arbeitsweisen. Spontaner Freigeist und Team fähig.</span></td>
				</tr>

				<tr class="boxcomments1">
					<td><img src="img/dani1b.jpg" width="150" height="164"></td>
					<td><span class="smalltext">&nbsp;</td>
				</tr>

				<!--
				<tr class="boxcomments1">
					<td valign="top" width="25%" id="tmp2"><a href="javascript:ShowContent(\'Profilefields1\')"><img src="img/plus.gif" align="middle" width="13" height="13" border="0" alt="Ausklappen" title="Ausklappen" id="ContentImgProfilefields1"></a> <span class="smalltext">PC Equipment</span></td>
					<td id="tmp">&nbsp;</td>
					<td id="ContentProfilefields1" style="display: none;" class="smalltext">
						<strong>Mainboard</strong>: Gigabyte GA-K8NXP-9, S.939<br>
						<strong>CPU</strong>: AMD Athlon64 3500+ S.939 BOX<br>
						<strong>Arbeitsspeicher</strong>: 2048MB DDR Corsair PC3200/400 CL 2, Corsair Twinx XL<br>
						<strong>Grafikkarte</strong>: Leadtek PX6800GT TDH, 256MB NVIDIA Geforce 6800GT<br>
						<strong>Festplatte</strong>: Maxtor 7B300S0, 300 GB S-ATA<br>
						<strong>Monitor</strong>: Samsung SyncMaster 913N, 19 Zoll TFT<br>
						<strong>Laufwerk</strong>: Plextor PX 712SA Retail DVD+-RW/+-R-Laufwerk S-ATA<br>
						<strong>Tastatur</strong>: Logitech Media Keyboard Elite<br>
						<strong>Maus</strong>: Razer Copperhead Gaming Mouse (blau)<br>
						<strong>Gehäuse</strong>: Thermalrock Dragon (blau/silber)
					</td>
				</tr>
				-->', '100%', 'left', 'handler.php?s='.$session_obj->GetSession().'&goto=aboutme')."</td></tr>";

            return $text;
        }
    }
