<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Links Class
    */

    require_once "kernel/db_session_class.php";
    require_once "kernel/session_class.php";
    require_once "page_class.php";

    class PageLinksClass extends PageClass
    {
        public function __construct($get, $post, $session_obj, $user_obj, $files)
        {
            $db_session_obj = new DBSessionClass();

            if ($session_obj != null) {
                if (!empty($get['a'])) {
                    $action = strip_tags($get['a']);
                    if (empty($action)) {
                        $action = strip_tags($post['a']);
                    }
                } else {
                    $action = "";
                }
            }

            //CSS-Stile werden eingebunden
            $this->AddCSS('news');
            $this->AddCSS('forms');

            $this->body .= $this->GetBody($session_obj);
        }

        private function GetBody($session_obj)
        {
            return '
			<table width="650" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td width="100%" valign="top">
						<img src="img/menu/links_k.gif" width="100" height="30" border="0" titel="News">
					</td>
				</tr>
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td width="100%" valign="top" height="15" class="smalltext">Hier werden die einen oder anderen Links stehen, die auf befreundete Seiten verweisen. Es steht natürlich jedem frei, auch seine Seite mit meiner zu linken.
<br>Falls dies der Fall sein sollte, bitte ich einfach sich kurz bei mir zu melden und mir Namen und URL zu seiner Homepage via Email an  <a href="mailto:sicbb@bb-network.de" class="orangelink">sicbb@bb-network.de</a> zu schreiben. Unter Umständen werde ich dann auch seine Seite hier auflisten.</td>
				</tr>
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td width="100%" valign="top">
						<table width="100%" cellspacing="0" cellpadding="0" border="0">
							'.$this->GetLinks($session_obj).'
						</table>
					</td>
				</tr>
			</table>';
        }

        private function GetLinks($session_obj)
        {
            $text = "<tr><td>
				".$this->UseBox2('<strong>&nbsp;#&nbsp;<a href="http://www.planetboehmer.de" target="_blank" class="whitelink">Der offizielle Planetboehmer Banner</a></strong>', '<a href="http://www.planetboehmer.de" target="_blank"><img src="img/planetboehmer_banner.jpg" alt="Planetboehmer Banner" title="Planetboehmer Banner" width="400" height="95" border="0"></a><br><br>Sourcecode:<br><textarea cols="120" rows="2" class="form"><a href="http://www.planetboehmer.de" target="_blank"><img src="http://www.bb-network.de/~sicbb/planetboehmer/img/planetboehmer_banner.jpg" alt="Planetboehmer Banner" title="Planetboehmer Banner" width="400" height="95" border="0"></a></textarea>', '100%', '19', 'left')."
				<td></tr>";

            $text .= "<tr><td>
				".$this->UseBox1('<strong>&nbsp;#&nbsp;<a href="http://www.mrdukes.de" target="_blank" class="whitelink">MRDUKES.DE</a></strong>', '<A href="http://www.mrdukes.de" target="_blank"><img src="http://www1.bb-network.de/~mrduke/images/logo.jpg" alt="MrDukes V1" height="100" width="400" border="0"></a><br>Die Seite vom MrDuke.', '100%', '19', 'left')."
				<td></tr>";

            $text .= "<tr><td>
				".$this->UseBox1('<strong>&nbsp;#&nbsp;<a href="http://www.spreadfirefox.com/?q=affiliates&amp;id=0&amp;t=53" target="_blank" class="whitelink">SPREADFIREFOX.COM</a></strong>', '<a href="http://www.spreadfirefox.com/?q=affiliates&amp;id=0&amp;t=53" target="_blank"><img border="0" alt="Get Firefox!" title="Get Firefox!" src="http://www.spreadfirefox.com/community/images/affiliates/Banners/468x60/rediscover.png"/></a><br>Mozilla Firefox, einer der besten Browser!', '100%', '19', 'left')."
				<td></tr>";

        /*
            $text .= "<tr><td>
                " . $this->UseBox1('<strong>&nbsp;#&nbsp;<a href="http://www.u-froeschke.de/" target="_blank" class="whitelink">U-FROESCHKE.DE</a></strong>', '<a href="http://www.u-froeschke.de/" target="_blank"><img src="img/banner/u-froeschke.gif" alt="http://www.u-froeschke.de/" width="88" height="32" border="0"></a><br>Eine sehr gelungene Homepage mit inzwischen &uuml;ber 500 Power-Point Pr&auml;sentationen. Und eine Ende ist nicht abzusehen...', '100%', '19', 'left') . "
                <td></tr>";
        */

            return $text;
        }
    }
