<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Dcus Class
    */

    require_once "kernel/db_session_class.php";
    require_once "kernel/session_class.php";
    require_once "kernel/db_dcushead_class.php";
    require_once "kernel/dcushead_class.php";
    require_once "kernel/db_dcusimg_class.php";
    require_once "kernel/dcusimg_class.php";
    require_once "kernel/db_dcusmain_class.php";
    require_once "kernel/dcusmain_class.php";
    require_once "kernel/db_dcuscomments_class.php";
    require_once "kernel/dcuscomments_class.php";
    require_once "kernel/db_user_class.php";
    require_once "kernel/user_class.php";
    require_once "kernel/constant.php";

    require_once "page_class.php";

    define("news_count", 5);

    class PageDcusClass extends PageClass
    {
        public function __construct($get, $post, $session_obj, $user_obj, $files)
        {
            $db_session_obj = new DBSessionClass();
            $db_dcushead_obj = new DBDcusheadClass();
            $db_dcusimg_obj = new DBDcusimgClass();
            $db_user_obj = new DBUserClass();
            $db_dcusmain_obj = new DBDcusmainClass();
            $db_dcuscomment_obj = new DBDcuscommentsClass();

            if ($session_obj != null) {
                if (!empty($get['a'])) {
                    $action = strip_tags($get['a']);
                    if (empty($action)) {
                        $action = strip_tags($post['a']);
                    }
                } else {
                    $action = "";
                }
                if (empty($get['p'])) {
                    $page = 1;
                } else {
                    $page = strip_tags($get['p']);
                }
            }

            //CSS-Stile werden eingebunden
            $this->AddCSS('style');
            $this->AddCSS('news');
            $this->AddCSS('forms');

            if ($action == "showmain") {
                $dcusheadid = $get['id'];
                $all_dcus = $db_dcusimg_obj->GetAll($dcusheadid);

                $this->AddJavaScriptFile('img');

                $this->body .= $this->GetMain1($session_obj, $db_dcusimg_obj, $db_dcushead_obj, $db_dcusmain_obj, $db_dcuscomment_obj, $all_dcus, $dcusheadid, $page);
            } elseif ($action == "showcomments") {
                $dcusheadid = $get['id'];

                $this->body .= $this->GetComments($session_obj, $user_obj, $db_dcusimg_obj, $db_dcushead_obj, $db_dcusmain_obj, $db_dcuscomment_obj, $dcusheadid, $page);
            } elseif ($action == "setcomment") {
                $login = trim(strip_tags($post['login']));
                $text = addslashes(trim(strip_tags($post['text'])));
                $dcusheadid = $post['dcusheadid'];

                if (!empty($login)) {
                    if (!empty($text)) {
                        $dcuscomment_obj = new Dcus_commentsClass(0, $dcusheadid, $user_obj->GetId(), $text, time(), $_SERVER['REMOTE_ADDR']);
                        $db_dcuscomment_obj->Insert($dcuscomment_obj);
                    } else {
                        $this->AddJavaScript('alert("Error: Bitte geben Sie einen Text ein!")');
                    }
                } else {
                    $this->AddJavaScript('alert("Error: Ihr Login ist leer oder ungültig!")');
                }

                $this->body .= $this->GetComments($session_obj, $user_obj, $db_dcusimg_obj, $db_dcushead_obj, $db_dcusmain_obj, $db_dcuscomment_obj, $dcusheadid, $page);
            } elseif ($action == "delcomment") {
                $commentid = $get['id'];
                $dcusheadid = $get['headid'];

                if (!empty($commentid)) {
                    $dcuscomment_obj = $db_dcuscomment_obj->GetCommentById($commentid);
                } else {
                    $dcuscomment_obj = null;
                }

                if ($dcuscomment_obj != null && $user_obj->GetId() == $dcuscomment_obj->GetUser_id() || $dcuscomment_obj != null && $user_obj->GetFlag() == "admin") {
                    $db_dcuscomment_obj->DeleteById($commentid);
                } else {
                    $this->AddJavaScript('alert("Error: Keine Löschrechte!")');
                }

                $this->body .= $this->GetComments($session_obj, $user_obj, $db_dcusimg_obj, $db_dcushead_obj, $db_dcusmain_obj, $db_dcuscomment_obj, $dcusheadid, $page);
            } else {
                /*
                // Seitenverwaltung der DCUS Main
                if($page == 1)
                {
                    $all_dcus = $db_dcushead_obj->SelectAll(0, news_count);
                    $scrolling = '<span class="smalltext"><strong>Seite 1</strong>&nbsp;<a href="handler.php?s=' . $session_obj->GetSession() . '&p=2&goto=dcus">-></a></span>';
                }
                else
                {
                    $all_dcus = $db_dcushead_obj->SelectAll(($page-1)*news_count, news_count);
                    $scrolling = '<span class="smalltext"><a href="handler.php?s=' . $session_obj->GetSession() . '&p=' . ($page-1) . '&goto=dcus"><-</a> <strong>Seite ' . $page . '</strong>';
                    if($db_dcushead_obj->GetNewsCount()>(($page-1)*news_count)+news_count)
                    {
                        $scrolling .= '&nbsp;<a href="handler.php?s=' . $session_obj->GetSession() . '&p='.($page+1).'&goto=dcus">-></a></span>';
                    }
                }
                */

                // Seitenverwaltung der DCUS Main
                $scrolling = '<span class="smalltext"><strong>Seite ';

                $all_dcus = $db_dcushead_obj->SelectAll(($page-1)*news_count, news_count);

                for ($i = 0; $i<($db_dcushead_obj->GetNewsCount()/news_count); $i++) {
                    if ($i % 10 == 0) {
                        $scrolling .= '<br>';
                    }

                    if (($i+1) == $page) {
                        $scrolling .= '[<font color="red">'.($i+1).'</font>] ';
                    } else {
                        $scrolling .= '<a href="handler.php?s='.$session_obj->GetSession().'&p='.($i+1).'&goto=dcus">'.($i+1).'</a> ';
                    }
                }

                $scrolling .= '</strong></span>';

                $this->body .= $this->GetHead1($session_obj, $db_user_obj, $db_dcuscomment_obj, $all_dcus, $scrolling, $page);
            }
        }

        private function GetHead1($session_obj, $db_user_obj, $db_dcuscomment_obj, $all_dcus, $scrolling, $page)
        {
            return '
			<table width="660" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td width="100%" valign="top">
						<img src="img/menu/dcus_k.gif" width="100" height="30" border="0" titel="News">
					</td>
				</tr>
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td width="100%" valign="top" height="15" class="smalltext"><strong>Die Coolsten unter der Sonne,</strong><br>nennen sich ein paar Leute die sich schon Jahre lang kennen und wöchentlich die Partylandschaften unsicher machen. Um auch anderen Leuten einen kleinen Eindruck davon zu vermitteln, werden wir bei jeder größeren Veranstaltung bzw. beim wöchentlichen Vortrinken Bilder machen und diese mit Texten Kommentieren. Des weiteren kommen hier natürlich auch andere Artikel rein die interessant zu sein scheinen.</td>
				</tr>
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td width="100%" valign="top">
						<table width="100%" cellspacing="0" cellpadding="0" border="0">
							'.$this->GetHead2($session_obj, $db_user_obj, $db_dcuscomment_obj, $all_dcus, $scrolling, $page).'
						</table>
					</td>
				</tr>
			</table><br>';
        }

        private function GetHead2($session_obj, $db_user_obj, $db_dcuscomment_obj, $all_dcus, $scrolling, $page)
        {
            $text = "";

            if ($all_dcus) {
                foreach ($all_dcus as $dcushead_obj) {
                    $user_obj = $db_user_obj->GetUserById($dcushead_obj->GetUser_id());
                    $commentscount = $db_dcuscomment_obj->GetCommentCountByHeadId($dcushead_obj->GetId());

                    if ($commentscount == 1) {
                        $comm = " Kommentar";
                    } else {
                        $comm = " Kommentare";
                    }

                    $text .= "<tr><td>".$this->UseBox1('&nbsp;'.$dcushead_obj->GetDate().'&nbsp;<strong><a href="handler.php?s='.$session_obj->GetSession().'&p='.$page.'&a=showmain&id='.$dcushead_obj->GetId().'&goto=dcus" target="bottomFrame" class="whitelink">'.$dcushead_obj->GetHeadline().'</a></strong>', '
					<table width="100%" cellspacing="1" cellpadding="2" border="0">
						</tr>
							<td width="20%">
								<a href="handler.php?s='.$session_obj->GetSession().'&p='.$page.'&a=showmain&id='.$dcushead_obj->GetId().'&goto=dcus" target="bottomFrame"><img src="'.img_dcus_dir.$dcushead_obj->GetImg().'" width="160" height="120" alt="'.$dcushead_obj->GetHeadline().'" title="'.$dcushead_obj->GetHeadline().'" border="0" style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000;"></a>
							</td>
							<td width="80%" class="smalltext" valign="top">
								'.$dcushead_obj->GetText().'<br><strong>Kommentiert von '.$user_obj->GetLogin().'</strong>
							</td>
						<tr>
						<tr>
							<td colspan="2" class="smalltext">
								'.$commentscount.' <a href="handler.php?s='.$session_obj->GetSession().'&p='.$page.'&a=showcomments&id='.$dcushead_obj->GetId().'&goto=dcus" class="orangelink" target="bottomFrame">'.$comm.'</a> - '.$dcushead_obj->GetClicks().' <b>Klicks</b>
							</td>
						</tr>
					</table>
					', '100%', '19', 'left')."</td></tr>";
                }
            }

            /*
            $text .= "<tr><td>
                " . $this->UseBox2('<strong>&nbsp;#&nbsp;Umbau!</strong>', 'Zur Zeit befindet sich diese Sektion im Umbau.', '500', '19', 'center') . "
                <td></tr>";
            */
            $text .= "<tr><td>".$scrolling."</td></tr>";

            return $text;
        }

        private function GetMain1($session_obj, $db_dcusimg_obj, $db_dcushead_obj, $db_dcusmain_obj, $db_dcuscomment_obj, $all_dcus, $dcusheadid, $page)
        {
            if (!empty($dcusheadid)) {
                $dcushead_obj = $db_dcushead_obj->GetHeadById($dcusheadid);
                $dcusmain_obj = $db_dcusmain_obj->GetMainById($dcusheadid);
            } else {
                $dcushead_obj = null;
                $dcusmain_obj = null;
            }

            if ($dcushead_obj != null) {
                $db_dcushead_obj->UpdateClicks($dcushead_obj->GetId());

                return '
				<table width="660" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td width="100%" valign="top">
							<img src="img/menu/dcus_k.gif" width="100" height="30" border="0" titel="News">
						</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15" class="smalltext"><strong>'.$dcushead_obj->GetHeadline().'</strong><br>'.$dcusmain_obj->GetText().'</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%" valign="top">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								'.$this->GetMain2($session_obj, $db_dcusimg_obj, $db_dcushead_obj, $all_dcus, $dcusheadid).'
							</table>
						</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15"><a href="handler.php?s='.$session_obj->GetSession().'&p='.$page.'&goto=dcus" class="blacklink" target="bottomFrame">zurück</a></td>
					</tr>
				</table><br>';
            } else {
                throw new Exception("ERROR: Fehlende DCUS HEAD ID!");
            }
        }

        private function GetMain2($session_obj, $db_dcusimg_obj, $db_dcushead_obj, $all_dcus, $dcusheadid)
        {
            $text = "";
            $count = 0;

            if ($all_dcus) {
                foreach ($all_dcus as $dcusimg_obj) {
                    $count++;

                    if ($dcusheadid > 30) {
                        $imglink = '<a href="javascript:imgshow(\''.img_dcus_dir.$dcusimg_obj->GetImg().'\', \'640\', \'480\')"><img src="'.img_dcus_dir.$dcusimg_obj->GetImg().'" width="320" height="240" border="0"></a>';
                    } else {
                        $imglink = "<img src='".img_dcus_dir.$dcusimg_obj->GetImg()."' width='320' height='240' border='0'>";
                    }

                    if (($count % 2) != 0) {
                        if ($count >= 2) {
                            $text .= "</tr>";
                        }

                        $text .= "<tr><td>".$this->UseBox4('
						<table width="100%" cellspacing="1" cellpadding="2" border="0">
							</tr>
								<td>
									'.$imglink.'
								</td>
							<tr>
						</table>
						', '100%', 'left')."</td>";
                    } else {
                        $text .= "<td>".$this->UseBox4('
						<table width="100%" cellspacing="1" cellpadding="2" border="0">
							</tr>
								<td>
									'.$imglink.'
								</td>
							<tr>
						</table>
						', '100%', 'left')."</td>";
                    }
                }
            }

            $text .= "</tr>";

            return $text;
        }

        private function GetComments($session_obj, $user_obj, $db_dcusimg_obj, $db_dcushead_obj, $db_dcusmain_obj, $db_dcuscomment_obj, $dcusheadid, $page)
        {
            if (!empty($dcusheadid)) {
                $dcushead_obj = $db_dcushead_obj->GetHeadById($dcusheadid);
                $dcusmain_obj = $db_dcusmain_obj->GetMainById($dcusheadid);
            } else {
                $dcushead_obj = null;
                $dcusmain_obj = null;
            }

            if ($dcushead_obj != null) {
                return '
				<table width="660" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td width="100%" valign="top">
							<img src="img/menu/dcus_k.gif" width="100" height="30" border="0" titel="News">
						</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15" class="smalltext"><strong>D.C.U.S. Kommentare ...<br></strong>Hier könnt ihr Kommentare zu diesem Thema abgeben und oder natürlich auch lesen. Viel Spass dabei.<br><font color="red"><b>*Wichtig*</b></font> Nur registrierte und eingeloggte User können Kommentare schreiben!</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%" valign="top">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">

								'.$this->GetCommentsHead($session_obj, $user_obj, $db_dcushead_obj, $db_dcuscomment_obj, $dcusheadid, $page).'

							</table>
						</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15"><a href="handler.php?s='.$session_obj->GetSession().'&p='.$page.'&goto=dcus" class="blacklink" target="bottomFrame">zurück</a></td>
					</tr>
				</table><br>';
            } else {
                throw new Exception("ERROR: Fehlende DCUS HEAD ID!");
            }
        }

        private function GetCommentsHead($session_obj, $user_obj, $db_dcushead_obj, $db_dcuscomment_obj, $dcusheadid, $page)
        {
            if (!empty($dcusheadid)) {
                $dcushead_obj = $db_dcushead_obj->GetHeadById($dcusheadid);
            } else {
                $dcushead_obj = null;
            }

            $text = "";

            if ($dcushead_obj != null) {
                $text .= "<tr><td>".$this->UseBox1('&nbsp;'.$dcushead_obj->GetDate().'&nbsp;<strong><a href="handler.php?s='.$session_obj->GetSession().'&a=showmain&id='.$dcushead_obj->GetId().'&goto=dcus" target="bottomFrame" class="whitelink">'.$dcushead_obj->GetHeadline().'</a></strong>', '
					<table width="100%" cellspacing="1" cellpadding="2" border="0">
						</tr>
							<td width="20%">
								<a href="handler.php?s='.$session_obj->GetSession().'&p='.$page.'&a=showmain&id='.$dcushead_obj->GetId().'&goto=dcus" target="bottomFrame"><img src="'.img_dcus_dir.$dcushead_obj->GetImg().'" width="160" height="120" alt="'.$dcushead_obj->GetHeadline().'" title="'.$dcushead_obj->GetHeadline().'" border="0" style="border-top: 1px solid #000000; border-left: 1px solid #000000; border-bottom: 1px solid #000000; border-right: 1px solid #000000;"></a>
							</td>
							<td width="80%" class="smalltext" valign="top">
								'.$dcushead_obj->GetText().'
							</td>
						<tr>
					</table>
					', '100%', '19', 'left')."</td></tr>";

                $db_user_obj = new DBUserClass();

                if (!empty($dcusheadid)) {
                    $dcuscomments_objs = $db_dcuscomment_obj->SelectAllByHeadId($dcusheadid);
                } else {
                    $dcuscomments_objs = null;
                }

                if ($user_obj != null) {
                    if ($session_obj->GetUser_id() > 0) {
                        $text .= "<tr><td>".$this->UseBox3('<strong>Kommentareingabe:</strong>', 'Kommentar schreiben.',
                        '<tr class="boxcomments1">
							<input name="dcusheadid" type="hidden" id="dcusheadid" value="'.$dcusheadid.'">
							<input name="login" type="hidden" id="text" value="'.$user_obj->GetLogin().'">
							<td class="smalltext"><input type="text" class="form" size="20" value="'.$user_obj->GetLogin().'" disabled></td>
		  				 </tr>
		  				 <tr class="boxcomments1">
							<td class="smalltext"><textarea name="text" rows="3" cols="75" class="form"></textarea></td>
		  				 </tr>
		  				 <tr class="boxcomments1">
							<td><input type="submit" name="submit" value="Eintragen" class="form"></td>
		  				 </tr>', '50%', 'left', 'handler.php?s='.$session_obj->GetSession().'&a=setcomment&goto=dcus&id='.$dcushead_obj->GetId())."</td></tr>";
                    }
                }

                if ($dcuscomments_objs != null) {
                    foreach ($dcuscomments_objs as $dcuscomments_obj) {
                        $user_tmp_obj = $db_user_obj->GetUserById($dcuscomments_obj->GetUser_id());

                        if ($user_obj != null && $user_obj->GetFlag() == "admin" || $user_obj != null && $user_obj->GetId() == $dcuscomments_obj->GetUser_id()) {
                            //Autor oder Admin

                            $del = '<A HREF="handler.php?s='.$session_obj->GetSession().'&a=delcomment&id='.$dcuscomments_obj->GetId().'&goto=dcus&headid='.$dcushead_obj->GetId().'"><IMG SRC="img/delete.gif" WIDTH="8" HEIGHT="8" BORDER="0" ALT="Löschen" TITLE="Löschen"></A>';
                        } else {
                            $del = "";
                        }

                        if ($user_tmp_obj != null) {
                            $text .= "<tr><td>".$this->UseBox1('&nbsp;'.date("d", $dcuscomments_obj->GetDate()).'.'.date("m", $dcuscomments_obj->GetDate()).'.'.date("Y", $dcuscomments_obj->GetDate()).'&nbsp;<strong>'.$user_tmp_obj->GetLogin().'</strong>', '
								<table width="100%" cellspacing="1" cellpadding="2" border="0">
									</tr>
										<td class="smalltext">
											'.nl2br($dcuscomments_obj->GetText()).' '.$del.'
										</td>
									<tr>
								</table>
								', '100%', '19', 'left')."</td></tr>";
                        }
                    }
                } else {
                    $text .= "
						<tr>
							<td class='smalltext'><b>Keine Kommentare vorhanden.</b></td>
						</tr>
						<tr>
							<td class='smalltext'>&nbsp;</td>
						</tr>
					";
                }
            }

            return $text;
        }
    }
