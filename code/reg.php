<?php
    /*
        Autor: Daniel Böhmer
        Erstellt am: 5.6.05
        Funktion: Funktion zum Freischalten eines Users
    */

    try {
        require_once "kernel/db_mapper_class.php";
        require_once "kernel/db_user_class.php";
        require_once "kernel/user_class.php";

        $key = $_GET['key'];

        if (empty($key)) {
            throw new Exception("ERROR: Kein Key!");
        } else {
            $db_mapper_obj = new DBMapperClass();

            $sql_query = "SELECT * FROM user WHERE activationkey = '".$key."'";
            $result = $db_mapper_obj->ExecSql($sql_query);

            if ($result->num_rows != 1) {
                throw new Exception("ERROR: Key nicht vorhanden!");
            } else {
                $row = $result->fetch_assoc();
                $db_user_obj = new DBUserClass();
                $user_obj = $db_user_obj->GetUserById($row['id']);
                if ($user_obj != null) {
                    $user_obj->SetActivationkey("");
                    $user_obj->SetStatus("unlocked");
                    $db_user_obj->UpdateUser($user_obj);

                    // Admin wird durch eine IM benachrichtigt
                    $reg_msg = "Der folgende User \n\n- ".$user_obj->GetLogin()."\n\n hat sich mit der eMail \n\n- ".$user_obj->GetEmail()."\n\n erfolgreich registriert und freigeschaltet!";
                    $sql_query = "INSERT INTO intern_message values(NULL, '86', '1', 'Account Registrierung', '".$reg_msg."', '".time()."', 'unread', 'false', '127.0.0.1')";
                    $db_mapper_obj->ExecSql($sql_query);

                    // User erhält eine Willkommens IM
                    $welcome_msg = "Herzlich Willkommen ".$user_obj->GetLogin()." auf der Planetboehmer Seite! Bei Fragen oder Anregungen kann man jeder Zeit dem User sicbb eine IM schreiben.";
                    $sql_query = "INSERT INTO intern_message values(NULL, '86', '".$user_obj->GetId()."', 'Willkommen', '".$welcome_msg."', '".time()."', 'unread', 'false', '127.0.0.1')";
                    $db_mapper_obj->ExecSql($sql_query);

                    throw new Exception("Sie wurden erfolgreich freigeschaltet!<br><br><A HREF='http://www.planetboehmer.de'>www.planetboehmer.de</A>");
                }
            }
        }
    } catch (Exception $e) {
        print("<br>".$e->getMessage()."<br><br>");
    }
