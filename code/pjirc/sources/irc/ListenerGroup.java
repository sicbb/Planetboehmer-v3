/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

import java.util.*;

/**
 * Handles a group of listeners.
 */
public class ListenerGroup
{
  private Hashtable _listeners;

  /**
   * Create a new ListenerGroup.
   */
  public ListenerGroup()
  {
    _listeners=new Hashtable();
  }

  /**
   * Add a listener to the group.
   * @param listener listener to add.
   */
  public synchronized void addListener(Object listener)
  {
    _listeners.put(listener,listener);
  }

  /**
   * Remove a listener from the group.
   * @param listener listener to remove.
   */
  public synchronized void removeListener(Object listener)
  {
    _listeners.remove(listener);
  }

  /**
   * Send an event to all listeners in the group.
   * @param method method to call on each listener.
   * @param params parameters to pass to the called method.
   * @return a result from any called listener.
   */
  public synchronized Object sendEvent(String method,Object[] params)
  {
    Enumeration e=_listeners.elements();
    Object ans=null;
    while(e.hasMoreElements())
    {
      ans=EventDispatcher.dispatchEvent(e.nextElement(),method,params);
    }
    return ans;
  }

  /**
   * Send an event to all listeners in the group.
   * @param method method to call on each listener.
   * @return a result from any called listener.
   */
  public synchronized Object sendEvent(String method)
  {
    return sendEvent(method,new Object[0]);
  }

  /**
   * Send an event to all listeners in the group.
   * @param method method to call on each listener.
   * @param param1 first parameter to pass to the called method.
   * @return a result from any called listener.
   */
  public synchronized Object sendEvent(String method,Object param1)
  {
    Object[] p={param1};
    return sendEvent(method,p);
  }

  /**
   * Send an event to all listeners in the group.
   * @param method method to call on each listener.
   * @param param1 first parameter to pass to the called method.
   * @param param2 second parameter to pass to the called method.
   * @return a result from any called listener.
   */
  public synchronized Object sendEvent(String method,Object param1,Object param2)
  {
    Object[] p={param1,param2};
    return sendEvent(method,p);
  }

  /**
   * Send an event to all listeners in the group.
   * @param method method to call on each listener.
   * @param param1 first parameter to pass to the called method.
   * @param param2 second parameter to pass to the called method.
   * @param param3 third parameter to pass to the called method.
   * @return a result from any called listener.
   */
  public synchronized Object sendEvent(String method,Object param1,Object param2,Object param3)
  {
    Object[] p={param1,param2,param3};
    return sendEvent(method,p);
  }

}
