/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * A query interpretor.
 */
public class QueryInterpretor extends IRCInterpretor
{
  /**
   * Create a new QueryInterpretor.
   * @param config global irc configuration.
   * @param filter CTCPFilter to use.
   */
  public QueryInterpretor(IRCConfiguration config,CTCPFilter filter)
  {
    super(config,filter);
  }
}

