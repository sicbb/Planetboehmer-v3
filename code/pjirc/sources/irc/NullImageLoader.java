/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

import java.awt.*;

/**
 * Null image loader. This class always fail to load image.
 */
public class NullImageLoader implements ImageLoader
{
  public Image getImage(String source)
	{
	  return null;
	}
}

