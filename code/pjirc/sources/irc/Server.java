/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * The server class.
 */
public interface Server
{
  /**
   * Say the specified string. This string is sent as it to the remote server.
   * @param destination message destination.
   * @param str message itself.
   */
  public void say(String destination,String str);

  /**
   * Execute the given command on the remote server.
   * @param str the command to execute.
   */
  public void execute(String str);

  /**
   * Send the given message to the server's status.
   * @param str string to send to the status.
   */
  public void sendStatusMessage(String str);

  /**
   * Get the nickname on this server.
   * @return the nickname on the server.
   */
  public String getNick();
}

