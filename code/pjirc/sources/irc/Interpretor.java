/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * A source interpretor.
 */
public interface Interpretor
{
  /**
   * Send the given string to the server.
   * @param s the source.
   * @param str the string to send.
   */
  public void sendString(Source s,String str);

}

