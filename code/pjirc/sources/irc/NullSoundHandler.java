/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

import java.applet.*;

/**
 * Null sound handler. This handler never plays sounds.
 */
public class NullSoundHandler implements SoundHandler
{
	public void playSound(String name)
	{
	}
}

