/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * Server irc protocol event listener.
 */
public interface ServerProtocolListener
{
  /**
   * A new server numeric reply has been received.
   * @param prefix reply prefix.
   * @param id reply id.
   * @param params reply parameters.
   */
  public void replyReceived(String prefix,String id,String params[]);

  /**
   * A new server message has been received.
   * @param prefix message prefix.
   * @param command message command.
   * @param params message parameters.
   */
  public void messageReceived(String prefix,String command,String params[]);

  /**
   * The server is now connected.
   */
  public void connected();

  /**
   * The connection coulnd't be established.
   * @param message error message.
   */
  public void connectionFailed(String message);

  /**
   * Connection to server has been lost.
   */
  public void disconnected();
}

