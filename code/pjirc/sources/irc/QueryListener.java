/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * Query listener.
 */
public interface QueryListener extends SourceListener
{
  /**
   * The remote nick has changed.
   * @param newNick new remote nick.
   */
  public void nickChanged(String newNick);

}

