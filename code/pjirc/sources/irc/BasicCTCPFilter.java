/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

import java.util.*;
import java.io.*;
import irc.dcc.*;

/**
 * Basic CTCPFilter, used in sources.
 */
public class BasicCTCPFilter extends IRCObject implements CTCPFilter,DCCChatClosingListener,DCCFileClosingListener
{

  private ListenerGroup _listeners;
	private DCCWraper _wraper;

  /**
   * Create a new BasicCTCPFilter, using the given IRCConfiguration.
   * @param config IRCConfiguration to use.
   */
  public BasicCTCPFilter(IRCConfiguration config)
  {
    super(config);
    _listeners=new ListenerGroup();
		_wraper=new DCCWraper(config);
  }

  /**
   * Add a DCCListener.
   * @param lis listener to add.
   */
  public void addDCCListener(DCCListener lis)
  {
    _listeners.addListener(lis);
  }

  /**
   * Remote a DCCListener.
   * @param lis listener to remove.
   */
  public void removeDCCListener(DCCListener lis)
  {
    _listeners.removeListener(lis);
  }

  public void chatClosing(DCCChat c)
  {
    _listeners.sendEvent("DCCChatRemoved",c);
  }

  public void fileClosing(DCCFile c)
  {
    _listeners.sendEvent("DCCFileRemoved",c);
  }

  private void send(Server s,String destination,String msg)
  {
    s.say(destination,"\1"+msg.toUpperCase()+"\1");
  }

  public void action(Server s,String destination,String msg)
  {
    send(s,destination,"ACTION "+msg);
  }

  public void ping(Server s,String nick)
  {
    send(s,nick,"PING "+(new Date()).getTime());
  }

  public void chat(Server s,String nick)
  {
	  try
		{
	    DCCChat chat=_wraper.createDCCChat(s.getNick(),nick,this);

      _listeners.sendEvent("DCCChatCreated",chat,new Boolean(true));
  		String arg=chat.openPassive();
      if(arg.length()==0)
      {
        chat.report(getText(TextProvider.DCC_UNABLE_PASSIVE_MODE));
      }
      else
      {
        send(s,nick,"DCC CHAT chat "+arg);
      }
	  }
		catch(Throwable ex)
		{
		}
  }

  public void sendFile(Server s,String nick,String fname)
  {
	  try
		{
	    File f=new File(fname);
	    DCCFile file=_wraper.createDCCFile(f,nick,this);
      char guil=34;
      String filename=f.getName();
      if(filename.indexOf(" ")!=-1) filename=guil+filename+guil;
      String arg=filename+" "+file.send();
      send(s,nick,"DCC SEND "+arg);
      _listeners.sendEvent("DCCFileCreated",file);

		}
		catch(Throwable ex)
		{
		}
  }

  public void genericSend(Server s,String nick,String message)
  {
    send(s,nick,message);
  }


  public void perform(String nick,Source source,String msg)
  {
    String cmd="";
    String param="";
    int pos=msg.indexOf(' ');
    if(pos==-1)
    {
      cmd=msg.toLowerCase();
    }
    else
    {
      cmd=msg.substring(0,pos).toLowerCase();
      param=msg.substring(pos+1);
    }

    boolean show=true;
    if(cmd.equals("action"))
    {
      source.action(nick,param);
      show=false;
    }
    else if(cmd.equals("version"))
    {
      String data="Plouf's IRC Client de la mort qui tue en Java";
      source.getServer().execute("NOTICE "+nick+" :\1VERSION "+data+"\1");
    }
    else if(cmd.equals("ping"))
    {
      source.getServer().execute("NOTICE "+nick+" :\1PING "+param+"\1");
    }
    else if(cmd.equals("time"))
    {
      String data=new Date().toString();
      source.getServer().execute("NOTICE "+nick+" :\1TIME "+data+"\1");
    }
    else if(cmd.equals("finger"))
    {
      String data="A lucky Plouf's IRC user";
      source.getServer().execute("NOTICE "+nick+" :\1FINGER "+data+"\1");
    }
    else if(cmd.equals("userinfo"))
    {
      String data="A lucky Plouf's IRC user";
      source.getServer().execute("NOTICE "+nick+" :\1USERINFO "+data+"\1");
    }
    else if(cmd.equals("clientinfo"))
    {
      String data="This client is a Java application supporting the following CTCP tags : ACTION VERSION PING TIME FINGER USERINFO CLIENTINFO DCC";
      source.getServer().execute("NOTICE "+nick+" :\1CLIENTINFO "+data+"\1");
    }
    else if(cmd.equals("dcc"))
    {
      StringParser sp=new StringParser();
      String[] args=sp.parseString(param.toLowerCase());
      if(args.length>=2)
      {
        if(args[0].equals("chat") && args[1].equals("chat"))
        {
          if(args.length>=4)
          {
            Boolean b=(Boolean)_listeners.sendEvent("DCCChatRequest",nick);
            if(b.booleanValue())
            {
						  try
							{
						    DCCChat chat=_wraper.createDCCChat(source.getServer().getNick(),nick,this);
							  chat.openActive(args[2],args[3]);
                _listeners.sendEvent("DCCChatCreated",chat,new Boolean(false));
							}
							catch(Throwable ex)
							{
							}
            }
          }
        }
        if(args[0].equals("send"))
        {
          if(args.length>=5)
          {
            String fname=args[1];
            String ip=args[2];
            String port=args[3];
            String size=args[4];
            File dest=(File)_listeners.sendEvent("DCCSendRequest",nick,fname,new Integer(size));
            if(dest!=null)
            {
						  try
							{
							  DCCFile file=_wraper.createDCCFile(dest,nick,this);
							  file.receive(ip,port,size);
                _listeners.sendEvent("DCCFileCreated",file);
							}
							catch(Throwable ex)
							{
							}
            }
          }
        }
      }
    }
    else if(cmd.equals(""))
    {

    }
    if(show) source.getServer().sendStatusMessage("\2\3"+"4"+"["+nick+" "+cmd.toUpperCase()+"]");
  }

  public void CTCPReply(String nick,Source source,String msg)
  {
    String cmd="";
    String param="";
    int pos=msg.indexOf(' ');
    if(pos==-1)
    {
      cmd=msg.toLowerCase();
    }
    else
    {
      cmd=msg.substring(0,pos).toLowerCase();
      param=msg.substring(pos+1);
    }

    if(cmd.equals("ping"))
    {
      long d=(new Long(param)).longValue();
      long delta=(new Date()).getTime()-d;
			source.report("\2\3"+"4"+getText(TextProvider.CTCP_PING_REPLY,nick,(delta/1000.0)+""));
    }
    else
    {
      source.report("\2\3"+"4"+"["+nick+" "+cmd.toUpperCase()+" reply] : "+param);
    }
  }
}

