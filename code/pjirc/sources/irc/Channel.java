/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

import java.util.*;

class Nick
{
  public Nick(String n,String m)
  {
    Name=n;
    Mode=new ModeHandler(m);
		Whois="";
  }

  public String Name;
	public String Whois;
  public ModeHandler Mode;
}

/**
 * A channel source.
 */
public class Channel extends IRCSource implements ReplyServerListener
{
  private String _name;
  private String _topic;
  private ModeHandler _mode;
  private ListenerGroup _listeners;
  private Hashtable _nicks;

  /**
   * Create a new Channel.
   * @param config the global configuration.
   * @param name channel name.
   * @param s the source server.
   */
  public Channel(IRCConfiguration config,String name,IRCServer s)
  {
    super(config,s);
    _name=name;
    _topic="";
    _mode=new ModeHandler();
    _listeners=new ListenerGroup();
    _nicks=new Hashtable();
		s.addReplyServerListener(this);
		if(_ircConfiguration.getASL()) getIRCServer().execute("WHO "+_name);
  }

  /**
   * Add a channel listener.
   * @param lis listener to add.
   */
  public void addChannelListener(ChannelListener lis)
  {
    _listeners.addListener(lis);
  }

  /**
   * Remove a channel listener.
   * @param lis listener to remove.
   */
  public void removeChannelListener(ChannelListener lis)
  {
    _listeners.removeListener(lis);
  }

  /*private void triggerJoinListeners(String nick,String mode)
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      ChannelListener lis=(ChannelListener)e.nextElement();
      lis.nickJoin(nick,mode);
    }
  }

  private void triggerSetListeners(String[] nicks,String modes[])
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      ChannelListener lis=(ChannelListener)e.nextElement();
      lis.nickSet(nicks,modes);
    }
  }

  private void triggerQuitListeners(String nick,String reason)
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      ChannelListener lis=(ChannelListener)e.nextElement();
      lis.nickQuit(nick,reason);
    }
  }

  private void triggerPartListeners(String nick,String reason)
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      ChannelListener lis=(ChannelListener)e.nextElement();
      lis.nickPart(nick,reason);
    }
  }

  private void triggerKickListeners(String nick,String by,String reason)
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      ChannelListener lis=(ChannelListener)e.nextElement();
      lis.nickKick(nick,by,reason);
    }
  }

  private void triggerTopicListeners(String topic,String by)
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      ChannelListener lis=(ChannelListener)e.nextElement();
      lis.topicChanged(topic,by);
    }
  }

  private void triggerModeListeners(String mode,String from)
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      ChannelListener lis=(ChannelListener)e.nextElement();
      lis.modeApply(mode,from);
    }
  }

  private void triggerNickModeListeners(String nick,String mode,String from)
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      ChannelListener lis=(ChannelListener)e.nextElement();
      lis.nickModeApply(nick,mode,from);
    }
  }

  private void triggerNickChange(String oldNick,String newNick)
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      ChannelListener lis=(ChannelListener)e.nextElement();
      lis.nickChanged(oldNick,newNick);
    }
  }

  private void triggerNickWhoisUpdate(String nick,String whois)
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      ChannelListener lis=(ChannelListener)e.nextElement();
      lis.nickWhoisUpdated(nick,whois);
    }
  }*/

  public String getName()
  {
    return _name;
  }

  public boolean talkable()
  {
    return true;
  }

  public void leave()
  {
    getIRCServer().leaveChannel(getName());
  }

  /**
   * Test wether this channel has the given nickname.
   * @param nick nickname to test.
   * @return true if nick is present on this channel, false otherwise.
   */
  public boolean hasNick(String nick)
  {
    return _nicks.get(nick)!=null;
  }

  /**
   * Notify this channel the given nick has joined, with the given options.
   * @param nick new nick.
   * @param mode nick mode.
   */
  public void joinNick(String nick,String mode)
  {
    _nicks.put(nick,new Nick(nick,mode));
		if(_ircConfiguration.getASL()) getIRCServer().execute("WHO "+nick);
    //triggerJoinListeners(nick,mode);
    _listeners.sendEvent("nickJoin",nick,mode);
  }

  /**
   * Notify this channel it should change its hole nick list.
   * @param nicks new nicks.
   * @param modes new modes. There is a one to one mapping between nicks and modes.
   */
  public void setNicks(String[] nicks,String[] modes)
  {
    for(int i=0;i<nicks.length;i++) _nicks.put(nicks[i],new Nick(nicks[i],modes[i]));
    //triggerSetListeners(nicks,modes);
    _listeners.sendEvent("nickSet",nicks,modes);
  }

  /**
   * Notify this channel the given nick has left the channel.
   * @param nick the nick.
   * @param reason the reason.
   */
  public void partNick(String nick,String reason)
  {
    _nicks.remove(nick);
    _listeners.sendEvent("nickPart",nick,reason);
    //triggerPartListeners(nick,reason);
  }

  /**
   * Notify this channel the given nick has been kicked.
   * @param nick the kicked nick.
   * @param by the nick who kicked nick.
   * @param reason the kick reason.
   */
  public void kickNick(String nick,String by,String reason)
  {
    _nicks.remove(nick);
    //triggerKickListeners(nick,by,reason);
    _listeners.sendEvent("nickKick",nick,by,reason);
  }

  /**
   * Notify this channel the given nick has quit.
   * @param nick the nick who quit.
   * @param reason reason.
   */
  public void quitNick(String nick,String reason)
  {
    _nicks.remove(nick);
    //triggerQuitListeners(nick,reason);
    _listeners.sendEvent("nickQuit",nick,reason);
  }

  /**
   * Get all the nicks in this channel.
   * @return array of all nick names.
   */
  public String[] getNicks()
  {
    String[] ans=new String[_nicks.size()];
    Enumeration e=_nicks.elements();
    int i=0;
    while(e.hasMoreElements())
      ans[i++]=((Nick)e.nextElement()).Name;
    return ans;
  }

  /**
   * Get the nick mode associated with the given nick.
   * @param nick nickname to get mode.
   * @return nick mode or null if nick not found.
   */
  public String getNickMode(String nick)
  {
    Nick n=(Nick)_nicks.get(nick);
    if(n==null) return null;
    return n.Mode.getMode();
  }

  /**
   * Notify this channel its topic has changed.
   * @param topic new topic.
   * @param by nickname who changed topic.
   */
  public void setTopic(String topic,String by)
  {
    _topic=topic;
    //triggerTopicListeners(topic,by);
    _listeners.sendEvent("topicChanged",topic,by);
  }

  /**
   * Notify this channel a nick mode has changed.
   * @param nick the nick.
   * @param mode the applied mode.
   * @param from the nick who changed mode.
   */
  public void applyUserMode(String nick,String mode,String from)
  {
    Nick n=(Nick)_nicks.get(nick);
    if(n!=null) n.Mode.apply(mode);
    //triggerNickModeListeners(nick,mode,from);
    _listeners.sendEvent("nickModeApply",nick,mode,from);
  }

  /**
   * Notify this channel its mode has changed.
   * @param mode applied mode.
   * @param from user who changed mode.
   */
  public void applyMode(String mode,String from)
  {
    _mode.apply(mode);
    //triggerModeListeners(mode,from);
    _listeners.sendEvent("modeApply",mode,from);
  }

  /**
   * Get this channel mode.
   * @return channel mode.
   */
  public String getMode()
  {
    return _mode.getMode();
  }

  /**
   * Get this channel topic.
   * @return channel topic.
   */
  public String getTopic()
  {
    return _topic;
  }

  /**
   * Notify this channel a nick has been renamed.
   * @param oldNick old nickname.
   * @param newNick new nickname.
   */
  public void changeNick(String oldNick,String newNick)
  {
    Nick n=(Nick)_nicks.get(oldNick);
    _nicks.remove(oldNick);
    n.Name=newNick;
    _nicks.put(newNick,n);

    //triggerNickChange(oldNick,newNick);
    _listeners.sendEvent("nickChanged",oldNick,newNick);
  }

	private void learn(String nick,String whois)
	{
	  Nick n=(Nick)_nicks.get(nick);
		if(n==null) return;
		n.Whois=whois;
		//triggerNickWhoisUpdate(nick,whois);
    _listeners.sendEvent("nickWhoisUpdated",nick,whois);
		//System.out.println("learned "+nick+" is "+whois);
	}

  /**
   * Get bufferised whois data for the given nick.
   * @param nick nickname.
   * @return whois data for nick, or "" if not found.
   */
	public String whois(String nick)
	{
	  Nick n=(Nick)_nicks.get(nick);
	  if(n==null) return "";
		return n.Whois;
	}

  public void replyReceived(String prefix,String id,String params[])
	{
	 /* System.out.print(id+" - ");
		for(int i=0;i<params.length;i++) System.out.print(params[i]+" ");
		System.out.println("");*/
	  if(id.equals("352"))
  	{
		  String name=params[params.length-1];
			int pos=name.indexOf(" ");
			if(pos!=-1) name=name.substring(pos+1);
			String nick=params[5];
			learn(nick,name);
		}
		/*else if(id.equals("311"))
		{
		  String name=params[params.length-1];
			String nick=params[2];
			learn(nick,name);
		}*/
	}
}

