/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

import java.applet.*;
import java.net.*;

/**
 * Sound handling via applet.
 */
public class AppletSoundHandler implements SoundHandler
{
  private Applet _applet;
  
  /**
   * Create a new AppletSoundHandler, using the given Applet.
   * @param applet the applet to use.
   */
  public AppletSoundHandler(Applet applet)
  {
    _applet=applet;
  }
  
  public void playSound(String name)
  {
    AudioClip clip=_applet.getAudioClip(_applet.getCodeBase(),name);
    clip.play();
  }

}

