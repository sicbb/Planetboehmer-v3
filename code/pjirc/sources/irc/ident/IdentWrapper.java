/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.ident;

import irc.*;
import irc.ident.prv.*;

/**
 * Ident wrapper.
 */
public class IdentWrapper extends IRCObject
{
  private IdentServer _ident;
	private IRCConfiguration _config;
  private IdentListener _lis;

  /**
   * Create a new IdentWrapper.
   * @param config the global configuration.
   */
  public IdentWrapper(IRCConfiguration config)
	{
	  super(config);
	}

  /**
   * Start the execution of the ident server, using default configuration.
   * @param lis the listener to use.
   */
	public Exception start(IdentListener lis)
	{
	  _lis=lis;
    _ident=new IdentServer(_ircConfiguration);
    _ident.addIdentListener(lis);
    _ident.setDefaultUser("JAVA","PJIRC built-in Ident server user");
    try
    {
      int port=113;
      _ident.start(port);
			return null;
    }
    catch(Exception e)
    {
		  return e;
    }
	}

	/**
	 * Stop the execution of the ident server.
	 */
	public void stop()
	{
    _ident.removeIdentListener(_lis);
    _ident.stop();
	}
}

