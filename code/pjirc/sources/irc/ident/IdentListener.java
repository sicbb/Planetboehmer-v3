/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.ident;

/**
 * The ident server listener.
 */
public interface IdentListener
{
  public static final int IDENT_ERROR=-1;
  public static final int IDENT_OK=0;
  public static final int IDENT_DEFAULT=1;
  public static final int IDENT_NOT_FOUND=2;

  /**
   * The ident server has received a request.
   * @param source the request source.
   * @param result the request result.
   * @param reply the replied result.
   */
  public void identRequested(String source,Integer result,String reply);

  /**
   * The ident server is running.
   * @param port port on wich the server is running.
   */
  public void identRunning(Integer port);

  /**
   * The ident server is leaving.
   * @param message leaving message.
   */
  public void identLeaving(String message);

}

