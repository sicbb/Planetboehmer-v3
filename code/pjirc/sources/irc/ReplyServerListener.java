/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * A reply server listener.
 */
public interface ReplyServerListener
{
  /**
   * A new numeric reply has been received from the irc server.
   * @param prefix reply prefix.
   * @param id reply id.
   * @param params reply parameters.
   */
  public void replyReceived(String prefix,String id,String params[]);

}

