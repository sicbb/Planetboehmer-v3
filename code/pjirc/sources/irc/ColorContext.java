/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * Color context.
 */
public class ColorContext
{
  /**
   * Chanlist boolean flag.
   */
  public boolean chanlist;
  /**
   * Name.
   */
  public String name;
}

