/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.gui;

import java.awt.*;
import java.awt.event.*;
import irc.*;
import irc.dcc.*;

/**
 * The AWT dcc file interface.
 */
public class AWTDCCFile implements DCCFileListener,WindowListener
{
  protected DCCFile _file;
  protected Frame _frame;
  protected AWTProgressBar _bar;
  private boolean _closed;
  private boolean _activated;
  private IRCConfiguration _ircConfiguration;

  /**
   * Create a new AWTDCCFile.
   * @param config the global irc configuration.
   * @param file the source DCCFile.
   */
  public AWTDCCFile(IRCConfiguration config,DCCFile file)
  {
    _ircConfiguration=config;
    _activated=false;
    _closed=false;
    _file=file;
    _file.addDCCFileListener(this);

    String str="";
    if(file.isDownloading())
      str=getText(TextProvider.GUI_RETREIVING_FILE,_file.getName(),_file.getSize()+"");
    else
      str=getText(TextProvider.GUI_SENDING_FILE,_file.getName(),_file.getSize()+"");


    Label label=new Label(str);

    _frame=new Frame();
    _frame.setBackground(Color.white);

    _frame.setLayout(new BorderLayout());
    _frame.addWindowListener(this);

    _bar=new AWTProgressBar();
    _frame.add(label,"North");
    _frame.add(_bar,"Center");

    _frame.setTitle(_file.getName());
    _frame.setSize(400,80);
    activate();
  }

  /**
   * Get formatted text associated with the given text code, with no parameter.
   * @param code text code.
   * @return formatted text.
   */
  public String getText(int code)
  {
    return _ircConfiguration.getText(code);
  }

  /**
   * Get formatted text associated with the given text code, with one parameter.
   * @param code text code.
   * @param p1 first parameter.
   * @return formatted text.
   */
  public String getText(int code,String p1)
  {
    return _ircConfiguration.getText(code,p1);
  }

  /**
   * Get formatted text associated with the given text code, with two parameters.
   * @param code text code.
   * @param p1 first parameter.
   * @param p2 second parameter.
   * @return formatted text.
   */
  public String getText(int code,String p1,String p2)
  {
    return _ircConfiguration.getText(code,p1,p2);
  }

  /**
   * Get formatted text associated with the given text code, with three parameters.
   * @param code text code.
   * @param p1 first parameter.
   * @param p2 second parameter.
   * @param p3 third parameter.
   * @return formatted text.
   */
  public String getText(int code,String p1,String p2,String p3)
  {
    return _ircConfiguration.getText(code,p1,p2,p3);
  }

	/**
	 * Get the source DCCFile.
	 * @return source DCCFile.
	 */
  public DCCFile getFile()
  {
    return _file;
  }

  /**
   * Activate this frame.
   */
  public void activate()
  {
    if(_activated) return;
    _activated=true;
    if(_closed) return;
    _frame.show();
  }

  /**
   * Close this transfert.
   */
  public void close()
  {
    _frame.hide();
    _frame.dispose();
    _closed=true;
  }

  public void transmitted(Integer icount)
  {
    activate();
    int count=icount.intValue();
    if((count&32767)==0)
    {
      double pc=count;
      pc/=_file.getSize();
      _bar.setColor(Color.blue);
      _bar.setValue(pc);
      _bar.repaint();
      try
      {
        Thread.sleep(10);
      }
      catch(Exception e)
      {
      }
    }
  }

  public void finished()
  {
    activate();
    _frame.setTitle(getText(TextProvider.GUI_TERMINATED,_file.getName()));
    _bar.setColor(Color.green);
    _bar.repaint();
  }

  public void failed()
  {
    activate();
    _frame.setTitle(getText(TextProvider.GUI_FAILED,_file.getName()));
    _bar.setColor(Color.red);
    _bar.repaint();

  }

  public void windowActivated(WindowEvent e) {}
  public void windowClosed(WindowEvent e) {}
  public void windowClosing(WindowEvent e)
  {
    _file.leave();
  }

  public void windowDeactivated(WindowEvent e) {}
  public void windowDeiconified(WindowEvent e) {}
  public void windowIconified(WindowEvent e) {}
  public void windowOpened(WindowEvent e) {}

}

