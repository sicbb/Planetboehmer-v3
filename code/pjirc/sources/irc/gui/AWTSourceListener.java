/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.gui;

/**
 * The awt source listener.
 */
public interface AWTSourceListener
{
  /**
   * This source title has changed.
   * @param source the source whose title has changed.
   */
  public void titleChanged(AWTSource source);

  /**
   * An event has occured.
   * @param source the source on whose event has occured.
   */
  public void eventOccured(AWTSource source);

  /**
   * The source has been activated.
   * @param source the activated source.
   */
  public void activated(AWTSource source);
}

