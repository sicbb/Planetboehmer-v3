/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.gui.pixx;

import java.awt.event.*;

/**
 * Pixx nick list listener.
 */
public interface PixxNickListListener
{
  /**
   * Some mouse event occured on a nick.
   * @param nick the nick.
   * @param e the mouse event.
   */
  public void eventOccured(String nick,MouseEvent e);

  /**
   * An ASL event occured.
   * @param nick the nickname.
   * @param info the nick name whois information string.
   */
	public void ASLEventOccured(String nick,String info);
}

