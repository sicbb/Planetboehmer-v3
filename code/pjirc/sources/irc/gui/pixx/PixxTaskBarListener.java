/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.gui.pixx;

import java.awt.event.*;
import irc.gui.*;

/**
 * Task bar listener.
 */
public interface PixxTaskBarListener
{
  /**
   * The active awt source has been desactivated.
   * @param bar the taskbar.
   * @param source the desactivated source.
   */
  public void AWTSourceDesactivated(PixxTaskBar bar,AWTSource source);

  /**
   * A new awt source has been activated.
   * @param bar the taskbar.
   * @param source the activated source.
   */
  public void AWTSourceActivated(PixxTaskBar bar,AWTSource source);

  /**
   * A new awt source has been added in the taskbar.
   * @param bar the taskbar.
   * @param source the added source.
   */
  public void AWTSourceAdded(PixxTaskBar bar,AWTSource source);

  /**
   * An awt source has been removed from the taskbar.
   * @param bar the taskbar.
   * @param source the removed source.
   */
  public void AWTSourceRemoved(PixxTaskBar bar,AWTSource source);

  /**
   * An mouse event occured on an awt source icon.
   * @param bar the taskbar.
   * @param source the awt source.
   * @param e the mouse event.
   */
  public void eventOccured(PixxTaskBar bar,AWTSource source,MouseEvent e);
}

