/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.gui.pixx;

/**
 * Pixx Scrollbar listener.
 */
public interface PixxScrollBarListener
{
  /**
   * Position has changed.
   * @param pixScrollBar scrollbar whose position has changed.
   */
  public void valueChanged(PixxScrollBar pixScrollBar);
}

