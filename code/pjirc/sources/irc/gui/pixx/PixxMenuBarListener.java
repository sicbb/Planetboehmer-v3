/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.gui.pixx;

/**
 * The menu bar listener.
 */
public interface PixxMenuBarListener
{
  /**
   * The connection button has been clicked.
   * @param bar the menu bar.
   */
  public void connectionClicked(PixxMenuBar bar);

  /**
   * The chanlist button has been clicked.
   * @param bar the menu bar.
   */
  public void chanListClicked(PixxMenuBar bar);

  /**
   * The about button has been clicked.
   * @param bar the menu bar.
   */
  public void aboutClicked(PixxMenuBar bar);

  /**
   * The help button has been clicked.
   * @param bar the menu bar.
   */
  public void helpClicked(PixxMenuBar bar);

  /**
   * The close button has been clicked.
   * @param bar the menu bar.
   */
  public void closeClicked(PixxMenuBar bar);
}

