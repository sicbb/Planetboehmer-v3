/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.gui.pixx;

import irc.gui.*;

/**
 * The listener on the multiple document interface.
 */
public interface PixxMDIInterfaceListener
{
  /**
   * The active source has changed.
   * @param source the new active source.
   * @param mdi the mdi.
   */
  public void activeChanged(AWTSource source,PixxMDIInterface mdi);

  /**
   * The connect button has been triggered.
   * @param mdi the mdi.
   */
  public void connectTriggered(PixxMDIInterface mdi);

  /**
   * The about button has been triggered.
   * @param mdi the mdi.
   */
  public void aboutTriggered(PixxMDIInterface mdi);

  /**
   * The help button has been triggered.
   * @param mdi the mdi.
   */
  public void helpTriggered(PixxMDIInterface mdi);
}

