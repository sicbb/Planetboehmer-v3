/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.gui;

import java.util.*;
import irc.*;
import irc.style.*;
import java.awt.*;
import java.awt.event.*;
import irc.gui.pixx.*;

/**
 * The AWT source.
 */
public class AWTSource extends Panel implements SourceListener,ActionListener,PixxScrollBarListener,FocusListener,StyledListListener,WindowListener
{
  protected Source _source;
  protected PixxScrollBar _scroll;
  protected Panel _panel;
  protected StyledList _list;
  protected AWTIrcTextField _textField;
  protected FormattedStringDrawer _styler;
  protected String _title;
  protected String _strippedTitle;
  private ListenerGroup _listeners;
  protected IRCConfiguration _ircConfiguration;

  /**
   * Create a new AWTSource.
   * @param config the global configuration.
   * @param source the source of this awt source.
   */
  public AWTSource(IRCConfiguration config,Source source)
  {
    _ircConfiguration=config;
    _listeners=new ListenerGroup();
    _source=source;
    addFocusListener(this);
    _source.addSourceListener(this);
    _panel=new Panel();
    _panel.addFocusListener(this);
    _panel.setBackground(Color.white);
    _scroll=new PixxScrollBar(_ircConfiguration,0,0,PixxScrollBar.VERTICAL,0.1);
    _scroll.addPixxScrollBarListener(this);
    setLayout(new BorderLayout());
    _list=new StyledList(_ircConfiguration,new Font(_ircConfiguration.getChannelFontName(),Font.PLAIN,_ircConfiguration.getChannelFontSize()),_ircConfiguration.getColorContext(source.getName()));
    _list.addFocusListener(this);
    _list.addStyledListListener(this);
    _styler=new FormattedStringDrawer(new Font("Monospaced",Font.PLAIN,12),_ircConfiguration,getColorContext());
    _textField=new AWTIrcTextField();

    Color[] c=_ircConfiguration.getStyleColors(getColorContext());
    _textField.setBackground(c[0]);
    _textField.setForeground(c[1]);
    _textField.addFocusListener(this);
    Panel p=new Panel();
    p.setLayout(new BorderLayout());
    p.add(_panel,"Center");
    p.add(new PixxSeparator(PixxSeparator.BORDER_LEFT),"West");
    p.add(new PixxSeparator(PixxSeparator.BORDER_RIGHT),"East");
    p.add(new PixxSeparator(PixxSeparator.BORDER_UP),"North");
    p.add(new PixxSeparator(PixxSeparator.BORDER_DOWN),"South");

    add(p,"Center");
    _panel.setLayout(new BorderLayout());
    _panel.add(_scroll,"East");
    add(_textField,"South");

    _textField.addActionListener(this);
    setTitle(_source.getName());
    setSize(640,400);
    _panel.add(_list,"Center");
  }

  /**
   * Get color context for this awt source.
   * @return color context for this awt source.
   */
	public ColorContext getColorContext()
	{
    return _ircConfiguration.getColorContext(_source.getName());
	}

  /**
   * Get formatted text associated with the given text code, with no parameter.
   * @param code text code.
   * @return formatted text.
   */
  public String getText(int code)
  {
    return _ircConfiguration.getText(code);
  }

  /**
   * Get formatted text associated with the given text code, with one parameter.
   * @param code text code.
   * @param p1 first parameter.
   * @return formatted text.
   */
  public String getText(int code,String p1)
  {
    return _ircConfiguration.getText(code,p1);
  }

  /**
   * Get formatted text associated with the given text code, with two parameters.
   * @param code text code.
   * @param p1 first parameter.
   * @param p2 second parameter.
   * @return formatted text.
   */
  public String getText(int code,String p1,String p2)
  {
    return _ircConfiguration.getText(code,p1,p2);
  }

  /**
   * Get formatted text associated with the given text code, with three parameters.
   * @param code text code.
   * @param p1 first parameter.
   * @param p2 second parameter.
   * @param p3 third parameter.
   * @return formatted text.
   */
  public String getText(int code,String p1,String p2,String p3)
  {
    return _ircConfiguration.getText(code,p1,p2,p3);
  }

  /**
   * Add listener.
   * @param lis the listener to add.
   */
  public void addAWTSourceListener(AWTSourceListener lis)
  {
    _listeners.addListener(lis);
  }

  /**
   * Remove listener.
   * @param lis the listener to remove.
   */
  public void removeAWTSourceListener(AWTSourceListener lis)
  {
    _listeners.removeListener(lis);
  }

  /*private void triggerTitleChangedListeners()
  {
    triggerEventListeners();
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      AWTSourceListener lis=(AWTSourceListener)e.nextElement();
      lis.titleChanged(this);
    }
  }

  private void triggerEventListeners()
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      AWTSourceListener lis=(AWTSourceListener)e.nextElement();
      lis.eventOccured(this);
    }
  }

  private void triggerActivatedListeners()
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      AWTSourceListener lis=(AWTSourceListener)e.nextElement();
      lis.activated(this);
    }
  }*/

  /**
   * Set this source title.
   */
  public void setTitle(String title)
  {
	  if(title.equals(_title)) return;
    _title=title;
    _strippedTitle=_styler.getStripped(title);
    //triggerTitleChangedListeners();
    _listeners.sendEvent("titleChanged",this);
  }

  /**
   * Get the stripped title.
   * @return the stripped title.
   */
  public String getStrippedTitle()
  {
    return _strippedTitle;
  }

  /**
   * Get the title.
   * @return the title.
   */
  public String getTitle()
  {
    return _title;
  }

  /**
   * Get a shorter title.
   * @return a shorter title.
   */
  public String getShortTitle()
  {
    return _source.getName();
  }

  /**
   * Get the source.
   * @return the source.
   */
  public Source getSource()
  {
    return _source;
  }

  public void actionPerformed(ActionEvent e)
  {
    _source.sendString(_textField.getText());
  }

  /**
   * Clear this awt source display.
   */
  public void clear()
  {
    _list.clear();
    _scroll.setMaximum(_list.getLineCount()-1);
    _scroll.setValue(_list.getLast());
    //triggerEventListeners();
    _listeners.sendEvent("eventOccured",this);
  }

  /**
   * Test wether the specified line should be highlighted.
   * @param msg line to test.
   * @return true if msg should be highlighted, false otherwise.
   */
  protected boolean needHighLight(String msg)
  {
    msg=msg.toLowerCase();
    if(_ircConfiguration.highLightNick())
    {
      String myNick=_source.getServer().getNick().toLowerCase();
      if(msg.indexOf(myNick)!=-1) return true;
    }

    Enumeration e=_ircConfiguration.getHighLightWords();
    while(e.hasMoreElements())
    {
      String word=((String)(e.nextElement())).toLowerCase();
      if(msg.indexOf(word)!=-1) return true;
    }
    return false;
  }

  /**
   * Print the given message on the awt source display.
   * @param mes message to print.
   * @param color color to use.
   * @param bold true if message should be in bold.
   * @param underline if message should be underlined.
   */
  protected void print(String msg,int color,boolean bold,boolean underline)
  {
    _source.activate();
    if(color!=1) msg="\3"+color+msg;
    if(bold) msg="\2"+msg;
    if(underline) msg=((char)31)+msg;



    if(_ircConfiguration.getTimeStamp())
    {
      Calendar cal=Calendar.getInstance();
      String hour=""+cal.get(Calendar.HOUR_OF_DAY);
      if(hour.length()==1) hour="0"+hour;
      String min=""+cal.get(Calendar.MINUTE);
      if(min.length()==1) min="0"+min;
      msg="["+hour+":"+min+"] "+msg;
    }
    _list.addLine(msg);
    _scroll.setMaximum(_list.getLineCount()-1);
    _scroll.setValue(_list.getLast());
    //triggerEventListeners();
    _listeners.sendEvent("eventOccured",this);
  }

  /**
   * Print the given message on the awt source display.
   * @param mes message to print.
   * @param color color to use.
   */
  protected void print(String msg,int color)
  {
    print(msg,color,false,false);
  }

  /**
   * Print the given message on the awt source display.
   * @param msg message to print.
   */
  protected void print(String msg)
  {
    print(msg,1,false,false);
  }

  public void messageReceived(String source,String str)
  {
    if(needHighLight(str))
    {
		  //_ircConfiguration.getAudioConfiguration().onHighLight();
      print("<"+source+"> "+str,_ircConfiguration.highLightColor());
    }
    else
    {
      print("<"+source+"> "+str);
    }
  }

  public void reportReceived(String msg)
  {
    print(msg);
  }

  public void noticeReceived(String from,String msg)
  {
    print("-"+from+"- "+msg,5);
  }

  public void action(String nick,String msg)
  {
    print("* "+nick+" "+msg,6);
  }

  public void activate()
  {
    _listeners.sendEvent("activated",this);
    //triggerActivatedListeners();
  }

  /**
   * Return the active state for this awt source.
   * @return awt source active state.
   */
  public boolean isActive()
  {
    return _source.isActive();
  }

  /**
   * Leave this awt source.
   */
  public void leave()
  {
    _source.leave();
  }

  public void valueChanged(PixxScrollBar pixScrollBar)
  {
    _list.setLast(_scroll.getValue());
  }

  public void focusGained(FocusEvent e)
  {
    if(e.getComponent()!=_textField)
      _textField.requestFocus();
  }

  public void focusLost(FocusEvent e)
  {
  }

  public void channelEvent(StyledList lis,String chan,MouseEvent e)
  {
    if(e.getClickCount()>1)
    {
      _source.sendString("/join "+chan);
    }
  }

  public void URLEvent(StyledList lis,String url,MouseEvent e)
  {
    if(e.getClickCount()>1)
    {
      _source.sendString("/url "+url);
    }
  }

  public void nickEvent(StyledList lis,String nick,MouseEvent e)
  {
    if(e.getClickCount()>1)
    {
      _source.sendString("/query "+nick);
    }
  }

  public String toString()
  {
    return "AWTSource : "+getStrippedTitle();
  }

  public void setVisible(boolean b)
  {
    super.setVisible(b);
    if(!b) _list.dispose();
  }

  public void copyEvent(StyledList lis,String txt,MouseEvent e)
  {
    Frame f=new Frame();
    f.setTitle(getText(TextProvider.GUI_COPY_WINDOW));
    f.addWindowListener(this);
    f.setLayout(new GridLayout(1,1));
    Panel p=new Panel();
    p.setLayout(new GridLayout(1,1));
    f.add(p);
    TextArea c=new TextArea();
    c.setText(txt);
    p.add(c);
    f.setSize(400,300);
    f.show();
  }

  public void virtualSizeChanged(StyledList lis)
  {
  }

  public void windowActivated(WindowEvent e) {}
  public void windowClosed(WindowEvent e) {}
  public void windowClosing(WindowEvent e)
  {
    e.getWindow().hide();
    e.getWindow().dispose();
  }
  public void windowDeactivated(WindowEvent e) {}
  public void windowDeiconified(WindowEvent e) {}
  public void windowIconified(WindowEvent e) {}
  public void windowOpened(WindowEvent e) {}

}

