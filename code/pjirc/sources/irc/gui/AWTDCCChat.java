/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.gui;

import java.awt.event.*;
import irc.*;
import irc.gui.pixx.*;
import irc.dcc.*;

/**
 * The AWTDCCChat.
 */
public class AWTDCCChat extends AWTSource
{
  /**
   * Create a new AWTDCCChat.
   * @param config global irc configuration.
   * @param s source DCCChat.
   */
  public AWTDCCChat(IRCConfiguration config,DCCChat s)
  {
    super(config,s);
  }

}

