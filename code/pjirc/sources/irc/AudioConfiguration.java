/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * Audio configuration class.
 */
public class AudioConfiguration
{
  private SoundHandler _sound;

	private String _query;
	private String _beep;

  /**
   * Create a new AudioConfiguration, using the given SoundHandler.
   * @param sound the SoundHandler to use.
   */
  public AudioConfiguration(SoundHandler sound)
	{
	  _sound=sound;
		_query=null;
		_beep=null;
	}

  /**
   * Set the query sound.
   * @param snd sound name.
   */
  public void setQuery(String snd)
  {
    _query=snd;
  }

	/**
	 * Set the beep sound.
	 * @param snd sound name.
	 */
	public void setBeep(String snd)
	{
	  _beep=snd;
	}

  /**
   * Play the sound associated with the new query.
   */
	public void onQuery()
	{
    if(_query!=null) _sound.playSound(_query);
	}

	/**
	 * Play the beep sound.
	 */
	public void beep()
	{
	  if(_beep!=null) _sound.playSound(_beep);
	}
}

