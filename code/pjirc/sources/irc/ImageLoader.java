/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

import java.awt.*;

/**
 * Image loader.
 */
public interface ImageLoader
{
  /**
   * Load the given image.
   * @param source image to load.
   * @return loaded image, or null if unable.
   */
  public Image getImage(String source);
}

