/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

import java.util.*;
import java.awt.*;

class SmileyItem
{
  public String match;
	public Image img;

  public SmileyItem(String match,Image img)
	{
	  this.match=match;
		this.img=img;
	}
}

/**
 * Smiley table.
 */
public class SmileyTable
{
  private Vector _table;

	/**
	 * Create a new, empty, smiley table.
	 */
	public SmileyTable()
	{
	  _table=new Vector();
	}

	/**
	 * Add a smiley in the table.
	 * @param match the macthing text.
	 * @param img image of the smiley.
	 */
	public void addSmiley(String match,Image img)
	{
	  if(img!=null) _table.insertElementAt(new SmileyItem(match,img),_table.size());
	}

	/**
	 * Get the smileys count.
	 * @return the amount of smileys in the table.
	 */
	public int getSize()
	{
	  return _table.size();
	}

	/**
	 * Get the i'th match in the smiley table.
	 * @param index table index.
	 * @return i'th smiley match.
	 */
	public String getMatch(int index)
	{
	  SmileyItem item=(SmileyItem)_table.elementAt(index);
		return item.match;
	}

	/**
	 * Get the i'th image in the smiley table.
	 * @param index table index.
	 * @return i'th image.
	 */
	public Image getImage(int index)
	{
	  if(index<0) return null;
		if(index>=getSize()) return null;
	  SmileyItem item=(SmileyItem)_table.elementAt(index);
		return item.img;
	}
}

