/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * Channel listener.
 */
public interface ChannelListener extends SourceListener
{
  /**
   * The channel has changed all its nick list.
   * @param nicks new nicks.
   * @param modes new modes.
   */
  public void nickSet(String nicks[],String modes[]);

  /**
   * A new nick has joined.
   * @param nick the nick who joined.
   * @param mode nick mode.
   */
  public void nickJoin(String nick,String mode);

  /**
   * A nick has quit.
   * @param nick the nick who quit.
   * @param reason reason.
   */
  public void nickQuit(String nick,String reason);

  /**
   * A nick has part.
   * @param nick the nick who part.
   * @param reason reason.
   */
  public void nickPart(String nick,String reason);

  /**
   * A nick has been kicked.
   * @param nick the nick who has been kicked.
   * @param by the nick who kicked.
   * @param reason kick reason.
   */
  public void nickKick(String nick,String by,String reason);

  /**
   * The topic has been changed.
   * @param topic new topic.
   * @param by user who changed topic.
   */
  public void topicChanged(String topic,String by);

  /**
   * Channel mode applied.
   * @param mode applied mode.
   * @param from user who applied mode.
   */
  public void modeApply(String mode,String from);

  /**
   * Nick mode applied.
   * @param nick user on wich mode applied.
   * @param mode applied mode.
   * @param from user who applied mode.
   */
  public void nickModeApply(String nick,String mode,String from);

  /**
   * Nick changed.
   * @param oldNick old nick.
   * @param newNick new nick.
   */
  public void nickChanged(String oldNick,String newNick);

  /**
   * Whois bufferised information has been updated.
   * @param nick nick on wich new whois information is available.
   * @param whois whois string for nick.
   */
  public void nickWhoisUpdated(String nick,String whois);
}

