/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

import java.io.*;

/**
 * File handling.
 */
public interface FileHandler
{
  /**
   * Get an input stream from the given filename.
   * @param fileName file name to get inputstream from.
   * @return inputstream from the file, or null if unable.
   */
  public InputStream getInputStream(String fileName);
}

