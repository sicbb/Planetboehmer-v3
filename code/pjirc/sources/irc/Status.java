/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * The status.
 */
public class Status extends IRCSource implements ReplyServerListener
{
  private ListenerGroup _listeners;

  /**
   * Create a new Status.
   * @param config global irc configuration.
   * @param s server.
   */
  public Status(IRCConfiguration config,IRCServer s)
  {
    super(config,s);
    _listeners=new ListenerGroup();
  }

  public String getName()
  {
	  if(_ircConfiguration.getInfo())
      return getText(TextProvider.SOURCE_INFO);
		else
      return getText(TextProvider.SOURCE_STATUS);
  }

  public boolean talkable()
  {
    return false;
  }

  public void leave()
  {
    sendString("/quit");
  }

  /**
   * Get this status nick.
   * @return status nick.
   */
  public String getNick()
  {
    return _server.getNick();
  }

  /**
   * Get this status mode.
   * @return status mode.
   */
  public String getMode()
  {
    return getIRCServer().getMode();
  }

  /**
   * Add listener.
   * @param lis listener to add.
   */
  public void addStatusListener(StatusListener lis)
  {
    _listeners.addListener(lis);
  }

  /**
   * Remove listener.
   * @param lis listener to remove.
   */
  public void removeStatusListener(StatusListener lis)
  {
    _listeners.removeListener(lis);
  }

  /**
   * Notify this status that the nick has changed.
   * @param nick new nick.
   */
  public void nickChanged(String nick)
  {
    _listeners.sendEvent("nickChanged",nick);
  }

  /**
   * Notify this status that the mode has changed.
   * @param mode new mode.
   */
  public void modeChanged(String mode)
  {
    _listeners.sendEvent("modeChanged",mode);
  }

  public void replyReceived(String prefix,String id,String params[])
	{
	  if(_ircConfiguration.getInfo())
		{
		  int i=new Integer(id).intValue();
			if((i>=300) && (i!=372)) return;
		}
		if(id.equals("322")) return;
    String toSend="";
    for(int i=1;i<params.length;i++) toSend+=" "+params[i];
    toSend=toSend.substring(1);
    report(toSend);
	}
}

