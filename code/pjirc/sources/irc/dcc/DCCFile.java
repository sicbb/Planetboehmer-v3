/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.dcc;

import irc.dcc.prv.*;
import java.io.*;
import java.util.*;
import irc.security.*;
import irc.*;

/**
 * The DCCFile, used for file transferts.
 */
public class DCCFile
{
  private OutputStream _os;
  private InputStream _is;
  private File _file;
  private ListenerGroup _listeners;
  private boolean _down=false;
  private int _size;
  private int _count;
  private DCCFileHandler _handler;

  /**
   * Create a new DCCFile.
   * @param f the file to transfert.
   * @param handler the file handler.
   */
  public DCCFile(File f,DCCFileHandler handler)
  {
    _listeners=new ListenerGroup();
    _handler=handler;
    _count=0;
    _file=f;
  }

  /**
   * Add a listener.
   * @param lis listener to add.
   */
  public void addDCCFileListener(DCCFileListener lis)
  {
    _listeners.addListener(lis);
  }

  /**
   * Remove a listener.
   * @param lis listener to remove.
   */
  public void removeDCCFileListener(DCCFileListener lis)
  {
    _listeners.removeListener(lis);
  }

  /*private void triggerTransmitted()
  {
    if((_count&4095)!=0) return;
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      DCCFileListener lis=(DCCFileListener)e.nextElement();
      lis.transmitted(new Integer(_count));
    }
  }

  private void triggerFinished()
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      DCCFileListener lis=(DCCFileListener)e.nextElement();
      lis.finished();
    }
  }

  private void triggerFailed()
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      DCCFileListener lis=(DCCFileListener)e.nextElement();
      lis.failed();
    }
  }*/

  /**
   * Prepare to send the file.
   */
  public void prepareSend()
  {
    try
    {
      _size=SecurityProvider.getSecurityProvider().getFileSize(_file);
   //   _size=(int)_file.length();
      _is=new BufferedInputStream(SecurityProvider.getSecurityProvider().getFileInputStream(_file));
      _down=false;
    }
    catch(Exception e)
    {
    }
  }

  /**
   * Read the next byte while sending.
   * @return next byte, or -1 if end of file reached.
   */
  public byte readByte()
  {
    try
    {
      _count++;
      //triggerTransmitted();
      _listeners.sendEvent("transmitted",new Integer(_count));
      return (byte)_is.read();
    }
    catch(Exception e)
    {
      return -1;
    }
  }

  /**
   * Get the file size.
   * @return the file size, in byte.
   */
  public int getSize()
  {
    return _size;
  }

  /**
   * Return true if the transfert is an upload transfert.
   * @return true if uplading, false otherwise.
   */
  public boolean isUploading()
  {
    return !isDownloading();
  }

  /**
   * Return true if the transfert is a download transfert.
   * @return true if downloading, false otherwise.
   */
  public boolean isDownloading()
  {
    return _down;
  }

  /**
   * Notify this file the sending is terminated.
   */
  public void fileSent()
  {
    try
    {
      //triggerFinished();
      _listeners.sendEvent("finished");
      _is.close();
    }
    catch(Exception e)
    {
    }
  }

  /**
   * Notify this file the sending has failed.
   */
  public void fileSentFailed()
  {
    try
    {
      //triggerFailed();
      _listeners.sendEvent("failed");
      _is.close();
    }
    catch(Exception e)
    {
    }
  }

  /**
   * Prepare to receive file.
   * @param size the file size.
   */
  public void prepareReceive(int size)
  {
    _down=true;
    _size=size;
    try
    {
      _os=new BufferedOutputStream(SecurityProvider.getSecurityProvider().getFileOutputStream(_file));
    }
    catch(Exception e)
    {
      _os=null;
    }
  }

  /**
   * Write a new byte in the destination file.
   * @param b the new received byte.
   */
  public void byteReceived(byte b)
  {
    try
    {
      _count++;
      //triggerTransmitted();
      _listeners.sendEvent("transmitted",new Integer(_count));
      _os.write(b);
    }
    catch(Exception e)
    {
    }
  }

  /**
   * Notify this dcc file the file reception is terminated.
   */
  public void fileReceived()
  {
    try
    {
      _listeners.sendEvent("finished");
      //triggerFinished();
      _os.close();
    }
    catch(Exception e)
    {
    }

  }

  /**
   * Notify this dcc file the file reception has failed.
   */
  public void fileReceiveFailed()
  {
    try
    {
      //triggerFailed();
      _listeners.sendEvent("failed");
      _os.close();
    }
    catch(Exception e)
    {
    }
  }

  /**
   * Get the file name.
   */
  public String getName()
  {
    return _file.getName();
  }

  /**
   * Request this transfert to be closed.
   */
  public void leave()
  {
    _handler.close();
  }

  /**
   * Prepare the file sending in passive mode.
   * @return the string to be sent to peer in order to begin the file transfert.
   */
	public String send()
	{
	  return _handler.send(this);
	}

  /**
   * Prepare the file reception in active mode.
   * @param ip ip to contact.
   * @param port port to contact.
   * @param size file size to transfert.
   */
	public void receive(String ip,String port,String size)
	{
	  _handler.receive(this,ip,port,size);
	}
}

