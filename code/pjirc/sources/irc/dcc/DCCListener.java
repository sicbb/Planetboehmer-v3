/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.dcc;

import java.io.*;

/**
 * DCCListener, provided to catch dcc events triggered by the CTCP filter.
 */
public interface DCCListener
{
  /**
   * A new DCC reception is requested.
   * @param nick the source nick.
   * @param fileName the file to transfert.
   * @param size the file size.
   * @return the local destination file.
   */
  public File DCCFileRequest(String nick,String fileName,int size);

  /**
   * A DCCFile has been created.
   * @param file the DCCFile.
   */
  public void DCCFileCreated(DCCFile file);

  /**
   * A DCCFile has been removed.
   * @param file the DCCFile.
   */
  public void DCCFileRemoved(DCCFile file);

  /**
   * A new DCCChat is requested.
   * @param nick the requesting nick.
   * @return true if the request is accepted, false otherwise.
   */
  public boolean DCCChatRequest(String nick);

  /**
   * A new DCCChat has been created.
   * @param chat the DCCChat.
   * @param local true if the chat has been created following a local user action, and
   * not following a remote request.
   */
  public void DCCChatCreated(DCCChat chat,Boolean local);

  /**
   * A DCCChat has been removed.
   * @param chat the DCCChat.
   */
  public void DCCChatRemoved(DCCChat chat);
}

