/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.dcc;

/**
 * The DCC file transfert listener.
 */
public interface DCCFileListener
{
  /**
   * Notify a transfert progression.
   * @param count amount of byte that are already transferred.
   */
  public void transmitted(Integer count);

  /**
   * The transfert is finished sucessfully.
   */
  public void finished();

  /**
   * The transfert failed.
   */
  public void failed();
}

