/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.dcc;

/**
 * A dcc file closing listener.
 */
public interface DCCFileClosingListener
{
  /**
   * The given file transfert has been closed.
   * @param file the closed file transfert.
   */
  public void fileClosing(DCCFile file);
}

