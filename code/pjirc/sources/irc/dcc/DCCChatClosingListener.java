/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.dcc;

/**
 * The dcc chat closing listener.
 */
public interface DCCChatClosingListener
{
  /**
   * The dcc chat is closing.
   * @param c the dcc chat.
   */
  public void chatClosing(DCCChat c);
}

