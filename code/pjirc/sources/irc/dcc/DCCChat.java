/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.dcc;

import irc.*;
import irc.dcc.prv.*;

/**
 * The DCCChat source.
 */
public class DCCChat extends DCCSource
{
  private String _nick;

  /**
   * Create a new DCCChat.
   * @param config global configuration.
   * @param server dcc chat server.
   * @param nick remove nick.
   */
  public DCCChat(IRCConfiguration config,DCCChatServer s,String nick)
  {
    super(config,s);
    _nick=nick;
  }

  public String getName()
  {
    return _nick;
  }
  public boolean talkable()
  {
    return true;
  }

  public void leave()
  {
    getDCCChatServer().close();
  }

	/**
	 * Open passive communication.
	 * @return string to send to peer in order to initiate the communication.
	 */
	public String openPassive()
	{
	  return ((DCCChatServer)_server).openPassive(this);
	}

	/**
	 * Open active communication.
	 * @param ip ip to contact.
	 * @param port port to contact.
	 */
	public void openActive(String ip,String port)
	{
	  ((DCCChatServer)_server).openActive(this,ip,port);
	}
}

