/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.dcc;

import irc.*;
import irc.dcc.*;
import irc.dcc.prv.*;

/**
 * The DCC Source.
 */
public abstract class DCCSource extends Source
{
  /**
   * Create a new DCCSource.
   * @param config global irc configuration.
   * @param s dcc server.
   */
  public DCCSource(IRCConfiguration config,DCCChatServer s)
  {
    super(config,s);
  }

  /**
   * Get the DCCChatServer.
   * @return the DCCChatServer associated with this source.
   */
  public DCCChatServer getDCCChatServer()
  {
    return (DCCChatServer)_server;
  }


}

