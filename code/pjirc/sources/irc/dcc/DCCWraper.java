/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.dcc;

import irc.dcc.prv.*;
import irc.*;
import java.io.*;

/**
 * The DCC Wraper.
 */
public class DCCWraper extends IRCObject
{
  /**
   * Create a new DCCWraper.
   * @param config the global irc configuration.
   */
  public DCCWraper(IRCConfiguration config)
	{
	  super(config);
	}

  /**
   * Create a new DCCChat.
   * @param myNick the local nick.
   * @param remoteNick the remote nick.
   * @param lis the closing listener.
   * @return a new DCCChat.
   */
  public DCCChat createDCCChat(String myNick,String remoteNick,DCCChatClosingListener lis)
	{
    DCCChatServer cserver=new DCCChatServer(_ircConfiguration,myNick,remoteNick,lis);
    return new DCCChat(_ircConfiguration,cserver,remoteNick);
	}

	/**
	 * Create a new DCCFile.
	 * @param f the associated local file.
	 * @param remoteNick the remote nick.
	 * @param lis the closing listener.
	 * @return a new DCCFile.
	 */
	public DCCFile createDCCFile(File f,String remoteNick,DCCFileClosingListener lis)
	{
    DCCFileHandler fhandler=new DCCFileHandler(_ircConfiguration,remoteNick,lis);
    return new DCCFile(f,fhandler);
	}

}

