/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * Status listener.
 */
public interface StatusListener extends SourceListener
{
  /**
   * Our nick has changed.
   * @param nick new nick.
   */
  public void nickChanged(String nick);

  /**
   * Our mode has changed.
   * @param mode new mode.
   */
  public void modeChanged(String mode);
}

