/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * Status interpretor.
 */
public class StatusInterpretor extends IRCInterpretor
{
  /**
   * Create a new status interpretor.
   * @param config irc configuration.
   * @param filter ctcp filter to use.
   */
  public StatusInterpretor(IRCConfiguration config,CTCPFilter filter)
  {
    super(config,filter);
  }

  protected void handleCommand(Source source,String cmd,String[] parts,String[] cumul)
  {
    Server server=source.getServer();
    if(cmd.equals("leave"))
    {
      source.report(getText(TextProvider.INTERPRETOR_BAD_CONTEXT,"/leave"));
    }
    else
    {
      super.handleCommand(source,cmd,parts,cumul);
    }
  }
}

