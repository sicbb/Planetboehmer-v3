/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * Channel list listener.
 */
public interface ChanListListener
{
  /**
   * A new channel has been added in the channel list.
   * @param item the newly added item.
   */
  public void channelAdded(ChannelInfo item);

  /**
   * A new listing has began.
   */
  public void channelBegin();

  /**
   * The current listing is terminated.
   */
  public void channelEnd();
}

