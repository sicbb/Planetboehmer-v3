/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * Url handler.
 */
public interface URLHandler
{
  /**
   * Print the given url in the status box of the application.
   * @param url url to print.
   */
  public void stateURL(String url);

  /**
   * Open the given url.
   * @param url url to open.
   */
  public void openURL(String url);
}

