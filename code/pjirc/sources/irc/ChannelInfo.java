/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * Channel information.
 */
public class ChannelInfo
{
  /**
   * Create a new ChannelInfo.
   * @param n channel name.
   * @param t channel topic.
   * @param c user count.
   */
  public ChannelInfo(String n,String t,int c)
  {
    name=n;
    topic=t;
    userCount=c;
  }

  /**
   * Channel name.
   */
  public String name;
  /**
   * Channel topic.
   */
  public String topic;
  /**
   * User count.
   */
  public int userCount;
}

