/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

import java.util.*;

/**
 * A channel list handler.
 */
public class ChanList extends IRCObject
{
  private ListenerGroup _listeners;
  private Vector _channels;
  private IRCServer _server;
  private String _name;

  /**
   * Create a new ChanList.
   * @param config the global configuration.
   * @param server the IRCServer from where to retreive channel list.
   * @param name the chanlist name.
   */
  public ChanList(IRCConfiguration config,IRCServer server,String name)
  {
    super(config);
    _name=name;
    _server=server;
    _listeners=new ListenerGroup();
    _channels=new Vector();
  }

  /**
   * Get the chanlist name.
   */
  public String getName()
  {
    return _name;
  }

  /**
   * Get the channels.
   * @return array of all channels.
   */
  public ChannelInfo[] getChannels()
  {
    ChannelInfo[] ans=new ChannelInfo[_channels.size()];
    for(int i=0;i<_channels.size();i++) ans[i]=(ChannelInfo)_channels.elementAt(i);
    return ans;
  }

  /**
   * Add a channel in the channel list.
   * @param nfo new channel to add.
   */
  public void addChannel(ChannelInfo nfo)
  {
    _channels.insertElementAt(nfo,_channels.size());
    //triggerChanListListeners(nfo);
    _listeners.sendEvent("channelAdded",nfo);
  }

  /**
   * Begin a new channel listing. The channel list is cleared.
   */
  public void begin()
  {
    _channels=new Vector();
    //triggerChanListBeginListeners();
    _listeners.sendEvent("channelBegin");
  }

  /**
   * End the channel listing.
   */
  public void end()
  {
    //triggerChanListEndListeners();
    _listeners.sendEvent("channelEnd");
  }

  /**
   * Get the IRCServer.
   * @return the irc server.
   */
  public IRCServer getServer()
  {
    return _server;
  }

  /**
   * Add a ChanListListener.
   * @param lis listener to add.
   */
  public void addChanListListener(ChanListListener lis)
  {
    _listeners.addListener(lis);
  }

  /**
   * Remove a chanListListener.
   * @param lis listener to remove.
   */
  public void removeChanListListeners(ChanListListener lis)
  {
    _listeners.removeListener(lis);
  }

  /*private void triggerChanListListeners(ChannelInfo nfo)
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      ChanListListener lis=(ChanListListener)e.nextElement();
      lis.channelAdded(nfo);
    }
  }

  private void triggerChanListBeginListeners()
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      ChanListListener lis=(ChanListListener)e.nextElement();
      lis.channelBegin();
    }
  }

  private void triggerChanListEndListeners()
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      ChanListListener lis=(ChanListListener)e.nextElement();
      lis.channelEnd();
    }
  }*/

  /**
   * Request the destruction of this chanlist.
   */
  public void leave()
  {
    _server.leaveChanList(_name);
  }
}

