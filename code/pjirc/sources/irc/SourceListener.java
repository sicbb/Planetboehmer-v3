/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * The Source Listener.
 */
public interface SourceListener
{
  /**
   * A new message has been received.
   * @param source source nick.
   * @param msg message.
   */
  public void messageReceived(String source,String msg);

  /**
   * A new report has been received.
   * @param message report.
   */
  public void reportReceived(String message);

  /**
   * A new notice has been received.
   * @param nick source nick.
   * @param message notice.
   */
  public void noticeReceived(String nick,String message);

  /**
   * A new action has been received.
   * @param nick source nick.
   * @param msg message.
   */
  public void action(String nick,String msg);

  /**
   * A new clear request has been received.
   */
  public void clear();

  /**
   * This source has been activated.
   */
  public void activate();
}

