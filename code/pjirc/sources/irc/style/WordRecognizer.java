/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.style;

/**
 * The word recognizer.
 */
public interface WordRecognizer
{
  /**
   * Try to recognize a word.
   * @param word word to analyse.
   * @return true if this word matches.
   */
  public boolean recognize(String word);

  /**
   * Get this recognizer type.
   * @return recognizer type name.
   */
  public String getType();
}

