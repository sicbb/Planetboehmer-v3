/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.style;

import java.awt.*;
import java.awt.geom.*;

/**
 * A drawing result.
 */
public class DrawResult
{
  /**
   * The result rectangle.
   */
  public StyledRectangle rectangle;

  /**
   * An array of words in the rectangle.
   */
  public DrawResultItem[] items;

  public String toString()
  {
    String res="";
    for(int i=0;i<items.length;i++) res+=items[i].originalstrippedword;
    return res;
  }
}

