/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.style;

import java.awt.event.*;

/**
 * StyledListListener.
 */
public interface StyledListListener
{
  /**
   * A mouse event has occured on a channel.
   * @param list the source list.
   * @param channel the channel name.
   * @param e the mouse event associated with this event.
   */
  public void channelEvent(StyledList list,String channel,MouseEvent e);

  /**
   * An url event has occured on a channel.
   * @param list the source list.
   * @param url the url name.
   * @param e the mouse event associated with this event.
   */
  public void URLEvent(StyledList list,String url,MouseEvent e);

  /**
   * A nick event has occured on a channel.
   * @param list the source list.
   * @param nick the nick name.
   * @param e the mouse event associated with this event.
   */
  public void nickEvent(StyledList list,String nick,MouseEvent e);

  /**
   * A copy event has occured on a channel.
   * @param list the source list.
   * @param text the copied text.
   * @param e the mouse event associated with this event.
   */
  public void copyEvent(StyledList list,String text,MouseEvent e);

  /**
   * The logical width or height of the list has changed.
   * @param list the source list.
   */
  public void virtualSizeChanged(StyledList list);
}

