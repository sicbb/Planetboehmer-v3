/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.style;

/**
 * A word catcher.
 */
public interface WordCatcher
{
  /**
   * Get the type of this word, or null if not found.
   * @param word word to analyse.
   * @return word type, or null if not found.
   */
  public String getType(String word);
}

