/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.style;

/**
 * A rectangle.
 */
class StyledRectangle
{
  /**
   * X position.
   */
  public int x;

  /**
   * Y position.
   */
  public int y;

  /**
   * Width.
   */
  public int width;

  /**
   * Height.
   */
  public int height;

  /**
   * Create a new StyledRectangle.
   * @param x x position.
   * @param y y position.
   * @param w width.
   * @param y height.
   */
  public StyledRectangle(int x,int y,int w,int h)
  {
    this.x=x;
    this.y=y;
    width=w;
    height=h;
  }

  public boolean equals(Object o)
  {
    if(!(o instanceof StyledRectangle)) return false;
    StyledRectangle r=(StyledRectangle)o;
    return (r.x==x) && (r.y==y) && (r.width==width) && (r.height==height);
  }

  public int hashCode()
  {
    return x+y+width+height;
  }

  /**
   * Test wether the given point is inside the rectangle or not.
   * @param px x coordinate of point to test.
   * @param py y coordinate of point to test.
   * @return true if the point is inside the rectangle, false otherwise.
   */
  public boolean contains(int px,int py)
  {
    return (px>=x) && (py>=y) && (px<x+width) && (py<y+height);
  }

  private boolean noEmpty(int v1,int lv1,int v2,int lv2)
  {
    if(v1<v2) return v1+lv1>v2;
    return v2+lv2>v1;
  }

  /**
   * Test wether the given rectangle is partially or fully inside the rectangle or
   * not.
   * @param r rectangle to test.
   * @return true if r is partially or fully inside the rectangle, false otherwise.
   */
  public boolean hit(StyledRectangle r)
  {
    if(!noEmpty(r.x,r.width,x,width)) return false;
    return noEmpty(r.y,r.height,y,height);
  }
}

