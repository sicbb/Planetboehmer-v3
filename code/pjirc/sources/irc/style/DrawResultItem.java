/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.style;

import java.awt.*;
import java.awt.geom.*;

/**
 * A recognized item.
 */
public class DrawResultItem
{
  /**
   * The item rectangle on display, relative to its parent.
   */
  public StyledRectangle rectangle;
  /**
   * The item content. This content is the trimmed version of the stripped content.
   * Characters like parenthesis and braces are also trimmed. For instance, "(text)"
   * gives "text" in the itemized version. Item is used for word recognition.
   */
  public String item;
  /**
   * The original word content.
   */
  public String originalword;
  /**
   * The original word content, but stripped from all special codes.
   */
  public String originalstrippedword;


  public boolean equals(Object o)
  {
    if(!(o instanceof DrawResultItem)) return false;
    DrawResultItem i=(DrawResultItem)o;
    return i.rectangle.equals(rectangle) && i.originalword.equals(originalword);
  }

  public int hashCode()
  {
    return rectangle.hashCode()+originalword.hashCode();
  }
}

