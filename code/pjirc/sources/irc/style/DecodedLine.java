/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.style;

/**
 * A decoded line, ready for drawing.
 */
public abstract class DecodedLine
{
  /**
   * Original string.
   */
  public String original;
  /**
   * Decoded string.
   */
  public String decoded;
  /**
   * Decoded string, but stripped of all color codes.
   */
  public String decoded_stripped;
}
