/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.style;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import irc.*;

class ResultPair
{
  public int line;
  public DrawResult result;
}

/**
 * A styled list is a panel designed for displaying lines of styled text, using
 * special color and style codes, and smileys.
 */
public class StyledList extends Panel implements MouseListener,MouseMotionListener
{
  private Vector _list;
  private boolean _wrap;
  private int _last;
  private int _first;
  private int _left;
  private int _width;
  private int _toScrollX;
  private int _toScrollY;
  private FormattedStringDrawer _drawer;
  private Image _buffer;
  private int _bufferWidth;
  private int _bufferHeight;
  private Font _fonts;
  private Hashtable _results;
  private MultipleWordCatcher _catcher;
  private WordListRecognizer _wordListRecognizer;
  private IRCConfiguration _ircConfiguration;

  private int _pressedX;
  private int _pressedY;
  private int _draggedX;
  private int _draggedY;
  private boolean _dragging;
  private DrawResultItem _currentItem;
  private String _copiedString;
  private boolean _fullDraw;

  private ListenerGroup _listeners;

  private ResultPair[] _addedResults;
  private int _addedCount;

  private int _direction;

  private final static int BOTTOM=FormattedStringDrawer.BOTTOM;
  private final static int TOP=FormattedStringDrawer.TOP;

  private final static boolean _doubleBuffer=true; //NON-BUFFERED DISPLAY NOT YET FUNCTIONAL

  /**
   * Create a new StyledList with automatic text wraping.
   * @param config global irc configuration.
   * @param fnt font to be used.
   * @param context color context.
   */
  public StyledList(IRCConfiguration config,Font fnt,ColorContext context)
  {
    this(config,fnt,true,context);
  }

  /**
   * Create a new StyledList.
   * @param config global irc configuration.
   * @param fnt font to be used.
   * @param wrap true if wrapping must occur, false otherwise.
   * @param context color context.
   */
  public StyledList(IRCConfiguration config,Font fnt,boolean wrap,ColorContext context)
  {
    super();
    _fullDraw=false;
    _addedResults=new ResultPair[64];
    for(int i=0;i<_addedResults.length;i++) _addedResults[i]=new ResultPair();
    _addedCount=0;
    _direction=BOTTOM;
    _ircConfiguration=config;
    _copiedString="";
    _dragging=false;
    _currentItem=null;
    _toScrollX=0;
    _toScrollY=0;
    _left=0;
    _fonts=fnt;
    _wrap=wrap;
    _buffer=null;
    _drawer=new FormattedStringDrawer(_fonts,_ircConfiguration,context);
    _drawer.setDirection(_direction);
    _catcher=new MultipleWordCatcher();
    _wordListRecognizer=new WordListRecognizer();
    _catcher.addRecognizer(new ChannelRecognizer());
    _catcher.addRecognizer(new URLRecognizer());
    _catcher.addRecognizer(_wordListRecognizer);
    _results=new Hashtable();
    _listeners=new ListenerGroup();
    addMouseListener(this);
    addMouseMotionListener(this);
    clear();
  }

  private void expandResult()
  {
    ResultPair[] n=new ResultPair[_addedResults.length*2];
    System.arraycopy(_addedResults,0,n,0,_addedResults.length);
    for(int i=_addedResults.length;i<n.length;i++) n[i]=new ResultPair();
    _addedResults=n;
  }

	/**
	 * Set the nick list for recognition.
	 * @param list the nick list.
	 */
  public synchronized void setNickList(String[] list)
  {
    _wordListRecognizer.setList(list);
  }

  /**
   * Add a listener.
   * @param lis the new listener.
   */
  public synchronized void addStyledListListener(StyledListListener lis)
  {
    _listeners.addListener(lis);
  }

  /**
   * Remove a listener.
   * @param lis the listener to remove.
   */
  public synchronized void removeStyledListListener(StyledListListener lis)
  {
    _listeners.removeListener(lis);
  }

  /**
   * Set the left offset for this list rendering.
   * @param left the left offset, in pixel.
   */
  public synchronized void setLeft(int left)
  {
	  int w=getSize().width;
    int oldLeft=_left;
    _left=left;
    if(_left<0) _left=0;
    if(_left>=getLogicalWidth()) _left=getLogicalWidth()-1;
    if(_left!=oldLeft)
    {
      addToScroll(_left-oldLeft,0);
      repaint();
    }
  }

  /**
   * Get the left offset.
   * @return the left offset, in pixel.
   */
  public int getLeft()
  {
    return _left;
  }

  /**
   * Set the first line to be displayed.
   * @param first the first line to be displayed.
   */
	public synchronized void setFirst(int first)
	{
	 if(_direction!=TOP) _fullDraw=true;
    _direction=TOP;
    _drawer.setDirection(TOP);
    int oldFirst=_first;
    _first=first;
    if(_first<0) _last=0;
    if(_first>=_list.size()) _last=_list.size()-1;
    if(_first!=oldFirst)
    {
      addToScroll(0,_first-oldFirst);
      repaint();
    }
	}

  /**
   * Set the last line to be displayed.
   * @param last last line to be displayed.
   */
  public synchronized void setLast(int last)
  {
    if(_direction!=BOTTOM) _fullDraw=true;
    _direction=BOTTOM;
    _drawer.setDirection(BOTTOM);
    int oldLast=_last;
    _last=last;
    if(_last<0) _last=0;
    if(_last>=_list.size()) _last=_list.size()-1;
    if(_last!=oldLast)
    {
      addToScroll(0,_last-oldLast);
      repaint();
    }
  }

  /**
   * Get the logical width of this list.
   * @return the logical width, in pixel.
   */
	public int getLogicalWidth()
	{
	  return _width;
	}

  /**
   * Get the last displayed line.
   * @return the last displayed line.
   */
  public int getLast()
  {
    return _last;
  }

  /**
   * Get the number of line in this list.
   * @return the line count.
   */
  public synchronized int getLineCount()
  {
    return _list.size();
  }

  /**
   * Add a line at the end of this list.
   * @param line new line to add.
   */
  public synchronized void addLine(String line)
  {
    DecodedLine dline=_drawer.decodeLine(line);
    _list.insertElementAt(dline,_list.size());

    if(_direction==BOTTOM)
    {
      if(_last==_list.size()-2) setLast(_last+1);
    }
    else if(_direction==TOP)
    {
      _fullDraw=true;
      repaint();
    }
  }

  public synchronized void addLines(String[] lines)
  {
    boolean willScroll=(_list.size()-1==_last);

    for(int i=0;i<lines.length;i++) _list.insertElementAt(_drawer.decodeLine(lines[i]),_list.size());

    if(_direction==BOTTOM)
    {
      if(willScroll) setLast(_list.size()-1);
    }
    else if(_direction==TOP)
    {
      _fullDraw=true;
      repaint();
    }
  }

  private void reinit()
  {
    if(_buffer!=null) _buffer.flush();
    _buffer=null;
    _results=new Hashtable();
  }

  /**
   * Dispose any off-screen ressources used by the list. This method won't put the
   * list in a non-drawable state, but next screen refresh might me slower.
   */
  public synchronized void dispose()
  {
    reinit();
  }

  /**
   * Clear all the lines in this list.
   */
  public synchronized void clear()
  {
    _list=new Vector();
    _last=_list.size()-1;
    _first=0;
    setLeft(0);
		_width=getSize().width;
		_fullDraw=true;
    repaint();
  }

  private void drawPart(Graphics g,int x,int y,int w,int h,boolean analyse)
  {
    //System.out.println("draw part "+x+","+y+","+w+","+h);
    if(y<0)
    {
      h+=y;
      y=0;
    }
    g.setColor(_drawer.getColor(0));
    g.fillRect(x,y,w,h);
    if(_direction==BOTTOM)
    {
      int first=_last;
      int posY=getSize().height;
      while((posY>y+h) && (first>=0)) posY-=getHeight(first--,g);
      if(first!=_last) posY+=getHeight(++first,g);
      draw(g,0,first,posY,y,x,x+w-1,analyse);
    }
    else if(_direction==TOP)
    {
      int first=_first;
      int posY=0;
      while((posY<y) && (first<_list.size())) posY+=getHeight(first++,g);
      if(first!=_first) posY-=getHeight(--first,g);
      draw(g,first,_list.size()-1,posY,y+h,x,x+w-1,analyse);
    }
  }

  public synchronized void paint(Graphics g)
  {
    if(_doubleBuffer || (_toScrollX!=0) || (_toScrollY!=0))
    {
      if((_toScrollX!=0) || (_toScrollY!=0)) _fullDraw=true;
      update(g);
      return;
    }
    int x=0;
    int y=0;
    int w=getSize().width;
    int h=getSize().height;
    Rectangle cl=g.getClipBounds();
    if(cl!=null)
    {
      x=cl.x;
      y=cl.y;
      w=cl.width;
      h=cl.height;
    }
    drawPart(g,x,y,w,h,false);
  }

  private int getHeight(Graphics g,int a,int b)
  {
    if(b<a)
    {
      int tmp=a;
      a=b;
      b=tmp;
    }
    int res=0;
    for(int i=a;i<=b;i++) res+=getHeight(i,g);
    return res;
  }

  private void draw(Graphics g,int from,int to,int y,int crossy,int debx,int finx,boolean analyse)
  {
    int w=getSize().width;
    int h=getSize().height;
    int wrapPos=w;
    _addedCount=0;

    DrawResult res=new DrawResult();

    if(_direction==BOTTOM)
    {
      int index=to;
      while((index>=from) && (y>crossy))
      {
        DecodedLine str=(DecodedLine)_list.elementAt(index);
        _drawer.draw(str,g,-_left,y,wrapPos,debx,finx,analyse,_wrap,res);
        StyledRectangle rect=res.rectangle;
        if(rect.width>_width)
        {
          _width=rect.width;
          _listeners.sendEvent("virtualSizeChanged",this);
        }
        if(analyse)
        {
          ResultPair p=_addedResults[_addedCount++];
          if(_addedCount==_addedResults.length) expandResult();
          p.line=index;
          p.result=res;
          res=new DrawResult();
        }
        y-=rect.height;
        index--;
      }
    }
    else
    {
      int index=from;
      while((index<=to) && (y<crossy))
      {
        DecodedLine str=(DecodedLine)_list.elementAt(index);
        _drawer.draw(str,g,-_left,y,wrapPos,debx,finx,analyse,_wrap,res);
        StyledRectangle rect=res.rectangle;
        if(rect.width>_width)
        {
          _width=rect.width;
          _listeners.sendEvent("virtualSizeChanged",this);
        }
        if(analyse)
        {
          ResultPair p=_addedResults[_addedCount++];
          if(_addedCount==_addedResults.length) expandResult();
          p.line=index;
          p.result=res;
          res=new DrawResult();
        }
        y+=rect.height;
        index++;
      }

    }
  }

  private void addToScroll(int vx,int vy)
  {
    _toScrollX+=vx;
    _toScrollY+=vy;
  }

  private int getScrollX()
  {
    if(_dragging) return 0;
    int res=_toScrollX;
    _toScrollX=0;
    return res;
  }

  private int getScrollY()
  {
    if(_dragging) return 0;
    int res=_toScrollY;
    _toScrollY=0;
    return res;
  }

  private void scrollDrawItems(int dx,int dy)
  {
    int h=getSize().height;
    Enumeration e=_results.keys();
    while(e.hasMoreElements())
    {
      Integer key=(Integer)e.nextElement();
      DrawResult res=(DrawResult)_results.get(key);
      res.rectangle.x+=dx;
      res.rectangle.y+=dy;
      if((res.rectangle.y+res.rectangle.height<0) || (res.rectangle.y>=h))
      {
        _results.remove(key);
      }
    }
  }

  private void combineItems()
  {
    for(int i=0;i<_addedCount;i++)
    {
      ResultPair k=_addedResults[i];
      _results.put(new Integer(k.line),k.result);
    }
    _addedCount=0;
  }

  private DrawResultItem findItem(int x,int y)
  {
    Enumeration e=_results.elements();
    while(e.hasMoreElements())
    {
      DrawResult result=(DrawResult)e.nextElement();
      if(result.rectangle.contains(x,y))
      {
        x-=result.rectangle.x;
        y-=result.rectangle.y;
        for(int i=0;i<result.items.length;i++)
        {
          DrawResultItem item=result.items[i];
          if(item.rectangle.contains(x,y)) return item;
        }
      }
    }
    return null;
  }

  private int findLine(int y)
  {
    Enumeration e=_results.keys();
    while(e.hasMoreElements())
    {
      Integer i=(Integer)e.nextElement();

      DrawResult result=(DrawResult)_results.get(i);
      if((result.rectangle.y<=y) && (result.rectangle.y+result.rectangle.height>y))
      {
        return i.intValue();
      }
    }
    return -1;
  }

  private int getHeight(int lineIndex,Graphics g)
  {
    DrawResult r=(DrawResult)_results.get(new Integer(lineIndex));
    if(r!=null) return r.rectangle.height;

    int wrapPos=getSize().width;
    DecodedLine str=(DecodedLine)_list.elementAt(lineIndex);
    return _drawer.getHeight(str,g,-_left,wrapPos,_wrap);
  }

  public synchronized void update(Graphics g)
  {
    int w=getSize().width;
    int h=getSize().height;
    Graphics gra=g;
    if(_doubleBuffer)
    {
      if(_buffer!=null)
      {
        if((_bufferWidth<w) || (_bufferHeight<h)) reinit();
        if((_bufferWidth>w) || (_bufferHeight>h)) if(_direction==BOTTOM) _fullDraw=true;
      }

      if(_buffer==null)
      {
        _buffer=createImage(w,h);
        if(_buffer==null)
        {
          repaint();
          return;
        }
        _bufferWidth=w;
        _bufferHeight=h;
        _fullDraw=true;
      }

      gra=_buffer.getGraphics();
    }

    int scrx=getScrollX();
    int scry=getScrollY();

    if(!_fullDraw)
    {
			if(scrx<0)
			{
			  gra.copyArea(0,0,w+scrx,h,-scrx,0);
        scrollDrawItems(-scrx,0);
        drawPart(gra,0,0,-scrx,h,false);
			}
 			else if(scrx>0)
 			{
        gra.copyArea(scrx,0,w-scrx,h,-scrx,0);
        scrollDrawItems(-scrx,0);
        drawPart(gra,w-scrx,0,scrx,h,false);
      }

      if(scry>0)
      {
        int baseY;
        if(_direction==BOTTOM)
          baseY=getHeight(gra,_last-scry+1,_last);
        else
          baseY=getHeight(gra,_first-scry,_first-1);


        gra.copyArea(0,baseY,w,h-baseY,0,-baseY);
        scrollDrawItems(0,-baseY);
        drawPart(gra,0,h-baseY,w,baseY,true);
        combineItems();
      }
      else if(scry<0)
      {
        int baseY;
        if(_direction==BOTTOM)
          baseY=getHeight(gra,_last+1,_last-scry);
        else
          baseY=getHeight(gra,_first,_first-scry-1);

        gra.copyArea(0,0,w,h-baseY,0,baseY);
        scrollDrawItems(0,baseY);
        drawPart(gra,0,0,w,baseY,true);
        combineItems();
      }
    }
    else
    {
      _results=new Hashtable();
      drawPart(gra,0,0,w,h,true);
      combineItems();
      _fullDraw=false;
    }

    if(_dragging) makeXor(gra);
    if(_doubleBuffer) g.drawImage(_buffer,0,0,this);
    if(_dragging) makeXor(gra);
  }

  private void makeXor(Graphics g)
  {
    String res="";
    int dw=_draggedX-_pressedX;
    int dh=_draggedY-_pressedY;

    int pressedX=_pressedX;
    int pressedY=_pressedY;

    g.setXORMode(Color.white);
    g.setColor(Color.black);
    int i=findLine(pressedY);
    int basei=i;
    DrawResult result=(DrawResult)_results.get(new Integer(i));

    if(result==null)
    {
      _copiedString="";
      return;
    }

    int px,py;

    px=pressedX-result.rectangle.x;
    py=pressedY-result.rectangle.y;

    DrawResultItem item=null;
    int a,b=0;
    for(a=0;a<result.items.length;a++)
    {
      if(result.items[a].rectangle.contains(px,py))
      {
        item=result.items[a];
        b=a;
      }
    }

    if((item==null) || ((px+dw<item.rectangle.x) && (py+dh<item.rectangle.y)) || (py+dh<item.rectangle.y))
    {
      _copiedString="";
      return;
    }
    boolean terminated=false;
    while(!terminated)
    {
      res+=item.originalstrippedword;
      StyledRectangle r=item.rectangle;
      g.fillRect(r.x+result.rectangle.x,r.y+result.rectangle.y,r.width,r.height);

      if(!((i==basei)&&(a==b)) && (item.rectangle.contains(px+dw,py+dh))) break;
      b++;
      if(b>=result.items.length)
      {
        b=0;
        i++;
        result=(DrawResult)_results.get(new Integer(i));
        if(result==null) break;
        px=pressedX-result.rectangle.x;
        py=pressedY-result.rectangle.y;
        res+="\n";
      }
      item=result.items[b];
      if(item.rectangle.y>py+dh) terminated=true;
      if((item.rectangle.x>px+dw) && (item.rectangle.y+item.rectangle.height>py+dh)) terminated=true;
    }

    _copiedString=res;
    g.setPaintMode();
  }

  public synchronized void mouseClicked(MouseEvent e)
  {
    if((e.getModifiers()&e.SHIFT_MASK)!=0)
    {
      String res="";
      for(int i=0;i<_list.size();i++) res+=((DecodedLine)(_list.elementAt(i))).original+"\n";
      _listeners.sendEvent("copyEvent",this,res,e);
    }
  }

  public void mouseEntered(MouseEvent e)
  {
  }

  public void mouseExited(MouseEvent e)
  {
  }

  public synchronized void mousePressed(MouseEvent e)
  {
    _pressedX=e.getX();
    _pressedY=e.getY();
    _draggedX=_pressedX;
    _draggedY=_pressedY;
    _dragging=false;
    _currentItem=null;
    DrawResultItem item=findItem(e.getX(),e.getY());
    if(item!=null)
    {
      String type=_catcher.getType(item.item);
      if(type==null)
      {

      }
      else if(type.equals("channel"))
      {
        _listeners.sendEvent("channelEvent",this,item.item,e);
      }
      else if(type.equals("url"))
      {
        _listeners.sendEvent("URLEvent",this,item.item,e);
      }
      else if(type.equals("wordlist"))
      {
        _listeners.sendEvent("nickEvent",this,item.item,e);
      }
    }
  }

  public synchronized void mouseReleased(MouseEvent e)
  {
    if(_dragging)
    {
      _dragging=false;
      repaint();
      if(_copiedString.length()>0) _listeners.sendEvent("copyEvent",this,_copiedString,e);
    }
  }

  public synchronized void mouseDragged(MouseEvent e)
  {
    _draggedX=e.getX();
    _draggedY=e.getY();
    _dragging=true;
    DrawResultItem item=findItem(e.getX(),e.getY());
    if(item!=_currentItem)
    {
      _currentItem=item;
      repaint();
    }
  }

  private void handCursor()
  {
    if(!getCursor().equals(new Cursor(Cursor.HAND_CURSOR))) setCursor(new Cursor(Cursor.HAND_CURSOR));
  }

  private void defCursor()
  {
    if(!getCursor().equals(new Cursor(Cursor.DEFAULT_CURSOR))) setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }

  public synchronized void mouseMoved(MouseEvent e)
  {
    DrawResultItem item=findItem(e.getX(),e.getY());
    if(item!=_currentItem)
    {
      _currentItem=item;
      if(item!=null)
      {
        String type=_catcher.getType(item.item);
        if(type!=null) handCursor();
        else defCursor();
      }
      else defCursor();
    }
  }
}
