/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * Null URLHandler. This handler always fail to perform request operations.
 */
public class NullURLHandler implements URLHandler
{
  /**
   * Create a new NullURLHandler.
   */
  public NullURLHandler()
  {
  }

  public void stateURL(String url)
  {
  }

  public void openURL(String url)
  {
  }

}

