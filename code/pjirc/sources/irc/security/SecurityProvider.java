/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.security;

import java.io.*;
import java.net.*;
import java.awt.*;

/**
 * The security provider. Provides an interface for accessing protected data or operation.
 */
public class SecurityProvider
{

  private SecuredProvider _provider;

  private static final SecurityProvider _securityProvider=new SecurityProvider();

  /**
   * Internally used.
   */
  protected SecurityProvider()
  {
    try
    {
      Class cl=Class.forName("irc.security.SpecificSecuredProvider");
      _provider=(SecuredProvider)cl.newInstance();
      if(!_provider.tryProvider()) throw new Exception();
    }
    catch(Exception ex)
    {
      _provider=new DefaultSecuredProvider();
    }
  }

  /**
   * Get the used security provider.
   * @return the security provider to use.
   */
  public static final SecurityProvider getSecurityProvider()
  {
    return _securityProvider;
  }

  /**
   * Get the used provider name.
   * @return used provider name.
   */
  public String getProviderName()
  {
    return _provider.getName();
  }

  /**
   * Get a new Socket.
   * @param host server host.
   * @param port server port.
   * @throws UnknownHostException is host is not found.
   * @throws IOException if an error occurs.
   * @return a new Socket.
   */
  public Socket getSocket(String host,int port) throws UnknownHostException,IOException
  {
    return _provider.getSocket(host,port);
  }

  /**
   * Get a new ServerSocket.
   * @param port local port.
   * @throws IOException if an error occurs.
   */
  public ServerSocket getServerSocket(int port) throws IOException
  {
    return _provider.getServerSocket(port);
  }

  /**
   * Get an FileInputStream from a local file.
   * @param file the local file.
   * @throws IOException if an error occurs.
   * @return an FileInputStream from the file.
   */
  public FileInputStream getFileInputStream(File file) throws IOException
  {
    return _provider.getFileInputStream(file);
  }

  /**
   * Get an FileOutputStream to a local file.
   * @param file the local file.
   * @throws IOException if an error occurs.
   * @return an FileOutputStream from the file.
   */
  public FileOutputStream getFileOutputStream(File file) throws IOException
  {
    return _provider.getFileOutputStream(file);
  }

  /**
   * Get the file size.
   * @param file the file to get size.
   * @return the file size, of a negative value if not able.
   */
  public int getFileSize(File file)
  {
    return _provider.getFileSize(file);
  }

  /**
   * Open a load file dialog with the given title and return the user choice.
   * @param title dialog title.
   * @return user choice.
   */
  public File getLoadFile(String title)
  {
    return _provider.getLoadFile(title);
  }

  /**
   * Open a save file dialog with the given title and return the user choice.
   * @param title dialog title.
   * @return user choice.
   */
  public File getSaveFile(String title)
  {
    return _provider.getSaveFile(title);
  }

  /**
   * Get the local host address.
   * @return local host address.
   * @throws UnknownHostException if error occurs.
   */
  public InetAddress getLocalHost() throws UnknownHostException
  {
    return _provider.getLocalHost();
  }

  /**
   * Perform a dns resolve of the given address.
   * @param addr address to resolve.
   * @return resolved address.
   */
  public String resolve(InetAddress addr)
  {
    return _provider.resolve(addr);
  }


}

