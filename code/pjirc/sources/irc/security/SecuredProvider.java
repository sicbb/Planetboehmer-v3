/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.security;


import java.awt.*;
import java.io.*;
import java.net.*;

/**
 * Provide methods for accessing protected operations.
 */
public interface SecuredProvider
{
  /**
   * Get a new Socket.
   * @param host server host.
   * @param port server port.
   * @throws UnknownHostException is host is not found.
   * @throws IOException if an error occurs.
   * @return a new Socket.
   */
  public Socket getSocket(String host,int port) throws UnknownHostException,IOException;

  /**
   * Get a new ServerSocket.
   * @param port local port.
   * @throws IOException if an error occurs.
   */
  public ServerSocket getServerSocket(int port) throws IOException;

  /**
   * Get an FileInputStream from a local file.
   * @param file the local file.
   * @throws IOException if an error occurs.
   * @return an FileInputStream from the file.
   */
  public FileInputStream getFileInputStream(File file) throws IOException;

  /**
   * Get an FileOutputStream to a local file.
   * @param file the local file.
   * @throws IOException if an error occurs.
   * @return an FileOutputStream from the file.
   */
  public FileOutputStream getFileOutputStream(File file) throws IOException;

  /**
   * Get the file size.
   * @param file the file to get size.
   * @return the file size, of a negative value if not able.
   */
  public int getFileSize(File file);

  /**
   * Open a load file dialog with the given title and return the user choice.
   * @param title dialog title.
   * @return user choice.
   */
  public File getLoadFile(String title);

  /**
   * Open a save file dialog with the given title and return the user choice.
   * @param title dialog title.
   * @return user choice.
   */
  public File getSaveFile(String title);

  /**
   * Get the local host address.
   * @return local host address.
   * @throws UnknownHostException if error occurs.
   */
  public InetAddress getLocalHost() throws UnknownHostException;

  /**
   * Perform a dns resolve of the given address.
   * @param addr address to resolve.
   * @return resolved address.
   */
  public String resolve(InetAddress addr);

  /**
   * Try to use this provider. Return false if this provider is not able to perform its
   * task.
   * @return true if this provider can be used, false otherwise.
   */
  public boolean tryProvider();

  /**
   * Get this provider's name.
   * @return the provder name.
   */
  public String getName();
}

