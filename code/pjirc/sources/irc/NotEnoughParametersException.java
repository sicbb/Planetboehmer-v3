/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * Exception thrown by interpretor if not enough parameters are provided for a command.
 */
public class NotEnoughParametersException extends Exception
{
  /**
   * Create a new NotEnoughParametersException.
   * @param msg message.
   */
  public NotEnoughParametersException(String msg)
  {
    super(msg);
  }
}

