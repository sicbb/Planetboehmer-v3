/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

import java.awt.*;

/**
 * Color Model.
 */
public interface IRCColorModel
{
  /**
   * Replace the i'th color with the given color.
   * @param i color index. Must be smaller than getColorCount.
   * @param c new color.
   */
  public void setColor(int i,Color c);

  /**
   * Get the number of colors.
   * @return color count.
   */
  public int getColorCount();

  /**
   * Get the i'th color.
   * @param i color index.
   * @return i'th color.
   */
  public Color getColor(int i);

}

