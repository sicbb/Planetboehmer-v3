/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc.tree;

/**
 * Interface used for traversing trees.
 */
public interface TreeTraversalListener
{
  /**
   * The traversal is about to begin.
   * @param param user parameter.
   */
  public void begin(Object param);

  /**
   * Next item in the tree.
   * @param item item.
   * @param param user parameter.
   */
  public void nextItem(Object item,Object param);

  /**
   * The traversal is finished.
   * @param param user parameter.
   */
  public void end(Object param);

}

