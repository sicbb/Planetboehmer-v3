/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * Null interpretor. This interpretor does nothing.
 */
public class NullInterpretor implements Interpretor
{
  public NullInterpretor()
  {
  }

  public void sendString(Source source,String str)
  {
  }

}

