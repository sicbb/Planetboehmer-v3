/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

//import java.util.*;

/**
 * A query.
 */
public class Query extends IRCSource
{
  private String _nick;

  private ListenerGroup _listeners;

  /**
   * Create a new Query.
   * @param config global irc configuration.
   * @param nick the remote nick.
   * @param s the server.
   */
  public Query(IRCConfiguration config,String nick,IRCServer s)
  {
    super(config,s);
    _listeners=new ListenerGroup();
    _nick=nick;
  }

  /*private void queryChangeNickListeners(String nick)
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      QueryListener ql=(QueryListener)e.nextElement();
      ql.nickChanged(nick);
    }
  }*/

  /**
   * Add a listener.
   * @param lis listener to add.
   */
  public void addQueryListener(QueryListener lis)
  {
    _listeners.addListener(lis);
  }

  /**
   * Remove a listener.
   * @param lis listener to remove.
   */
  public void removeQueryListeners(QueryListener lis)
  {
    _listeners.removeListener(lis);
  }

  public String getName()
  {
    return _nick;
  }

  public boolean talkable()
  {
    return true;
  }

  public void leave()
  {
    getIRCServer().leaveQuery(getName());
  }

  /**
   * Notify this query the remote nick has changed.
   * @param newNick new remote nick.
   */
  public void changeNick(String newNick)
  {
    _nick=newNick;
    _listeners.sendEvent("nickChanged",newNick);
    //queryChangeNickListeners(newNick);
  }
}

