/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * CTCPFilter interface.
 */
public interface CTCPFilter
{
  /**
   * Notify this filter the given source has received a CTCP message that should
   * be processed.
   * @param nick the sender nickname.
   * @param source the source.
   * @param msg the message to be processed.
   */
  public void perform(String nick,Source source,String msg);

  /**
   * Send an action on the given destination.
   * @param s the server where to send the action.
   * @param destination the destination, channel name or nick name.
   * @param msg the message to send.
   */
  public void action(Server s,String destination,String msg);

  /**
   * Send a PING request on the given nick.
   * @param s the server.
   * @param nick the nick.
   */
  public void ping(Server s,String nick);

  /**
   * Send a DCCChat request on the given nick.
   * @param s the server.
   * @param nick the nick.
   */
  public void chat(Server s,String nick);

  /**
   * Send a DCC file send request on the given nick.
   * @param s the server.
   * @param nick the nick.
   * @param file the file to send.
   */
  public void sendFile(Server s,String nick,String file);

  /**
   * Send a generic CTCP message to the given nick.
   * @param s the server.
   * @param nick the nick.
   * @param message the message to send.
   */
  public void genericSend(Server s,String nick,String message);

  /**
   * Notify this Filter the source has received a CTCP reply.
   * @param nick the replier nick.
   * @param source the source.
   * @param msg the reply message.
   */
  public void CTCPReply(String nick,Source source,String msg);
}

