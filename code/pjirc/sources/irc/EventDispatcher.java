/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

import java.lang.reflect.*;
import java.util.*;

/**
 * Event dispatcher, using reflection.
 */
public class EventDispatcher
{
  private static Hashtable _cache=new Hashtable();

  private static boolean match(Class[] t1,Class[] t2)
  {
    if(t1.length!=t2.length) return false;
    for(int i=0;i<t1.length;i++) if(!t1[i].isAssignableFrom(t2[i])) return false;
    return true;
  }

  /**
   * Dispatch a new event to the given target.
   * @param target target event listener.
   * @param method method name to call.
   * @param params parameters to pass to the called method.
   * @return method result.
   */
  public static Object dispatchEvent(Object target,String method,Object[] params)
  {
    try
    {
      Class c=target.getClass();
      Method m[]=(Method[])_cache.get(c);
      if(m==null)
      {
        m=c.getMethods();
        _cache.put(c,m);
      }
      Class types[]=new Class[params.length];
      for(int i=0;i<params.length;i++) types[i]=params[i].getClass();
      for(int i=0;i<m.length;i++)
      {
        if(m[i].getName().equals(method))
        {
          if(match(m[i].getParameterTypes(),types)) return m[i].invoke(target,params);
        }
      }
      throw new NoSuchMethodException(method);
    }
    catch(Throwable ex)
    {
      ex.printStackTrace();
      return null;
    }
  }
}
