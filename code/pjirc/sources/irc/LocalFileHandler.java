/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

import java.io.*;

/**
 * Local file handler.
 */
public class LocalFileHandler implements FileHandler
{
  public InputStream getInputStream(String fileName)
	{
	  try
		{
		  return new FileInputStream(new File(fileName));
		}
		catch(Exception ex)
		{
		  return null;
		}
	}
}

