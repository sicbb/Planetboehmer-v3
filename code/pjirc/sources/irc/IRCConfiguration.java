/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

import java.awt.*;
import java.util.*;

/**
 * Global IRC configuration.
 */
public class IRCConfiguration
{
  private boolean _timeStamp;
  private boolean _smileys;
  private TextProvider _textProvider;
  private IRCColorModel _colorModel;
	private ImageLoader _loader;
  private URLHandler _handler;
	private FileHandler _file;
	private SoundHandler _sound;
  private boolean _highLight;
  private int _highLightColor;
  private boolean _highLightNick;
  private Vector _highLightWords;
  private String _quitMessage;
	private boolean _showConnect;
	private boolean _showChanlist;
	private boolean _showHelp;
	private boolean _showAbout;
	private boolean _handleASL;
	private String _aslMale;
	private String _aslFemale;
	private SmileyTable _table;
  private boolean _bitmap;
	private int _chanlistFontSize;
	private String _chanlistFontName;
	private int _channelFontSize;
	private String _channelFontName;
	private int _nickListWidth;
	private boolean _info;
	private boolean _nickField;
	private Color[] _chanlistColors;
	private Color[] _defaultColors;
	private Hashtable _colors;
	private AudioConfiguration _audioConfig;

  /**
   * Create a new IRCConfiguration.
   * @param timeStamp timestamp on/off.
   * @param smileys smileys on/off.
   * @param text text provider to use.
   * @param model color model to use.
   * @param handler URL handler to use.
   * @param loader image loader to use.
   * @param sound sound handler to use.
   * @param file file handler to use.
   */
  public IRCConfiguration(boolean timeStamp,boolean smileys,TextProvider text,IRCColorModel model,URLHandler handler,ImageLoader loader,SoundHandler sound,FileHandler file)
  {
	  _colors=new Hashtable();
		_defaultColors=new Color[16];
		_chanlistColors=new Color[16];
		loadDefaultColors(_defaultColors);
		loadDefaultColors(_chanlistColors);

	  _file=file;
		_sound=sound;

		_audioConfig=new AudioConfiguration(_sound);

	  _loader=loader;
    _timeStamp=timeStamp;
    _smileys=smileys;
    _textProvider=text;
    _colorModel=model;
    _handler=handler;
    setQuitMessage("");
    enableHighLight(false);
		setShowConnect(true);
		setShowChanlist(true);
		setShowHelp(true);
		setShowAbout(true);
		setASL(false);
		setASLMale("m");
		setASLFemale("f");
		setBitmapSmileys(false);
		_table=new SmileyTable();
		setChannelFontSize(12);
		setChanlistFontSize(12);
		setChannelFontName("Monospaced");
		setChanlistFontName("Monospaced");
		setNickListWidth(130);
		setInfo(false);
		setNickField(false);
  }

  /**
   * Get the file handler.
   * @return the file handler.
   */
  public FileHandler getFileHandler()
	{
	  return _file;
	}

  /**
   * Get the audio configuration.
   * @return the audio configuration.
   */
  public AudioConfiguration getAudioConfiguration()
	{
	  return _audioConfig;
	}

  /**
   * Get the sound handler.
   * @return the sound handler.
   */
  public SoundHandler getSoundHandler()
	{
	  return _sound;
	}

  /**
   * Get the default color context.
   * @return the default color context.
   */
  public ColorContext getDefaultColorContext()
	{
	  return getColorContext(null);
	}

  /**
   * Get the color context associated with the given source name.
   * @param name source name.
   * @return associated color context.
   */
  public ColorContext getColorContext(String name)
	{
	  ColorContext ctx=new ColorContext();
		ctx.chanlist=false;
		ctx.name=name;
	  return ctx;
	}

	/**
	 * Get the channel list color context.
	 * @return the channel list color context.
	 */
	public ColorContext getChanlistColorContext()
	{
	  ColorContext ctx=new ColorContext();
		ctx.chanlist=true;
		ctx.name="";
	  return ctx;
	}

	private void loadDefaultColors(Color[] cols)
	{
    cols[0]=new Color(0xFFFFFF);
    cols[1]=new Color(0x000000);
    cols[2]=new Color(0x00007F);
    cols[3]=new Color(0x009300);
    cols[4]=new Color(0xFF0000);
    cols[5]=new Color(0x7F0000);
    cols[6]=new Color(0x9C009C);
    cols[7]=new Color(0xFC7F00);
    cols[8]=new Color(0xFFFF00);
    cols[9]=new Color(0x00FC00);
    cols[10]=new Color(0x009393);
    cols[11]=new Color(0x00FFFF);
    cols[12]=new Color(0x0000FC);
    cols[13]=new Color(0xFF00FF);
    cols[14]=new Color(0x7F7F7F);
    cols[15]=new Color(0xD2D2D2);
	}

	/**
	 * Set the i'th chanlist color.
	 * @param i index.
	 * @param c color to replace.
	 */
	public void setChanlistColor(int i,Color c)
	{
	  _chanlistColors[i]=c;
	}

  /**
   * Set the i'th default color.
   * @param i index.
   * @param c color to replace.
   */
  public void setDefaultColor(int i,Color c)
	{
	  _defaultColors[i]=c;
	}

  /**
   * Set the i'th source color.
   * @param name source name.
   * @param i index.
   * @param c color to replace.
   */
  public void setSourceColor(String name,int index,Color c)
	{
	  if(_colors.get(name.toLowerCase())==null)
		{
		  Color[] n=new Color[16];
			for(int i=0;i<16;i++) n[i]=_defaultColors[i];
			_colors.put(name.toLowerCase(),n);
		}
	  Color[] cols=(Color[])_colors.get(name.toLowerCase());
		cols[index]=c;
	}

	/**
	 * Get the colors associated with the given color context.
	 * @param context the context to get colors from.
	 * @return color array for the given context.
	 */
  public Color[] getStyleColors(ColorContext context)
	{
	  if(context.chanlist) return _chanlistColors;
		if(context.name==null) return _defaultColors;
		Color[] cols=(Color[])_colors.get(context.name.toLowerCase());
		if(cols==null) return _defaultColors;
		return cols;
	}

  /**
   * Set the info button visibility.
   * @param s info button visibility.
   */
  public void setInfo(boolean s)
	{
	  _info=s;
	}

	/**
	 * Get the info button visibility.
	 * @return info button visibility.
	 */
	public boolean getInfo()
	{
	  return _info;
	}

  /**
   * Set the nickfield button visibility.
   * @param s nickfield button visibility.
   */
  public void setNickField(boolean s)
	{
	  _nickField=s;
	}

  /**
   * Get the nickfield button visibility.
   * @return nickfield button visibility.
   */
  public boolean getNickField()
	{
	  return _nickField;
	}

  /**
   * Set the nick list width.
   * @param w new nick list width.
   */
  public void setNickListWidth(int w)
	{
	  _nickListWidth=w;
	}

	/**
	 * Get the nick list width.
	 * @return nick list width, in pixel.
	 */
	public int getNickListWidth()
	{
	  return _nickListWidth;
	}

  /**
   * Set channel font size.
   * @param s channel font size.
   */
  public void setChannelFontSize(int s)
	{
	  _channelFontSize=s;
	}

  /**
   * Set channel list font size.
   * @param s channel list font size.
   */
  public void setChanlistFontSize(int s)
	{
	  _chanlistFontSize=s;
	}

  /**
   * Set channel font name.
   * @param s channel font name.
   */
  public void setChannelFontName(String s)
	{
	  _channelFontName=s;
	}

  /**
   * Set channel list font name.
   * @param s channel list font name.
   */
  public void setChanlistFontName(String s)
	{
	  _chanlistFontName=s;
	}

	/**
	 * Get channel font size.
	 * @return font size.
	 */
	public int getChannelFontSize()
	{
	  return _channelFontSize;
	}

  /**
   * Get channel list font size.
   * @return font size.
   */
  public int getChanlistFontSize()
	{
	  return _chanlistFontSize;
	}

  /**
   * Get channel font name.
   * @return font name.
   */
  public String getChannelFontName()
	{
	  return _channelFontName;
	}

  /**
   * Get channel list font name.
   * @return font name.
   */
  public String getChanlistFontName()
	{
	  return _chanlistFontName;
	}

	/**
	 * Add a smiley in the smiley table.
	 * @param match the matching text to replace.
	 * @param file image file name.
	 */
  public void addSmiley(String match,String file)
	{
	  _table.addSmiley(match,_loader.getImage(file));
	}

  /**
   * Get the image loader.
   * @return the image loader.
   */
  public ImageLoader getImageLoader()
	{
	  return _loader;
	}

  /**
   * Set the bitmap smileys flag.
   * @param s bitmap smileys flag.
   */
  public void setBitmapSmileys(boolean s)
	{
	  _bitmap=s;
	}

	/**
	 * Get the bitmap smileys flag.
	 * @return bitmap smileys flag.
	 */
	public boolean getBitmapSmileys()
	{
	  return _bitmap;
	}

  /**
   * Get the smileys table.
   * @return the smiley table.
   */
  public SmileyTable getSmileyTable()
	{
	  return _table;
	}

  /**
   * Set male ASL text.
   * @param m asl male text.
   */
  public void setASLMale(String m)
	{
	  _aslMale=m;
	}

  /**
   * Set female ASL text.
   * @param m asl female text.
   */
  public void setASLFemale(String f)
	{
	  _aslFemale=f;
	}

	/**
	 * Get male ASL text.
	 * @return male ASL text.
	 */
  public String getASLMale()
	{
	  return _aslMale;
	}

  /**
   * Get female ASL text.
   * @return female ASL text.
   */
  public String getASLFemale()
	{
	  return _aslFemale;
	}

	/**
	 * Set the connect button visibility.
	 * @param s connect button visibility.
	 */
  public void setShowConnect(boolean s)
	{
	  _showConnect=s;
	}

  /**
   * Get the connect button visibility.
   * @return connect button visilibity.
   */
	public boolean getShowConnect()
	{
	  return _showConnect;
	}

  /**
   * Set the chanlist button visibility.
   * @param s chanlist button visibility.
   */
  public void setShowChanlist(boolean s)
	{
	  _showChanlist=s;
	}

  /**
   * Get the chanlist button visibility.
   * @return chanlist button visilibity.
   */
  public boolean getShowChanlist()
	{
	  return _showChanlist;
	}

  /**
   * Set the help button visibility.
   * @param s help button visibility.
   */
  public void setShowHelp(boolean s)
	{
	  _showHelp=s;
	}

  /**
   * Get the help button visibility.
   * @return help button visilibity.
   */
  public boolean getShowHelp()
	{
	  return _showHelp;
	}

  /**
   * Set the about button visibility.
   * @param s about button visibility.
   */
  public void setShowAbout(boolean s)
	{
	  _showAbout=s;
	}

  /**
   * Get the about button visibility.
   * @return about button visilibity.
   */
  public boolean getShowAbout()
	{
	  return _showAbout;
	}

	/**
	 * Set ASL enable flag.
	 * @param s ASL flag.
	 */
	public void setASL(boolean s)
	{
	  _handleASL=s;
	}

	/**
	 * Get ASL flag.
	 * @return ASL flag.
	 */
	public boolean getASL()
	{
	  return _handleASL;
	}

  /**
   * Set quit message.
   * @param str the quit message.
   */
  public void setQuitMessage(String str)
  {
    _quitMessage=str;
  }

  /**
   * Get quit message.
   * @return quit message.
   */
  public String quitMessage()
  {
    return _quitMessage;
  }

  /**
   * Set highlight flag.
   * @param b highlight flag.
   */
  public void enableHighLight(boolean b)
  {
    _highLight=b;
  }

  /**
   * Set highlight configuration.
   * @param color hightlight color.
   * @param nick true if nick highlight enabled.
   * @param words hightlight words set.
   */
  public void setHighLightConfig(int color,boolean nick,Vector words)
  {
    _highLightColor=color;
    _highLightNick=nick;
    _highLightWords=new Vector();
    for(int i=0;i<words.size();i++) _highLightWords.insertElementAt(words.elementAt(i),_highLightWords.size());
  }

  /**
   * Get highlight flag.
   * @return highlight flag.
   */
  public boolean highLight()
  {
    return _highLight;
  }

  /**
   * Get nick highlight flag.
   * @return nick highlight flag.
   */
  public boolean highLightNick()
  {
    return _highLightNick && _highLight;
  }

  /**
   * Get highlight words.
   * @return enumeration of String.
   */
  public Enumeration getHighLightWords()
  {
    if(!_highLight) return new Vector().elements();
    return _highLightWords.elements();
  }

  /**
   * Get highlight color.
   * @return highlight color.
   */
  public int highLightColor()
  {
    return _highLightColor;
  }

  /**
   * Get URLHandler.
   * @return URLHandler.
   */
  public URLHandler getURLHandler()
  {
    return _handler;
  }

  /**
   * Set timestamping flag.
   * @param ts timestamping flag.
   */
  public void setTimeStamp(boolean ts)
  {
    _timeStamp=ts;
  }

  /**
   * Get timestamping flag.
   * @return timestamping flag.
   */
  public boolean getTimeStamp()
  {
    return _timeStamp;
  }

  /**
   * Set smileys flag.
   * @param sm smileys flag.
   */
  public void setSmileys(boolean sm)
  {
    _smileys=sm;
  }

  /**
   * Get smileys flag.
   * @return smileys flag.
   */
  public boolean getSmileys()
  {
    return _smileys;
  }

  /**
   * Get text provider.
   * @return text provider.
   */
  public TextProvider getTextProvider()
  {
    return _textProvider;
  }

  /**
   * Get IRC color model.
   * @return color model.
   */
  public IRCColorModel getIRCColorModel()
  {
    return _colorModel;
  }

  /**
   * Get formatted text associated with the given text code, with no parameter.
   * @param code text code.
   * @return formatted text.
   */
  public String getText(int code)
  {
    return _textProvider.getString(code);
  }

  /**
   * Get formatted text associated with the given text code, with one parameter.
   * @param code text code.
   * @param p1 first parameter.
   * @return formatted text.
   */
  public String getText(int code,String p1)
  {
    return _textProvider.getString(code,p1);
  }

  /**
   * Get formatted text associated with the given text code, with two parameters.
   * @param code text code.
   * @param p1 first parameter.
   * @param p2 second parameter.
   * @return formatted text.
   */
  public String getText(int code,String p1,String p2)
  {
    return _textProvider.getString(code,p1,p2);
  }

  /**
   * Get formatted text associated with the given text code, with three parameters.
   * @param code text code.
   * @param p1 first parameter.
   * @param p2 second parameter.
   * @param p3 third parameter.
   * @return formatted text.
   */
  public String getText(int code,String p1,String p2,String p3)
  {
    return _textProvider.getString(code,p1,p2,p3);
  }

  /**
   * Get the i'th color of the color model.
   * @param i index.
   * @return color at index i.
   */
  public Color getColor(int i)
  {
    return _colorModel.getColor(i);
  }

  /**
   * Open the given URL.
   * @param str URL to open.
   */
  public void openURL(String str)
  {
    _handler.openURL(str);
  }

}

