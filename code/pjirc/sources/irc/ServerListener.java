/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

/**
 * Server listener.
 */
public interface ServerListener
{
  /**
   * The server connected.
   */
  public void serverConnected();
  /**
   * The server disconnected.
   */
  public void serverDisconnected();

  /**
   * A new channel has been created.
   * @param chan channel created.
   */
  public void channelCreated(Channel chan);

  /**
   * An existing channel has been removed.
   * @param chan remove channel.
   */
  public void channelRemoved(Channel chan);

  /**
   * A new query has been created.
   * @param query create query.
   * @param local true if this query was created at our initiative, and not
   * from a server event.
   */
  public void queryCreated(Query query,Boolean local);

  /**
   * An existing query has been removed.
   * @param query removed query.
   */
  public void queryRemoved(Query query);

  /**
   * A new channel list has been created.
   * @param list the created channel list.
   */
  public void chanListCreated(ChanList list);

  /**
   * An existing channel list has been removed.
   * @param list the removed chanlist.
   */
  public void chanListRemoved(ChanList list);
}

