/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

package irc;

//import java.util.*;

/**
 * The root source.
 */
public abstract class Source extends IRCObject
{
  /**
   * The server.
   */
  protected Server _server;
  private ListenerGroup _listeners;
  /**
   * The interpretor.
   */
  protected Interpretor _in;
  private CTCPFilter _filter;
  private boolean _activated;

  /**
   * Create a new Source.
   * @param config the global configuration.
   * @param s the bound server.
   */
  public Source(IRCConfiguration config,Server s)
  {
    super(config);
    _activated=false;
    _listeners=new ListenerGroup();
    _in=new NullInterpretor();
    _server=s;
    _filter=new NullCTCPFilter();
  }

  /**
   * Get this source name.
   * @return source name.
   */
  public abstract String getName();

  /**
   * Test wether this source can accept messages from user.
   * @return true if this source accepts user input, false otherwise.
   */
  public abstract boolean talkable();

  /**
   * Request to leave (close) this source.
   */
  public abstract void leave();

  /**
   * Set this source's interpretor.
   * @param in new interpretor.
   */
  public void setInterpretor(Interpretor in)
  {
    _in=in;
  }

  /**
   * Set this source's CTCPFilter.
   * @param filter new CTCPFilter.
   */
  public void setCTCPFilter(CTCPFilter filter)
  {
    _filter=filter;
  }

  /**
   * Send a string from user input to this source.
   * @param str user input.
   */
  public void sendString(String str)
  {
    _in.sendString(this,str);
  }

  /**
   * Get this source's interpretor.
   * @return this source's interpretor.
   */
  public Interpretor getInterpretor()
  {
    return _in;
  }

  /**
   * Request this source to clear all the history it could have.
   */
  public void clear()
  {
    _listeners.sendEvent("clear");
  }

  /**
   * Notify this source a new message has arrived.
   * @param source the source of the message. It can be a user nickname or a channel name.
   * @param msg the message.
   */
  public void messageReceived(String source,String msg)
  {
    if(msg.startsWith("\1"))
    {
      msg=msg.substring(1);
      msg=msg.substring(0,msg.length()-1);
      _filter.perform(source,this,msg);
    }
    else
    {

      _listeners.sendEvent("messageReceived",source,msg);
    }
  }

  /**
   * Notify this source a new notice message has arrived.
   * @param source the source of the message. It can be a user nickname or a channel name.
   * @param msg the message.
   */
  public void noticeReceived(String source,String msg)
  {
    if(msg.startsWith("\1"))
    {
      msg=msg.substring(1);
      msg=msg.substring(0,msg.length()-1);
      _filter.CTCPReply(source,this,msg);
    }
    else
    {

      _listeners.sendEvent("noticeReceived",source,msg);
    }
  }

  /**
   * Notify this source a new action message has arrived.
   * @param nick the user who sent the action.
   * @param msg the message.
   */
  public void action(String nick,String msg)
  {

    _listeners.sendEvent("action",nick,msg);
  }

  /**
   * Notify this source a new report has arrived.
   * @param msg the report message.
   */
  public void report(String msg)
  {

    _listeners.sendEvent("reportReceived",msg);
  }

  /**
   * Add a new SourceListener.
   * @param lis listener to add.
   */
  public void addSourceListener(SourceListener lis)
  {
    _listeners.addListener(lis);
  }

  /**
   * Remove a SourceListener.
   * @param lis the listener to remove.
   */
  public void removeSourceListener(SourceListener lis)
  {
    _listeners.removeListener(lis);
  }

  /**
   * Trigger message listeners.
   * @param source the nick or channel source.
   * @param msg the message.
   */
  /*private void triggerListeners(String source,String msg)
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      SourceListener lis=(SourceListener)e.nextElement();
      lis.messageReceived(source,msg);
    }
  }*/

  /**
   * Trigger report listeners.
   * @param msg the message.
   */
  /*private void triggerReportListeners(String msg)
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      SourceListener lis=(SourceListener)e.nextElement();
      lis.reportReceived(msg);
    }
  }*/

  /**
   * Trigger notice listeners.
   * @param from the nick or channel source.
   * @param msg the message.
   */
  /*private void triggerNoticeListeners(String from,String msg)
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      SourceListener lis=(SourceListener)e.nextElement();
      lis.noticeReceived(from,msg);
    }
  }*/

  /**
   * Trigger action listeners.
   * @param nick the nick source.
   * @param msg the message.
   */
  /*private void triggerActionListeners(String nick,String msg)
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      SourceListener lis=(SourceListener)e.nextElement();
      lis.action(nick,msg);
    }
  }*/

  /**
   * Trigger clear request listeners.
   */
  /*private void triggerClearListeners()
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      SourceListener lis=(SourceListener)e.nextElement();
      lis.clear();
    }
  }*/

  /**
   * Activate this source. At construct-time, the source is disabled.
   */
  public void activate()
  {
    if(!_activated)
    {
      _activated=true;
      _listeners.sendEvent("activate");
      //triggerActivateListeners();
    }
  }

  /**
   * Test wether this source is active.
   * @return active state.
   */
  public boolean isActive()
  {
    return _activated;
  }

  /**
   * Trigger activate listeners.
   */
  /*protected void triggerActivateListeners()
  {
    Enumeration e=_listeners.elements();
    while(e.hasMoreElements())
    {
      SourceListener lis=(SourceListener)e.nextElement();
      lis.activate();
    }
  }*/

  /**
   * Get the source server.
   * @return the source server.
   */
  public Server getServer()
  {
    return _server;
  }

}

