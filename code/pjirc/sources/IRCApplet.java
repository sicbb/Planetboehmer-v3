/***************************************************/
/*         This java file is a part of the         */
/*                                                 */
/*          -  Plouf's Java IRC Client  -          */
/*                                                 */
/*      Copyright (C) 2002 Philippe Detournay      */
/*                                                 */
/*   This file is licensed under the GPL license   */
/*                                                 */
/*        All contacts : theplouf@yahoo.com        */
/***************************************************/

import irc.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import irc.gui.pixx.*;

public class IRCApplet extends java.applet.Applet implements ActionListener
{

  private IRCApplication _application;
  private TextField _nickField;
  private TextField _nameField;
  private TextField _hostField;
  private TextField _portField;

  private String getNick()
  {
    String nick=getParameter("nick");
    if(nick==null) return null;
		return nick;
  }

	private boolean getBoolean(String key,boolean def)
	{
    String v=getParameter(key);
    if(v==null) return def;
    v=v.toLowerCase().trim();
    if(v.equals("true") || v.equals("on") || v.equals("1")) return true;
    return false;
	}

  private String getString(String key,String def)
  {
    String v=getParameter(key);
    if(v==null) return def;
    return v;
  }

  private int getInt(String key,int def)
  {
    String v=getParameter(key);
    if(v==null) return def;
    try
    {
      return new Integer(v).intValue();
    }
    catch(Exception e)
    {
      return def;
    }

  }

	private String getASLMale()
	{
	  return getString("aslmale","m");
	}

	private String getASLFemale()
	{
	  return getString("asmfemale","f");
	}

	private boolean getUseInfo()
	{
	  return getBoolean("useinfo",false);
	}

	private boolean getNickField()
	{
	  return getBoolean("nickfield",false);
	}

	private boolean getShowConnect()
	{
	  return getBoolean("showconnect",true);
	}

	private boolean getShowChanlist()
	{
	  return getBoolean("showchanlist",true);
	}

	private boolean getShowAbout()
	{
	  return getBoolean("showabout",true);
	}

	private boolean getShowHelp()
	{
	  return getBoolean("showhelp",true);
	}

	private boolean getASL()
	{
	  return getBoolean("asl",false);
	}

  private boolean getTimeStamp()
  {
	  return getBoolean("timestamp",false);
  }

	private boolean getBitmapSmileys()
	{
	  return getBoolean("bitmapsmileys",false);
	}

  private TextProvider getTextProvider()
  {
    String lang=getString("language","english");
		return new FileTextProvider(lang+".lng",new AppletFileHandler(this));
  }

	private int getNickListWidth()
	{
    return getInt("nicklistwidth",130);
	}

  private String[] getCommands()
  {
    Vector v=new Vector();
    String cmd;
    int i=1;
    do
    {
      cmd=getParameter("command"+i);
      if(cmd!=null) v.insertElementAt(cmd,v.size());
      i++;
    }
    while(cmd!=null);
    String[] ans=new String[v.size()];
    for(i=0;i<v.size();i++) ans[i]=(String)v.elementAt(i);
    return ans;
  }

	private void readSmileys(IRCConfiguration config)
	{
    String cmd;
    int i=1;
    do
    {
      cmd=getParameter("smiley"+i);
      if(cmd!=null)
			{
			  int pos=cmd.indexOf(" ");
				if(pos!=-1)
				{
				  String match=cmd.substring(0,pos).trim();
					String file=cmd.substring(pos+1).trim();
					config.addSmiley(match,file);
				}
			}
      i++;
    }
    while(cmd!=null);
	}

	private void readChannelFont(IRCConfiguration config)
	{
	  String param=getParameter("channelfont");
		if(param==null) return;
		int pos=param.indexOf(" ");
		if(pos==-1) return;
		String size=param.substring(0,pos).trim();
		String name=param.substring(pos+1).trim();
		//name="Monospaced";
		config.setChannelFontSize(new Integer(size).intValue());
		config.setChannelFontName(name);
	}

	private void readChanlistFont(IRCConfiguration config)
	{
	  String param=getParameter("chanlistfont");
		if(param==null) return;
		int pos=param.indexOf(" ");
		if(pos==-1) return;
		String size=param.substring(0,pos).trim();
		String name=param.substring(pos+1).trim();
		//name="Monospaced";
		config.setChanlistFontSize(new Integer(size).intValue());
		config.setChanlistFontName(name);
	}

  private IRCColorModel getIRCColorModel()
  {
    PixxColorModel model;

    String base=getParameter("basecolor");
    if(base!=null)
    {
      try
      {
        int pos=base.indexOf(",");
        int r=new Integer(base.substring(0,pos)).intValue();
        base=base.substring(pos+1);
        pos=base.indexOf(",");
        int g=new Integer(base.substring(0,pos)).intValue();
        base=base.substring(pos+1);
        int b=new Integer(base).intValue();
        model=new PixxColorModel(r,g,b);
      }
      catch(Exception e)
      {
        e.printStackTrace();
        model=new PixxColorModel();
      }
    }
    else
    {
      model=new PixxColorModel();
    }

    for(int i=0;i<model.getColorCount();i++)
    {
      String color=getParameter("color"+i);
      try
      {
        if(color!=null)
				{
				  model.setColor(i,new Color(Integer.parseInt(color,16)));
				}
      }
      catch(Exception e)
      {
      }
    }
    return model;
  }

	private void configureTextColors(IRCConfiguration config)
	{
    for(int i=0;i<16;i++)
    {
      String color=getParameter("chanlisttextcolor"+i);
      try
      {
        if(color!=null) config.setChanlistColor(i,new Color(Integer.parseInt(color,16)));
      }
      catch(Exception e)
      {
      }
    }

    for(int i=0;i<16;i++)
    {
      String color=getParameter("defaultsourcetextcolor"+i);
      try
      {
        if(color!=null) config.setDefaultColor(i,new Color(Integer.parseInt(color,16)));
      }
      catch(Exception e)
      {
      }
    }

    String cmd;
    int i=1;
    do
    {
      cmd=getParameter("sourcecolorconfig"+i);
      if(cmd!=null)
			{
			  int pos=cmd.indexOf(" ");
				if(pos!=-1)
				{
				  String source=cmd.substring(0,pos).trim();
					cmd=cmd.substring(pos+1).trim();
					pos=cmd.indexOf(" ");
					if(pos!=-1)
					{
					  String index=cmd.substring(0,pos).trim();
						String color=cmd.substring(pos+1).trim();
						try
						{
						  int ind=new Integer(index).intValue();
						  Color col=new Color(Integer.parseInt(color,16));
							config.setSourceColor(source,ind,col);
						}
						catch(Exception e)
						{
						}
					}
				}
			}
      i++;
    }
    while(cmd!=null);

	}

  private boolean getSmileys()
  {
    return getBoolean("smileys",false);
  }

  private boolean getHighLight()
  {
    return getBoolean("highlight",false);
  }

  private boolean getHighLightNick()
  {
    return getBoolean("highlightnick",false);
  }

  private int getHighLightColor()
  {
    return getInt("highlightcolor",5);
  }

  private String[] getHighLightWords()
  {
    String words=getParameter("highlightwords");
    if(words==null) return new String[0];
    return new StringParser().parseString(words);
  }

  private String getQuitMessage()
  {
    return getString("quitmessage","");
  }

	private void readSound(IRCConfiguration config)
	{
	  AudioConfiguration ac=config.getAudioConfiguration();
		if(getParameter("soundbeep")!=null) ac.setBeep(getParameter("soundbeep"));
    if(getParameter("soundquery")!=null) ac.setQuery(getParameter("soundquery"));
  }

  private IRCConfiguration getIRCConfiguration()
  {
    IRCConfiguration config=new IRCConfiguration(getTimeStamp(),getSmileys(),getTextProvider(),getIRCColorModel(),new AppletURLHandler(getAppletContext()),new AppletImageLoader(this),new AppletSoundHandler(this),new AppletFileHandler(this));

    config.setQuitMessage(getQuitMessage());

    Vector v=new Vector();
    String[] words=getHighLightWords();
    for(int i=0;i<words.length;i++) v.insertElementAt(words[i],v.size());
    config.setHighLightConfig(getHighLightColor(),getHighLightNick(),v);
    config.enableHighLight(getHighLight());

		config.setASL(getASL());
		config.setASLMale(getASLMale());
		config.setASLFemale(getASLFemale());
		config.setShowConnect(getShowConnect());
		config.setShowChanlist(getShowChanlist());
		config.setShowAbout(getShowAbout());
		config.setShowHelp(getShowHelp());
		config.setBitmapSmileys(getBitmapSmileys());
		config.setNickListWidth(getNickListWidth());
		config.setInfo(getUseInfo());
		config.setNickField(getNickField());
		readSmileys(config);
		readChannelFont(config);
		readChanlistFont(config);
		configureTextColors(config);
		readSound(config);
    return config;
  }

  public void start()
  {
    try
    {
      String nick=getNick();
      if(nick==null) throw new Exception();
      String name=getParameter("name");
      if(name==null) throw new Exception();
      String host=getParameter("host");
      if(host==null) throw new Exception();
      String pass=getParameter("password");
      if(pass==null) pass="";
      int port=new Integer(getParameter("port")).intValue();
      _application=new IRCApplication(getIRCConfiguration(),nick,name,pass,host,port,this,getParameter("helppage"));
      _application.init();
      _application.connect(getCommands());
    }
    catch(Exception e)
    {
      setLayout(new BorderLayout());
      Label l=new Label("Java IRC Client By Plouf");
      add(l,"North");
      Panel p=new Panel();
      p.setLayout(new GridLayout(4,1));

      _nickField=new TextField("Anonymous",10);
      _nameField=new TextField("Java User",10);
      _hostField=new TextField("irc.dwchat.net",10);
      //  _hostField=new TextField("localhost",10);
      _portField=new TextField("6667",10);

      Panel p1=new Panel(new FlowLayout(FlowLayout.RIGHT));
      p1.add(new Label("Nick :"));
      p1.add(_nickField);

      Panel p2=new Panel(new FlowLayout(FlowLayout.RIGHT));
      p2.add(new Label("Name :"));
      p2.add(_nameField);

      Panel p3=new Panel(new FlowLayout(FlowLayout.RIGHT));
      p3.add(new Label("Server :"));
      p3.add(_hostField);

      Panel p4=new Panel(new FlowLayout(FlowLayout.RIGHT));
      p4.add(new Label("Port :"));
      p4.add(_portField);

      p.add(p1);
      p.add(p2);
      p.add(p3);
      p.add(p4);
      Button btn=new Button("Connect!");
      btn.addActionListener(this);
      add(p,"Center");
      add(btn,"South");
      _application=null;
    }
  }

  public void stop()
  {
    if(_application!=null)
    {
      _application.disconnect();
      _application.uninit();
    }

  }

  public void actionPerformed(ActionEvent e)
  {
    String nick=_nickField.getText();
    String name=_nameField.getText();
    String host=_hostField.getText();
    int port=new Integer(_portField.getText()).intValue();
    _application=new IRCApplication(getIRCConfiguration(),nick,name,"",host,port,null,getParameter("helppage"));
    _application.init();
    _application.connect(getCommands());

  }

}


