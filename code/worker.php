<?php
    /*
        Autor: Daniel Böhmer
        Erstellt am: 8.2.06
        Funktion: Funktion zum Abarbeiten von Jobs
    */

    try {
        require_once "kernel/db_mapper_class.php";
        require_once "kernel/db_user_class.php";
        require_once "kernel/user_class.php";
        require_once "kernel/db_news_class.php";
        require_once "kernel/news_class.php";

        $key = "luXJGif2Gr";
        $logmsg = "";

        if ($_GET['key'] == $key) {
            $logmsg .= "(".date("d", time()).".".date("m", time()).".".date("Y", time())." - ".date("H", time()).":".date("i", time()).":".date("s", time()).") Die Datei wurde aufgerufen!\r\n";

            $db_mapper_obj = new DBMapperClass();

            // ********************************************************************************
            // * User löschen die sich nach 24 Stunden immer noch nicht authentifiziert haben *
            // ********************************************************************************
            $stamp = time() - 60*60*24;

            $sql_query1 = "SELECT id, login FROM user WHERE activationkey != '' AND status = 'locked'";
            $result1 = $db_mapper_obj->ExecSql($sql_query1);

            if ($result1->num_rows > 0) {
                while ($row1 = $result1->fetch_assoc()) {
                    $user_id = $row1['id'];
                    $login = $row1['login'];

                    $sql_query2 = "SELECT id FROM user_reg WHERE reg_date < '".$stamp."' AND user_id = '".$user_id."'";
                    $result2 = $db_mapper_obj->ExecSql($sql_query2);

                    if ($result2->num_rows == 1) {
                        $sql_query3 = "DELETE FROM user WHERE id = '".$user_id."'";
                        $sql_query4 = "DELETE FROM user_reg WHERE user_id = '".$user_id."'";

                        $db_mapper_obj->ExecSql($sql_query3);
                        $db_mapper_obj->ExecSql($sql_query4);

                        $logmsg .= "(".date("d", time()).".".date("m", time()).".".date("Y", time())." - ".date("H", time()).":".date("i", time()).":".date("s", time()).") Der User ".$login." mit der ID ".$user_id." wurde aus der Datenbank gelöscht (keine Authentifizierung nach 24 Stunden).\r\n";
                    }
                }
            }

            // ***********************************
            // * Sucht nach Usern die Geb. haben *
            // ***********************************
            $sql_query = "SELECT id FROM user WHERE birthday LIKE '%-".date("m", time())."-".date("d", time())."' ORDER BY id";
            $result = $db_mapper_obj->ExecSql($sql_query);

            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $db_user_obj = new DBUserClass();
                    $user_obj = $db_user_obj->GetUserById($row['id']);

                    if ($user_obj != null) {
                        // News wird angelegt
                        $db_news_obj = new DBNewsClass();
                        $news_obj = new NewsClass("Happy Birthday ".$user_obj->GetLogin(), "Wir wünschen dir alles Gute zum Geburtstag!\n\n<img src=\'img/Geburtstagskarte.jpg\' width=\'250\' heigth=\'147\' border=\'0\'>\n\nLiebe Grüße\nP.S. Diese News wurde vom Planetboehmer Geburtstagsreminder generiert!");
                        $news_obj->SetUser_id(86);
                        $db_news_obj->Insert($news_obj);

                        // Geburtstagskind wird vom System gratuliert
                        $reg_msg = "Planetboehmer und alle User wünschen dir heute zu deinem Geburtstag alles Gute!";
                        $sql_query = "INSERT INTO intern_message values(NULL, '86', '".$user_obj->GetId()."', 'Geburtstags Grüße', '".$reg_msg."', '".time()."', 'unread', 'false', '127.0.0.1')";
                        $db_mapper_obj->ExecSql($sql_query);

                        $sql_query = "SELECT id FROM user ORDER BY id";
                        $result = $db_mapper_obj->ExecSql($sql_query);

                        if ($result->num_rows > 0) {
                            while ($row = $result->fetch_assoc()) {
                                if ($row['id'] != $user_obj->GetId()) {
                                    // Jeder User erhält die IM, dass der User X Geburtstag hat
                                    $reg_msg = "Diese IM wurde vom System generiert. Der User ".$user_obj->GetLogin()." hat heute Geburtstag! Es wäre eine nette Geste ihm zu schreiben.";
                                    $sql_query = "INSERT INTO intern_message values(NULL, '86', '".$row['id']."', 'Geburtstags Reminder', '".$reg_msg."', '".time()."', 'unread', 'false', '127.0.0.1')";
                                    $db_mapper_obj->ExecSql($sql_query);
                                }
                            }
                        }

                        // gebnotice auf true setzen
                        //$sql_query = "UPDATE user SET gebnotice = 'true' WHERE id = '" . $user_obj->GetId() . "'";
                        //$db_mapper_obj->ExecSql($sql_query);

                        $logmsg .= "(".date("d", time()).".".date("m", time()).".".date("Y", time())." - ".date("H", time()).":".date("i", time()).":".date("s", time()).") Der User ".$user_obj->GetLogin()." hat heute Geburtstag.\r\n";
                    }
                }
            } else {
                $logmsg .= "(".date("d", time()).".".date("m", time()).".".date("Y", time())." - ".date("H", time()).":".date("i", time()).":".date("s", time()).") Es wurde kein User in der Datenbank gefunden, der heute Geburtstag hat.\r\n";
            }
        } else {
            $logmsg .= "(".date("d", time()).".".date("m", time()).".".date("Y", time())." - ".date("H", time()).":".date("i", time()).":".date("s", time()).") Datei wurde mit einem falschem Passwort versucht zu öffnen.\r\n";
        }

        // Logging
        if ($file = fopen("../logs/worklog.txt", "a")) {
            fwrite($file, $logmsg);

            fclose($file);
        }
    } catch (Exception $e) {
        print("<br>".$e->getMessage()."<br><br>");
    }
