<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/22/05
        Function: Login Class
    */

    require_once("kernel/db_session_class.php");
    require_once("kernel/session_class.php");
    require_once("kernel/db_user_class.php");
    require_once("kernel/user_class.php");
    require_once("kernel/db_internmessage_class.php");
    require_once("kernel/internmessage_class.php");
    require_once("kernel/db_dcushead_class.php");
    require_once("kernel/dcushead_class.php");
    require_once("kernel/db_dcusmain_class.php");
    require_once("kernel/dcusmain_class.php");
    require_once("kernel/db_dcusimg_class.php");
    require_once("kernel/dcusimg_class.php");
    require_once("kernel/db_news_class.php");
    require_once("kernel/news_class.php");
    require_once("kernel/db_userstats_class.php");
    require_once("kernel/userstats_class.php");
    require_once("kernel/php_salt.php");
    require_once("kernel/constant.php");
    
    require_once("page_class.php");

    class PageLoginClass extends PageClass
    {
        public function __construct($get, $post, $session_obj, $user_obj, $files)
        {
            $db_session_obj = new DBSessionClass();
            $db_user_obj = new DBUserClass();
            $db_internmessage_obj = new DBInternmessageClass();
            $db_userstats_obj = new DBUserStatsClass();
            
            if ($session_obj != null) {
                if (!empty($get['a'])) {
                    $action = strip_tags($get['a']);
                    if (empty($action)) {
                        $action = strip_tags($post['a']);
                    }
                } else {
                    $action = "";
                }
            }
            
            //CSS-Stile werden eingebunden
            $this->AddCSS('navigation');
            $this->AddCSS('news');
            $this->AddCSS('forms');
            
            $this->AddJavaScriptFile('common');
            $this->AddJavaScriptFile('allmessages');
            
            // Actionverwaltung
            if ($action == "login") {
                $login = strip_tags($post['login']);
                $pass = strip_tags($post['pass']);
                
                $this->body .= $this->Login($login, $pass, $session_obj, $db_user_obj, $db_internmessage_obj, $db_userstats_obj);
            } elseif ($action == "logout") {
                $session_obj->SetUser_id(0);
                $db_session_obj->UpdateSession($session_obj);
                $this->body .= $this->Normal($session_obj);
            } elseif ($action == "im_read" && $session_obj->GetUser_id() != 0) {
                if (!empty($get['id'])) {
                    $imid = $get['id'];
                    $this->body .= $this->ReadIM($session_obj, $db_user_obj, $db_internmessage_obj, $imid);
                } else {
                    throw new Exception("ERROR: Fehlende IM ID!");
                }
            } elseif ($action == "im_write" && $session_obj->GetUser_id() != 0) {
                $java = '
					function getvalue()
					{
						if(document.getElementsByName("userlist")[0].value != "[EMPFÄNGER]")
						{
							addName(document.getElementsByName("userlist")[0].value);
						}
					}
					
					function addName(name)
					{
						document.getElementsByName("receiver")[0].value = document.getElementsByName("receiver")[0].value + name + ";";
					}
					
					function addAll(names)
					{
						if(document.getElementsByName("all_user")[0].checked == true)
						{
							document.getElementsByName("receiver")[0].value = names;
							document.getElementById("userlist_td").style.display = "none";
						}
						else if(document.getElementsByName("all_user")[0].checked == false)
						{
							document.getElementsByName("receiver")[0].value = "";
							document.getElementById("userlist_td").style.display = "";
						}
					}';
                
                $this->AddJavaScript($java);
                $imid = isset($get['id']) ? $get['id'] : null;
                $this->body .= $this->WriteIM($session_obj, $db_user_obj, $db_internmessage_obj, $imid);
            } elseif ($action == "im_add" && $session_obj->GetUser_id() != 0) {
                $this->AddIM($session_obj, $db_user_obj, $db_internmessage_obj, $post);
                $this->body .= $this->LoginArea($user_obj, $session_obj, $db_user_obj, $db_internmessage_obj);
            } elseif ($action == "im_out" && $session_obj->GetUser_id() != 0) {
                $this->body .= $this->OutIM($session_obj, $db_user_obj, $db_internmessage_obj);
            } elseif ($action == "im_del" && $session_obj->GetUser_id() != 0) {
                $this->DelIM($session_obj, $db_user_obj, $db_internmessage_obj, $post);
                $this->body .= $this->LoginArea($user_obj, $session_obj, $db_user_obj, $db_internmessage_obj);
            } elseif ($action == "get_user_update" && $session_obj->GetUser_id() != 0) {
                $this->body .= $this->GetUserUpdate($session_obj, $db_user_obj);
            } elseif ($action == "set_user_update" && $session_obj->GetUser_id() != 0) {
                $this->SetUserUpdate($session_obj, $db_user_obj, $post, $files);
                $this->body .= $this->GetUserUpdate($session_obj, $db_user_obj);
            } elseif ($action == "register") {
                $this->body .= $this->RegisterUser($session_obj);
            } elseif ($action == "registerset") {
                $this->body .= $this->RegisterUserSet($session_obj, $db_user_obj, $post);
            } elseif ($action == "newpass") {
                $this->body .= $this->NewPass($session_obj);
            } elseif ($action == "getnewpass") {
                $this->GetNewPass($session_obj, $db_user_obj, $post);
                $this->body .= $this->Normal($session_obj);
            } elseif ($action == "user_info") {
                if (!empty($get['id'])) {
                    $uid = $get['id'];
                    $this->body .= $this->GetUserInfo($session_obj, $db_user_obj, $uid);
                }
                
                // Stats holen und die profil_views inkrementieren
                $stats_obj = $db_userstats_obj->GetAllUserStats($session_obj->GetUser_id());
                
                if ($stats_obj != null) {
                    $stats_obj->SetProfils_view($stats_obj->GetProfils_view()+1);
                    $db_userstats_obj->UpdateUserStats($stats_obj);
                }
            } elseif ($action == "user_stats") {
                if (!empty($get['id'])) {
                    $uid = $get['id'];
                    $this->body .= $this->GetUserStats($session_obj, $db_user_obj, $db_userstats_obj, $uid);
                }
            } elseif ($action == "getusersearch") {
                if (!empty($post['search'])) {
                    $search = $post['search'];
                    $this->body .= $this->GetUserSearch($session_obj, $db_user_obj, $search);
                } else {
                    $this->AddJavaScript('alert("Error: Ihre Suche ist leer!")');
                    $this->body .= $this->LoginArea($user_obj, $session_obj, $db_user_obj, $db_internmessage_obj);
                }
            } elseif ($action == "dcusadd") {
                $this->body .= $this->DcusAdd($user_obj, $session_obj);
            } elseif ($action == "dcusset") {
                $this->body .= $this->DcusSet($user_obj, $session_obj, $post, $files);
                $this->body .= $this->LoginArea($user_obj, $session_obj, $db_user_obj, $db_internmessage_obj);
            } elseif ($session_obj->GetUser_id() != 0) {
                //header("Location: handler.php?s=" . $session_obj->GetSession() . "&goto=news");
                //exit();
                $this->body .= $this->LoginArea($user_obj, $session_obj, $db_user_obj, $db_internmessage_obj);
            } else {
                $this->body .= $this->Normal($session_obj);
            }
        }
        
        private function Normal($session_obj)
        {
            return '
			<table width="660" cellspacing="0" cellpadding="0" border="0">
				<tr>
				    <td width="100%"><img src="img/menu/interactive_k.gif" width="100" height="30"></td>
  				</tr>
  				
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15"><span class="smalltext"><b>User Bereich.</b><br>Hier können sich User einloggen die registriert sind und interaktiv agieren. Viele Funktionen stehen dem User zur verfügung, unteranderem auch Interne Messages.</span></td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				
				<tr>
					<td valign="top">' .
                        $this->UseBox3('User Bereich', 'Hier kannst du dich im User Bereich einloggen.',
                        '<tr class="boxcomments1">
							<td><span class="smalltext">Login</span> </td>
							<td><input name="login" type="text" maxlength="50" id="login" class="form" size="35" /></td>
						</tr>
						<tr class="boxcomments1">
							<td width="25%"><span class="smalltext">Passwort</span></td>
							<td><input name="pass" type="password" maxlength="50" id="pass" class="form" size="35" /></td>
						</tr>
						<tr class="boxcomments1">
							<td></td>
							<td><input type="submit" name="submit" value="Login" class="form"><br></td>
						</tr>
						<tr class="boxcomments1">
							<td>&nbsp;</td>
							<td><span class="smalltext"><a href="handler.php?s=' . $session_obj->GetSession() . '&a=register&goto=login" class="orangelink">Umsonst registrieren!</a> - <a href="handler.php?s=' . $session_obj->GetSession() . '&a=newpass&goto=login" class="orangelink">Passwort vergessen?</a></span></td>
						</tr>', '500', 'center', 'handler.php?s=' . $session_obj->GetSession() . '&a=login&goto=login') .
                    '</td>
				</tr>
			</table>';
        }
        
        private function Login($login, $pass, $session_obj, $db_user_obj, $db_internmessage_obj, $db_userstats_obj)
        {
            $page = "";
            
            if ($session_obj->GetUser_id() == 0) {
                $user_obj = $db_user_obj->CheckLogin($login, $pass, $session_obj);
                
                if ($user_obj == null) {
                    //$page .= $this->UseBox2('<strong>&nbsp;Fehler</strong>', 'Falsche Login-Daten!<br><a href="handler.php?s=' . $session_obj->GetSession() . '&a=newpass&goto=login" class="orangelink">Passwort vergessen</a>', '500', '19', 'center');
                    $page .= $this->Normal($session_obj);
                    $page .= $this->AddJavaScript('alert("Error: Falsche Login-Daten!")');
                } elseif ($user_obj->GetStatus() == "locked") {
                    //$page .= $this->UseBox2('<strong>&nbsp;Fehler</strong>', 'Ihr Account ist gesperrt oder noch nicht freigeschaltet!', '500', '19', 'center');
                    $page .= $this->Normal($session_obj);
                    $page .= $this->AddJavaScript('alert("Error: Ihr Account ist gesperrt oder noch nicht freigeschaltet!")');
                } else {
                    // Alle alten IMs löschen
                    $db_internmessage_obj->DeleteOldMessages();
                    
                    // Last Online setzen
                    $user_obj->SetLast_online(date("Y-m-d H:i:s", time()));
                    $db_user_obj->UpdateUser($user_obj); // User Updaten
                    
                    // Sessionzeit erneuern und auf die Gegenwart setzen
                    $session_obj->SetSdate(time());
                    $db_session_obj = new DBSessionClass();
                    $db_session_obj->UpdateSession($session_obj); // Session Updaten
                    
                    // Prüfen und löschen einer doppelten Login Session
                    $db_session_obj->DeleteDoubleUserSession($user_obj, $session_obj->GetSession());
                    
                    // Stats holen und den Login inkrementieren
                    $stats_obj = $db_userstats_obj->GetAllUserStats($user_obj->GetId());
                    
                    if ($stats_obj != null) {
                        $stats_obj->SetLogins($stats_obj->GetLogins()+1);
                        $db_userstats_obj->UpdateUserStats($stats_obj);
                    }

                    // Logging für Logins
                    if ($file = @fopen("logs/systemlog.txt", "a")) {
                        $logmsg = "(" . date("d", time()) . "." . date("m", time()) . "." . date("Y", time()) . " - " . date("H", time()) . ":" . date("i", time()) . ":" . date("s", time()) . ") LOGIN --> Der User " . $user_obj->GetLogin() . " hat sich eingeloggt.\r\n";
                        fwrite($file, $logmsg);
                        
                        fclose($file);
                    }
                                    
                    $page .= $this->LoginArea($user_obj, $session_obj, $db_user_obj, $db_internmessage_obj);
                }
            } else {
                $page .= $this->LoginArea($user_obj, $session_obj, $db_user_obj, $db_internmessage_obj);
            }
            
            return $page;
        }
        
        private function LoginArea(UserClass $user_obj, $session_obj, $db_user_obj, $db_internmessage_obj)
        {
            if ($user_obj != null) {
                $page = "";
                
                $user = "";
                $userall_objs = $db_user_obj->GetAllOnline();
                
                if ($userall_objs) {
                    foreach ($userall_objs as $userall_obj) {
                        $user .= "<a href='handler.php?s=" . $session_obj->GetSession() . "&a=user_info&id=" . $userall_obj->GetId() . "&goto=login' class='blacklink'>" . $userall_obj->GetLogin() . "</a> ";
                    }
                }
                
                $page .= '
	  			<table width="660" cellspacing="0" cellpadding="0" border="0" align="center">
					<tr>
					    <td width="100%"><img src="img/menu/interactive_k.gif" width="100" height="30"></td>
	  				</tr>
	  				
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					
					<tr>
						<td width="100%" valign="top" height="15"><span class="smalltext"><li>User Ebene, willkommen <a href="handler.php?s=' . $session_obj->GetSession() . '&a=user_info&id=' . $user_obj->GetId() . '&goto=login" class="blacklink">' . $user_obj->GetLogin() . '</a> ...</li></span></td>
					</tr>
					
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					
					<tr>
						<td valign="top">' .
                            $this->UseBox1('&nbsp;<strong>#&nbsp;Menü</strong>', $this->LoginAreaMenu($user_obj, $session_obj), '650', '19', 'center')
                        . '</td>
					</tr>
				';
    
            
                $page .= '
					<tr>
						<td valign="top">' .
                            $this->UseBox2('&nbsp;<strong>#&nbsp;User Online</strong>', $user, '650', '19', 'center')
                        . '</td>
					</tr>';
                
                $page .= '
					<tr>
						<td valign="top">' .
                            $this->UseBox5('Interne Message - Posteingang', 'Hier siehst du alle deine Internen Messages.', $this->GetMyIM($session_obj, $db_user_obj, $db_internmessage_obj), '650', 'center', 'handler.php?s=' . $session_obj->GetSession() . '&a=im_del&goto=login', 'MI')
                        . '</td>
					</tr>';
                
                /*
                $page .= '
                    <tr>
                        <td valign="top" align="center">' .
                            $this->UseBox2('&nbsp;<strong>#&nbsp;Logout</strong>', '
                            <form method="POST" action="handler.php?s=' . $session_obj->GetSession() . '&a=logout&goto=login">
                            <input name="submit" type="submit" value="Logout" class="form"></form>
                            ', '80', '19', 'right')
                        . '</td>
                    </tr>';
                */
                        
                $page .= '
				</table>';
                
                return $page;
            } else {
                return null;
            }
        }
        
        private function LoginAreaMenu($user_obj, $session_obj)
        {
            if ($user_obj->GetFlag() == "admin") {
                $colspan = "5";
            } elseif ($user_obj->GetFlag() == "special user" || $user_obj->GetFlag() == "user") {
                $colspan = "4";
            }
            
            $page = '
  			<!-- Menü Start -->
			<table id="NavigationControl" align="center" border="0" cellspacing="0" cellpadding="0" style="width: 100%; height: 20px; border-left: 1px solid black; border-right: 1px solid black;">

									
				<tr style="height: 1px;">
				<td bgcolor="#000000" colspan="' . $colspan . '"><img src="img/navi/none.gif" style="height: 1px" alt=""></td>
				</tr>
				
				<tr style="height: 2px;">';
            
            if ($user_obj->GetFlag() == "admin" || $user_obj->GetFlag() == "special user" || $user_obj->GetFlag() == "user") {
                $page .= '
						<td id="nav1" style="background-color: #879bbc; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 2px; width: 1px;"></td>
						<td id="nav2" style="background-color: #879bbc; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 2px; width: 1px;"></td>
						<td id="nav3" style="background-color: #879bbc; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 2px; width: 1px;"></td>
				';
            }
            
            if ($user_obj->GetFlag() == "admin") {
                $page .= '
						<td id="nav4" style="background-color: #879bbc; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 2px; width: 1px;"></td>
						<td id="nav5" style="background-color: #879bbc;"><img src="img/navi/none.gif" alt="" style="height: 2px; width: 1px;"></td>
				';
            } elseif ($user_obj->GetFlag() == "special user" || $user_obj->GetFlag() == "user") {
                $page .= '
						<td id="nav4" style="background-color: #879bbc;"><img src="img/navi/none.gif" alt="" style="height: 2px; width: 1px;"></td>
				';
            }
            
            $page .= '
				</tr>
				
				<tr style="height: 1px;">
					<td bgcolor="#000000" colspan="'. $colspan. '"><img src="img/navi/none.gif" style="height: 1px;" alt=""></td>
				</tr>
				
				<tr style="height: 18px">
			';
            
            if ($user_obj->GetFlag() == "admin" || $user_obj->GetFlag() == "special user" || $user_obj->GetFlag() == "user") {
                $page .= '
						<td id="nav1_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, \'#FFFFFF\', \'#879bbc\', 0, \'Hier geht es um den Programmierer und Besitzer dieser Seite\')"  onmouseout="MainMenuOut1(this, \'#FFFFFF\', \'#7B7B7B\')" onclick="window.location=\'handler.php?s=' . $session_obj->GetSession() . '&a=get_user_update&goto=login\'">Mein Profil</td>
						<td id="nav2_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, \'#FFFFFF\', \'#879bbc\', 95, \'Hier findest Du News und eine Shoutbox\')"  onmouseout="MainMenuOut1(this, \'#CECECE\', \'#7B7B7B\')" onclick="window.location=\'handler.php?s=' . $session_obj->GetSession() . '&a=im_write&goto=login\'">IM schreiben</td>
						<td id="nav3_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, \'#FFFFFF\', \'#879bbc\', 190, \'Hier findest Du besondere Features der Seite\')"  onmouseout="MainMenuOut1(this, \'#CECECE\', \'#7B7B7B\')" onclick="window.location=\'handler.php?s=' . $session_obj->GetSession() . '&a=im_out&goto=login\'">IM Ausgang</td>
						
				';
            }
            
            if ($user_obj->GetFlag() == "admin") {
                $page .= '
						<td id="nav4_level1" align="center" class="navdefaultlink" style="border-right: 1px solid black; color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, \'#FFFFFF\', \'#879bbc\', 285, \'Hier findest Du die Aktionen der D.C.U.S. Crew\')"  onmouseout="MainMenuOut1(this, \'#CECECE\', \'#7B7B7B\')" onclick="window.location=\'handler.php?s=' . $session_obj->GetSession() . '&a=user_stats&id=' . $session_obj->GetUser_id() . '&goto=login\'">Meine Stats</td>
						<td id="nav5_level1" align="center" class="navdefaultlink" style="color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, \'#FFFFFF\', \'#879bbc\', 285, \'Hier findest Du die Aktionen der D.C.U.S. Crew\')"  onmouseout="MainMenuOut1(this, \'#CECECE\', \'#7B7B7B\')" onclick="window.location=\'handler.php?s=' . $session_obj->GetSession() . '&a=dcusadd&goto=login\'">D.C.U.S. Add</td>
				';
            } elseif ($user_obj->GetFlag() == "special user" || $user_obj->GetFlag() == "user") {
                $page .= '
						<td id="nav4_level1" align="center" class="navdefaultlink" style="color: #CECECE; background-color:#7B7B7B; width:95; font-weight: normal;" onmouseover="MainMenuOver1(this, \'#FFFFFF\', \'#879bbc\', 285, \'Hier findest Du die Aktionen der D.C.U.S. Crew\')"  onmouseout="MainMenuOut1(this, \'#CECECE\', \'#7B7B7B\')" onclick="window.location=\'handler.php?s=' . $session_obj->GetSession() . '&a=user_stats&id=' . $session_obj->GetUser_id() . '&goto=login\'">Meine Stats</td>
				';
            }
            
            $page .= '
				</tr>
				
				<tr style="height: 1px;">
			';
            
            if ($user_obj->GetFlag() == "admin" || $user_obj->GetFlag() == "special user" || $user_obj->GetFlag() == "user") {
                $page .= '
						<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
						<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
						<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
				';
            }
            
            if ($user_obj->GetFlag() == "admin") {
                $page .= '
						<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
						<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
				';
            } elseif ($user_obj->GetFlag() == "special user" || $user_obj->GetFlag() == "user") {
                $page .= '
						<td style="background-color: #000000; border-right: 1px solid black;"><img src="img/navi/none.gif" alt="" style="height: 1px; width: 1px;"></td>
				';
            }
            
            $page .= '
				</tr>
				
				<tr style="height: 3px;">
					<td colspan="'. $colspan. '" style="background-color: #879bbc;"><img src="img/navi/none.gif" alt="" style="height: 3px; width: 1px;"></td>
				</tr>
										
				<tr style="height: 1px;">
					<td bgcolor="#000000" colspan="'. $colspan. '"><img src="img/navi/none.gif" style="height: 1px" alt=""></td>
				</tr>
				
			</table>
				
			<!-- Menü Ende -->
  			';
            
            return $page;
        }
        
        private function GetMyIM($session_obj, $db_user_obj, $db_internmessage_obj)
        {
            $page = "";
            $internmessage_objs = $db_internmessage_obj->GetInByUserId($session_obj->GetUser_Id());
            
            if ($internmessage_objs) {
                foreach ($internmessage_objs as $internmessage_obj) {
                    $user_obj = $db_user_obj->GetUserById($internmessage_obj->GetFrom_user_id());
                    
                    if ($db_user_obj->CheckUserExists($user_obj)) {
                        if ($internmessage_obj->GetStatus() == "read") {
                            $page .= '<tr class="boxcomments1">
								<td width="20%"><span class="smalltext"><input name="' . $internmessage_obj->GetId() . '" type="checkbox" value="' . $internmessage_obj->GetId() . '"></span></td>
								<td width="80%"><span class="smalltext"><b><a href="handler.php?s=' . $session_obj->GetSession() . '&a=user_info&id=' . $internmessage_obj->GetFrom_user_id() . '&goto=login" class="blacklink">' . $user_obj->GetLogin() . '</a></b> - <b><a href="handler.php?s=' . $session_obj->GetSession() . '&a=im_read&id=' . $internmessage_obj->GetId() . '&goto=login" class="blacklink">' . stripslashes($internmessage_obj->GetReason()) . '</a></b> ' . date("d.m.Y - H:i:s", $internmessage_obj->GetDate()) . '</span></td>
							</tr>';
                        } else {
                            $page .= '<tr class="boxcomments1">
								<td width="20%"><span class="smalltext"><input name="' . $internmessage_obj->GetId() . '" type="checkbox" value="' . $internmessage_obj->GetId() . '"></span></td>
								<td width="80%"><span class="smalltext"><b><a href="handler.php?s=' . $session_obj->GetSession() . '&a=user_info&id=' . $internmessage_obj->GetFrom_user_id() . '&goto=login" class="blacklink">' . $user_obj->GetLogin() . '</a></b> - <font color="RED">neu <a href="handler.php?s=' . $session_obj->GetSession() . '&a=im_read&id=' . $internmessage_obj->GetId() . '&goto=login" class="blacklink"><img src="img/m_on2.gif" width="14" height="9" border="0" alt="Neue IM lesen" title="Neue IM lesen"></a></font>, <b><a href="handler.php?s=' . $session_obj->GetSession() . '&a=im_read&id=' . $internmessage_obj->GetId() . '&goto=login" class="blacklink">' . stripslashes($internmessage_obj->GetReason()) . '</a></b> ' . date("d.m.Y - H:i:s", $internmessage_obj->GetDate()) . '</span></td>
							</tr>';
                        }
                    }
                }
                
                $page .= '<tr class="boxcomments1">
					<td><span class="smalltext">&nbsp;</td>
					<td>&nbsp;</td>
				</tr>';
                
                $page .= '<tr class="boxcomments1">
					<td><input type="checkbox" name="ALLMSGS" value="ALLMSGS" onClick="AllMessages(this.form);"><span class="smalltext">Alle</span></td>
					<td align="left"><input name="submit" type="submit" value="löschen" class="form"></td>
				</tr>';
            } else {
                $page .= '<tr class="boxcomments1">
  					<td colspan="2"><span class="smalltext">Keine neuen Nachrichten!</span></td>
				</tr>';
            }
                        
            return $page;
        }
        
        private function ReadIM($session_obj, $db_user_obj, $db_internmessage_obj, $imid)
        {
            $internmessage_obj = $db_internmessage_obj->GetMessageById($imid);
            $user_obj = $db_user_obj->GetUserById($internmessage_obj->GetFrom_user_id());
            
            if ($internmessage_obj->GetTo_user_id() == $session_obj->GetUser_id() && $internmessage_obj->GetStatus != "read") {
                // IM als gelesen makieren
                $internmessage_obj->SetStatus("read");
                $db_internmessage_obj->UpdateMessage($internmessage_obj);
            }
            
            if ($internmessage_obj->GetFrom_user_id() == $session_obj->GetUser_id()) {
                $submit = "";
            } elseif ($internmessage_obj->GetFrom_user_id() == 86) {
                $submit = "";
            } else {
                $submit = '
						<tr class="boxcomments1">
							<td></td>
							<td><input type="submit" name="submit" value="Antworten" class="form"><br></td>
						</tr>';
            }
            
            return '
			<table width="660" cellspacing="0" cellpadding="0" border="0">
				<tr>
				    <td width="100%"><img src="img/menu/im_k.gif" width="100" height="30"></td>
  				</tr>
  				
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15"><span class="smalltext"><b>Interne Message:</b><br>Diese Nachricht enthält die folgenden Informationen.</span></td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				
				<tr>
					<td valign="top">' .
                        $this->UseBox3(date("d.m.Y - H:i:s", $internmessage_obj->GetDate()), 'Hier siehst du folgende Interne Message:',
                        '<tr class="boxcomments1">
							<td width="20%" valign="top"><span class="smalltext">Sender</span> </td>
							<td><span class="smalltext"><strong><a href="handler.php?s=' . $session_obj->GetSession() . '&a=user_info&id=' . $user_obj->GetId() . '&goto=login" class="blacklink">' . $user_obj->GetLogin() . '</a></span></td>
						</tr>
						<tr class="boxcomments1">
							<td valign="top"><span class="smalltext">Betreff</span></td>
							<td><span class="smalltext">' . stripslashes($internmessage_obj->GetReason()) . '</span></td>
						</tr>
						<tr class="boxcomments1">
							<td valign="top"><span class="smalltext">Text</span></td>
							<td><span class="smalltext">' . stripslashes(nl2br($internmessage_obj->GetText())) . '</span></td>
						</tr>' . $submit, '650', 'center', 'handler.php?s=' . $session_obj->GetSession() . '&a=im_write&id=' . $internmessage_obj->GetId() . '&goto=login') .
                    '</td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15"><a href="handler.php?s=' . $session_obj->GetSession() . '&goto=login" class="blacklink">zurück</a></td>
				</tr>
			</table><br>';
        }
        
        private function WriteIM($session_obj, $db_user_obj, $db_internmessage_obj, $imid)
        {
            if (empty($imid)) {
                $user_objs = $db_user_obj->GetAllActiv();
                
                if ($user_objs) {
                    $z = 0;
                    foreach ($user_objs as $user_obj) {
                        $user_arr[$z++] = $user_obj->GetLogin() . ";";
                        $options = "<option value='" . $user_obj->GetLogin() . "'>"  .$user_obj->GetLogin() . "</option>";
                    }
                }
                
                if (!empty($user_arr)) {
                    foreach ($user_arr as $user_rec) {
                        $users = $user_rec;
                    }
                }
                
                $page = $this->UseBox3('Interne Message schreiben', 'Hier kannst du eine Interne Message schreiben.',
                        '<tr class="boxcomments1" id="userlist_td">
							<td width="20%" valign="top"><span class="smalltext">Userliste</span> </td>
							<td><select name="userlist" class="form" onChange="getvalue()"><option value="[EMPFÄNGER]" selected>[EMPFÄNGER]</option>' . $options . '</select></td>
						</tr>
						<tr class="boxcomments1">
							<td width="20%"><span class="smalltext">Empfänger</span> </td>
							<td><span class="smalltext"><input name="receiver" id="receiver" type="text" size="35" class="form"> <input id="all_user" name="all_user" type="checkbox" onClick="addAll(\'' . $users . '\')"> Alle Auswählen</span></td>
						</tr>
						<tr class="boxcomments1">
							<td valign="top"><span class="smalltext">Betreff</span></td>
							<td><input name="reason" id="reason" type="text" size="35" class="form"></td>
						</tr>
						<tr class="boxcomments1">
							<td valign="top"><span class="smalltext">Text</span></td>
							<td><textarea name="text" rows="7" cols="100" class="form">(Maximal 2000 Zeichen!)</textarea></td>
						</tr>
						<tr class="boxcomments1">
							<td>&nbsp;</td>
							<td><input type="submit" name="submit" value="Abschicken" class="form"></td>
						</tr>', '650', 'center', 'handler.php?s=' . $session_obj->GetSession() . '&a=im_add&goto=login');
            } else {
                $internmessage_obj = $db_internmessage_obj->GetMessageById($imid);
                $user_obj = $db_user_obj->GetUserById($internmessage_obj->GetFrom_user_id());
                
                $page = $this->UseBox3('Interne Message schreiben', 'Hier kannst du eine Interne Message schreiben.',
                        '<tr class="boxcomments1">
							<td width="20%" valign="top"><span class="smalltext">Empfänger</span> </td>
							<td><input name="receiver" id="receiver" type="text" size="35" class="form" value="' . $user_obj->GetLogin() . ';"></td>
						</tr>
						<tr class="boxcomments1">
							<td valign="top"><span class="smalltext">Betreff</span></td>
							<td><input name="reason" id="reason" type="text" size="35" class="form" value="RE:' . stripslashes($internmessage_obj->GetReason()) . '"></td>
						</tr>
						<tr class="boxcomments1">
							<td valign="top"><span class="smalltext">Text</span></td>
							<td><textarea name="text" rows="7" cols="100" class="form">



' . $user_obj->GetLogin() . ' schrieb:
-----------------------
' . stripslashes($internmessage_obj->GetText()) . '</textarea></td>
						</tr>
						<tr class="boxcomments1">
							<td></td>
							<td><input type="submit" name="submit" value="Antworten" class="form"><br></td>
						</tr>', '100%', 'left', 'handler.php?s=' . $session_obj->GetSession() . '&a=im_add&goto=login');
            }
            
            return '
			<table width="660" cellspacing="0" cellpadding="0" border="0">
				<tr>
				    <td width="100%"><img src="img/menu/im_k.gif" width="100" height="30"></td>
  				</tr>
  				
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15"><span class="smalltext"><b>Interne Message:</b><br>Mit dieser Funktion kann man einem anderen User eine private Nachricht schicken. Diese wird er nach seinem Login direkt angezeigt bekommen.</span></td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				
				<tr>
					<td valign="top">' . $page . '</td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15"><a href="handler.php?s=' . $session_obj->GetSession() . '&goto=login" class="blacklink">zurück</a></td>
				</tr>
			</table><br>';
        }
        
        private function NotifyIM($user_obj)
        {
            $db_user_obj = new DBUserClass();
                        
            $message = '<html>
			<body>
			<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr>
			<td><img src="http://www1.bb-network.de/planetboehmer/img/planetboehmer_banner.jpg" width="400" height="95" border="0"></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td><strong>Hallo ' . $user_obj->GetLogin() . ', du hast eine neue IM auf Planetboehmer.de erhalten.</strong></td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td>Um die IM zu lesen, musst du dich zuerst auf <a href="http://www.planetboehmer.de" title="Planetboehmer V3.0">Planetboehmer</a> einloggen. Danach wird die IM direkt angezeigt.</td>
			</tr>
			<tr>
			<td>&nbsp;</td>
			</tr>
			<tr>
			<td>Kontakt: sicbb@gmx.de</td>
			</tr>
			</table>

			</body>
			</html>';
            
            $to = $user_obj->GetEmail();
            $headers  = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
            $headers .= "From: Planetboehmer.de <noreply@bb-network.de>\r\n";
            $headers .= "TO: " . $to . "\r\n";
            $headers .= "SUBJECT: Planetboehmer.de IM Notifier\r\n";
            $db_user_obj->mail($to, $message, $headers);
        }
        
        private function AddIM($session_obj, $db_user_obj, $db_internmessage_obj, $post)
        {
            $receiver = $post['receiver'];
            $reason = addslashes(trim(strip_tags($post['reason'])));
            $text = addslashes(trim(strip_tags($post['text'])));
            $ip = $_SERVER['REMOTE_ADDR'];
            
            $db_userstats_obj = new DBUserStatsClass();
            $db_user_obj = new DBUserClass();
            
            if (!empty($receiver)) {
                if (!empty($reason)) {
                    if (!empty($text) && strlen($text) <= 2000 && $text != "(Maximal 2000 Zeichen!)") {
                        // Receiver vom Semikolon trennen
                        $receiver = trim($receiver);
                        $receiver = explode(";", $receiver);
                        
                        for ($i=0; $i<sizeof($receiver)-1; $i++) {
                            $date = time();
                            $user_obj = $db_user_obj->GetUserByLogin($receiver[$i]);
                            $internmessage_obj = new InternmessageClass('', $session_obj->GetUser_id(), $user_obj->GetId(), $reason, $text, $date, "unread", "false", $ip);
                            $db_internmessage_obj->InsertMessage($internmessage_obj);
                            
                            // Stats holen und die IM_write inkrementieren
                            // (User der die IM geschrieben hat)
                            $stats_obj = $db_userstats_obj->GetAllUserStats($session_obj->GetUser_id());
                            
                            if ($stats_obj != null) {
                                $stats_obj->SetIm_write($stats_obj->GetIm_write()+1);
                                $db_userstats_obj->UpdateUserStats($stats_obj);
                            }
                            
                            // Stats holen und die IM_get inkrementieren
                            // (User der die IM erhalten hat)
                            $stats_obj = $db_userstats_obj->GetAllUserStats($user_obj->GetId());
                            
                            if ($stats_obj != null) {
                                $stats_obj->SetIm_get($stats_obj->GetIm_get()+1);
                                $db_userstats_obj->UpdateUserStats($stats_obj);
                            }
                            
                            // Empfänger eine Email schicken
                            if ($user_obj->GetEmailme() == "true") {
                                $this->NotifyIM($user_obj);
                            }
                        }
                        
                        $this->AddJavaScript('alert("Info: Ihre IM wurde erfolgreich versendet!")');
                    } else {
                        $this->AddJavaScript('alert("Error: Ihr Text ist entweder zu lang, leer oder ungültig!")');
                    }
                } else {
                    $this->AddJavaScript('alert("Error: Bitte geben Sie einen Betreff ein!")');
                }
            } else {
                $this->AddJavaScript('alert("Error: Empfänger ungültig oder nicht vorhanden!")');
            }
        }
        
        private function DelIM($session_obj, $db_user_obj, $db_internmessage_obj, $post)
        {
            $internmessage_objs = $db_internmessage_obj->GetInByUserId($session_obj->GetUser_id());
            
            if ($internmessage_objs) {
                foreach ($internmessage_objs as $internmessage_obj) {
                    if (!empty($post[$internmessage_obj->GetId()])) {
                        if ($internmessage_obj->GetTo_user_id() == $session_obj->GetUser_id()) {
                            $internmessage_obj->SetDelete_flag("true");
                            $db_internmessage_obj->UpdateMessage($internmessage_obj);
                        }
                    }
                }
                
                return true;
            }
            
            return false;
        }
        
        private function OutIM($session_obj, $db_user_obj, $db_internmessage_obj)
        {
            $page = '';
            $output = '';
            $internmessage_objs = $db_internmessage_obj->GetOutByUserId($session_obj->GetUser_Id());
            
            if ($internmessage_objs) {
                foreach ($internmessage_objs as $internmessage_obj) {
                    $user_obj = $db_user_obj->GetUserById($internmessage_obj->GetTo_user_id());
                    
                    if ($internmessage_obj->GetStatus() == "read") {
                        $status = "<font color='GREEN'><b>gelesen</b></font>";
                    } else {
                        $status = "<font color='RED'><b>ungelesen</b></font>";
                    }
                    
                    $output = '
						<tr class="boxcomments1">
							<td width="20%"><span class="smalltext"><a href="handler.php?s=' . $session_obj->GetSession() . '&a=user_info&id=' . $user_obj->GetId() . '&goto=login" class="blacklink">' . $user_obj->GetLogin() . '</a></span></td>
							<td width="60%"><span class="smalltext"><a href="handler.php?s=' . $session_obj->GetSession() . '&a=im_read&id=' . $internmessage_obj->GetId() . '&goto=login" class="blacklink"><strong>' . stripslashes($internmessage_obj->GetReason()) . '</strong></a> - ' . date("d.m.Y - H:i:s", $internmessage_obj->GetDate()) . '</span></td>
							<td width="20%"><span class="smalltext">' . $status . '</span></td>
					 	</tr>
					';
                }
            }
            
            return '
			<table width="660" cellspacing="0" cellpadding="0" border="0">
				<tr>
				    <td width="100%"><img src="img/menu/im_k.gif" width="100" height="30"></td>
  				</tr>
  				
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15"><span class="smalltext"><b>Interne Message:</b><br>Mit dieser Funktion sieht man alle verschickten IM\'s. Man kann den Empfänger sehen und ob er Ihre IM schon gelesen hat.</span></td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				
				<tr>
					<td valign="top">' .
                        $this->UseBox3('Interne Messages Ausgang', 'Hier siehst du alle deine geschriebenen Messages.',
                        '<tr class="boxcomments1">
							<td width="20%"><span class="smalltext"><b>Empfänger</b></span></td>
							<td width="60%"><span class="smalltext"><b>Maildaten</b></span></td>
							<td width="20%"><span class="smalltext"><b>Status</b></span></td>
					 	</tr>
						<tr class="boxcomments1">
							<td width="20%"><span class="smalltext">&nbsp;</span></td>
							<td width="60%"><span class="smalltext">&nbsp;</span></td>
							<td width="20%"><span class="smalltext">&nbsp;</span></td>
					 	</tr>
					 	
					 	' . $output . '
					 	
					 	', '650', 'center', 'handler.php?s=' . $session_obj->GetSession() . '&a=login&goto=login') .
                    '</td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15"><a href="handler.php?s=' . $session_obj->GetSession() . '&goto=login" class="blacklink">zurück</a></td>
				</tr>
			</table><br>';
        }
        
        private function GetUserUpdate($session_obj, $db_user_obj)
        {
            $user_obj = $db_user_obj->GetUserById($session_obj->GetUser_id());
            
            if ($user_obj->GetPicture() != "") {
                $size = getimagesize(img_user_dir . $user_obj->GetPicture());
                $height = $size[1];
                $width = $size[0];
                $this->CalcPicture($width, $height);
                
                $page = '<tr class="boxcomments1">
							<td width="20%"><span class="smalltext"><img src="' . img_user_dir . $user_obj->GetPicture() . '" width="' . $width . '" height="' . $height . '" border="0"></span></td>
							<td width="80%" valign="bottom"><span class="smalltext"><input name="del_pic" type="checkbox" value="del_pic" class="form"> Bild löschen</span></td>
					 	</tr>';
                $picture_value = $user_obj->GetPicture();
            }
            
            list($jahr, $monat, $tag) = explode("-", $user_obj->GetBirthday());
            
            for ($k=1; $k<=31; $k++) {
                if ($tag == $k) {
                    $day = "<option selected>" . $k . "</option>";
                } else {
                    $day = "<option>" . $k . "</option>";
                }
            }
            
            for ($j=1; $j<=12; $j++) {
                if ($monat == $j) {
                    $month = "<option selected>" . $j . "</option>";
                } else {
                    $month = "<option>" . $j . "</option>";
                }
            }
            
            for ($n=1900; $n<=2010; $n++) {
                if ($jahr == $n) {
                    $year = "<option selected>" . $n . "</option>";
                } else {
                    $year = "<option>" . $n . "</option>";
                }
            }
            
            if ($user_obj->GetAteam() == "true") {
                $ateam = '<input name="ateam" type="checkbox" class="form" id="ateam" value="ateam" checked>';
            } else {
                $ateam = '<input name="ateam" type="checkbox" class="form" id="ateam" value="ateam">';
            }
            
            if ($user_obj->GetEmailme() == "true") {
                $emailme = '<input name="emailme" type="checkbox" class="form" id="emailme" value="emailme" checked>';
            } else {
                $emailme = '<input name="emailme" type="checkbox" class="form" id="emailme" value="emailme">';
            }

            
            return '
			<table width="660" cellspacing="0" cellpadding="0" border="0">
				<tr>
				    <td width="100%"><img src="img/menu/profile_k.gif" width="100" height="30"></td>
  				</tr>
  				
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15"><span class="smalltext"><b>User Profile:</b><br>Diese Funktion erlaubt es Usern Ihre persönlichen Daten einzusehen und beibedarf zu ändern. Diese werden dann im Benutzerprofil für andere User sichtbar sein.</span></td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				
				<tr>
					<td valign="top">' .
                        $this->UseBox6('User Profile', 'Hier kannst du deine Informationen updaten.', $page .
                        '<tr class="boxcomments1">
							<td width="20%"><span class="smalltext">Login</span></td>
							<td width="80%"><span class="smalltext"><input name="login" type="text" maxlength="50" id="login" class="form" size="50" value="' . $user_obj->GetLogin() . '"> (erforderlich)</span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td width="20%"><span class="smalltext">Passwort</span></td>
							<td width="80%"><span class="smalltext"><input name="pass" type="password" maxlength="50" id="login" class="form" size="50"></span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td width="20%"><span class="smalltext">eMail</span></td>
							<td width="80%"><span class="smalltext"><input name="email" type="text" maxlength="50" id="login" class="form" size="50" value="' . $user_obj->GetEmail() . '"> (erforderlich)</span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td width="20%"><span class="smalltext">Geburtsdatum</span></td>
							<td width="80%"><span class="smalltext"><select name="day" class="form">' . $day . '</select><select name="month" class="form">' . $month . '</select><select name="year" class="form">' . $year . '</select></span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td width="20%"><span class="smalltext">Wohnort</span></td>
							<td width="80%"><span class="smalltext"><input name="location" type="text" maxlength="50" id="login" class="form" size="50" value="' . $user_obj->GetLocation() . '"></span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td width="20%"><span class="smalltext">ICQ</span></td>
							<td width="80%"><span class="smalltext"><input name="icq" type="text" maxlength="50" id="login" class="form" size="50" value="' . $user_obj->GetIcq() . '"></span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td width="20%"><span class="smalltext">Homepage</span></td>
							<td width="80%"><span class="smalltext"><input name="hp" type="text" maxlength="50" id="login" class="form" size="50" value="' . $user_obj->GetHp() . '"></span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td><span class="smalltext">Bild hochladen</span></td>
							<td><input name="picture" type="file" maxlength="50" id="picture" class="form" size="50" value="' . $picture_value . '"></td>
					  	</tr>
					 	<tr class="boxcomments1">
							<td width="20%" valign="top"><span class="smalltext">Charakter/Info</span></td>
							<td width="80%"><span class="smalltext"><textarea name="userinfo" cols="100" rows="5" wrap="VIRTUAL" class="form">' . $user_obj->GetUserinfo()  . '</textarea></span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td width="20%" valign="top"><span class="smalltext">Daten unter Specials anzeigen?</span></td>
							<td width="80%"><span class="smalltext">' . $ateam . ' Ja, Daten anzeigen</td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td width="20%" valign="top"><span class="smalltext">Emails von Planetboehmer empfangen?</span></td>
							<td width="80%"><span class="smalltext">' . $emailme . ' Ja, Planetboehmer darf mir Emails senden.</td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td width="20%"><span class="smalltext">&nbsp;</span></td>
							<td width="80%"><span class="smalltext"><input name="submit" type="submit" value="Update" class="form"></span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td width="20%" valign="top">&nbsp;</td>
							<td width="80%" class="smalltext"><b>*WICHTIG*</b> Bitte vor dem upload des Bildes, zuerst die Datei auf Größe und Form prüfen! Eine Datei sollte nicht größer sein als 200kb und auch nicht in der Auflösung > 320*240!</td>
					 	</tr>', '650', 'center', '<form enctype="multipart/form-data" name="form1" method="post" action="handler.php?s=' . $session_obj->GetSession() . '&a=set_user_update&goto=login">') .
                    '</td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15"><a href="handler.php?s=' . $session_obj->GetSession() . '&goto=login" class="blacklink">zurück</a></td>
				</tr>
			</table><br>';
        }
        
        private function CheckEmail($email) // Prüft die eMail
        {
            $check = true;
            $at = false;
            $dot = false;
            $charallowed = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789@._-";

            for ($i=0; $i<strlen($email); $i++) {
                $c_email = substr($email, $i, 1);

                if (!strstr($charallowed, $c_email)) {
                    $check = false;
                    break;
                }

                if ($c_email == "@") {
                    $at = true;
                }

                if ($c_email == ".") {
                    $dot = true;
                }
            }

            if ($dot != true || $at != true) {
                $check = false;
            }
            
            return $check;
        }
        
        private function SetUserUpdate($session_obj, $db_user_obj, $post, $files)
        {
            $user_obj = $db_user_obj->GetUserById($session_obj->GetUser_id());
            
            // Daten
            $login = strtolower(strip_tags($post['login']));
            $pass = strip_tags($post['pass']);
            $email = strtolower(strip_tags($post['email']));
            $day = $post['day'];
            $month = $post['month'];
            $year = $post['year'];
            $birthday = $year . "-" . $month . "-" . $day;
            $location = strip_tags($post['location']);
            $icq = strip_tags($post['icq']);
            $hp = strip_tags($post['hp']);
            $userinfo = strip_tags($post['userinfo']);
            $del_pic = strip_tags($post['del_pic']);
            $ateam = strip_tags($post['ateam']);
            $emailme = strip_tags($post['emailme']);
            
            if (empty($ateam)) {
                $ateam = "false";
            } else {
                $ateam = "true";
            }
            
            if (empty($emailme)) {
                $emailme = "false";
            } else {
                $emailme = "true";
            }
            
            if (!empty($login)) {
                if (!empty($email)) {
                    if ($this->CheckEmail($email) != false) {
                        // Prüft die eMail --> Funktion siehe oben

                        if (!empty($files['picture']['name']) && empty($del_pic)) {
                            if ($files['picture']['size'] < 204800) {
                                if ($files['picture']['type'] == "image/pjpeg" || $files['picture']['type'] == "image/gif" || $files['picture']['type'] == "image/jpeg") {
                                    @move_uploaded_file($files['picture']['tmp_name'], img_user_dir . time() . $files['picture']['name']);
                                    @chmod(img_user_dir . time() . $files['picture']['name'], 0775);
                                    $picture = time() . $files['picture']['name'];
                                    
                                    if (!empty($pass)) {
                                        $user_obj->SetPass(sha1($pass . SHA1_SALT));
                                    }
                                
                                    $current_login = strtolower($user_obj->GetLogin());
                                    $current_email = strtolower($user_obj->GetEmail());
                                    
                                    $user_obj->SetLogin($login);
                                    $user_obj->SetEmail($email);
                                    $user_obj->SetBirthday($birthday);
                                    $user_obj->SetLocation($location);
                                    $user_obj->SetIcq($icq);
                                    $user_obj->SetHp($hp);
                                    $user_obj->SetUserinfo($userinfo);
                                    $user_obj->SetPicture($picture);
                                    $user_obj->SetAteam($ateam);
                                    $user_obj->SetEmailme($emailme);
                                    
                                    if ($current_login == $login && $current_email == $email) {
                                        $db_user_obj->UpdateUser($user_obj);
                                        $this->AddJavaScript('alert("Info: Userdaten wurden erfolgreich geändert!")');
                                    } elseif ($current_login == $login && !$db_user_obj->CheckEmailExists($email)) {
                                        $db_user_obj->UpdateUser($user_obj);
                                        $this->AddJavaScript('alert("Info: Userdaten wurden erfolgreich geändert!")');
                                    } elseif ($current_email == $email && !$db_user_obj->CheckLoginExists($login)) {
                                        $db_user_obj->UpdateUser($user_obj);
                                        $this->AddJavaScript('alert("Info: Userdaten wurden erfolgreich geändert!")');
                                    } else {
                                        $this->AddJavaScript('alert("Error: Der Login oder die eMail wird bereits verwendet!")');
                                    }
                                } else {
                                    $this->AddJavaScript('alert("Error: Ihr Bild ist nicht in einem jpeg oder gif Format!")');
                                }
                            } else {
                                $this->AddJavaScript('alert("Error: Ihr Bild ist größer als 200 KB!")');
                            }
                        } elseif (!empty($del_pic)) {
                            if (!empty($pass)) {
                                $user_obj->SetPass(sha1($pass . SHA1_SALT));
                            }
                        
                            $current_login = strtolower($user_obj->GetLogin());
                            $current_email = strtolower($user_obj->GetEmail());
                            $current_picture = $user_obj->GetPicture();
                            
                            unlink(img_user_dir . $current_picture); // Bild physikalisch löschen
                            
                            $user_obj->SetLogin($login);
                            $user_obj->SetEmail($email);
                            $user_obj->SetBirthday($birthday);
                            $user_obj->SetLocation($location);
                            $user_obj->SetIcq($icq);
                            $user_obj->SetHp($hp);
                            $user_obj->SetUserinfo($userinfo);
                            $user_obj->SetPicture("");
                            $user_obj->SetAteam($ateam);
                            $user_obj->SetEmailme($emailme);
                            
                            if ($current_login == $login && $current_email == $email) {
                                $db_user_obj->UpdateUser($user_obj);
                                $this->AddJavaScript('alert("Info: Userdaten wurden erfolgreich geändert!")');
                            } elseif ($current_login == $login && !$db_user_obj->CheckEmailExists($email)) {
                                $db_user_obj->UpdateUser($user_obj);
                                $this->AddJavaScript('alert("Info: Userdaten wurden erfolgreich geändert!")');
                            } elseif ($current_email == $email && !$db_user_obj->CheckLoginExists($login)) {
                                $db_user_obj->UpdateUser($user_obj);
                                $this->AddJavaScript('alert("Info: Userdaten wurden erfolgreich geändert!")');
                            } else {
                                $this->AddJavaScript('alert("Error: Der Login oder die eMail wird bereits verwendet!")');
                            }
                        } else {
                            if (!empty($pass)) {
                                $user_obj->SetPass(sha1($pass . SHA1_SALT));
                            }
                        
                            $current_login = strtolower($user_obj->GetLogin());
                            $current_email = strtolower($user_obj->GetEmail());
                            
                            $user_obj->SetLogin($login);
                            $user_obj->SetEmail($email);
                            $user_obj->SetBirthday($birthday);
                            $user_obj->SetLocation($location);
                            $user_obj->SetIcq($icq);
                            $user_obj->SetHp($hp);
                            $user_obj->SetUserinfo($userinfo);
                            $user_obj->SetAteam($ateam);
                            $user_obj->SetEmailme($emailme);
                            
                            if ($current_login == $login && $current_email == $email) {
                                $db_user_obj->UpdateUser($user_obj);
                                $this->AddJavaScript('alert("Info: Userdaten wurden erfolgreich geändert!")');
                            } elseif ($current_login == $login && !$db_user_obj->CheckEmailExists($email)) {
                                $db_user_obj->UpdateUser($user_obj);
                                $this->AddJavaScript('alert("Info: Userdaten wurden erfolgreich geändert!")');
                            } elseif ($current_email == $email && !$db_user_obj->CheckLoginExists($login)) {
                                $db_user_obj->UpdateUser($user_obj);
                                $this->AddJavaScript('alert("Info: Userdaten wurden erfolgreich geändert!")');
                            } else {
                                $this->AddJavaScript('alert("Error: Der Login oder die eMail wird bereits verwendet!")');
                            }
                        }
                    } else {
                        $this->AddJavaScript('alert("Error: Bitte geben Sie eine gültige eMail ein!")');
                    }
                } else {
                    $this->AddJavaScript('alert("Error: Sie müssen eine eMail angeben!")');
                }
            } else {
                $this->AddJavaScript('alert("Error: Sie müssen einen Login angeben!")');
            }
        }
        
        private function RegisterUser($session_obj)
        {
            return '
			<table width="660" cellspacing="0" cellpadding="0" border="0">
				<tr>
				    <td width="100%"><img src="img/menu/interactive_k.gif" width="100" height="30"></td>
  				</tr>
  				
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15"><span class="smalltext"><b>Planetboehmer Anmeldung</b><br>Registrieren Sie sich noch jetzt und genießen Sie anschließend den exquisiten Hauch vom Planetboehmer Touch.</span></td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				
				<tr>
					<td valign="top">' .
                        $this->UseBox3('Planetboehmer Anmeldung', 'Hier kannst du dich anmelden.',
                        '<tr class="boxcomments1">
							<td width="25%"><span class="smalltext">Login</span></td>
							<td width="75%"><span class="smalltext"><input name="login" type="text" maxlength="35" id="login" class="form" size="35"> *</span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td><span class="smalltext">Passwort</span></td>
							<td><span class="smalltext"><input name="pass1" type="password" maxlength="50" id="login" class="form" size="35"> (min. 6 Zeichen) *</span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td><span class="smalltext">Passwort wiederholen</span></td>
							<td><span class="smalltext"><input name="pass2" type="password" maxlength="50" id="login" class="form" size="35"> (min. 6 Zeichen) *</span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td><span class="smalltext">eMail</span></td>
							<td><span class="smalltext"><input name="email" type="text" maxlength="50" id="login" class="form" size="35"> *</span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td width="20%"><span class="smalltext">&nbsp;</span></td>
							<td width="80%"><span class="smalltext"><input name="submit" type="submit" value="Anmelden" class="form"></span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td><span class="smalltext">&nbsp;</span></td>
							<td><span class="smalltext">(Diese Daten werden vertraulich behandelt und nicht an Dritte weitergegeben!)<br>* => erforderliche Einträge</span></td>
					 	</tr>', '600', 'center', 'handler.php?s=' . $session_obj->GetSession() . '&a=registerset&goto=login') .
                    '</td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15"><a href="handler.php?s=' . $session_obj->GetSession() . '&goto=login" class="blacklink">zurück</a></td>
				</tr>
			</table><br>';
        }
        
        private function RegisterUserSet($session_obj, $db_user_obj, $post)
        {
            $login = strip_tags($post['login']);
            $pass1 = strip_tags($post['pass1']);
            $pass2 = strip_tags($post['pass2']);
            $email = strip_tags($post['email']);
            
            if (!empty($login) && !empty($pass1) && !empty($pass2) && ($pass1 == $pass2) && !empty($email) && strlen($pass1) >= 6) {
                $reg_key = $db_user_obj->CreateRegKey();
                $last_online = date("Y-m-d H:i:s", time());
                $user_obj = new UserClass(0, $login, sha1($pass1 . SHA1_SALT), $email, "", "", "", "", "", "", "user", "locked", $reg_key, $last_online, "false", "true");
                
                if (!$db_user_obj->CheckUserExists($user_obj)) {
                    if ($user_obj != null) {
                        $db_user_obj->InsertUser($user_obj);
                        
                        $page .= $this->UseBox2('<strong>&nbsp;#&nbsp;Useranmeldung . Info', '<br>User wurde erfolgreich angelegt! Die endgültige Authentifikation erfolgt durch eine Email.<br><br>', '600', '19', 'center');
                    } else {
                        $page .= $this->UseBox2('<strong>&nbsp;#&nbsp;Useranmeldung - Fehler', '<br>Ein unbekannter Fehler ist aufgetreten! Bitte versuchen Sie es erneut.<br><br>', '600', '19', 'center');
                    }
                } else {
                    $page .= $this->UseBox2('<strong>&nbsp;#&nbsp;Useranmeldung - Fehler', '<br>Die von Ihnen eingegebenen Daten existieren bereits. Bitte wählen Sie einen anderen Vorname und eine andere Email.<br><br>', '600', '19', 'center');
                }
            } else {
                $page .= $this->UseBox2('<strong>&nbsp;#&nbsp;Useranmeldung - Fehler', '<br>Fehler, bitte füllen Sie alle erforderlichen Fehler aus und achten Sie darauf, dass Ihr Passwort 6 oder mehr Zeichen enthält!<br><br>', '600', '19', 'center');

                $page .= $this->UseBox3('Planetboehmer Anmeldung', 'Hier kannst du dich anmelden.',
                        '<tr class="boxcomments1">
							<td width="25%"><span class="smalltext">Login</span></td>
							<td width="75%"><span class="smalltext"><input name="login" type="text" maxlength="35" id="login" class="form" size="35"> *</span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td><span class="smalltext">Passwort</span></td>
							<td><span class="smalltext"><input name="pass1" type="password" maxlength="50" id="login" class="form" size="35"> (min. 6 Zeichen) *</span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td><span class="smalltext">Passwort wiederholen</span></td>
							<td><span class="smalltext"><input name="pass2" type="password" maxlength="50" id="login" class="form" size="35"> (min. 6 Zeichen) *</span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td><span class="smalltext">eMail</span></td>
							<td><span class="smalltext"><input name="email" type="text" maxlength="50" id="login" class="form" size="35"> *</span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td width="20%"><span class="smalltext">&nbsp;</span></td>
							<td width="80%"><span class="smalltext"><input name="submit" type="submit" value="Anmelden" class="form"></span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td><span class="smalltext">&nbsp;</span></td>
							<td><span class="smalltext">(Diese Daten werden vertraulich behandelt und nicht an Dritte weitergegeben!)<br>* => erforderliche Einträge</span></td>
					 	</tr>', '600', 'center', 'handler.php?s=' . $session_obj->GetSession() . '&a=registerset&goto=login');
            }
            
            $output = '
				<table width="660" cellspacing="0" cellpadding="0" border="0">
					<tr>
					    <td width="100%"><img src="img/menu/interactive_k.gif" width="100" height="30"></td>
	  				</tr>
	  				
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					
					<tr>
						<td width="100%" valign="top" height="15"><span class="smalltext"><b>Planetboehmer Anmeldung</b><br>Registrieren Sie sich noch jetzt und genießen Sie anschließend den exquisiten Hauch vom Planetboehmer Touch.</span></td>
					</tr>
					
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					
					<tr>
						<td valign="top">' . $page . '</td>
					</tr>
				</table>';
            
            return $output;
        }

        private function NewPass($session_obj)
        {
            return '
			<table width="660" cellspacing="0" cellpadding="0" border="0">
				<tr>
				    <td width="100%"><img src="img/menu/interactive_k.gif" width="100" height="30"></td>
  				</tr>
  				
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15"><span class="smalltext"><b>Planetboehmer Passwort Anforderung</b><br>Falls einem das Passwort nicht mehr einfällt, kann sich hier ein neues zusenden lassen. Das neue Passwort kann man selbstverständlich danach im Profil wieder ändern.</span></td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				
				<tr>
					<td valign="top">' .
                        $this->UseBox3('Passwort Anforderung', 'Hier kannst du dir ein neues Passwort zusenden lassen.',
                        '<tr class="boxcomments1">
							<td><span class="smalltext">eMail</span></td>
							<td><span class="smalltext"><input name="email" type="text" maxlength="50" id="login" class="form" size="35"></span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td width="20%"><span class="smalltext">&nbsp;</span></td>
							<td width="80%"><span class="smalltext"><input name="submit" type="submit" value="Anfordern" class="form"></span></td>
					 	</tr>
					 	<tr class="boxcomments1">
							<td><span class="smalltext">&nbsp;</span></td>
							<td><span class="smalltext">Bitte geben Sie die Emailadresse an, die Sie bei der Registrierung benutzt haben! Ihnen wird sofort ein neues Passwort zugeschickt.</span></td>
					 	</tr>', '600', 'center', 'handler.php?s=' . $session_obj->GetSession() . '&a=getnewpass&goto=login') .
                    '</td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15"><a href="handler.php?s=' . $session_obj->GetSession() . '&goto=login" class="blacklink">zurück</a></td>
				</tr>
			</table><br>';
        }
        
        private function GetNewPass($session_obj, $db_user_obj, $post)
        {
            $email = strip_tags($post['email']);
            
            if (!empty($email)) {
                if ($db_user_obj->CreateNewPass($email)) {
                    $this->AddJavaScript('alert("Info: Ihr Passwort wurde geändert und Ihnen zugesendet!")');
                } else {
                    $this->AddJavaScript('alert("Error: Ihre eMail kann nicht im System gefunden werden!")');
                }
            } else {
                $this->AddJavaScript('alert("Error: Sie müssen eine eMail angeben!")');
            }
        }
        
        private function GetUserInfo($session_obj, $db_user_obj, $uid)
        {
            $user_obj = $db_user_obj->GetUserById($uid);
                        
            // Picturedaten laden (Höhe und Breite)
            if ($user_obj->GetPicture() != "") {
                $size = getimagesize(img_user_dir . $user_obj->GetPicture());
                $height = $size[1];
                $width = $size[0];
                
                // Picturedaten auf standard trimmen (Höhe und Breite)
                $this->CalcPicture($width, $height);
                
                $page .= '<tr class="boxcomments1">
						<td width="20%"><span class="smalltext"><img src="' . img_user_dir . $user_obj->GetPicture() . '" width="' . $width . '" height="' . $height . '" border="0"></span></td>
						<td width="80%"><span class="smalltext">&nbsp;</span></td>
					 </tr>';
            }
            if ($user_obj->GetLogin() != "") {
                $page .= '<tr class="boxcomments1">
						<td width="20%"><span class="smalltext">Login</span></td>
						<td width="80%"><span class="smalltext"><strong>' . $user_obj->GetLogin() . ' (<a href="handler.php?s=' . $session_obj->GetSession() . '&a=user_stats&id=' . $user_obj->GetId() . '&goto=login" class="orangelink">Stats anschauen</a>)</strong></span></td>
					 </tr>';
            }
            if ($user_obj->GetBirthday() != "" && $user_obj->GetBirthday() != "1900-01-01" && $user_obj->GetBirthday() != "0000-00-00") {
                list($jahr, $monat, $tag) = explode("-", $user_obj->GetBirthday());
                $geb = $tag . "." . $monat . "." . $jahr;
                
                $page .= '<tr class="boxcomments1">
						<td width="20%"><span class="smalltext">Geburtsdatum</span></td>
						<td width="80%"><span class="smalltext">' . $geb . '</span></td>
					 </tr>';
            }
            if ($user_obj->GetLocation() != "") {
                $page .= '<tr class="boxcomments1">
						<td width="20%"><span class="smalltext">Wohnort</span></td>
						<td width="80%"><span class="smalltext">' . $user_obj->GetLocation() . '</span></td>
					 </tr>';
            }
            if ($user_obj->GetIcq() != "0") {
                $page .= '<tr class="boxcomments1">
						<td width="20%"><span class="smalltext">ICQ</span></td>
						<td width="80%"><span class="smalltext">' . $user_obj->GetIcq() . '</span></td>
					 </tr>';
            }
            if ($user_obj->GetHp() != "") {
                $page .= '<tr class="boxcomments1">
						<td width="20%"><span class="smalltext">Homepage</span></td>
						<td width="80%"><a href="' . $user_obj->GetHp() . '" target="_blank" class="orangelink">' . $user_obj->GetHp() . '</a></td>
					 </tr>';
            }
            if ($user_obj->GetUserinfo() != "") {
                $page .= '<tr class="boxcomments1">
						<td width="20%" valign="top"><span class="smalltext">Charakter/Info</span></td>
						<td width="80%"><span class="smalltext">' . $user_obj->GetUserinfo() . '</span></td>
					 </tr>';
            }
            
            return '
			<table width="660" cellspacing="0" cellpadding="0" border="0">
				<tr>
				    <td width="100%"><img src="img/menu/profile_k.gif" width="100" height="30"></td>
  				</tr>
  				
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15"><span class="smalltext"><b>User Informationen</b><br>Diese Funktion erlaubt es einem, detaillierte Userinformationen über einen registrierten Benutzer zu erfahren, <br>falls dieser Informationen hinterlassen hat.</span></td>
				</tr>
				
				<tr>
					<td width="100%" valign="top" height="15">&nbsp;</td>
				</tr>
				
				<tr>
					<td valign="top">' .
                        $this->UseBox3('User Informationen', 'Hier kannst du die Informationen über alle User einsehen.',
                        $page . '<tr class="boxcomments1">
							<td><span class="smalltext">Letzter Login</span></td>
							<td><span class="smalltext">' . $user_obj->GetLast_online() . ' <font color="#C90000">(' . $this->CalcLastOnline($user_obj) . ')</font></span></td>
						  </tr>', '650', 'center', 'handler.php?s=' . $session_obj->GetSession() . '&goto=login') .
                    '</td>
				</tr>
				
				<td valign="top">' .
                        $this->UseBox3('User Suche', 'Hier kannst du nach Usern suchen.',
                        '<tr class="boxcomments1">
							<td width="20%"><span class="smalltext">User</span></td>
							<td width="80%"><span class="smalltext"><input name="search" type="text" size="35" class="form"></span></td>
						  </tr>
						  <tr class="boxcomments1">
							<td width="20%"><span class="smalltext">&nbsp;</span></td>
							<td width="80%"><span class="smalltext"><input name="submit" type="submit" value="Suchen" class="form"></span></td>
						  </tr>', '650', 'center', 'handler.php?s=' . $session_obj->GetSession() . '&a=getusersearch&goto=login') .
                    '</td>
				
				<tr>
					<td width="100%" valign="top" height="15"><a href="javascript:history.back()" class="blacklink">zurück</a></td>
				</tr>
			</table><br>';
        }
        
        private function GetUserStats($session_obj, $db_user_obj, $db_userstats_obj, $uid)
        {
            $user_obj = $db_user_obj->GetUserById($uid);
            
            if ($user_obj != null) {
                $stats_obj = $db_userstats_obj->GetAllUserStats($user_obj->GetId());
                
                if ($stats_obj != null) {
                    $page = '<tr class="boxcomments1">
						<td width="30%"><span class="smalltext">Login</span></td>
						<td width="70%"><span class="smalltext"><strong>' . $user_obj->GetLogin() . '</strong></span></td>
					 </tr>';
                    
                    $page .= '<tr class="boxcomments1">
						<td width="30%"><span class="smalltext">Anzahl logins</span></td>
						<td width="70%"><span class="smalltext"><strong>' . $stats_obj->GetLogins() . '</strong></span></td>
					 </tr>';
                    
                    $page .= '<tr class="boxcomments1">
						<td width="30%"><span class="smalltext">Anzahl geschriebener IM\'s</span></td>
						<td width="70%"><span class="smalltext"><strong>' . $stats_obj->GetIm_write() . '</strong></span></td>
					 </tr>';
                    
                    $page .= '<tr class="boxcomments1">
						<td width="30%"><span class="smalltext">Anzahl erhaltener IM\'s</span></td>
						<td width="70%"><span class="smalltext"><strong>' . $stats_obj->GetIm_get() . '</strong></span></td>
					 </tr>';
                    
                    $page .= '<tr class="boxcomments1">
						<td width="30%"><span class="smalltext">Anzahl Shoutboxeinträge</span></td>
						<td width="70%"><span class="smalltext"><strong>' . $stats_obj->GetShoutbox_write() . '</strong></span></td>
					 </tr>';
                    
                    $page .= '<tr class="boxcomments1">
						<td width="30%"><span class="smalltext">User Profile angesehen</span></td>
						<td width="70%"><span class="smalltext"><strong>' . $stats_obj->GetProfils_view() . '</strong></span></td>
					 </tr>';
                    
                    $page .= '<tr class="boxcomments1">
						<td width="30%"><span class="smalltext">Seitenaufrufe</span></td>
						<td width="70%"><span class="smalltext"><strong>' . $stats_obj->GetClicks() . '</strong></span></td>
					 </tr>';
                    
                    $page .= '<tr class="boxcomments1">
						<td width="30%"><span class="smalltext">Letzter Login</span></td>
						<td width="70%"><span class="smalltext"><strong>' . $user_obj->GetLast_online() . '</strong></span></td>
					 </tr>';
                    
                    $page .= '<tr class="boxcomments1">
						<td width="30%"><span class="smalltext">Userstatus</span></td>
						<td width="70%"><span class="smalltext"><strong>' . $user_obj->GetFlag() . '</strong></span></td>
					 </tr>';
                    
                    return '
						<table width="660" cellspacing="0" cellpadding="0" border="0">
							<tr>
							    <td width="100%"><img src="img/menu/profile_k.gif" width="100" height="30"></td>
			  				</tr>
			  				
							<tr>
								<td width="100%" valign="top" height="15">&nbsp;</td>
							</tr>
							
							<tr>
								<td width="100%" valign="top" height="15"><span class="smalltext"><b>User Stats</b><br>Diese Funktion erlaubt es einem, detaillierte UserStats über einen registrierten Benutzer zu erfahren.</span></td>
							</tr>
							
							<tr>
								<td width="100%" valign="top" height="15">&nbsp;</td>
							</tr>
							
							<tr>
								<td valign="top">' .
                                    $this->UseBox3('User Stats', 'Hier kannst du die UserStats über einen User einsehen.',
                                    $page . '<tr class="boxcomments1">
									  </tr>', '650', 'center', 'handler.php?s=' . $session_obj->GetSession() . '&goto=login') .
                                '</td>
							</tr>
							
							<tr>
								<td width="100%" valign="top" height="15"><a href="javascript:history.back()" class="blacklink">zurück</a></td>
							</tr>
						</table><br>
					';
                }
            }
        }
        
        private function CalcLastOnline($user_obj)
        {
            if ($user_obj != null) {
                // DateTime splitten und einen Stamp anlegen
                list($datum, $zeit) = explode(" ", $user_obj->GetLast_online());
                list($jahr, $monat, $tag) = explode("-", $datum);
                list($stunde, $minute, $sekunde) = explode(":", $zeit);
                
                $stamp = mktime($stunde, $minute, $sekunde, $monat, $tag, $jahr);
                
                // Letzter login eines Users in Tagen
                $last_on = round(((time() - $stamp) / 60 / 60 / 24), 3);
                $pos = strpos($last_on, ".");
                if (empty($pos)) {
                    $pos = strlen($last_on);
                }
                $last_on = substr($last_on, 0, $pos);
                
                // Detaillierte Anzeige von Tagen oder Stunden
                if ($last_on == 1) {
                    $format = " Tag";
                } elseif ($last_on >= 2) {
                    $format = " Tage";
                } elseif ($last_on == 0) {
                    // Wenn es 0 Tage sind, wechselt die Einheit nach Stunden

                    // Letzter login eines Users in Tagen
                    $last_on = round(((time() - $stamp) / 60 / 60), 3);
                    $pos = strpos($last_on, ".");
                    if (empty($pos)) {
                        $pos = strlen($last_on);
                    }
                    $last_on = substr($last_on, 0, $pos);
                    
                    if ($last_on == 1) {
                        $format = " Stunde";
                    } elseif ($last_on >= 2) {
                        $format = " Stunden";
                    } elseif ($last_on == 0) {
                        // Wenn es 0 Stunden sind, wechselt die Einheit nach Minuten

                        // Letzter login eines Users in Minuten
                        $last_on = round(((time() - $stamp) / 60), 3);
                        $pos = strpos($last_on, ".");
                        if (empty($pos)) {
                            $pos = strlen($last_on);
                        }
                        $last_on = substr($last_on, 0, $pos);
                        
                        if ($last_on == 1) {
                            $format = " Minute";
                        } elseif ($last_on >= 2) {
                            $format = " Minuten";
                        } elseif ($last_on == 0) {
                            // Letzter login eines Users in Sekunden
                            $last_on = round(((time() - $stamp)), 3);
                            $pos = strpos($last_on, ".");
                            if (empty($pos)) {
                                $pos = strlen($last_on);
                            }
                            $last_on = substr($last_on, 0, $pos);
                            
                            if ($last_on == 1) {
                                $format = " Sekunde";
                            } elseif ($last_on >= 2) {
                                $format = " Sekunden";
                            } else {
                                $format = " Sekunden";
                            }
                        }
                    }
                }
                $laston = $last_on . $format . " her";
                
                return $laston;
            }
        }
        
        private function CalcPicture(&$width, &$height)
        {
            $std_w = 200;
            $std_h = 200;
            
            $calc = false;
            
            if ($width > $height && $width > $std_w) {
                $calc = true;
            } elseif ($height > $width && $height > $std_h) {
                $calc = true;
            } elseif ($width == $height && $width > $std_w) {
                $calc = true;
            }
        
            if ($calc == true) {
                if ($width > $height) {
                    $verh = $width / $height;
                    $w_diff = $width - $std_w;
                    $width -= $w_diff;
                    $h_diff = $w_diff / $verh;
                    $height -= $h_diff;
                } elseif ($width < $height) {
                    $verh = $height / $width;
                    $h_diff = $height - $std_h;
                    $height -= $h_diff;
                    $w_diff = $h_diff / $verh;
                    $width -= $w_diff;
                } elseif ($width == $height) {
                    $w_diff = $width - $std_w;
                    $width -= $w_diff;
                    $height -= $w_diff;
                }
            }
        }
        
        private function GetUserSearch($session_obj, $db_user_obj, $search)
        {
            $user_objs = $db_user_obj->GetUsersBySearch($search);
            
            if ($user_objs != null) {
                foreach ($user_objs as $user_obj) {
                    $erg .= '<a href="handler.php?s=' . $session_obj->GetSession() . '&a=user_info&id=' . $user_obj->GetId() . '&goto=login" target="bottomFrame" class="blacklink">' . $user_obj->GetLogin() . '</a><br>';
                }
                
                return '
					<table width="660" cellspacing="0" cellpadding="0" border="0">
						<tr>
						    <td width="100%"><img src="img/menu/userinfo_k.gif" width="100" height="30"></td>
		  				</tr>
		  				
						<tr>
							<td width="100%" valign="top" height="15">&nbsp;</td>
						</tr>
						
						<tr>
							<td width="100%" valign="top" height="15"><span class="smalltext"><b>User Suche</b><br>Diese Funktion erlaubt es jedem, nach einen oder mehreren Usern zu suchen<br>und seine Informationen anzuzeigen.</span></td>
						</tr>
						
						<tr>
							<td width="100%" valign="top" height="15">&nbsp;</td>
						</tr>
						
						<tr>
							<td valign="top">
								' . $this->UseBox2("&nbsp;<strong>#&nbsp;User Suche</strong>", $erg, '500', '19', 'left') . '
							</td>
						</tr>
						
						<tr>
							<td width="100%" valign="top" height="15"><a href="javascript:history.back()" class="blacklink">zurück</a></td>
						</tr>
					</table><br>';
            } else {
                return '
					<table width="660" cellspacing="0" cellpadding="0" border="0">
						<tr>
						    <td width="100%"><img src="img/menu/userinfo_k.gif" width="100" height="30"></td>
		  				</tr>
		  				
						<tr>
							<td width="100%" valign="top" height="15">&nbsp;</td>
						</tr>
						
						<tr>
							<td width="100%" valign="top" height="15"><span class="smalltext"><b>User Suche</b><br>Diese Funktion erlaubt es jedem, nach einen oder mehreren Usern zu suchen<br>und seine Informationen anzuzeigen.</span></td>
						</tr>
						
						<tr>
							<td width="100%" valign="top" height="15">&nbsp;</td>
						</tr>
						
						<tr>
							<td valign="top">
								' . $this->UseBox2("&nbsp;<strong>#&nbsp;User Suche</strong>", 'Es wurden keine User anhand des Suchbegriffes <strong><i>' . $search . '</i></strong> gefunden.', '650', '19', 'center') . '
							</td>
						</tr>
						
						<tr>
							<td width="100%" valign="top" height="15"><a href="javascript:history.back()" class="blacklink">zurück</a></td>
						</tr>
					</table><br>';
            }
        }
        
        private function DcusAdd($user_obj, $session_obj)
        {
            if ($user_obj->GetFlag() == "admin") {
                return '
					<table width="660" cellspacing="0" cellpadding="0" border="0">
						<tr>
						    <td width="100%"><img src="img/menu/dcus_k.gif" width="100" height="30"></td>
		  				</tr>
		  				
						<tr>
							<td width="100%" valign="top" height="15">&nbsp;</td>
						</tr>
						
						<tr>
							<td width="100%" valign="top" height="15"><span class="smalltext"><b>D.C.U.S. Admin Add:</b><br>Hier werden D.C.U.S. Artikel erstellt und eingetragen.</span></td>
						</tr>
						
						<tr>
							<td width="100%" valign="top" height="15">&nbsp;</td>
						</tr>
						
						<tr>
							<td valign="top">' .
                                $this->UseBox6('D.C.U.S. Admin Add', 'D.C.U.S. Artikel Verwaltung.',
                                '<tr class="boxcomments1">
									<td width="20%" valign="top"><span class="smalltext">Headline</span></td>
									<td width="80%" valign="top"><span class="smalltext"><input name="headline" type="text" maxlength="50" id="headline" class="form" size="35"></span></td>
							 	</tr>
							 	<tr class="boxcomments1">
									<td width="20%" valign="top"><span class="smalltext">Einleitung</span></td>
									<td width="80%" valign="top"><span class="smalltext"><textarea name="smalltext" cols="60" rows="3" wrap="VIRTUAL" class="form"></textarea></span></td>
							 	</tr>
							 	<tr class="boxcomments1">
									<td width="20%" valign="top"><span class="smalltext">Head Bild</span></td>
									<td width="80%" valign="top"><span class="smalltext"><input name="headpic" type="file" maxlength="50" id="headpic" class="form" size="50"></span></td>
							 	</tr>
							 	<tr class="boxcomments1">
									<td width="20%" valign="top"><span class="smalltext">Text</span></td>
									<td width="80%" valign="top"><span class="smalltext"><textarea name="text" cols="70" rows="5" wrap="VIRTUAL" class="form"></textarea></span></td>
							 	</tr>
							 	<tr class="boxcomments1">
									<td width="20%"><span class="smalltext">&nbsp;</span></td>
									<td width="80%"><span class="smalltext"><input name="submit" type="submit" value="Eintragen" class="form"></span></td>
							 	</tr>', '660', 'center', '<form enctype="multipart/form-data" name="form1" method="post" action="handler.php?s=' . $session_obj->GetSession() . '&a=dcusset&goto=login">') .
                            '</td>
						</tr>
						
						<tr>
							<td width="100%" valign="top" height="15"><a href="handler.php?s=' . $session_obj->GetSession() . '&goto=login" class="blacklink">zurück</a></td>
						</tr>
					</table><br>';
            } else {
                $this->AddJavaScript('alert("Error: Keine Befugnis!")');
            }
        }
        
        private function DcusSet($user_obj, $session_obj, $post, $files)
        {
            if ($user_obj->GetFlag() == "admin") {
                $headline = strip_tags($post['headline']);
                $smalltext = strip_tags($post['smalltext']);
                $text = strip_tags($post['text']);
                
                if (!empty($headline) && !empty($smalltext) && !empty($text) && !empty($files['headpic']['name'])) {
                    if ($files['headpic']['size'] < 5242880) {
                        if ($files['headpic']['type'] == "image/pjpeg" || $files['headpic']['type'] == "image/jpeg") {
                            move_uploaded_file($files['headpic']['tmp_name'], img_dcus_dir . $files['headpic']['name']);
                            $headpic = $files['headpic']['name'];
                            
                            // Aktuelle Datum
                            $currentday = date("j", time());
                            $currentmonth = date("n", time());
                            $currentyear = date("Y", time());
                            
                            $db_dcushead_obj = new DBDcusheadClass();
                            $dcushead_obj = new DcusheadClass(0, $session_obj->GetUser_Id(), $headline, $headpic, $smalltext, $currentday . "." . $currentmonth . "." . $currentyear, 0);
                            
                            // DCUS HEAD in die Datenbank eintragen
                            $db_dcushead_obj->Insert($dcushead_obj);
                            
                            $maxid = $db_dcushead_obj->GetLastId();
                            
                            $db_dcusmain_obj = new DBDcusmainClass();
                            $dcusmain_obj = new DcusmainClass(0, $maxid, $text);
                            
                            // DCUS MAIN in die Datenbank eintragen
                            $db_dcusmain_obj->Insert($dcusmain_obj);
                            
                            // DIR Handle
                            $handle = opendir(img_dcustemp_dir);
                            
                            $count = 1;
                            
                            while (false !== ($file = readdir($handle))) {
                                if ($file != "." && $file != "..") {
                                    // Ändern des Dateinamens von $file
                                    $dateiname = $currentyear . $currentmonth . $currentday . $count . ".jpg";
                                    
                                    // Zielort des Bildes
                                    $dateiname_dir = img_dcus_dir . $dateiname;
                                    
                                    // Ausgangsort des Bildes
                                    $file_dir = img_dcustemp_dir . $file;
                                    
                                    // Kopieren des Originals vom Ordner upload_tmp nach upload + Veränderung des Dateinamens
                                    copy($file_dir, $dateiname_dir);
                                    
                                    // DCUS IMG in die Datebank eintragen
                                    $db_dcusimg_obj = new DBDcusimgClass();
                                    $dcusimg_obj = new DcusimgClass(0, $dcushead_obj->GetId(), $dateiname);
                                    $db_dcusimg_obj->Insert($dcusimg_obj);
                                    
                                    $count++;
                                }
                            }
                            closedir($handle);

                            // DCUS Artikel in den News eintragen
                            $db_news_obj = new DBNewsClass();
                            $news_obj = new NewsClass($dcushead_obj->GetHeadline(), "Diese News wurde vom System generiert und dient dazu, einen neuen D.C.U.S. Eintrag vorzustellen. Der Eintrag " . $dcushead_obj->GetHeadline() . " wurde von " . $user_obj->GetLogin() . " erstellt. Bei Interesse einfach unter D.C.U.S. den neuen Eintrag anschauen. Danke.");
                            $news_obj->SetUser_id(86);
                            $db_news_obj->Insert($news_obj);
                            
                            $this->AddJavaScript('alert("Info: Der DCUS Artikel wurde erfolgreich eingetragen!")');
                        } else {
                            $this->AddJavaScript('alert("Error: Das Head Bild hat ein falsches Format! (nur *.jpg)!")');
                        }
                    } else {
                        $this->AddJavaScript('alert("Error: Das Head Bild ist zu groß! (max. 5 MB)")');
                    }
                } else {
                    $this->AddJavaScript('alert("Error: Bitte Eingaben prüfen!")');
                }
            } else {
                $this->AddJavaScript('alert("Error: Keine Befugnis!")');
            }
        }
    }
