<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Page Class
    */

    class PageClass
    {
        protected $body = '';
        protected $java_script = '';
        protected $css = '';
        public $nohtml = false;

        protected function GetHtmlHead()
        {
            return '
			<head>
				<title>:: PlanetBöhmer v3.0 ::</title>
			</head>
			';
        }

        protected function AddJavaScriptFile($file)
        {
            $this->java_script .= "<script language='javascript' type='text/javascript' src='js/".$file.".js'></script>";
        }

        protected function AddJavaScript($java)
        {
            $this->java_script .= "<script language='javascript' type='text/javascript'>".$java."</script>";
        }

        protected function AddCSS($file)
        {
            $this->css .= "<link href='css/".$file.".css' rel='stylesheet' type='text/css'>";
        }

        protected function UseBox1($top, $text, $width = 0, $t_height = 0, $align)
        {
            if (empty($top)) {
                $top = "<br>";
            }
            if ($width != 0) {
                $width_string = "width='".$width."'";
            } else {
                $width_string = "";
            }
            if ($t_height != 0) {
                $height_string = "height='".$t_height."'";
            } else {
                $height_string = "";
            }
            if (empty($align)) {
                $align_string = "align='left'";
            } else {
                $align_string = "align='".$align."'";
            }

            $out = "
				<table cellpadding='0' cellspacing='0' border='0' ".$width_string." ".$align_string.">
					<tr>
						<td>

							<table cellpadding='0' cellspacing='0' border='0' width='100%' class='box'>
								<tr>
									<td colspan='1' height='1' style='background-color: #8C8C8C'></td>
								</tr>
								<tr style='background-color: #5B5B5B'>
									<td align='left' width='100%' ".$height_string."><span class='smalltext' style='color: white'>".$top."</span></td>
								</tr>
							</table>

							<table class='box2' cellpadding='3' cellspacing='1' border='0' width='100%'>
								<tr>
									<td class='smalltext'>".$text."</td>
								</tr>
							</table>

							<table cellpadding='0' cellspacing='0' border='0' width='100%'>
								<tr style='height: 3px;' align='center'>
									<td width='3px'><img alt='' src='img/corner.gif' width='3'></td>
									<td width='100%' style='BACKGROUND-IMAGE: url(img/shade_bottom.gif);'></td>
									<td width='3px'><img alt='' src='img/corner.gif' width='3'></td>
								</tr>
								<tr style='height: 6px;'>
									<td>&nbsp;</td>
								</tr>
							</table>

						</td>
					</tr>
				</table>";

            return $out;
        }

        protected function UseBox2($top, $text, $width = 0, $t_height = 0, $align)
        {
            if (empty($top)) {
                $top = "<br>";
            }
            if ($width != 0) {
                $width_string = "width='".$width."'";
            } else {
                $width_string = "";
            }
            if ($t_height != 0) {
                $height_string = "height='".$t_height."'";
            } else {
                $height_string = "";
            }
            if (empty($align)) {
                $align_string = "align='left'";
            } else {
                $align_string = "align='".$align."'";
            }

            $out = "
				<table cellpadding='0' cellspacing='0' border='0' ".$width_string." ".$align_string.">
					<tr>
						<td>

							<table cellpadding='0' cellspacing='0' border='0' class='box' width='100%'>
								<tr>
									<td colspan='1' height='1' style='background-color: #C9D2E1'></td>
								</tr>
								<tr style='background-color: #A59E70'>
									<td align='left' width='100%' ".$height_string.">
										<span class='smalltext' style='color: white'>".$top."</span>
									</td>
								</tr>
							</table>

							<table class='box2' cellpadding='3' cellspacing='1' border='0' width='100%'>
								<tr>
									<td class='smalltext'>".$text."</td>
								</tr>
							</table>

							<table cellpadding='0' cellspacing='0' border='0' width='100%'>
								<tr style='height: 3px;' align='center'>
									<td width='3px'><img alt='' src='img/corner.gif' width='3'></td>
									<td width='100%' style='BACKGROUND-IMAGE: url(img/shade_bottom.gif);'></td>
									<td width='3px'><img alt='' src='img/corner.gif' width='3'></td>
								</tr>
								<tr style='height: 6px;'>
									<td>&nbsp;</td>
								</tr>
							</table>

						</td>
					</tr>
				</table>";

            return $out;
        }

        protected function UseBox3($top, $headline, $text, $width = 0, $align, $action)
        {
            if (empty($top)) {
                $top = "<br>";
            }
            if ($width != 0) {
                $width_string = "width='".$width."'";
            } else {
                $width_string = "";
            }
            if (empty($align)) {
                $align_string = "align='left'";
            } else {
                $align_string = "align='".$align."'";
            }

            $out = "
				<form method='post' action='".$action."'>

				<table cellpadding='0' cellspacing='0' border='0' ".$width_string." ".$align_string.">
					<tr>
						<td>

							<table cellpadding='0' cellspacing='0' border='0' width='100%' class='box'>
								<tr>
									<td class='home3d' colspan='1' height='1'></td>
								</tr>
								<tr class='home'>
									<td align='left' width='100%' height='19'><span class='smalltext' style='color: white'>&nbsp;<strong>#&nbsp;".$top."</strong></span></td>
								</tr>
							</table>

							<table cellSpacing='2' cellPadding='1' width='100%' class='box1'>
								<tr>
									<td align='left'><span class='smalltext'>".$headline."</span></td>
								</tr>
							</table>

							<table cellSpacing='1' cellPadding='2' width='100%' bgcolor='#CECECE' style='border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black'>
								".$text."
							</table>

							<table cellpadding='0' cellspacing='0' border='0' width='100%'>
								<tr style='height: 3px;' align='center'>
									<td width='3px'><img alt='' src='img/corner.gif' width='3'></td>
									<td width='100%' style='BACKGROUND-IMAGE: url(img/shade_bottom.gif);'></td>
									<td width='3px'><img alt='' src='img/corner.gif' width='3'></td>
								</tr>
								<tr style='height: 6px;'>
									<td></td>
								</tr>
							</table>

						</td>
					</tr>
				</table>

				</form>";

            return $out;
        }

        protected function UseBox4($text, $width = 0, $align)
        {
            if ($width != 0) {
                $width_string = "width='".$width."'";
            } else {
                $width_string = "";
            }
            if (empty($align)) {
                $align_string = "align='left'";
            } else {
                $align_string = "align='".$align."'";
            }

            $out = "
				<table cellpadding='0' cellspacing='0' border='0' ".$width_string." ".$align_string.">
					<tr>
						<td>

							<table class='box3' cellpadding='1' cellspacing='0' border='0' width='315'>
								<tr>
									<td class='smalltext'>".$text."</td>
								</tr>
							</table>

							<table cellpadding='0' cellspacing='0' border='0' width='325'>
								<tr style='height: 3px;' align='center'>
									<td width='3px'><img alt='' src='img/corner.gif' width='3'></td>
									<td width='100%' style='BACKGROUND-IMAGE: url(img/shade_bottom.gif);'></td>
									<td width='3px'><img alt='' src='img/corner.gif' width='3'></td>
								</tr>
								<tr style='height: 6px;'>
									<td>&nbsp;</td>
								</tr>
							</table>

						</td>
					</tr>
				</table>";

            return $out;
        }

        protected function UseBox5($top, $headline, $text, $width = 0, $align, $action, $formname)
        {
            if (empty($top)) {
                $top = "<br>";
            }
            if ($width != 0) {
                $width_string = "width='".$width."'";
            } else {
                $width_string = "";
            }
            if (empty($align)) {
                $align_string = "align='left'";
            } else {
                $align_string = "align='".$align."'";
            }

            $out = "
				<form method='post' action='".$action."' name='".$formname."'>

				<table cellpadding='0' cellspacing='0' border='0' ".$width_string." ".$align_string.">
					<tr>
						<td>

							<table cellpadding='0' cellspacing='0' border='0' width='100%' class='box'>
								<tr>
									<td class='home3d' colspan='1' height='1'></td>
								</tr>
								<tr class='home'>
									<td align='left' width='100%' height='19'><span class='smalltext' style='color: white'>&nbsp;<strong>#&nbsp;".$top."</strong></span></td>
								</tr>
							</table>

							<table cellSpacing='2' cellPadding='1' width='100%' class='box1'>
								<tr>
									<td align='left'><span class='smalltext'>".$headline."</span></td>
								</tr>
							</table>

							<table cellSpacing='1' cellPadding='2' width='100%' bgcolor='#CECECE' style='border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black'>
								".$text."
							</table>

							<table cellpadding='0' cellspacing='0' border='0' width='100%'>
								<tr style='height: 3px;' align='center'>
									<td width='3px'><img alt='' src='img/corner.gif' width='3'></td>
									<td width='100%' style='BACKGROUND-IMAGE: url(img/shade_bottom.gif);'></td>
									<td width='3px'><img alt='' src='img/corner.gif' width='3'></td>
								</tr>
								<tr style='height: 6px;'>
									<td></td>
								</tr>
							</table>

						</td>
					</tr>
				</table>

				</form>";

            return $out;
        }

        protected function UseBox6($top, $headline, $text, $width = 0, $align, $form)
        {
            if (empty($top)) {
                $top = "<br>";
            }
            if ($width != 0) {
                $width_string = "width='".$width."'";
            } else {
                $width_string = "";
            }
            if (empty($align)) {
                $align_string = "align='left'";
            } else {
                $align_string = "align='".$align."'";
            }

            $out = $form."


				<table cellpadding='0' cellspacing='0' border='0' ".$width_string." ".$align_string.">
					<tr>
						<td>

							<table cellpadding='0' cellspacing='0' border='0' width='100%' class='box'>
								<tr>
									<td class='home3d' colspan='1' height='1'></td>
								</tr>
								<tr class='home'>
									<td align='left' width='100%' height='19'><span class='smalltext' style='color: white'>&nbsp;<strong>#&nbsp;".$top."</strong></span></td>
								</tr>
							</table>

							<table cellSpacing='2' cellPadding='1' width='100%' class='box1'>
								<tr>
									<td align='left'><span class='smalltext'>".$headline."</span></td>
								</tr>
							</table>

							<table cellSpacing='1' cellPadding='2' width='100%' bgcolor='#CECECE' style='border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black'>
								".$text."
							</table>

							<table cellpadding='0' cellspacing='0' border='0' width='100%'>
								<tr style='height: 3px;' align='center'>
									<td width='3px'><img alt='' src='img/corner.gif' width='3'></td>
									<td width='100%' style='BACKGROUND-IMAGE: url(img/shade_bottom.gif);'></td>
									<td width='3px'><img alt='' src='img/corner.gif' width='3'></td>
								</tr>
								<tr style='height: 6px;'>
									<td></td>
								</tr>
							</table>

						</td>
					</tr>
				</table>

				</form>";

            return $out;
        }

        public function GetPage()
        {
            $page = "";
            $page .= $this->GetHtmlHead();
            $page .= $this->css;
            $page .= $this->java_script;
            $page .= "<body>";
            $page .= $this->body;
            $page .= "</body></html>";

            return $page;
        }
    }
