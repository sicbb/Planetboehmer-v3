<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: News Class
    */

    require_once "kernel/db_session_class.php";
    require_once "kernel/session_class.php";
    require_once "kernel/db_user_class.php";
    require_once "kernel/user_class.php";
    require_once "kernel/db_news_class.php";
    require_once "kernel/news_class.php";
    require_once "kernel/db_shoutbox_class.php";
    require_once "kernel/shoutbox_class.php";
    require_once "kernel/db_counter_class.php";
    require_once "kernel/counter_class.php";
    require_once "kernel/userstats_class.php";
    require_once "kernel/user_class.php";

    require_once "page_class.php";

    define("news_count", 10);

    class PageNewsClass extends PageClass
    {
        public function __construct($get, $post, $session_obj, $user_obj, $files)
        {
            $db_session_obj = new DBSessionClass();
            $db_shoutbox_obj = new DBShoutboxClass();
            $db_news_obj = new DBNewsClass();
            $db_counter_obj = new DBCounterClass();
            $db_userstats_obj = new DBUserStatsClass();

            if ($session_obj != null) {
                if (!empty($get['a'])) {
                    $action = strip_tags($get['a']);
                    if (empty($action)) {
                        $action = strip_tags($post['a']);
                    }
                } else {
                    $action = "";
                }
                if (empty($get['p'])) {
                    $page = 1;
                } else {
                    $page = strip_tags($get['p']);
                }
            }

            if ($action == "in") {
                $user_id = $post['comuserid'];
                $text = addslashes(trim(strip_tags($post['comtext'])));
                if (strlen($text) < 250 && $text != "Max. 250 Zeichen") {
                    if (!empty($user_id)) {
                        if (!empty($text)) {
                            $new_in = new ShoutboxClass(0, $user_id, $text, time(), $_SERVER['REMOTE_ADDR']);
                            $db_shoutbox_obj->InsertDB($new_in);

                            // Stats holen und die profil_views inkrementieren
                            $stats_obj = $db_userstats_obj->GetAllUserStats($session_obj->GetUser_id());

                            if ($stats_obj != null) {
                                $stats_obj->SetShoutbox_write($stats_obj->GetShoutbox_write()+1);
                                $db_userstats_obj->UpdateUserStats($stats_obj);
                            }
                        } else {
                            $this->AddJavaScript('alert("Error: Bitte geben Sie einen Text ein!")');
                        }
                    } else {
                        $this->AddJavaScript('alert("Error: Ihr Login ist leer oder ungültig!")');
                    }
                } else {
                    $this->AddJavaScript('alert("Error: Ihr Text ist zu lang oder ungültig!")');
                }
            } elseif ($action == "del") {
                $id = strip_tags($get['id']);
                $shoutbox_obj = $db_shoutbox_obj->GetDatById($id);
                if ($shoutbox_obj->GetUser_id() == $session_obj->GetUser_id() || $user_obj != null && $user_obj->GetFlag() == "admin") {
                    $db_shoutbox_obj->DelFromDB($id);
                } else {
                    $this->AddJavaScript('alert("Error: Sie haben keine Berechtigung diesen Eintrag zu löschen!")');
                }
            } elseif ($action == "in_news") {
                //Admin News Einträge
                if ($user_obj->GetFlag() == "admin" || $user_obj->GetFlag() == "special user") {
                    $headline = addslashes(trim($post['headline']));
                    $text = addslashes(trim($post['text']));
                    $tmp_news_obj = new NewsClass($headline, $text, time(), -1, $user_obj->GetId());
                    $db_news_obj->Insert($tmp_news_obj);
                }
            } elseif ($action == "del_news") {
                if ($user_obj->GetFlag() == "admin") {
                    $id = strip_tags($get['id']);
                    $db_news_obj->DeleteById($id);
                }
            }

            // Seitenverwaltung der News
            if ($page == 1) {
                $all_shoutbox = $db_shoutbox_obj->SelectAll(0, news_count);
                $scrolling = '<span class="smalltext"><strong>Seite 1</strong>&nbsp;<a href="handler.php?s='.$session_obj->GetSession().'&p=2&goto=news">-></a></span>';
            } else {
                $all_shoutbox = $db_shoutbox_obj->SelectAll(($page-1)*news_count, news_count);
                $scrolling = '<span class="smalltext"><a href="handler.php?s='.$session_obj->GetSession().'&p='.($page-1).'&goto=news"><-</a> <strong>Seite '.$page.'</strong>';
                if ($db_shoutbox_obj->GetNewsCount()>(($page-1)*news_count)+news_count) {
                    $scrolling .= '&nbsp;<a href="handler.php?s='.$session_obj->GetSession().'&p='.($page+1).'&goto=news">-></a></span>';
                }
            }

            //CSS-Stile werden eingebunden
            $this->AddCSS('style');
            $this->AddCSS('news');
            $this->AddCSS('forms');

            $this->AddJavaScriptFile('common');
            $this->AddJavaScriptFile('main15');

            $this->body .= $this->GetBody($session_obj, $user_obj, $db_news_obj, $db_counter_obj, $all_shoutbox, $scrolling);
        }

        private function GetBody($session_obj, $user_obj, $db_news_obj, $db_counter_obj, $all_shoutbox, $scrolling)
        {
            $count = 9387 + $db_counter_obj->GetCounterCount();
            $date = '';
            $out = '';

            $out .= '
				<DIV id="Shoutbox" style="visibility:hidden;position:absolute;z-index:1000;top:-100"></DIV>
				<script language="JavaScript1.2" type="text/javascript">
				Text[0]=["Shoutbox","Die Eingabe ist unter allen Beiträgen zu sehen. Diese wird aber erst nach dem Einloggen sichtbar."]

				Style[0]=["#ffffff","#5b5b5b","","","verdana",1,"#000000","#cecece","","","verdana",1,160,60,1,"#000000",3,24,0.5,,2,"#999999",,,,]

				var TipId="Shoutbox"
				var FiltersEnabled = 1 // [for IE5.5+] if your not going to use transitions or filters in any of the tips set this to zero.
				mig_clay()
				</script>
			';

            $out .= '
			<table width="650" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td width="450" valign="top">
						<img src="img/menu/news_k.gif" width="100" height="30" border="0" titel="News">
					</td>
					<td width="40" valign="top">&nbsp;</td>
					<td width="160" align="top">
						<a href="" onMouseOver="stm(Text[0],Style[0])" onMouseOut="htm()"><img src="img/menu/shoutbox_k.gif" width="100" height="30" border="0" titel="Shoutbox"></a>
					</td>
				</tr>
				<tr>
					<td width="450" valign="top" height="15">&nbsp;</td>
					<td width="40" valign="top">&nbsp;</td>
					<td width="160" valign="top">&nbsp;</td>
				</tr>
				<tr>
					<td width="450" valign="top">
						<table width="100%" cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td valign="top" class="smalltext">
								'.$this->UseBox2("&nbsp;<strong>#&nbsp;Willkommen auf Planetboehmer.de</strong>, Freigeist Nr. <strong>".$count."</strong> - Heute: <strong>".$db_counter_obj->GetCountPerDay($date)."</strong>", 'Willkommen auf der privaten Homepage von Daniel Böhmer. Optimiert wurde die Seite für eine Auflösung von 1280*1024px. Des Weiteren verfügt Planetboehmer über einen User Bereich mit vielen Funktionen, <a href="#" onclick="javascript:linkz(\'oben.php\', \'topFrame\', \'handler.php?s='.$session_obj->GetSession().'&a=register&goto=login\', \'bottomFrame\', \'menu.php?s='.$session_obj->GetSession().'&id=7\', \'mainFrame\')" class="orangelink">anmelden</a> lohnt sich! Bereits registrierte User können sich auch direkt hier <a href="#" onclick="javascript:linkz(\'oben.php\', \'topFrame\', \'handler.php?s='.$session_obj->GetSession().'&goto=login\', \'bottomFrame\', \'menu.php?s='.$session_obj->GetSession().'&id=7\', \'mainFrame\')" class="orangelink">einloggen.</a> Wunder dich nicht über Rechtschreibfehler! Gut daran ist, sie sind gewollt.', "100%", '19', "left").'
								</td>
							</tr>
						</table><br>

						<table width="100%" cellspacing="0" cellpadding="0" border="0">
									'.$this->GetNews($session_obj, $user_obj, $db_news_obj).'
						</table>
					</td>
					<td width="40" valign="top">&nbsp;</td>
					<td width="160" class="smalltext" valign="top">
						<table width="100%" cellspacing="0" cellpadding="0" border="0">
									'.$this->GetShoutbox($session_obj, $user_obj, $all_shoutbox, $scrolling).'
						</table>
					</td>
				</tr>
			</table>';

            return $out;
        }

        private function GetNews($session_obj, $user_obj, $db_news_obj)
        {
            $text = '';
            $news_array = $db_news_obj->GetAll(10);  // Max. 10 News-Einträge werden angezeigt
            if ($news_array) {
                foreach ($news_array as $news_objs) {
                    if ($user_obj != null && $user_obj->GetFlag() == "admin") {
                        //Autor oder Admin

                        $del = '<A HREF="handler.php?s='.$session_obj->GetSession().'&a=del_news&id='.$news_objs->GetId().'&goto=news"><IMG SRC="img/delete.gif" WIDTH="8" HEIGHT="8" BORDER="0" ALT="Löschen" TITLE="Löschen"></A>';
                    } else {
                        $del = "";
                    }

                    if ($news_objs->GetDate()+60*60*24*2 > time()) {
                        $ticker = "<font color='red'>*new*</font>";
                    } else {
                        $ticker = "";
                    }

                    $text .= "<tr><td>".$this->UseBox1('&nbsp;'.date("d.m.Y", $news_objs->GetDate()).' <strong>'.stripslashes(htmlentities($news_objs->GetHeadline())).'</strong> '.$ticker, stripslashes(nl2br($news_objs->GetText())).' '.$del, '100%', '19', 'left')."</td></tr>";
                }

                if ($user_obj != null) {
                    if ($user_obj->GetFlag() == "admin" || $user_obj->GetFlag() == "special user") {
                        $text .= "<tr><td>".$this->UseBox3('Newseingabe: ', 'Hier gibt der Admin neue News ein.',
                        '<tr class="boxcomments1">
						 	<td valign="top"><span class="smalltext">Headline</span></td>
							<td><input name="headline" type="text" id="name" class="form" size="35"></td>
		  				 </tr>
		  				 <tr class="boxcomments1">
						 	<td valign="top"><span class="smalltext">Text</span></td>
							<td><textarea name="text" rows="4" cols="55" class="form"></textarea></td>
		  				 </tr>
		  				 <tr class="boxcomments1">
						 	<td>&nbsp;</td>
							<td><input type="submit" name="submit" value="Eintragen" class="form"></td>
		  				 </tr>', '450', 'left', 'handler.php?s='.$session_obj->GetSession().'&a=in_news&goto=news')."</td></tr>";
                    }
                }

                return $text;
            }
        }

        private function GetShoutbox($session_obj, $user_obj, $all_shoutbox, $scrolling)
        {
            $text = '';
            if ($all_shoutbox) {
                foreach ($all_shoutbox as $shoutbox_obj) {
                    if ($session_obj->GetUser_id() == $shoutbox_obj->GetUser_id() || $user_obj != null && $user_obj->GetFlag() == "admin") {
                        $del = '<A HREF="handler.php?s='.$session_obj->GetSession().'&a=del&id='.$shoutbox_obj->GetId().'&goto=news"><IMG SRC="img/delete.gif" WIDTH="8" HEIGHT="8" BORDER="0" ALT="Löschen" TITLE="Löschen"></A>';
                    } else {
                        $del = "";
                    }

                    $db_user_obj = new DBUserClass();
                    $user_login = $db_user_obj->GetOnlyUserLoginById($shoutbox_obj->GetUser_id());

                    $text .= "<tr><td>".$this->UseBox1('&nbsp;<strong>'.wordwrap($user_login, 25, '\n', 1).'</strong> '.date("d.m. - H:i", $shoutbox_obj->GetDate()), stripslashes(nl2br($shoutbox_obj->GetKommentar())).' '.$del, '100%', '10', 'left')."</td></tr>";
                }
                if ($user_obj != null) {
                    if ($session_obj->GetUser_id() > 0) {
                        $text .= "<tr><td>".$this->UseBox3('<strong>Shoutboxeingabe:</strong>', 'Beiträge schreiben.',
                        '<tr class="boxcomments1">
							<input name="comuserid" type="hidden" id="comuserid" value="'.$user_obj->GetId().'">
							<td class="smalltext"><input type="text" class="form" size="26" value="'.$user_obj->GetLogin().'" disabled></td>
		  				 </tr>
		  				 <tr class="boxcomments1">
							<td class="smalltext"><input name="comtext" type="text" id="comtext" maxlength="250" class="form" size="26" onClick="if(comtext.value==\'Max. 250 Zeichen\')comtext.value=\'\'" onFocus="if(comtext.value==\'Max. 250 Zeichen\')kommentar.value=\'\'" value="Max. 250 Zeichen"></td>
		  				 </tr>
		  				 <tr class="boxcomments1">
							<td><input type="submit" name="submit" value="Eintragen" class="form"></td>
		  				 </tr>', '100%', 'left', 'handler.php?s='.$session_obj->GetSession().'&a=in&goto=news')."</td></tr>";
                    }
                }

                $text .= "<tr><td>".$scrolling."</td></tr>";

                return $text;
            }
        }
    }
