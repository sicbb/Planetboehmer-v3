<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Special Class
    */

    require_once "kernel/db_session_class.php";
    require_once "kernel/session_class.php";
    require_once "kernel/db_user_class.php";
    require_once "kernel/user_class.php";
    require_once "kernel/counter_class.php";
    require_once "kernel/db_counter_class.php";
    require_once "kernel/cocktails_class.php";
    require_once "kernel/db_cocktails_class.php";
    require_once "page_class.php";
    require_once "kernel/constant.php";

    class PageSpecialClass extends PageClass
    {
        public function __construct($get, $post, $session_obj, $user_obj, $files)
        {
            $db_session_obj = new DBSessionClass();

            if ($session_obj != null) {
                if (!empty($get['a'])) {
                    $action = strip_tags($get['a']);
                    if (empty($action)) {
                        $action = strip_tags($post['a']);
                    }
                } else {
                    $action = "";
                }
            }

            //CSS-Stile werden eingebunden
            $this->AddCSS('news');
            $this->AddCSS('forms');

            // JAVA einbinden
            $this->AddJavaScriptFile('common');
            $this->AddJavaScriptFile('centerwindow');

            if ($action == "getcounter") {
                if (!empty($get['day']) && !empty($get['month']) && !empty($get['year'])) {
                    $date['day'] = $get['day'];
                    $date['month'] = $get['month'];
                    $date['year'] = $get['year'];
                } else {
                    $date['day'] = date("d", time());
                    $date['month'] = date("m", time());
                    $date['year'] = date("Y", time());
                }

                $this->body .= $this->GetCounter($session_obj, $date);
            } elseif ($action == "getchat") {
                $this->body .= $this->GetChat($session_obj);
            } elseif ($action == "getcocktails") {
                if (empty($get['order'])) {
                    $order = "name";
                } else {
                    $order = $get['order'];
                }
                $this->body .= $this->GetCocktails($session_obj, $order);
            } elseif ($action == "showcocktail") {
                $id = $get['id'];
                $this->body .= $this->ShowCocktailById($session_obj, $id);
            } elseif ($action == "getateam") {
                $this->body .= $this->GetATeamBody($session_obj);
            } else {
                $this->body .= $this->GetBody($session_obj);
            }
        }

        private function GetBody($session_obj)
        {
            return '
				<table width="650" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td width="100%" valign="top">
							<img src="img/menu/special_k.gif" width="100" height="30" border="0" titel="News">
						</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15" class="smalltext"><strong>Die Special Sektion.</strong><br>Hier werden besondere Planetboehmer Funktionen und Features aufgelistet. Es kann unter umständen sein, dass man einen eingeloggten User Account für bestimmte Bereiche braucht!</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%" valign="top">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								'.$this->GetSpecial($session_obj).'
							</table>
						</td>
					</tr>
				</table>
			';
        }

        private function GetSpecial($session_obj)
        {
            /*
            $text .= "<tr><td>
                " . $this->UseBox2('<strong>&nbsp;#&nbsp;Umbau!</strong>', 'Diese Sektion befindet sich im Umbau!', '500', '19', 'center') . "
                <td></tr>";
            */

            $text = '
				<tr><td>
				<table width="660" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td width="320">

							<!-- ANFANG -->
							<table cellpadding="0" cellspacing="0" border="0" width="320" class="box">
								<tr>
									<td colspan="1" height="1" style="background-color: #8C8C8C"></td>
								</tr>

								<tr style="background-color: #5B5B5B">
									<td align="left" width="100%" height="19">
										<span class="smalltext" style="color: white">&nbsp;#&nbsp;<a href="handler.php?s='.$session_obj->GetSession().'&goto=special&a=getcounter" class="whitelink">Planetboehmer Statistiken.</a></span>
									</td>
								</tr>
							</table>

							<table class="box2" cellpadding="0" cellspacing="0" border="0" width="320">
								<tr>
									<td class="smalltext">
										<table width="315" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="110" height="110" style="border-right: 1px solid #000000;"><a href="handler.php?s='.$session_obj->GetSession().'&goto=special&a=getcounter"><img src="img/stats.jpg" width="110" height="110" border="0" title="Planetboehmer Statistiken" alt="Planetboehmer Statistiken"></a></td>
												<td valign="top" class="smalltext">Diese Funktion zeigt Informationen &uuml;ber die Besucher der Seite.</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>

							<table cellpadding="0" cellspacing="0" border="0" width="320">
								<tr style="height: 3px;" align="center">
									<td width="3px"><img alt="" src="img/corner.gif" width="3"></td>
									<td width="100%" style="BACKGROUND-IMAGE: url(img/shade_bottom.gif);"></td>
									<td width="3px"><img alt="" src="img/corner.gif" width="3"></td>
								</tr>
								<tr style="height: 6px;">
									<td></td>
								</tr>
							</table>
							<!-- ENDE -->

						</td>
						<td width="20"></td>
						<td width="320">

							<!-- ANFANG -->
							<table cellpadding="0" cellspacing="0" border="0" width="320" class="box">
								<tr>
									<td colspan="1" height="1" style="background-color: #8C8C8C"></td>
								</tr>

								<tr style="background-color: #5B5B5B">
									<td align="left" width="100%" height="19">
										<span class="smalltext" style="color: white">&nbsp;#&nbsp;<a href="#" onclick="linkz(\'oben.php\',\'topFrame\',\'handler.php?s='.$session_obj->GetSession().'&goto=dcus\',\'bottomFrame\',\'menu.php?s='.$session_obj->GetSession().'&id=4\',\'mainFrame\')" class="whitelink">Party Bilder.</a></span>
									</td>
								</tr>
							</table>

							<table class="box2" cellpadding="0" cellspacing="0" border="0" width="320">
								<tr>
									<td class="smalltext">
										<table width="315" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="110" height="110" style="border-right: 1px solid #000000;"><a href="#" onclick="linkz(\'oben.php\',\'topFrame\',\'handler.php?s='.$session_obj->GetSession().'&goto=dcus\',\'bottomFrame\',\'menu.php?s='.$session_obj->GetSession().'&id=4\',\'mainFrame\')" class="whitelink"><img src="img/havana1.jpg" width="110" height="110" border="0" title="Party Bilder" alt="Party Bilder"></a></td>
												<td valign="top" class="smalltext">Die wildesten Bilder von verschiedenen Events. Reinschauen lohnt sich!</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>

							<table cellpadding="0" cellspacing="0" border="0" width="320">
								<tr style="height: 3px;" align="center">
									<td width="3px"><img alt="" src="img/corner.gif" width="3"></td>
									<td width="100%" style="BACKGROUND-IMAGE: url(img/shade_bottom.gif);"></td>
									<td width="3px"><img alt="" src="img/corner.gif" width="3"></td>
								</tr>
								<tr style="height: 6px;">
									<td></td>
								</tr>
							</table>
							<!-- ENDE -->

						</td>
					</tr>
					<tr>
						<td width="320">

							<!-- ANFANG -->
							<table cellpadding="0" cellspacing="0" border="0" width="320" class="box">
								<tr>
									<td colspan="1" height="1" style="background-color: #8C8C8C"></td>
								</tr>

								<tr style="background-color: #5B5B5B">
									<td align="left" width="100%" height="19">
										<span class="smalltext" style="color: white">&nbsp;#&nbsp;<a href="#" onclick="javascript:MitteFenster(\'handler.php?s='.$session_obj->GetSession().'&goto=special&a=getchat\', 700, 650)" class="whitelink">Planetboehmer Java Chat.</a></span>
									</td>
								</tr>
							</table>

							<table class="box2" cellpadding="0" cellspacing="0" border="0" width="320">
								<tr>
									<td class="smalltext">
										<table width="315" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="110" height="110" style="border-right: 1px solid #000000;"><a href="#" onclick="javascript:MitteFenster(\'handler.php?s='.$session_obj->GetSession().'&goto=special&a=getchat\', 700, 650)" class="whitelink"><img src="img/i_chaticon4.gif" width="110" height="110" border="0" title="Planetboehmer Java Chat" alt="Planetboehmer Java Chat"></a></td>
												<td valign="top" class="smalltext">Hier kann man in einen Live Chat eintreten und mit Leuten schreiben. Der Java Chat benutzt einen IRC Server und verfügt über die Standard IRC Befehle.</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>

							<table cellpadding="0" cellspacing="0" border="0" width="320">
								<tr style="height: 3px;" align="center">
									<td width="3px"><img alt="" src="img/corner.gif" width="3"></td>
									<td width="100%" style="BACKGROUND-IMAGE: url(img/shade_bottom.gif);"></td>
									<td width="3px"><img alt="" src="img/corner.gif" width="3"></td>
								</tr>
								<tr style="height: 6px;">
									<td></td>
								</tr>
							</table>
							<!-- ENDE -->

						</td>
						<td width="20"></td>
						<td width="320">

							<!-- ANFANG -->
							<table cellpadding="0" cellspacing="0" border="0" width="320" class="box">
								<tr>
									<td colspan="1" height="1" style="background-color: #8C8C8C"></td>
								</tr>

								<tr style="background-color: #5B5B5B">
									<td align="left" width="100%" height="19">
										<span class="smalltext" style="color: white">&nbsp;#&nbsp;<a href="handler.php?s='.$session_obj->GetSession().'&goto=special&a=getcocktails" class="whitelink">Die Cocktailbar.</a></span>
									</td>
								</tr>
							</table>

							<table class="box2" cellpadding="0" cellspacing="0" border="0" width="320">
								<tr>
									<td class="smalltext">
										<table width="315" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="110" height="110" style="border-right: 1px solid #000000;"><a href="handler.php?s='.$session_obj->GetSession().'&goto=special&a=getcocktails" class="whitelink"><img src="img/cocktail.jpg" width="110" height="110" border="0" title="Party Bilder" alt="Party Bilder"></a></td>
												<td valign="top" class="smalltext">Da mich doch einige Leute drengten mal ein paar Cocktails aufzulisten, wandelte ich diese Bitte in die Tat um und heraus kam diese Sektion ;) Viel Spaß beim reinschauen.</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>

							<table cellpadding="0" cellspacing="0" border="0" width="320">
								<tr style="height: 3px;" align="center">
									<td width="3px"><img alt="" src="img/corner.gif" width="3"></td>
									<td width="100%" style="BACKGROUND-IMAGE: url(img/shade_bottom.gif);"></td>
									<td width="3px"><img alt="" src="img/corner.gif" width="3"></td>
								</tr>
								<tr style="height: 6px;">
									<td></td>
								</tr>
							</table>
							<!-- ENDE -->

						</td>
					</tr>
					<tr>
						<td width="320">

							<!-- ANFANG -->
							<table cellpadding="0" cellspacing="0" border="0" width="320" class="box">
								<tr>
									<td colspan="1" height="1" style="background-color: #8C8C8C"></td>
								</tr>

								<tr style="background-color: #5B5B5B">
									<td align="left" width="100%" height="19">
										<span class="smalltext" style="color: white">&nbsp;#&nbsp;<a href="handler.php?s='.$session_obj->GetSession().'&a=getateam&goto=special" class="whitelink">Memberdaten der D.C.U.S. Leute.</a></span>
									</td>
								</tr>
							</table>

							<table class="box2" cellpadding="0" cellspacing="0" border="0" width="320">
								<tr>
									<td class="smalltext">
										<table width="315" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="110" height="110" style="border-right: 1px solid #000000;"><a href="handler.php?s='.$session_obj->GetSession().'&a=getateam&goto=special" class="whitelink"><img src="img/ateam.jpg" width="110" height="110" border="0" title="Memberdaten der D.C.U.S. Leute." alt="Memberdaten der D.C.U.S. Leute."></a></td>
												<td valign="top" class="smalltext">Hier befinden sich einige Daten über jene Leute, die sich gemeinsam ins Partyleben stürzen und so Gott will auch lebend zurückkehren.<br><br>Bitte nicht füttern!</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>

							<table cellpadding="0" cellspacing="0" border="0" width="320">
								<tr style="height: 3px;" align="center">
									<td width="3px"><img alt="" src="img/corner.gif" width="3"></td>
									<td width="100%" style="BACKGROUND-IMAGE: url(img/shade_bottom.gif);"></td>
									<td width="3px"><img alt="" src="img/corner.gif" width="3"></td>
								</tr>
								<tr style="height: 6px;">
									<td></td>
								</tr>
							</table>
							<!-- ENDE -->

						</td>
						<td width="20"></td>
						<td width="320">

							&nbsp

						</td>
					</tr>
				</table>
				<td></tr>
			';

            return $text;
        }

        private function GetCounter($session_obj, $date)
        {
            $currentday = date("d", time());
            $currentmonth = date("m", time());
            $currentyear = date("Y", time());

            $days_per_month = cal_days_in_month(CAL_GREGORIAN, $date['month'], $date['year']);

            // Nextday
            $nextday = $date['day'] + 1;
            $nextmonth = $date['month'];
            $nextyear = $date['year'];

            if ($nextday > $days_per_month) {
                $nextday = 1;
                $nextmonth++;

                if ($nextmonth > 12) {
                    $nextmonth = 1;
                    $nextyear++;
                }
            }

            // Lastday
            $lastday = $date['day'] - 1;
            $lastmonth = $date['month'];
            $lastyear = $date['year'];

            if ($lastday < 1) {
                $lastmonth -= 1;

                if ($lastmonth < 1) {
                    $lastmonth = 12;
                    $lastyear -= 1;
                    $lastday = cal_days_in_month(CAL_GREGORIAN, $lastmonth, $lastyear);
                } else {
                    $lastday = cal_days_in_month(CAL_GREGORIAN, $lastmonth, $lastyear);
                }
            }

            $db_counter_obj = new DBCounterClass();

            $count = 9387 + $db_counter_obj->GetCounterCount();

            $main2 = '
				<table width="600" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td class="smalltext">User Heute: <b>'.$db_counter_obj->GetCountPerDay($date).'</b></td>
					</tr>
					<tr>
						<td class="smalltext">User pro Monat: <b>'.$db_counter_obj->GetCountPerMonth($date).'</b></td>
					</tr>
					<tr>
						<td class="smalltext">User Insg.: <b>'.$count.'</b></td>
					</tr>
				</table>
			';

            $main = '
				<table width="600" cellspacing="0" cellpadding="0" border="0">
					<tr>
			';

            for ($i = 0; $i<24; $i++) {
                $main .= "<td class='smalltext' align='center'><strong>".$i."</strong></td>";
            }

            $main .= '
					</tr>
					<tr>
			';

            for ($i = 0; $i<24; $i++) {
                //$count_per_hour = $db_counter_obj->GetCounterCountPerHour(date("d", time()), date("m", time()), date("Y", time()), $i);
                $count_per_hour = $db_counter_obj->GetCounterCountPerHour($date['day'], $date['month'], $date['year'], $i);

                if ($count_per_hour != 0) {
                    $main .= '
							<td align="center" class="smalltext" valign="top">
					';

                    for ($j = 0; $j<$count_per_hour; $j++) {
                        $main .= '
							<img src="img/balken_h.gif" style="padding-top: 1px"><br>
						';
                    }

                    $main .= '</td>';
                } else {
                    $main .= '<td>&nbsp;</td>';
                }
            }
            $main .= '
					</tr>
					<tr>
						<td class="smalltext" colspan="24">Von 0-23 Uhr&nbsp;&nbsp;<img src="img/zeiger.gif" width="7" height="8" border="0"> mehr</td>
					</tr>
				</table>
			';

            // Blätternavigation (gestern - morgen)
            if ($date['day'] == $currentday && $date['month'] == $currentmonth && $date['year'] == $currentyear) {
                $datenav = '
					<tr>
						<td align="center"><a href="handler.php?s='.$session_obj->GetSession().'&goto=special&a=getcounter&day='.$lastday.'&month='.$lastmonth.'&year='.$lastyear.'" class="blacklink"><img src="img/zeiger_links.gif" width="8" height="7" border="0" alt="gestern" title="gestern"> gestern</a></td>
					</tr>
				';
            } else {
                $datenav = '
					<tr>
						<td align="center"><a href="handler.php?s='.$session_obj->GetSession().'&goto=special&a=getcounter&day='.$lastday.'&month='.$lastmonth.'&year='.$lastyear.'" class="blacklink"><img src="img/zeiger_links.gif" width="8" height="7" border="0" alt="gestern" title="gestern"> gestern</a> - <a href="handler.php?s='.$session_obj->GetSession().'&goto=special&a=getcounter&day='.$nextday.'&month='.$nextmonth.'&year='.$nextyear.'" class="blacklink">morgen <img src="img/zeiger_rechts.gif" width="8" height="7" border="0" alt="morgen" title="morgen"></a></td>
					</tr>
				';
            }

            $text = '
				<table width="650" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td width="100%" valign="top">
							<img src="img/menu/stats_k.gif" width="100" height="30" border="0" titel="News">
						</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15" class="smalltext"><strong>Statistik.</strong><br>Hier sehen Sie detaillierte Informationen über die Zugriffe auf der Planetboehmer Seite.</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%" valign="top">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<tr>
									<td>'.$this->UseBox2('<strong>&nbsp;#&nbsp;Besucherzahlen</strong>', $main2, '600', '19', 'center').'<td>
								</tr>
								<tr>
									<td>'.$this->UseBox2('<strong>&nbsp;#&nbsp;Stats vom '.$date['day'].'.'.$date['month'].'.'.$date['year'].'</strong>', $main, '600', '19', 'center').'<td>
								</tr>
								'.$datenav.'
								<tr>
									<td><a href="handler.php?s='.$session_obj->GetSession().'&goto=special" class="blacklink">zurück</a></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			';

            return $text;
        }

        private function GetChat($session_obj)
        {
            $db_user_obj = new DBUserClass();
            $user_obj = $db_user_obj->GetUserById($session_obj->GetUser_id());

            if ($user_obj == null) {
                $user = "Guest???";
            } else {
                $user = $user_obj->GetLogin();
            }

            $main = '
				<tr>
					<td>

						<table cellpadding="0" cellspacing="0" border="0" width="100%" class="box">
							<tr>
								<td colspan="1" height="1" style="background-color: #8C8C8C"></td>
							</tr>
							<tr style="background-color: #5B5B5B">
								<td align="left" width="100%" height="19"><span class="smalltext" style="color: white"><b>&nbsp;#&nbsp;Planetboehmer Java Chat</b></span></td>
							</tr>
						</table>

						<table class="box2" cellpadding="3" cellspacing="1" border="0" width="100%">
							<tr>
								<td class="smalltext"><applet code=IRCApplet.class archive="irc.jar" width=640 height=400 codebase="code/pjirc/">
									<param name="CABINETS" value="irc.cab,securedirc-unsigned.cab">
									<param name="nick" value="'.$user.'">
									<param name="name" value="Planetboehmer User">
									<param name="host" value="server.bb-network.de">
									<param name="port" value="6667">
									<!param name="password" value="">
									<param name="command1" value="join #planetboehmer">
									<param name="command2" value="">
									<!param name="basecolor" value="384,256,128">
									<!param name="color15" value="C0C000">
									<param name="helppage" value="http://www.IRC-Mania.de/faq.php">
									<!param name="timestamp" value="true">
									<param name="language" value="german">
									<param name="smileys" value="true">
									<param name="highlight" value="true">
									<param name="highlightnick" value="true">
									<param name="quitmessage" value="Planetboehmer WebChat">
									<param name="asl" value="true">
									<param name="aslmale" value="h">
									<param name="aslfemale" value="f">
									<param name="bitmapsmileys" value="true">
									<param name="smiley1" value=":) img/sourire.gif">
									<param name="smiley2" value=":-) img/sourire.gif">
									<param name="smiley3" value=":-D img/content.gif">
									<param name="smiley4" value=":d img/content.gif">
									<param name="smiley5" value=":-O img/OH-2.gif">
									<param name="smiley6" value=":o img/OH-1.gif">
									<param name="smiley7" value=":-P img/langue.gif">
									<param name="smiley8" value=":p img/langue.gif">
									<param name="smiley9" value=";-) img/clin-oeuil.gif">
									<param name="smiley10" value=";) img/clin-oeuil.gif">
									<param name="smiley11" value=":-( img/triste.gif">
									<param name="smiley12" value=":( img/triste.gif">
									<param name="smiley13" value=":-| img/OH-3.gif">
									<param name="smiley14" value=":| img/OH-3.gif">
									<param name="smiley15" value=":"( img/pleure.gif">
									<param name="smiley16" value=":$ img/rouge.gif">
									<param name="smiley17" value=":-$ img/rouge.gif">
									<param name="smiley18" value="(H) img/cool.gif">
									<param name="smiley19" value="(h) img/cool.gif">
									<param name="smiley20" value=":-@ img/enerve1.gif">
									<param name="smiley21" value=":@ img/enerve2.gif">
									<param name="smiley22" value=":-S img/roll-eyes.gif">
									<param name="smiley23" value=":s img/roll-eyes.gif">
									<!param name="channelfont" value="16 Arial">
									<!param name="chanlistfont" value="16 Arial">
									<param name="nickfield" value="true">
									<param name="useinfo" value="true">
									<!param name="chanlisttextcolor0" value="FF00FF">
									<!param name="defaultsourcetextcolor0" value="00FF00">
									<!param name="sourcecolorconfig1" value="status 0 000000">
									<!param name="sourcecolorconfig2" value="#IRC-Mania 1 FFFFFF">
									<!param name="soundbeep" value="snd/bell2.au">
									<param name="soundquery" value="snd/ding.au">
									<h1>Java - IRC - Client</h1>
									<p>Sorry, aber Du musst Java installiert und unterst&uuml;tzt haben. N&auml;here Informationen und DownloadM&ouml;glichkeiten findest Du unter <a href="http://www.IRC-Mania.de/java.php">http://www.IRC-Mania.de/java.php</a> <a href="http://www.IRC-Mania.de">IRC-Mania.de</a></p>
									</applet>
								</td>
							</tr>
						</table>

						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr style="height: 3px;" align="center">
								<td width="3px"><img alt="" src="img/corner.gif" width="3"></td>
								<td width="100%" style="BACKGROUND-IMAGE: url(img/shade_bottom.gif);"></td>
								<td width="3px"><img alt="" src="img/corner.gif" width="3"></td>
							</tr>
							<tr style="height: 6px;">
								<td></td>
							</tr>
						</table>

						<br><div align="center">-<a href="javascript:window.close()" class="blacklink"> Fenster schließen </a>-</div>

					</td>
				</tr>
			';

            $text = '
				<table width="650" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td width="100%" valign="top">
							<img src="img/menu/chat_k.gif" width="100" height="30" border="0" titel="News">
						</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15" class="smalltext"><strong>Planetboehmer Java Chat.</strong><br>Hier kann man in einen Java Live Chat eintreten und mit Leuten interaktiv schreiben. Der Java Chat benutzt einen IRC Server und verfügt über die Standard IRC Befehle. Der offizielle Channel lautet #planetboehmer.<br><font class="orangelink">*Wichtig* </font>Falls Sie kein Java installiert haben, müssen Sie es vorher noch <a href="http://java.com/en/download/index.jsp" target="_blank" class="orangelink">runterladen</a> und installieren.</font></td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%" valign="top">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								'.$main.'
							</table>
						</td>
					</tr>
				</table>
			';

            return $text;
        }

        private function GetCocktails($session_obj, $order)
        {
            $db_cocktails_obj = new DBCocktailsClass();
            $cocktails_objs = $db_cocktails_obj->GetAllCocktails($order);

            if ($cocktails_objs != null) {
                $main = '
					<!-- Ausgabe Start -->

					<table cellpadding="0" cellspacing="0" border="0" width="660px">
						<tr>
							<td>

								<table cellpadding="0" cellspacing="0" border="0" width="100%" class="box">
									<tr>
										<td class="home3d" colspan="5" height="1"></td>
									</tr>
									<tr class="home">
										<td align="left" width="30%" height="19"><span class="smalltext" style="color: white">&nbsp;<a href="handler.php?s='.$session_obj->GetSession().'&goto=special&a=getcocktails&order=name" class="whitelink">Name <img src="img/sort.gif" width="15" height="9" border="0"></a></span></td>
										<td align="left" width="20%" height="19"><span class="smalltext" style="color: white">&nbsp;<a href="handler.php?s='.$session_obj->GetSession().'&goto=special&a=getcocktails&order=art" class="whitelink">Art <img src="img/sort.gif" width="15" height="9" border="0"></a></span></td>
										<td align="left" width="15%" height="19"><span class="smalltext" style="color: white">&nbsp;<a href="handler.php?s='.$session_obj->GetSession().'&goto=special&a=getcocktails&order=menge" class="whitelink">M <img src="img/sort.gif" width="15" height="9" border="0"></a></span></td>
										<td align="left" width="15%" height="19"><span class="smalltext" style="color: white">&nbsp;<a href="handler.php?s='.$session_obj->GetSession().'&goto=special&a=getcocktails&order=alkoholgehalt" class="whitelink">A <img src="img/sort.gif" width="15" height="9" border="0"></a></span></td>
										<td align="left" width="20%" height="19"><span class="smalltext" style="color: white">&nbsp;<a href="handler.php?s='.$session_obj->GetSession().'&goto=special&a=getcocktails&order=geschmack" class="whitelink">Geschmack <img src="img/sort.gif" width="15" height="9" border="0"></a></span></td>
									</tr>
								</table>

								<table cellSpacing="2" cellPadding="1" width="100%" class="box1">
									<tr>
										<td align="left"><span class="smalltext">Hier werden nun alle Cocktails aufgelistet. </span></td>
									</tr>
								</table>

								<table cellSpacing="1" cellPadding="2" width="100%" bgcolor="#CECECE" style="border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black">
				';

                foreach ($cocktails_objs as $cocktails_obj) {
                    $main .= '
										<tr class="boxcomments1">
											<td width="30%"><span class="smalltext"><a href="handler.php?s='.$session_obj->GetSession().'&goto=special&a=showcocktail&id='.$cocktails_obj->GetId().'" class="blacklink">'.$cocktails_obj->GetName().'</a></span></td>
											<td width="20%"><span class="smalltext">'.$cocktails_obj->GetArt().'</span></td>
											<td width="15%"><span class="smalltext">'.$cocktails_obj->GetMenge().'</span></td>
											<td width="15%"><span class="smalltext">'.$cocktails_obj->GetAlkoholgehalt().'</span></td>
											<td width="20%"><span class="smalltext">'.$cocktails_obj->GetGeschmack().'</span></td>
										</tr>
					';
                }

                $main .= '
								</table>

								<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<tr style="height: 3px;" align="center">
										<td width="3px"><img alt="" src="img/corner.gif" width="3"></td>
										<td width="100%" style="BACKGROUND-IMAGE: url(img/shade_bottom.gif);"></td>
										<td width="3px"><img alt="" src="img/corner.gif" width="3"></td>
									</tr>
									<tr style="height: 6px;">
										<td></td>
									</tr>
								</table>

							</td>
						</tr>
						<tr>
							<td><a href="handler.php?s='.$session_obj->GetSession().'&goto=special" class="blacklink">zurück</a></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
					</table>

					<!-- Ausgabe Ende -->
				';
            } else {
                $main = 'Keine Cocktails vorhanden!';
            }

            $text = '
				<table width="650" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td width="100%" valign="top">
							<img src="img/menu/cocktails_k.gif" width="100" height="30" border="0" titel="News">
						</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15" class="smalltext"><strong>Cocktail Sektion.</strong><br>Hier findet man verschiedene Cocktails die am häufigsten getrunken werden.<br>Ein einfacher Klick auf den jeweiligen Cocktail genügt, um alle relevanten Informationen zu erhalten.<br>Falls jemand einen Cocktail vermisst, kann er mir gerne sagen welchen! Weitere folgen sicher.</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%" valign="top">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								'.$main.'
							</table>
						</td>
					</tr>
				</table>
			';

            return $text;
        }

        private function ShowCocktailById($session_obj, $id)
        {
            $db_cocktails_obj = new DBCocktailsClass();
            $cocktail_obj = $db_cocktails_obj->GetCocktailById($id);

            if ($cocktail_obj != null) {
                $cocktailname = "<strong>&nbsp;#&nbsp;".$cocktail_obj->GetName()."</strong>";

                $main = '
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="smalltext" width="225" valign="top"><b>Zutaten:</b><br>'.nl2br($cocktail_obj->GetZutaten()).'</td>
							<td class="smalltext" width="225" valign="top"><b>Infos:</b><br><b>Art: </b>'.$cocktail_obj->GetArt().'<br><b>Menge: </b>'.$cocktail_obj->GetMenge().'<br><b>Alkoholgehalt: </b>'.$cocktail_obj->GetAlkoholgehalt().' %<br><b>Geschmack: </b>'.$cocktail_obj->GetGeschmack().'</td>
							<td rowspan="4" width="200" valign="top"><img src="img/cocktails/'.$cocktail_obj->GetPic().'" alt="'.$cocktail_obj->GetName().'" title="'.$cocktail_obj->GetName().'" style="border: 1px solid black"></td>
						</tr>
						<tr>
							<td colspan="2" width="450" class="smalltext" valign="top"><b>Beschreibung:</b><br>'.nl2br($cocktail_obj->GetBeschreibung()).'</td>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
					</table>
				';

                $box = '
					<tr>
						<td>'.$this->UseBox2($cocktailname, $main, '650', '19', 'left').'<td>
					</tr>
					<tr>
						<td><a href="handler.php?s='.$session_obj->GetSession().'&goto=special&a=getcocktails" class="blacklink">zurück</a></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				';
            } else {
                $main = 'Fehler bei der Suche des Cocktails!';
            }

            $text = '
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td width="100%" valign="top">
							<img src="img/menu/cocktails_k.gif" width="100" height="30" border="0" titel="News">
						</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15" class="smalltext"><strong>Cocktail Sektion.</strong><br>Hier findet man verschiedene Cocktails die am häufigsten getrunken werden.<br>Ein einfacher Klick auf den jeweiligen Cocktail genügt, um alle relevanten Informationen zu erhalten.<br>Falls jemand einen Cocktail vermisst, kann er mir gerne sagen welchen! Weitere folgen sicher.</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%" valign="top">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								'.$box.'
							</table>
						</td>
					</tr>
				</table>
			';

            return $text;
        }

        private function GetATeamBody($session_obj)
        {
            return '
				<table width="660" cellspacing="0" cellpadding="0" border="0" align="center">
					<tr>
						<td width="100%" valign="top">
							<img src="img/menu/crew_k.gif" width="100" height="30" border="0" titel="News">
						</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15" class="smalltext"><strong>Die D.C.U.S. Hall of Fame.</strong><br>Hier sind jene aufgelistet die den Mut hatten, im Profil ein Häkchen bei "Anzeigen" zu setzen ;)</td>
					</tr>
					<tr>
						<td width="100%" valign="top" height="15">&nbsp;</td>
					</tr>
					<tr>
						<td width="100%" valign="top">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								'.$this->GetATeam($session_obj).'
							</table>
						</td>
					</tr>
				</table><br>
			';
        }

        private function CalcPicture(&$width, &$height)
        {
            $std_w = 150;
            $std_h = 150;

            $calc = false;

            if ($width > $height && $width > $std_w) {
                $calc = true;
            } elseif ($height > $width && $height > $std_h) {
                $calc = true;
            } elseif ($width == $height && $width > $std_w) {
                $calc = true;
            }

            if ($calc == true) {
                if ($width > $height) {
                    $verh = $width / $height;
                    $w_diff = $width - $std_w;
                    $width -= $w_diff;
                    $h_diff = $w_diff / $verh;
                    $height -= $h_diff;
                } elseif ($width < $height) {
                    $verh = $height / $width;
                    $h_diff = $height - $std_h;
                    $height -= $h_diff;
                    $w_diff = $h_diff / $verh;
                    $width -= $w_diff;
                } elseif ($width == $height) {
                    $w_diff = $width - $std_w;
                    $width -= $w_diff;
                    $height -= $w_diff;
                }
            }
        }

        private function GetATeam($session_obj)
        {
            $db_user_obj = new DBUserClass();
            $user_objs = $db_user_obj->GetAllAteamUser();

            $text = "";
            $count = 0;

            if ($user_objs != null) {
                foreach ($user_objs as $user_obj) {
                    $count++;

                    // Bild vorhanden?
                    if ($user_obj->GetPicture() != "") {
                        $size = getimagesize(img_user_dir.$user_obj->GetPicture());
                        $height = $size[1];
                        $width = $size[0];
                        $this->CalcPicture($width, $height);

                        $pic = '<img src="'.img_user_dir.$user_obj->GetPicture().'" width="'.$width.'" height="'.$height.'" border="0" style="border: 1px solid black;">';
                    } else {
                        $size = getimagesize(img_user_dir."nichtvorhanden.jpg");
                        $height = $size[1];
                        $width = $size[0];
                        $this->CalcPicture($width, $height);

                        $pic = '<img src="'.img_user_dir.'nichtvorhanden.jpg" width="'.$width.'" height="'.$height.'" border="0">';
                    }

                    // Geburtstag vorhanden?
                    if ($user_obj->GetBirthday() != "0000-00-00") {
                        list($jahr, $monat, $tag) = explode("-", $user_obj->GetBirthday());
                        $geb = $tag.'.'.$monat.'.'.$jahr;
                    } else {
                        $geb = "-";
                    }

                    // Location vorhanden?
                    if ($user_obj->GetLocation() != "") {
                        $location = $user_obj->GetLocation();
                    } else {
                        $location = "-";
                    }

                    // ICQ vorhanden?
                    if ($user_obj->GetIcq() != 0) {
                        $icq = $user_obj->GetIcq();
                    } else {
                        $icq = "-";
                    }

                    // HP vorhanden?
                    if ($user_obj->GetHp() != "") {
                        $hp = '<a href="'.$user_obj->GetHp().'" target="_blank" class="orangelink">'.$user_obj->GetHp().'</a>';
                    } else {
                        $hp = "-";
                    }

                    // Userinfo vorhanden?
                    if ($user_obj->GetUserinfo() != "") {
                        $userinfo = $user_obj->GetUserinfo();
                    } else {
                        $userinfo = "-";
                    }

                    if (($count % 2) != 0) {
                        if ($count >= 2) {
                            $text .= "</tr>";
                        }

                        $text .= "<tr><td valign='top'>".$this->UseBox3($user_obj->GetLogin(), $pic,
                                '<tr class="boxcomments1">
									<td width="25%"><span class="smalltext">Geburtsdatum</span></td>
									<td><span class="smalltext">'.$geb.'</span></td>
								</tr>
								<tr class="boxcomments1">
									<td><span class="smalltext">Wohnort</span></td>
									<td><span class="smalltext">'.$location.'</span></td>
								</tr>
								<tr class="boxcomments1">
									<td valign="top"><span class="smalltext">ICQ</span></td>
									<td><span class="smalltext">'.$icq.'</span></td>
								</tr>
								<tr class="boxcomments1">
									<td valign="top"><span class="smalltext">Homepage</span></td>
									<td><span class="smalltext">'.$hp.'</span></td>
								</tr>
								<tr class="boxcomments1">
									<td valign="top"><span class="smalltext">Info</span></td>
									<td><span class="smalltext">'.$userinfo.'</span></td>
								</tr>', '325', 'left', 'handler.php?s='.$session_obj->GetSession().'&a=getateam&goto=special')."</td>";

                        $text .= "
							<td width='10'>&nbsp;</td>
						";
                    } else {
                        $text .= "<td valign='top'>".$this->UseBox3($user_obj->GetLogin(), $pic,
                                '<tr class="boxcomments1">
									<td width="25%"><span class="smalltext">Geburtsdatum</span></td>
									<td><span class="smalltext">'.$geb.'</span></td>
								</tr>
								<tr class="boxcomments1">
									<td><span class="smalltext">Wohnort</span></td>
									<td><span class="smalltext">'.$location.'</span></td>
								</tr>
								<tr class="boxcomments1">
									<td valign="top"><span class="smalltext">ICQ</span></td>
									<td><span class="smalltext">'.$icq.'</span></td>
								</tr>
								<tr class="boxcomments1">
									<td valign="top"><span class="smalltext">Homepage</span></td>
									<td><span class="smalltext">'.$hp.'</span></td>
								</tr>
								<tr class="boxcomments1">
									<td valign="top"><span class="smalltext">Info</span></td>
									<td><span class="smalltext">'.$userinfo.'</span></td>
								</tr>', '330', 'left', 'handler.php?s='.$session_obj->GetSession().'&a=getateam&goto=special')."</td>";
                    }
                }
            } else {
                $text .= '
					<tr>
						<td><span class="smalltext"><b>Keine Daten vorhanden!</b></span></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
				';
            }

            $text .= '
				</tr>
				<tr>
					<td><a href="handler.php?s='.$session_obj->GetSession().'&goto=special" class="blacklink">zurück</a></td>
				</tr>
			';

            return $text;
        }
    }
