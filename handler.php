<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Handler
    */

    require_once "code/kernel/db_session_class.php";
    require_once "code/kernel/session_class.php";
    require_once "code/kernel/db_user_class.php";
    require_once "code/kernel/db_userstats_class.php";
    require_once "code/kernel/userstats_class.php";
    require_once "code/kernel/user_class.php";

    $time = microtime(true); //Benchmark

    try {
        $session = strip_tags($_GET['s']);
        if (empty($session)) {
            $session = strip_tags($_POST['s']);
        }

        $goto = strip_tags($_GET['goto']);
        if (empty($goto)) {
            $goto = strip_tags($_POST['goto']);
        }

        $db_session_obj = new DBSessionClass();
        $db_user_obj = new DBUserClass();
        $db_userstats_obj = new DBUserStatsClass();

        $session_obj = $db_session_obj->CheckSession($session);

        if ($session_obj != null && $session_obj->GetUser_id() != 0) {
            $user_obj = $db_user_obj->GetUserById($session_obj->GetUser_id());

            // Stats holen und die sideclicks inkrementieren
            $stats_obj = $db_userstats_obj->GetAllUserStats($user_obj->GetId());

            if ($stats_obj != null) {
                $stats_obj->SetClicks($stats_obj->GetClicks()+1);
                $db_userstats_obj->UpdateUserStats($stats_obj);
            }
        } else {
            $user_obj = null;
        }

        //Seite wird aufgebaut
        if (!empty($goto)) {
            require "code/page_".$goto."_class.php";
            $classname = "Page".ucfirst($goto)."Class";
            $page_obj = new $classname($_GET, $_POST, $session_obj, $user_obj, $_FILES);
            if ($page_obj->nohtml === false) {
                //Header wird nicht verwendet

                print($page_obj->GetPage());
            }
        }
    } catch (Exception $e) {
        print("<br>".$e->getMessage()."<br><br>");
    }

    $time = microtime(true) - $time;
    print('<font style="font-family: Verdana; font-size: 10; color: #CCCCCC;">Page: '.round($time, 4)."</font>");
