<?php
    /*
        Author: Daniel Böhmer
        eMail: sicbb@bb-network.de
        Date: 07/21/05
        Function: Seitenindex
    */
    
    require_once "code/kernel/db_session_class.php";
    require_once "code/kernel/session_class.php";
    require_once "code/kernel/db_counter_class.php";
    require_once "code/kernel/counter_class.php";

    try {
        $db_session_obj = new DBSessionClass();
        $db_counter_obj = new DBCounterClass();

        if (empty($_COOKIE['session']) || !$db_session_obj->SessionExists($_COOKIE['session'])) {
            $session_obj = new SessionClass();  //Session wird neu erstellt
            $db_session_obj->InsertSession($session_obj);  //Session wird in die Datenbank eingetragen
            setcookie("session", $session_obj->GetSession(), time()+60*60*24*30); //Cookie wird angelegt
        } else {
            $session_obj = $db_session_obj->CheckSession($_COOKIE['session']);
            if ($session_obj->GetIp() != $_SERVER['REMOTE_ADDR']) {
                $session_obj = new SessionClass(); //Session wird neu erstellt
                $db_session_obj->InsertSession($session_obj);  //Session wird in die Datenbank eingetragen
                setcookie("session", $session_obj->GetSession(), time()+60*60*24*30); //Cookie wird angelegt
            }
        }

        // Sessionzeit erneuern und auf die Gegenwart setzen
        //$session_obj->SetSdate(time());
        //$db_session_obj->UpdateSession($session_obj); // Session Updaten

        $session_obj->SetId($db_session_obj->GetIdBySession($session_obj->GetSession()));

        $counter_obj = $db_counter_obj->GetLastCounterBySession($session_obj->GetId());

        if ($counter_obj == null) {
            // Counter in die Datenbank eintragen
            $counter_obj = new CounterClass();
            if ($counter_obj != null) {
                $db_counter_obj->InsertCounter($counter_obj, $session_obj->GetId());
            }
        } elseif ($counter_obj->GetTimestamp() < time()-3600) {
            // Counter in die Datenbank eintragen
            $counter_obj = new CounterClass();
            if ($counter_obj != null) {
                $db_counter_obj->InsertCounter($counter_obj, $session_obj->GetId());
            }
        }
    } catch (Exception $e) {
        print("<br>".$e->getMessage()." ".$e->getFile()." ".$e->getLine()."<br>");
    }
?>

<head>
<TITLE>:: PlanetBöhmer v3.0 ::</TITLE>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
<META NAME="allow-search" content="YES">
<META NAME="keywords" CONTENT="Böhmer, Boehmer, Daniel Böhmer, Planetboehmer, Planet, Sascha Fröschke, Benni Reiners, Benjamin Reiners, Stefan Wege, Pitt, Benjamin Pietzner, Andre Arndt, Philipe Gerlach, Andre Stets, LiYaowei Shen, DCUS, D.C.U.S., Philipp Gaida, Pic of the Month">
<META NAME="description" CONTENT="Planetboehmer v3.0 by Daniel Boehmer">
<META NAME="revisit-after" CONTENT="5 days">
<META NAME="audience" CONTENT="All">
<META NAME="content-language" CONTENT="DE">
<META name="publisher" content="Daniel Böhmer (sicbb@bb-network.de)">
<META name="author" content="Daniel Böhmer (sicbb@bb-network.de)">
<META name="robots" content="index, follow">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/news.css">
</head>

<frameset cols="*,700,*" frameborder="NO" border="0" framespacing="0">
  <frame src="links.php" name="leftFrame" scrolling="NO" noresize>
  <frameset rows="216,*" frameborder="NO" border="0" framespacing="0">
    <frame src="oben.php" name="topFrame" scrolling="NO" noresize>
    <frameset rows="30,*" frameborder="NO" border="0" framespacing="0">
      <frame src="menu.php?s=<?php print($session_obj->GetSession()); ?>" name="mainFrame" scrolling="NO" noresize>
      <frame src="handler.php?s=<?php print($session_obj->GetSession()); ?>&goto=news" name="bottomFrame" noresize>
   </frameset>
  </frameset>
  <frame src="rechts.php" name="rightFrame" scrolling="NO" noresize>
  </frameset>
</frameset>
<noframes><body>
Ihr Browser unterstützt keine Frames!
</body></noframes>
</html>
